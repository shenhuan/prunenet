import torch 
import open3d as o3d
# from torch_cluster import fps
import os,sys,glob
import numpy as np
from tqdm import tqdm
import torch
import numpy.matlib
import scipy.io
from scipy.spatial.transform import Rotation
import MinkowskiEngine as ME
import re
import pandas as pd



def mFPS(in_path,out_path,expect_num,device,dtype):
    pcd=o3d.io.read_point_cloud(in_path)
    dense_pts=torch.tensor(pcd.points,dtype=dtype,device=device)
    n_pts=dense_pts.size()[0]
    ratio=expect_num/n_pts*1.0
    sparse_indice=fps(dense_pts,ratio=ratio,random_start=False)
    if(sparse_indice.size()[0]!=expect_num):
        sparse_indice=sparse_indice[list(np.linspace(0,sparse_indice.size()[0],expect_num,endpoint=False,dtype=int))]
    sparse_indice=sparse_indice.tolist()
    assert len(sparse_indice)==expect_num
    
    # sparse pcd
    sparse_points=np.asarray(pcd.points)[sparse_indice]
    pcd.points=o3d.utility.Vector3dVector()
    pcd.points=o3d.utility.Vector3dVector(sparse_points)

    # write
    assert o3d.io.write_point_cloud(out_path, pcd)==True

def ss1():
    files=sorted(glob.glob('../dataset/3DMatch/*/*.ply'))
    os.system('cp %s ./samples' % files[0])
    for num_sparse_points in [1024,2048,4096,8192,16384]:
        # device = torch.device('cpu')
        device=torch.device('cuda:0')
        dtype=torch.float
        mFPS(files[0],'samples/%d.ply' % num_sparse_points,num_sparse_points,device,dtype)


def ss2():
    files=sorted(glob.glob('../dataset/test/*/*.ply'))
    for eachfile in tqdm(files):
        name=eachfile.split('.ply')[0]
        new_name=name+'_raw.ply'
        os.system('mv %s %s' % (eachfile,new_name))
        
def transform_pcd(path):
    pcd=o3d.io.read_point_cloud(path)
    path_tsfm=path.split('_raw')[0]+'.info.txt'
    lines=open(path_tsfm,'r').readlines()
    tsfm=[]
    for i in range(1,5):
        eles=lines[i].split('	')
        a,b,c,d=float(eles[0]),float(eles[1]),float(eles[2]),float(eles[3])
        tsfm.append([a,b,c,d])
    tsfm=np.array(tsfm)
    pcd.transform(tsfm)
    path_output=path.split('_raw')[0]+'_transformed.ply'
    o3d.io.write_point_cloud(path_output, pcd)

def ss3():
    files=glob.glob('../dataset/test/*/*.ply')
    for eachfile in tqdm(files):
        transform_pcd(eachfile)

def ss4():
    base_dir='../dataset/test'
    folders=os.listdir(base_dir)
    for eachfolder in folders:
        c_folder=os.path.join(base_dir,eachfolder)
        pcd=os.path.join(c_folder,'02_Fragments')
        os.system('mkdir %s' % pcd)
        tsfm=os.path.join(c_folder,'03_Transformed')
        os.system('mkdir %s' % tsfm)
        info=os.path.join(c_folder,'04_Info')
        os.system('mkdir %s' % info)
        keypoints=os.path.join(c_folder,'01_Keypoints')
    
        files=glob.glob(c_folder+'/*raw.ply')
        for eachfile in files:
            os.system('mv %s %s' %(eachfile,pcd))
        
        files=glob.glob(c_folder+'/*ed.ply')
        for eachfile in files:
            os.system('mv %s %s' %(eachfile,tsfm))
        
        files=glob.glob(c_folder+'/*info.txt')
        for eachfile in files:
            os.system('mv %s %s'% (eachfile,info))

        files=os.listdir(keypoints)
        for eachfile in files:
            path=os.path.join(keypoints,eachfile)
            new=path.split('Keypoints.txt')[0]+'.txt'
            os.system('mv %s %s' % (path,new))    

def ss5():
    source_base_dir='/net/pf-pc27/scratch2/07_Training_Data'
    target_base_dir='../dataset/train'
    folders=next(os.walk(source_base_dir))[1]
    for eachfolder in tqdm(folders):
        c_folder=os.path.join(source_base_dir,eachfolder)
        os.makedirs(os.path.join(target_base_dir,eachfolder))
        source=os.path.join(c_folder,'01_Data')
        target=os.path.join(target_base_dir,eachfolder)
        os.system('cp -r %s %s' % (source,target))
        source=os.path.join(c_folder,'02_Keypoints')
        os.system('cp -r %s %s' % (source,target))
        
        tsfm=os.path.join(target,'03_Transformed')
        info=os.path.join(target,'04_Info')
        os.makedirs(tsfm)
        os.makedirs(info)

        files=glob.glob(c_folder+'/*info.txt')
        for eachfile in files:
            os.system('cp %s %s' % (eachfile,info))
        
        files=glob.glob(c_folder+'/*.ply')
        for eachfile in files:
            os.system('cp %s %s' % (eachfile,tsfm))

def ss6():
    base_dir='../dataset/3DMatch/test'
    folders=os.listdir(base_dir)
    for eachfolder in tqdm(folders):
        c_folder=os.path.join(base_dir,eachfolder,'01_Keypoints')
        files=os.listdir(c_folder)
        for eachfile in files:
            source=os.path.join(c_folder,eachfile)
            target=source.split('Keypoints.txt')[0]+'_Keypoints.txt'
            os.system('mv %s %s' % (source,target))

def mmFPS(in_path,out_path,expect_num,device,dtype):
    pcd=o3d.io.read_point_cloud(in_path)
    dense_pts=torch.tensor(pcd.points,dtype=dtype,device=device)
    n_pts=dense_pts.size()[0]
    ratio=expect_num/n_pts*1.0
    sparse_indice=fps(dense_pts,ratio=ratio,random_start=False)
    if(sparse_indice.size()[0]!=expect_num):
        sparse_indice=sparse_indice[list(np.linspace(0,sparse_indice.size()[0],expect_num,endpoint=False,dtype=int))]
    sparse_indice=sparse_indice.tolist()
    assert len(sparse_indice)==expect_num
    
    # sparse pcd
    sparse_points=np.asarray(pcd.points)[sparse_indice]
    pts=torch.from_numpy(sparse_points).cpu()
    torch.save(pts,out_path)

def ss7():
    base_dir='../dataset/3DMatch/test'
    folders=next(os.walk(base_dir))[1]
    num_sparse_points=1024
    device=torch.device('cuda:0')
    dtype=torch.float
    for eachfolder in tqdm(folders):
        c_folder=os.path.join(base_dir,eachfolder)
        target=os.path.join(c_folder,'fps_1024')
        if(not os.path.exists(target)):
            os.makedirs(target)
        files=os.listdir(os.path.join(c_folder,'02_Fragments'))
        for eachfile in files:
            path=os.path.join(c_folder,'02_Fragments',eachfile)
            out=os.path.join(target,eachfile.split('.')[0]+'.pth')
            mmFPS(path,out,num_sparse_points,device,dtype)

def ss8():
    base_dir='../dataset/3DMatch/train/*/fps_1024/*.pth'
    files=glob.glob(base_dir)
    sample=torch.load(files[0]).numpy()
    print(np.min(sample,0))
    print(np.max(sample,0))
    print(np.mean(sample,0))

def ss9():    
    path='../codes/prnet/dist.pth'
    x=torch.load(path)
    k=20
    # print(distance)
    # idx = distance.topk(k=20, dim=-1)[1]
    # print(idx)
    # print(torch.max(idx))
    # print(torch.min(idx))
    inner = -2 * torch.matmul(x.transpose(2, 1).contiguous(), x)
    xx = torch.sum(x ** 2, dim=1, keepdim=True)
    distance = -xx - inner - xx.transpose(2, 1).contiguous()

    idx = distance.topk(k=k, dim=-1)[1]  # (batch_size, num_points, k)
    assert torch.max(idx).item()<=768
    assert torch.min(idx).item()>=0

def ss10():
    # check resolution
    base_dir='samples'
    files=sorted(os.listdir(base_dir))
    for eachfile in files:
        path=os.path.join(base_dir,eachfile)
        pcd=o3d.io.read_point_cloud(path)
        pcd_tree = o3d.geometry.KDTreeFlann(pcd)

        distances=[]
        for i, point in enumerate(pcd.points):
            [count,vec1, vec2] = pcd_tree.search_knn_vector_3d(point,2)
            distances.append(np.sqrt(vec2[1]))
        print(eachfile,np.median(distances))

def ss11():
    # check the inputs to PRNet
    sys.path.append('..')
    from torch.utils.data import DataLoader
    from codes.prnet.data import D3Match,get_3DMatch_split
    base_dir='../dataset/3DMatch/train/*/fps_1024/*.pth'
    train_files,test_files=get_3DMatch_split(base_dir)

    train_loader = DataLoader(D3Match(train_files, 1024,
                                        num_subsampled_points=768,
                                        gaussian_noise=True,
                                        rot_factor=4),
                            batch_size=1, shuffle=True, drop_last=True, num_workers=6)
    for i,data in enumerate(train_loader):
        if(i==10):
            break
        pcd1,pcd2,_,_,_,_,_,_=data
        pcd1=pcd1.numpy().squeeze().T
        pcd2=pcd2.numpy().squeeze().T
        np.savetxt('partial_samples/%d_1.txt' % i,pcd1)
        np.savetxt('partial_samples/%s_2.txt' % i,pcd2)

def get_matching_count(source,target,threshold=0.0203):
    pcd_tree = o3d.geometry.KDTreeFlann(target)
    
    match_count=0
    for i, point in enumerate(source.points):
        [count, _, _] = pcd_tree.search_radius_vector_3d(point, threshold)
        if(count!=0):
            match_count+=1
    return match_count

def cal_overlap_mat(c_folder):
    print(c_folder)
    base_dir=os.path.join(c_folder,'03_Transformed')
    fragments=sorted(glob.glob(base_dir+'/*.ply'),key=natural_key)
    n_fragments=len(fragments)
    overlap_mat=np.ones((n_fragments,n_fragments),dtype='float')

    for i in range(n_fragments):
        for j in range(i+1,n_fragments):
            path1,path2=fragments[i],fragments[j]
            
            # load, downsample and transform
            pcd1=o3d.io.read_point_cloud(path1)
            pcd2=o3d.io.read_point_cloud(path2)
            pcd1=pcd1.voxel_down_sample(0.025)
            pcd2=pcd2.voxel_down_sample(0.025)

            # calculate overlap
            matching1=get_matching_count(pcd1,pcd2)
            matching2=get_matching_count(pcd2,pcd1)
            overlap1=matching1/len(pcd1.points)
            overlap2=matching2/len(pcd2.points)
            overlap_mat[i,j]=overlap1
            overlap_mat[j,i]=overlap2
    
    np.save(os.path.join(c_folder,'overlap_0.025.npy'),overlap_mat)
    print('%s...done' % c_folder)

def ss13():
    base_dir='../../3DMatch/*/*/overlap_0.02.npy'
    files=glob.glob(base_dir)
    for eachfile in files:
        name1=eachfile.split('/')[-2]
        name2=eachfile.split('/')[-3]
        new_name='overlaps/'+name2+'_'+name1+'_overlap_0.02.npy'
        os.system('cp %s %s' % (eachfile,new_name))

def knn(x, k):
    inner = -2 * torch.matmul(x.transpose(2, 1).contiguous(), x)
    xx = torch.sum(x ** 2, dim=1, keepdim=True)
    distance = -xx - inner - xx.transpose(2, 1).contiguous()

    idx = distance.topk(k=k, dim=-1)[1]  # (batch_size, num_points, k)
    return idx


def get_graph_feature(x, k=20):
    x = x.view(*x.size()[:3])
    idx = knn(x, k=k)  # (batch_size, num_points, k)
    
    batch_size, num_points, _ = idx.size()
    device = torch.device('cuda')

    idx_base = torch.arange(0, batch_size, device=device).view(-1, 1, 1) * num_points

    idx = idx + idx_base

    idx = idx.view(-1)

    _, num_dims, _ = x.size()

    x = x.transpose(2, 1).contiguous()  # (batch_size, num_points, num_dims)  -> (batch_size*num_points, num_dims) #   batch_size * num_points * k + range(0, batch_size*num_points)
    feature = x.view(batch_size * num_points, -1)[idx, :]
    feature = feature.view(batch_size, num_points, k, num_dims)
    x = x.view(batch_size, num_points, 1, num_dims).repeat(1, 1, k, 1)

    feature = torch.cat((feature, x), dim=3).permute(0, 3, 1, 2)

    return feature

def ss14():
    device=torch.device('cuda:0')
    dtype=torch.float
    for num_sparse_points in [2048,4096,8192]:
        for base_dir in ['../dataset/3DMatch/train','../dataset/3DMatch/test']:
            folders=next(os.walk(base_dir))[1]
            for eachfolder in tqdm(folders):
                c_folder=os.path.join(base_dir,eachfolder)
                target=os.path.join(c_folder,'fps_%d' % num_sparse_points)
                if(not os.path.exists(target)):
                    os.makedirs(target)
                files=os.listdir(os.path.join(c_folder,'02_Fragments'))
                for eachfile in files:
                    path=os.path.join(c_folder,'02_Fragments',eachfile)
                    out=os.path.join(target,eachfile.split('.')[0]+'.pth')
                    mmFPS(path,out,num_sparse_points,device,dtype)

def ss15():
    data=np.load('../codes/prnet/easy_reg_pred.npz')
    red=np.matlib.repmat(np.array([255,0,0]),768,1)
    green=np.matlib.repmat(np.array([0,255,0]),768,1)   
    blue=np.matlib.repmat(np.array([0,0,255]),768,1)
    purple=np.matlib.repmat(np.array([128,0,128]),768,1)

    r_gt=data['rotations_ab']
    t_gt=data['translations_ab']
    r_pred=data['rotations_ab_pred']
    t_pred=data['translations_ab_pred']
    srcs=data['srcs']
    tgts=data['tgts']
    # print(r_gt.shape,t_gt.shape,r_pred.shape,t_pred.shape)

    ind=np.random.randint(0,len(srcs),1)[0]
    source=srcs[ind]
    target=tgts[ind]
    tsfm=np.matmul(r_pred[ind],source)+np.expand_dims(t_pred[ind],axis=1)
    gt=np.matmul(r_gt[ind],source)+np.expand_dims(t_gt[ind],axis=1)
    np.savetxt('easy_reg/%d_target_red.txt' % ind,np.concatenate([target.T,red],axis=1))
    np.savetxt('easy_reg/%d_pred_blue.txt' % ind,np.concatenate([tsfm.T,blue],axis=1))
    np.savetxt('easy_reg/%d_source_purple.txt' % ind,np.concatenate([source.T,purple],axis=1))
    np.savetxt('easy_reg/%d_gt_green.txt' % ind,np.concatenate([gt.T,green],axis=1))

def ss16():
    data=np.load('../codes/prnet/hard_reg_pred.npz')
    red=np.matlib.repmat(np.array([255,0,0]),768,1)
    green=np.matlib.repmat(np.array([0,255,0]),768,1)   
    blue=np.matlib.repmat(np.array([0,0,255]),768,1)
    purple=np.matlib.repmat(np.array([128,0,128]),768,1)

    r_gt=data['rotations_ab']
    t_gt=data['translations_ab']
    r_pred=data['rotations_ab_pred']
    t_pred=data['translations_ab_pred']
    srcs=data['srcs']
    tgts=data['tgts']
    # print(r_gt.shape,t_gt.shape,r_pred.shape,t_pred.shape)

    ind=np.random.randint(0,len(srcs),1)[0]
    source=srcs[ind]
    target=tgts[ind]
    tsfm=np.matmul(r_pred[ind],source)+np.expand_dims(t_pred[ind],axis=1)
    gt=np.matmul(r_gt[ind],source)+np.expand_dims(t_gt[ind],axis=1)
    np.savetxt('hard_reg/%d_target_red.txt' % ind,np.concatenate([target.T,red],axis=1))
    np.savetxt('hard_reg/%d_pred_blue.txt' % ind,np.concatenate([tsfm.T,blue],axis=1))
    np.savetxt('hard_reg/%d_source_purple.txt' % ind,np.concatenate([source.T,purple],axis=1))
    np.savetxt('hard_reg/%d_gt_green.txt' % ind,np.concatenate([gt.T,green],axis=1))


def ss18():
    path='../dataset/3DMatch/test/kitchen/02_Fragments/cloud_bin_0.ply'
    pcd=o3d.io.read_point_cloud(path)
    print(len(pcd.points))

    pcd_1=pcd.voxel_down_sample(0.1)
    print(len(pcd_1.points))

    pcd_2=pcd.voxel_down_sample(0.05)
    print(len(pcd_2.points))

    pcd_3=pcd.voxel_down_sample(0.02)
    print(len(pcd_3.points))

def ss19():
    base_dir='../dataset/3DMatch/*/*/02_Fragments/*.ply'
    files=glob.glob(base_dir)
    for eachfile in files:
        pcd=np.array(o3d.io.read_point_cloud(eachfile).points)
        down_pcd=ME.utils.sparse_quantize(pcd,quantization_size=0.025)
        print(pcd.shape,down_pcd.shape)

def ss20():
    # evaluate resolution of uniform voxel_downsampled 
    # search_knn_vector_3d returns dist(x1,x2)^2

    base_dir='../dataset/3DMatch/test/*/03_Transformed/*.ply'
    files=sorted(glob.glob(base_dir))
    r_pcd=o3d.io.read_point_cloud(files[0])
    print(len(r_pcd.points))
    for voxel_size in [0.01,0.02,0.04,0.06,0.08,0.1,0.2]:
        pcd=r_pcd.voxel_down_sample(voxel_size)
        pcd_tree = o3d.geometry.KDTreeFlann(pcd)
        distances=[]
        for i, point in enumerate(pcd.points):
                [count,vec1, vec2] = pcd_tree.search_knn_vector_3d(point,2)
                # source=point
                # retrieved=pcd.points[vec1[1]]
                # norm=np.linalg.norm(source-retrieved)
                # print(norm,norm**2,vec2[1])
                distances.append(np.sqrt(vec2[1]))
        print(voxel_size,len(pcd.points),np.median(distances)*100)


def natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]



def ss22():
    # determine \epsilon based on 3DMatch testset
    base_dir='../dataset/3DMatch/test/*/03_Transformed/*.ply'
    files=sorted(glob.glob(base_dir),key=natural_key)
    etas=[]
    for eachfile in files:
        pcd=o3d.io.read_point_cloud(eachfile)
        pcd=pcd.voxel_down_sample(0.025)
        pcd_tree = o3d.geometry.KDTreeFlann(pcd)
        distances=[]
        for i, point in enumerate(pcd.points):
            [count,vec1, vec2] = pcd_tree.search_knn_vector_3d(point,2)
            distances.append(np.sqrt(vec2[1]))
        etai=np.median(distances)
        etas.append(etai)
    print(max(etas),min(etas))

def ss23():
    # test the treshold
    base_dir='../dataset/3DMatch/test/kitchen/03_Transformed/*.ply'
    files=sorted(glob.glob(base_dir),key=natural_key)
    print(files[0],files[14])
    pcd1=o3d.io.read_point_cloud(files[0])
    pcd2=o3d.io.read_point_cloud(files[14])
    pcd1=pcd1.voxel_down_sample(0.025)
    pcd2=pcd2.voxel_down_sample(0.025)
    count1=get_matching_count(pcd1,pcd2,threshold=0.0203)
    count2=get_matching_count(pcd2,pcd1,threshold=0.0203)
    overlap1=count1/len(pcd1.points)
    overlap2=count2/len(pcd2.points)
    print(overlap1,overlap2)

def ss12():
    import multiprocessing as mp
    folders=[]
    base_dir='../dataset/3DMatch/train'
    folds=os.listdir(base_dir)
    folders.append([os.path.join(base_dir,ele) for ele in folds])
    base_dir='../dataset/3DMatch/test'
    folds=os.listdir(base_dir)
    folders.append([os.path.join(base_dir,ele) for ele in folds])
    folders=sorted([son for fa in folders for son in fa])

    p = mp.Pool(processes=mp.cpu_count())
    p.map(cal_overlap_mat,folders)
    p.close()
    p.join()

def ss24():
    # check registration recall w.r.t. overlap ratio
    base_dir='../codes/d3feat/geometric_registration/log_result/*.mat'
    files=glob.glob(base_dir)
    g_overlaps=[]
    g_results=[]
    for eachfile in files:
        path_pred=eachfile
        path_log=eachfile.split('.mat')[0]+'/D3Feat_11011055-53-pred.log'
        path_overlap='overlaps/test_%s_overlap_0.025.npy' % eachfile.split('/')[-1].split('.')[0].split('-evaluation')[0]

        mat=scipy.io.loadmat(path_pred)['results'].flatten().tolist()
        log=open(path_log,'r')
        lines=log.readlines()
        overlaps=np.load(path_overlap)
        olps=[]
        for eachline in lines:
            eles=eachline.split('\t')
            if(len(eles)==3):
                ind_frag0=int(eles[0])
                ind_frag1=int(eles[1])

                overlap=max(overlaps[ind_frag0,ind_frag1],overlaps[ind_frag1,ind_frag0])
                olps.append(overlap)
        g_overlaps.extend(olps)
        g_results.extend(mat)
    
    # get stats
    df=pd.DataFrame(columns=['overlap','results'],data=np.array([g_overlaps,g_results]).T)
    df.to_csv('stats.csv',index=False)

def ss25():
    train_files=glob.glob('overlaps/train_*025.npy')
    test_files=glob.glob('overlaps/test_*025.npy')
    for eachfile in train_files:
        folder=eachfile.split('/train_')[1].split('_overlap')[0]
        target=os.path.join('../dataset/3DMatch/train',folder,'overlap.npy')
        os.system('cp %s %s' % (eachfile,target))
    
    for eachfile in test_files:
        folder=eachfile.split('/test_')[1].split('_overlap')[0]
        target=os.path.join('../dataset/3DMatch/test',folder,'overlap.npy')
        os.system('cp %s %s' % (eachfile,target))

def get_r_t(path):
    f=open(path,'r')
    lines=f.readlines()[1:]
    rows=[]
    for eachline in lines:
        eles=eachline.split('	 ')
        rows.append([float(ele) for ele in eles])
    T=np.array(rows)
    r,t=T[:3,:3],T[:3,3]
    return r,t

def ss26():
    '''
    work on real 3DMatch dataset
    '''
    base_train='/net/pf-pc27/scratch2/shengyu/3DMatch/train'
    train_folders=os.listdir(base_train)
        
    n=0
    rots,trans=[],[]
    srcs,tgts=[],[]
    overlap_ratios=[]
    for eachfolder in train_folders:
        c_path=os.path.join(base_train,eachfolder,'overlap.npy')
        overlaps=np.load(c_path)
        c_path=os.path.join(base_train,eachfolder,'05_FCGF_feats_0.05/*.pth')
        samples=sorted(glob.glob(c_path),key=natural_key)
        c_path=os.path.join(base_train,eachfolder,'04_Info/*.txt')
        infos=sorted(glob.glob(c_path),key=natural_key)
        if len(infos)==len(samples): # 7-scenes-fire misses one transformation matrix
            real_overlap=np.where(overlaps>=0.2)
            indice_0=real_overlap[0]
            indice_1=real_overlap[1]

            for i in range(indice_0.shape[0]):
                ind0,ind1=indice_0[i],indice_1[i]
                if(ind0<ind1): # process each pair
                    n+=1
                    src=samples[ind0].split('/3DMatch/')[1]
                    tgt=samples[ind1].split('/3DMatch/')[1]

                    r0,t0=get_r_t(infos[ind0])
                    r1,t1=get_r_t(infos[ind1])
                    r=r1.T.dot(r0)
                    t=r1.T.dot(np.expand_dims(t0-t1,1))

                    rots.append(r)
                    trans.append(t)
                    srcs.append(src)
                    tgts.append(tgt)
                    overlap_ratios.append(overlaps[ind0,ind1])
    
    np.savez('fcgf_0.05_all_train.npz',src=srcs,tgt=tgts,rot=rots,trans=trans,overlap=overlap_ratios)
    print(n)

def ss27():
    # check the new dataset, it's correct
    infos=np.load('all_train_0.1.npz')
    for ind in range(10):
        src_path=os.path.join('../dataset/3DMatch/train',infos['src'][ind])
        tgt_path=os.path.join('../dataset/3DMatch/train',infos['tgt'][ind])
        src=np.array(o3d.io.read_point_cloud(src_path).points)
        tgt=np.array(o3d.io.read_point_cloud(tgt_path).points)
        rot=infos['rot'][ind]
        trans=infos['trans'][ind]

        src_trans=rot.dot(src.T)+trans
        src_trans=src_trans.T

        red=np.matlib.repmat(np.array([255,0,0]),src.shape[0],1)
        green=np.matlib.repmat(np.array([0,255,0]),tgt.shape[0],1)   
        blue=np.matlib.repmat(np.array([0,0,255]),src_trans.shape[0],1)

        np.savetxt('checks/%d_src_red.txt' % ind,np.concatenate([src.round(3),red],axis=1))
        np.savetxt('checks/%d_tgt_gree.txt' % ind,np.concatenate([tgt.round(3),green],axis=1))
        np.savetxt('checks/%d_src_trans_blue.txt' % ind,np.concatenate([src_trans.round(3),blue],axis=1))

def ss29():
    data=np.load('/scratch/shengyu/masterthesis/dataset/3DMatch/test/7-scenes-redkitchen/features/kitchen_000.npz')
    xyz=data['xyz']
    points=data['points']
    feature=data['feature']
    significance=np.linalg.norm(feature,axis=1)
    significance /= significance.max()

    data=np.hstack((xyz,np.expand_dims(significance,1)))
    np.savetxt('dump/fcgf.txt',data)

def ss30():
    ''''
    get the 3DMatch test pairs
    '''
    base_test='/net/pf-pc27/scratch2/shengyu/3DMatch/test'

    n=0
    rots,trans=[],[]
    srcs,tgts=[],[]
    overlap_ratios=[]

    base_dir='/scratch/shengyu/masterthesis/benchmark/3DMatch/gt_result/*/gt.log'
    files=sorted(glob.glob(base_dir))
    for eachfile in files:
        f=open(eachfile,'r').readlines()
        scene_name=eachfile.split('/')[-2]

        c_path=os.path.join(base_test,scene_name,'05_FCGF_feats_0.025/*.pth')
        samples=sorted(glob.glob(c_path),key=natural_key)

        c_path=os.path.join(base_test,scene_name,'04_Info/*.txt')
        infos=sorted(glob.glob(c_path),key=natural_key)

        c_path=os.path.join(base_test,scene_name,'overlap.npy')
        overlaps=np.load(c_path)
        # get pair list
        for eachline in f:
            eles = eachline.split('	 ')
            if(len(eles)==3): 
                ind0=int(eles[0])
                ind1=int(eles[1])

                # if(abs(ind0-ind1)>1): # ignore the consecutive frames
                n+=1

                src=samples[ind0].split('/3DMatch/')[1]
                tgt=samples[ind1].split('/3DMatch/')[1]

                r0,t0=get_r_t(infos[ind0])
                r1,t1=get_r_t(infos[ind1])
                r=r1.T.dot(r0)
                t=r1.T.dot(np.expand_dims(t0-t1,1))

                rots.append(r)
                trans.append(t)
                srcs.append(src)
                tgts.append(tgt)
                c_overlap = max(overlaps[ind0,ind1],overlaps[ind1,ind0])
                overlap_ratios.append(c_overlap)
    np.savez('fcgf_0.025_test_benchmark.npz',src=srcs,tgt=tgts,rot=rots,trans=trans,overlap=overlap_ratios)
    print(n)

def ss31():
    '''
    work on real 3DMatch dataset
    '''
    base_train='/net/pf-pc27/scratch2/shengyu/3DMatch/train'
    train_folders=os.listdir(base_train)
    n=0
    rots,trans=[],[]
    srcs,tgts=[],[]
    overlap_ratios=[]
    for eachfolder in train_folders:
        c_path=os.path.join(base_train,eachfolder,'overlap.npy')
        overlaps=np.load(c_path)
        c_path=os.path.join(base_train,eachfolder,'05_FCGF_feats_0.025/*.pth')
        samples=sorted(glob.glob(c_path),key=natural_key)
        c_path=os.path.join(base_train,eachfolder,'04_Info/*.txt')
        infos=sorted(glob.glob(c_path),key=natural_key)
        if len(infos)==len(samples): # 7-scenes-fire misses one transformation matrix
            flag1=overlaps>0.2
            flag2=overlaps<=0.9
            flag=flag1 & flag2
            real_overlap=np.where(flag)
            indice_0=real_overlap[0]
            indice_1=real_overlap[1]

            for i in range(indice_0.shape[0]):
                ind0,ind1=indice_0[i],indice_1[i]
                n+=1
                src=samples[ind0].split('/3DMatch/')[1]
                tgt=samples[ind1].split('/3DMatch/')[1]

                r0,t0=get_r_t(infos[ind0])
                r1,t1=get_r_t(infos[ind1])
                r=r1.T.dot(r0)
                t=r1.T.dot(np.expand_dims(t0-t1,1))

                rots.append(r)
                trans.append(t)
                srcs.append(src)
                tgts.append(tgt)
                overlap_ratios.append(overlaps[ind0,ind1])
    
    np.savez(f'fcgf_0.025_all_train.npz',src=srcs,tgt=tgts,rot=rots,trans=trans,overlap=overlap_ratios)


if __name__=='__main__':
    new_dict=dict()
    src,tgt,rot,trans,overlap=[],[],[],[],[]

    for i in [0,1,2,3]:
        path=f'../dataset/3DMatch/test/overlap_{i}.npz'
        data=dict(np.load(path))
        src.append(data['src'])
        tgt.append(data['tgt'])
        rot.append(data['rot'])
        trans.append(data['trans'])
        overlap.append(data['overlap'])
    
    src=np.concatenate(src,0)
    tgt=np.concatenate(tgt,0)
    rot=np.concatenate(rot,0)
    trans=np.concatenate(trans,0)
    overlap=np.concatenate(overlap,0)

    np.savez(f'overlap_0_4.npz',src=src,tgt=tgt,rot=rot,trans=trans,overlap=overlap)