import numpy as np
import os, sys, glob
import matplotlib.pyplot as plt
import open3d as o3d
from torch_cluster import fps
import copy
import multiprocessing as mp
import seaborn as sns
import torch
import math
import torch.nn.functional as F
import pandas as pd

def ss1():
    '''
    Plot weights and logits
    '''
    a=np.arange(-10,10,0.01)
    b=torch.from_numpy(a)
    c=torch.relu(torch.tanh(b))
    d=torch.sigmoid(b)
    plt.figure(figsize=(6,3),dpi=300)
    plt.plot(a,c.numpy(),label='Weights')
    plt.plot(a,d.numpy(),label='Logits')
    plt.legend()
    # plt.title('Weights and logits from LTFGC',fontsize=8)
    plt.savefig('../assets/activation.pdf',bbox_inches='tight')

def ss2():
    '''
    Plot Gumbel distribution
    '''
    from scipy.stats import gumbel_r
    plt.figure(figsize=(6,3),dpi=300)
    x = np.linspace(gumbel_r.ppf(0.01),
                    gumbel_r.ppf(0.99), 100)
    plt.plot(x, gumbel_r.pdf(x),
        '-', lw=2, alpha=0.6, label='gumbel_r pdf')
    plt.savefig('../assets/Gumbel.pdf',bbox_inches='tight')

def ss3():
    '''
    Plot temperature w.r.t. temperature
    '''
    scores=torch.load('../codes/devnet/dump/fcgf_scores.pth').cpu()
    for idx,scale in enumerate([1e0,1e-1,1e-2,1e-3,1e-4]):
        plt.figure(figsize=(5,3),dpi=300)
        soft_scores = F.softmax(scores/scale,dim=2)
        plt.plot(soft_scores[0][0].numpy())
        plt.ylabel('Scores',fontsize=10)
        plt.title('Temperature=%.4f' % scale,fontsize=10)
        plt.savefig('../assets/temp_%d.pdf' % idx,bbox_inches='tight')

def ss4():
    '''
    Plot the # points of 3DMatch dataset
    '''
    files=glob.glob('/net/pf-pc27/scratch2/shengyu/3DMatch/train/*/02_Fragments/*.ply')
    n_points=[]
    for eachfile in files:
        pcd=o3d.io.read_point_cloud(eachfile)
        n_points.append(len(pcd.points))
    plt.figure(figsize=(6,3),dpi=300)
    plt.hist(n_points)
    plt.xlabel('number of points')
    plt.ylabel('number of fragments')

    plt.title('Number of points of different fragments')
    print(np.median(n_points))
    plt.savefig('../assets/num_pts.pdf',bbox_inches='tight')

def ss5():
    '''
    Plot the overlap ratio of 3DMatch test set
    '''
    files=glob.glob('/net/pf-pc27/scratch2/shengyu/3DMatch/test/*/overlap.npy')
    plt.figure(figsize=(12,12),dpi=300)
    for i,eachfile in enumerate(files):
        plt.subplot(3,3,i+1)
        heatmap=np.load(eachfile)
        sns.heatmap(heatmap,cmap='Blues')
    
    plt.figure(figsize=(12,12),dpi=300)
    for i,eachfile in enumerate(files):
        heatmap=np.load(eachfile).flatten()
        stats=heatmap[heatmap>=0.1]
        plt.subplot(3,3,i+1)
        plt.hist(stats,bins=np.arange(0,1.1,0.1).tolist())

def ss6():
    '''
    Registration recall w.r.t. overlap ratio
    '''
    df=pd.read_csv('dump/stats.csv')
    dfs=df.groupby('results')
    plt.figure(figsize=(12,3),dpi=300)
    titles=['failed cases','successsful cases','consecutive cases']
    for ind,ele in enumerate(dfs):
        c_df=ele[1]['overlap'].values
        mean_overlap=round(np.mean(c_df),3)
        plt.subplot(1,3,ind+1)
        plt.hist(c_df,bins=np.arange(0,1.1,0.1).tolist())
        plt.xlabel('overlap ratio')
        plt.ylabel('# samples')
        plt.title(titles[ind]+', '+str(mean_overlap))
    plt.savefig('../assets/overlap_1.pdf',bbox_inches='tight')  

    good_df=df[df['results']==1.0]
    bad_df=df[df['results']==0.0]
    n_good,ticks=np.histogram(good_df['overlap'].values,bins=np.arange(0,1.1,0.1).tolist())
    n_bad,_=np.histogram(bad_df['overlap'].values,bins=np.arange(0,1.1,0.1).tolist())
    recalls=np.round(n_good/(n_good+n_bad),3).tolist()
    recalls[0]=0
    recalls[-1]=1
    recalls.append(1.0)
    plt.figure(figsize=(6,3),dpi=300)
    plt.plot(recalls,'-o')
    # plt.title('Registration recalls on 3DMatch test dataset using D3Feat',fontsize=8)
    plt.xlabel('Overlap ratio',fontsize=11)
    plt.ylabel('Registration recall',fontsize=11)
    plt.xticks(np.arange(len(ticks)),np.round(ticks,1).tolist())
    plt.savefig('../assets/registration_recall_overlap.pdf',bbox_inches='tight')

def ss7():
    '''
    FCGF feature match recall w.r.t overlap ratio
    '''
    data=np.load('../codes/devnet/dump/fcgf_stats.npz')
    overlap=data['overlap']
    recall=data['recall']
    inlier_ratio=data['inlier_ratio']
    mean_recall=[]
    mean_inlier_ratio=[]
    for i in range(2,10):
        indice_1=(overlap>i*0.1) 
        indice_2=(overlap<(i+1)*0.1)
        indice=indice_1&indice_2
        mean_recall.append(recall[indice].mean())
        mean_inlier_ratio.append(inlier_ratio[indice].mean())
    ticks=np.arange(2,10)*0.1

    plt.figure(figsize=(6,3),dpi=300)
    plt.plot(mean_recall)
    plt.xticks(np.arange(len(ticks)),np.round(ticks,1).tolist())
    plt.xlabel('Overlap ratio',fontsize=8)
    plt.ylabel('Feature match recall',fontsize=8)
    plt.savefig('../assets/feature_match_recall_overlap.pdf',bbox_inches='tight')

def ss8():
    '''
    plot metric learning 
    '''
    plt.figure(figsize=(8,3),dpi=300)
    x=np.arange(0,2,0.1)
    y1=1.4-x
    y2=x-0.1
    y1=[max(0,ele) for ele in y1.tolist()]
    y2=[max(0,ele) for ele in y2.tolist()]
    plt.subplot(1,2,1)
    plt.plot(x,y1,label='$d_{neg}=1.4$')
    plt.plot(x,y2,label='$d_{pos}=0.1$')
    plt.legend()

    x=np.arange(0,2,0.1)
    y1=1.4-x
    y2=x-0.1
    y1=[max(0,ele)**2 for ele in y1.tolist()]
    y2=[max(0,ele)**2 for ele in y2.tolist()]
    plt.subplot(1,2,2)
    plt.plot(x,y1,label='$d_{neg}=1.4$')
    plt.plot(x,y2,label='$d_{pos}=0.1$')
    plt.legend()
    plt.savefig('../assets/metric_learning.pdf',bbox_inches='tight')

if __name__=='__main__':
    ss6()