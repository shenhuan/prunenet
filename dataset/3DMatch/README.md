# 3DMatch dataset
Each npz file contains all the train/test samples, it's named after %s_all_%s_%s % (#points, phase, overlap threshold)

It contains:
- src: path to the source file [str]
- tgt: path to the target file [str]
- trans: translation [3,1]
- rot: rotation [3,3]
- overlap: overlap ratio , [1]