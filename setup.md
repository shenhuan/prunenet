# Create a working environment
```shell
# create virtual environment
mkdir temp
python3 -m venv torch
source torch/bin/activate
pip install pip --upgrade

# install requirements
TMPDIR=temp pip install -r requirements.txt

# install MinkowskiEngine
git clone https://github.com/StanfordVL/MinkowskiEngine.git
git checkout -b d212be80322ff035590eb1a053ffb5d7e896f013
cd MinkowskiEngine
python setup.py install
cd ..

# install pointnet2 utils
git clone https://github.com/DylanWusee/PointPWC.git
cd pointnet2
python setup.py install
cd ..
```