## Benchmarking data
This folder containes data used to benchmark pairwise registration algorithms. 

### Folder structure:
-   Each benchmark dataset has its own folder, e.g. 3dmatch
-   Each dataset contains following data:
    - **Raw point clouds**: files with the format '*.ply' containg the coordinates of the point cloud fragments
    - **Correspondences**: '*.npz' files for all point cloud pairs with more than 30% overlap. Correspondences were established in the FCGF feature space using the best performing model from the original repository. Files contains follwing arrays:
      - 'x': nx6 coordinates of the correspondences
      - 'y': distance after the ground truth transformation
      - 'mutuals': flag for the reciprocal matching
      - 'ratios': distance ration in the FCGF space between the first and the second NN
    - **Features**: '*.npz' files containing FCGF features for all the point cloud fragments.Files contains follwing arrays:
      - 'point': nx3 coordinates of the point from the original point cloud
      - 'xyz': mx3 coordinates of the points after the voxelization
      - 'feature': mx32 FCGF feature vectors for the remaining points after the voxelization
    - **01_Keypoints**: '*.txt' files containing the keypoint indices provided by the authors of the 3DMatch benchmark dataset.