"""
Script for benchmarking the 3DMatch dataset. Code is partially borrowed from the Chris Choy's FCGF repository (https://github.com/chrischoy/FCGF)

Author: Zan Gojcic
Last modified: 17.04.2020
"""

import os
import glob
import sys
import numpy as np
import argparse
import logging
import open3d as o3d
from collections import defaultdict 
import torch


from models import load_model
from lib.timer import Timer, AverageMeter
from lib.utils import ensure_dir, read_trajectory, write_trajectory, read_trajectory_info
from lib.utils import do_single_pair_RANSAC_reg, rotation_error, translation_error, evaluate_registration, compute_overlap_ratio, extract_corresponding_trajectors





ch = logging.StreamHandler(sys.stdout)
logging.getLogger().setLevel(logging.INFO)
logging.basicConfig(
        format='%(asctime)s %(message)s', datefmt='%m/%d %H:%M:%S', handlers=[ch])

o3d.utility.set_verbosity_level(o3d.utility.VerbosityLevel.Error)


def estimate_trans_param_RANSAC(feature_path, scene_names, method, model, mutuals, overwrite, save_data=False):

    for scene in scene_names:
        if mutuals:
            save_path = os.path.join(feature_path, method,'mutuals', scene.split('\\')[-2])
            ensure_dir(save_path)
        else:
            save_path = os.path.join(feature_path, method,'all', scene.split('\\')[-2])
            ensure_dir(save_path)

        # If overwrite or if the transformation data does not yet exist it should be estimated otherwise this step can be skipped to save time
        if os.path.exists(os.path.join(save_path,'traj.txt')) and not overwrite:
            logging.info("Trajectory is already computed and will be loaded for evaluation. If the trajectory should be recomputed select overwrite.")
        else:
            pairs, traj = read_trajectory(os.path.join(scene + "gt.log"))

            #Initialize the matrix
            est_trans_par = np.tile(np.expand_dims(np.eye(4),0), (len(pairs),1,1))
            print(len(pairs))
            reg_metadata = []

            for idx, pair in enumerate(pairs):
                
                data = np.load(os.path.join(scene,'correspondences','cloud_{}_{}.npz'.format(pair[0], pair[1])))['x']

                # Load both original point clouds
                pc_1 = np.load(os.path.join(scene,'features','cloud_{:03d}.npz'.format(int(pair[0]))))['points']
                pc_2 = np.load(os.path.join(scene,'features','cloud_{:03d}.npz'.format(int(pair[1]))))['points']

                if mutuals:
                    mutuals_idx = np.load(os.path.join(scene,'correspondences','cloud_{}_{}.npz'.format(pair[0], pair[1])))['mutuals']
                    data = data[np.where(mutuals_idx == 1)[0],:]

                overlap_flag, T_est = do_single_pair_RANSAC_reg(data[:,0:3],data[:,3:], pc_1, pc_2, method='FCGF')
                
                est_trans_par[idx,0:4,0:4] = T_est
                
                reg_metadata.append([pair[0], pair[1], overlap_flag])

                if save_data:
                    np.savez_compressed(
                        os.path.join(save_path, 'cloud_{}_{}.npz'.format(pair[0], pair[1])),
                        t_est=T_est[0:3,3],
                        R_est=T_est[0:3,0:3],
                        overlap=overlap_flag)

            write_trajectory(est_trans_par,reg_metadata,os.path.join(save_path, 'traj.txt'))


def infer_transformation_parameters(feature_path, scene_names, method, model_path, mutuals,overwrite, save_data=False):

    # Model initialization
    logging.info(f"Using the method {method}")
    
    checkpoint = torch.load(model_path)
    config = checkpoint['config']

    Model = load_model(method)

    model = Model(config)

    model_dict = model.state_dict()
    model_dict.update(checkpoint['state_dict'])
    model.load_state_dict(model_dict)
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.to(device)

    for scene in scene_names:
        if mutuals:
            save_path = os.path.join(feature_path, method, model_path.split('/')[-2],'mutuals', scene.split('\\')[-2])
            ensure_dir(save_path)
        else:
            save_path = os.path.join(feature_path, method, model_path.split('/')[-2],'all', scene.split('\\')[-2])
            ensure_dir(save_path)

        # If overwrite or if the transformation data does not yet exist it should be estimated otherwise this step can be skipped to save time
        if os.path.exists(os.path.join(save_path,'traj.txt')) and not overwrite:
            logging.info("Trajectory is already computed and will be loaded for evaluation. If the trajectory should be recomputed select overwrite.")
        else:
            pairs, traj = read_trajectory(os.path.join(scene + "gt.log"))

            #Initialize the variables
            est_trans_par = np.tile(np.expand_dims(np.eye(4),0), (len(pairs),1,1))
            reg_metadata = []

            for idx, pair in enumerate(pairs):
                data_dict = {}
                data = np.load(os.path.join(scene,'correspondences','cloud_{}_{}.npz'.format(pair[0], pair[1])))['x']

                if mutuals:
                    mutuals_idx = np.load(os.path.join(scene,'correspondences','cloud_{}_{}.npz'.format(pair[0], pair[1])))['mutuals']
                    data = data[np.where(mutuals_idx == 1)[0],:]
                # Swap the columns as 3DMatch/Redowod GT is given as R21 T21
                swaped_data = np.zeros_like(data)
                swaped_data[:,0:3] = data[:,3:]
                swaped_data[:,3:] = data[:,0:3]

                data_dict['xs'] = torch.from_numpy(swaped_data).unsqueeze(0).unsqueeze(0).float()

                _, _, rot_est, translation_est, _ = model(data_dict)
                
                transform_est = np.eye(4)
                transform_est[0:3,0:3] = rot_est[-1].squeeze(0).detach().cpu().numpy()
                transform_est[0:3,3] = translation_est[-1].squeeze().detach().cpu().numpy()
                est_trans_par[idx,0:4,0:4] = transform_est
                
                # Estimate the overlap 
                
                # Load both original point clouds
                pc_1 = np.load(os.path.join(scene,'features','cloud_{:03d}.npz'.format(int(pair[0]))))['points']
                pc_2 = np.load(os.path.join(scene,'features','cloud_{:03d}.npz'.format(int(pair[1]))))['points']
                overlap_ratio = compute_overlap_ratio(pc_1, pc_2, transform_est, method = 'FCGF')
                overlap_flag = True if overlap_ratio > 0.3 else False

                reg_metadata.append([pair[0], pair[1], overlap_flag])

                if save_data:
                    np.savez_compressed(
                        os.path.join(save_path, 'cloud_{}_{}.npz'.format(pair[0], pair[1])),
                        t_est=transform_est[0:3,3],
                        R_est=transform_est[0:3,0:,3],
                        overlap=overlap_flag)

            write_trajectory(est_trans_par,reg_metadata,os.path.join(save_path, 'traj.txt'))


def evaluate_registration_performance(source_path, feature_path, method, model, overwrite, num_rand_keypoints=5000, mutuals=False, tau_1=0.1, tau_2=0.05):

    # Extract scene names from the source folder
    scene_names = glob.glob(source_path + '/*/')
    short_scene_names = ['Kitchen','Home 1','Home 2','Hotel 1','Hotel 2','Hotel 3','Study','MIT Lab']
    logging.info("Tau 1: %f, Tau 2: %f" % (tau_1, tau_2))

    re_per_scene = defaultdict(list)
    te_per_scene = defaultdict(list)
    re_all, te_all, precision, recall = [], [], [], []

    # If the method is not RANSAC we first infer and save the transformation paramaters
    if method == 'RANSAC':
        estimate_trans_param_RANSAC(feature_path, scene_names, method, model, mutuals, overwrite)

    else:
        infer_transformation_parameters(feature_path, scene_names, method, model, mutuals, overwrite)


    logging.info("Scene\t¦ prec.\t¦ rec.\t¦ re\t¦ te\t¦")

    for idx, scene in enumerate(scene_names):
        # Extract the values from the gt trajectory and trajectory information files
        gt_pairs, gt_traj = read_trajectory(os.path.join(scene + "gt.log"))
        n_fragments, gt_traj_cov = read_trajectory_info(os.path.join(scene + "gt.info"))
        assert gt_traj.shape[0] > 0, "Empty trajectory file"

        # Extract the estimated transformation matrices
        if mutuals:
            if method == 'RANSAC':
                est_pairs, est_traj = read_trajectory(os.path.join(feature_path,method, 'mutuals', scene.split('\\')[-2], "traj.txt"))
            else:
                est_pairs, est_traj = read_trajectory(os.path.join(feature_path,method, model.split('/')[-2], 'mutuals', scene.split('\\')[-2], "traj.txt"))
        else:
            if method == 'RANSAC':
                est_pairs, est_traj = read_trajectory(os.path.join(feature_path,method, 'all', scene.split('\\')[-2], "traj.txt"))
            else:
                est_pairs, est_traj = read_trajectory(os.path.join(feature_path,method, model.split('/')[-2], 'all', scene.split('\\')[-2], "traj.txt"))
               

        temp_precision, temp_recall = evaluate_registration(n_fragments, est_traj, est_pairs, gt_pairs, gt_traj, gt_traj_cov)
        
        # Filter out only the estimated rotation matrices
        ext_gt_traj = extract_corresponding_trajectors(est_pairs,gt_pairs, gt_traj)

        re = rotation_error(torch.from_numpy(ext_gt_traj[:,0:3,0:3]), torch.from_numpy(est_traj[:,0:3,0:3])).cpu().numpy()
        te = translation_error(torch.from_numpy(ext_gt_traj[:,0:3,3:4]), torch.from_numpy(est_traj[:,0:3,3:4])).cpu().numpy()

        re_per_scene['mean'].append(np.mean(re))
        re_per_scene['median'].append(np.median(re))
        re_per_scene['min'].append(np.min(re))
        re_per_scene['max'].append(np.max(re))
        

        te_per_scene['mean'].append(np.mean(te))
        te_per_scene['median'].append(np.median(te))
        te_per_scene['min'].append(np.min(te))
        te_per_scene['max'].append(np.max(te))


        re_all.extend(re.reshape(-1).tolist())
        te_all.extend(te.reshape(-1).tolist())

        precision.append(temp_precision)
        recall.append(temp_recall)

        logging.info("{}\t¦ {:.3f}\t¦ {:.3f}\t¦ {:.3f}\t¦ {:.3f}\t¦".format(short_scene_names[idx], temp_precision, temp_recall, np.median(re), np.median(te)))

    logging.info("Mean precision {:.3f} +- {:.3f}".format(np.mean(precision),np.std(precision)))
    logging.info("Mean recall {:.3f} +- {:.3f}".format(np.mean(recall),np.std(recall)))





if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '--source', default='./benchmarking/3dmatch/data/', type=str, help='path to 3dmatch test dataset')
    parser.add_argument(
            '--target', default='./benchmarking/3dmatch/results/', type=str, help='path to produce generated data')
    parser.add_argument(
            '--method', default='OANet', type=str, help='Which method should be used [RANSAC, LTFGC, OANet, ACNe]')
    parser.add_argument(
            '-m',
            '--model',
            default=None,
            type=str,
            help='path to latest checkpoint (default: None)')
    parser.add_argument('--evaluate_feature_match_recall', action='store_true')
    parser.add_argument(
            '--evaluate_registration',
            action='store_true',
            help='The target directory must contain extracted features')
    parser.add_argument('--with_cuda', action='store_true')

    parser.add_argument(
            '--overwrite',
            action='store_true',
            help='If results for this method and dataset exist they will be overwritten, otherwised they will be loaded and used')

    parser.add_argument(
            '--num_rand_keypoints',
            type=int,
            default=5000,
            help='Number of random keypoints for each scene')
    parser.add_argument(
            '--mutuals',
            action='store_true',
            help='If only mutualy closest NN should be used (reciprocal matching).')

    args = parser.parse_args()

    device = torch.device('cuda' if args.with_cuda else 'cpu')

    # Ensure that the source and the target folders were provided
    assert args.source is not None
    assert args.target is not None

    ensure_dir(os.path.join(args.target, args.method))

    if args.evaluate_registration:
        with torch.no_grad():
            evaluate_registration_performance(args.source, args.target, args.method, args.model,
                             mutuals=args.mutuals, overwrite=args.overwrite)

