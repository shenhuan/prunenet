import math
import os 
import copy
import torch
from torch import optim
import numpy as np
import torch.nn.functional as F
from torch.distributions import normal
import open3d as o3d
import nibabel.quaternions as nq
import logging

def rotation_error(R1, R2):
    """
    Torch batch implementation of the rotation error between the estimated and the ground truth rotatiom matrix. 
    Rotation error is defined as r_e = \arccos(\frac{Trace(\mathbf{R}_{ij}^{T}\mathbf{R}_{ij}^{\mathrm{GT}) - 1}{2})

    Args: 
        R1 (torch tensor): Estimated rotation matrices [b,3,3]
        R2 (torch tensor): Ground truth rotation matrices [b,3,3]

    Returns:
        ae (torch tensor): Rotation error in angular degreees [b,1]

    """
    R_ = torch.matmul(R1.transpose(1,2), R2)
    e = torch.stack([(torch.trace(R_[_, :, :]) - 1) / 2 for _ in range(R_.shape[0])], dim=0).unsqueeze(1)

    # Clamp the errors to the valid range (otherwise torch.acos() is nan)
    e = torch.clamp(e, -1, 1, out=None)

    ae = torch.acos(e)
    pi = torch.Tensor([math.pi])
    ae = 180. * ae / pi.to(ae.device).type(ae.dtype)

    return ae


def translation_error(t1, t2):
    """
    Torch batch implementation of the rotation error between the estimated and the ground truth rotatiom matrix. 
    Rotation error is defined as r_e = \arccos(\frac{Trace(\mathbf{R}_{ij}^{T}\mathbf{R}_{ij}^{\mathrm{GT}) - 1}{2})

    Args: 
        t1 (torch tensor): Estimated translation vectors [b,3,1]
        t2 (torch tensor): Ground truth translation vectors [b,3,1]

    Returns:
        te (torch tensor): translation error in meters [b,1]

    """
    return torch.norm(t1-t2, dim=(1, 2))


def kabsch_transformation_estimation(x1, x2, weights, normalize_w = False):
    """
    Torch differentiable implementation of the weighted Kabsch algorithm (https://en.wikipedia.org/wiki/Kabsch_algorithm). Based on the correspondences and weights calculates
    the optimal rotation matrix in the sense of the Frobenius norm (RMSD), based on the estimate rotation matrix is then estimates the translation vector hence solving
    the Procrustes problem. This implementation supports batch inputs.

    Args:
        x1            (torch array): points of the first point cloud [b,n,3]
        x2            (torch array): correspondences for the PC1 established in the feature space [b,n,3]
        weights       (torch array): weights denoting if the coorespondence is an inlier (~1) or an outlier (~0) [b,n]
        normalize_w   (bool): flag for normalizing the weights to sum to 1
    Returns:
        rot_matrices  (torch array): estimated rotation matrices [b,3,3]
        trans_vectors (torch array): estimated translation vectors [b,3,1]
        res           (torch array): pointwise residuals (Eucledean distance) [b,n]
        valid_gradient (bool): Flag denoting if the SVD computation converged (gradient is valid)

    """
    
    if normalize_w:
        sum_weights = torch.sum(weights,dim=1,keepdim=True)
        weights = (weights/sum_weights).unsqueeze(2)
    else:
        weights = weights.unsqueeze(2)

    x1_mean = torch.matmul(weights.transpose(1,2), x1) / torch.sum(weights, dim=1).unsqueeze(1)
    x2_mean = torch.matmul(weights.transpose(1,2), x2) / torch.sum(weights, dim=1).unsqueeze(1)

    x1_centered = x1 - x1_mean
    x2_centered = x2 - x2_mean

    weight_matrix = torch.diag_embed(weights.squeeze(2))

    cov_mat = torch.matmul(x1_centered.transpose(1, 2),
                           torch.matmul(weight_matrix, x2_centered))

    try:
        u, s, v = torch.svd(cov_mat)
    except Exception as e:
        r = torch.eye(3,device='cuda')
        r = r.repeat(x1_mean.shape[0],1,1)
        t = torch.zeros((x1_mean.shape[0],3,1), device='cuda')

        res = transformation_residuals(x1, x2, r, t)

        return r, t, res, True

    tm_determinant = torch.det(torch.matmul(v.transpose(1, 2), u.transpose(1, 2)))

    determinant_matrix = torch.diag_embed(torch.cat((torch.ones((tm_determinant.shape[0],2),device='cuda'), tm_determinant.unsqueeze(1)), 1))

    rotation_matrix = torch.matmul(v,torch.matmul(determinant_matrix,u.transpose(1,2)))

    # translation vector
    translation_matrix = x2_mean.transpose(1,2) - torch.matmul(rotation_matrix,x1_mean.transpose(1,2))


    # Residuals
    res = transformation_residuals(x1, x2, rotation_matrix, translation_matrix)

    return rotation_matrix, translation_matrix, res, False


def transformation_residuals(x1, x2, R, t):
    """
    Computer the pointwise residuals based on the estimated transformation paramaters
    
    Args:
        x1  (torch array): points of the first point cloud [b,n,3]
        x2  (torch array): points of the second point cloud [b,n,3]
        R   (torch array): estimated rotation matrice [b,3,3]
        t   (torch array): estimated translation vectors [b,3,1]
    Returns:
        res (torch array): pointwise residuals (Eucledean distance) [b,n,1]
    """
    x2_reconstruct = torch.matmul(R, x1.transpose(1, 2)) + t 

    res = torch.norm(x2_reconstruct.transpose(1, 2) - x2, dim=2)

    return res

def transform_point_cloud(x1, R, t):
    """
    Transforms the point coud using the giver transformation paramaters
    
    Args:
        x1  (torch array): points of the point cloud [b,n,3]
        R   (torch array): estimated rotation matrice [b,3,3]
        t   (torch array): estimated translation vectors [b,3,1]
    Returns:
        x1_t (torch array): points of the transformed point clouds [b,n,3]
    """
    x1_t = (torch.matmul(R, x1.transpose(1, 2)) + t ).transpose(1,2)

    return x1_t


def knn_point(k, pos1, pos2):
    '''
    Input:
        k: int32, number of k in k-nn search
        pos1: (batch_size, ndataset, c) float32 array, input points
        pos2: (batch_size, npoint, c) float32 array, query points
    Output:
        val: (batch_size, npoint, k) float32 array, L2 distances
        idx: (batch_size, npoint, k) int32 array, indices to input points
    '''
    B, N, C = pos1.shape
    M = pos2.shape[1]
    pos1 = pos1.view(B,1,N,-1).repeat(1,M,1,1)
    pos2 = pos2.view(B,M,1,-1).repeat(1,1,N,1)
    dist = torch.sum(-(pos1-pos2)**2,-1)
    val,idx = dist.topk(k=k,dim = -1)
    return idx.int(), val



def axis_angle_to_rot_mat(axes, thetas):
    """
    Computer a rotation matrix from the axis-angle representation using the Rodrigues formula.
    \mathbf{R} = \mathbf{I} + (sin(\theta)\mathbf{K} + (1 - cos(\theta)\mathbf{K}^2), where K = \mathbf{I} \cross \frac{\mathbf{K}}{||\mathbf{K}||}

    Args:
    axes (numpy array): array of axes used to compute the rotation matrices [b,3]
    thetas (numpy array): array of angles used to compute the rotation matrices [b,1]

    Returns:
    rot_matrices (numpy array): array of the rotation matrices computed from the angle, axis representation [b,3,3]

    """

    R = []
    for k in range(axes.shape[0]):
        K = np.cross(np.eye(3), axes[k,:]/np.linalg.norm(axes[k,:]))
        R.append( np.eye(3) + np.sin(thetas[k])*K + (1 - np.cos(thetas[k])) * np.matmul(K,K))

    rot_matrices = np.stack(R)
    return rot_matrices


def sample_random_trans(pcd, randg, rotation_range=360):
    """
    Samples random transformation paramaters with the rotaitons limited to the rotation range

    Args:
    pcd (torch tensor): batch of tensors for which the transformation paramaters are sampled [b,n,3]
    randg (numpy random generator): numpy random generator

    Returns:
    T (torch tensor): batch of randomly sampled transformation paramaters [b,3,3]

    """

    T = torch.eye(4).unsqueeze(0).repeat(pcd.shape[0],1,1).to(pcd.device).type(pcd.dtype)

    axes = np.random.rand(pcd.shape[0],3) - 0.5

    angles = rotation_range * np.pi / 180.0 * (np.random.rand(pcd.shape[0],1) - 0.5)

    R = axis_angle_to_rot_mat(axes, angles)
    R = torch.from_numpy(R).to(pcd.device).type(pcd.dtype)

    T[:, :3, :3] = R
    T[:, :3, 3]  = torch.matmul(R,-torch.mean(pcd, axis=1).unsqueeze(-1)).squeeze()

    return T


def augment_data(data, step, config):
    """
    Function used for data augmention (random transformation) in the training process. It transforms the point from PC1 with a randomly sampled
    transformation matrix and updates the ground truth rotation and translation, respectively.

    Args:
    data (dict): data of the current batch 
    step (int): training iteration, used to determine the maximum rotation angle
    config (dict): input configuration paramaters

    Returns:
    data (dict): updated data of the current batch

    """

    # Compute the maximum angle for current step
    max_angle = math.floor((step - config['train']['data_aug_iter']) / config['train']['data_aug_freq']) * config['train']['data_aug_step']
   
    if max_angle > 360:
        max_angle = 360

    print('Data augmentation! Max angle {} degrees!'.format(max_angle))

    # Sample random transformation matrix for each example in the batch
    T_rand = sample_random_trans(data['xs'][:, 0, :, 0:3], np.random.RandomState(), max_angle)

    # Compute the updated ground truth transformation paramaters R_n = R_gt*R_s^-1, R_N = R_gt*R_s^-1, t_n = t_gt - R_gt*R_s^-1*t_s
    rotation_matrix_inv = torch.inverse(T_rand[:,:3,:3])
    transformed_rs = torch.matmul(data['R'],rotation_matrix_inv)
    transformed_ts = data['t'] - torch.matmul(data['R'], torch.matmul(rotation_matrix_inv, T_rand[:,:3,3].unsqueeze(-1)))

    # Transform the coordinates of the first point cloud with the sampled transformation parmaters
    transformed_data = transform_point_cloud(data['xs'][:, 0, :, 0:3], T_rand[:,:3,:3], T_rand[:,:3,3].unsqueeze(-1))

    # Update the batch data
    data['xs'][:, 0, :, 0:3] = transformed_data
    data['t'] = transformed_ts
    data['R'] = transformed_rs

    return data

class ClippedStepLR(optim.lr_scheduler._LRScheduler):
    def __init__(self, optimizer, step_size, min_lr, gamma=0.1, last_epoch=-1):
        self.step_size = step_size
        self.min_lr = min_lr
        self.gamma = gamma
        super(ClippedStepLR, self).__init__(optimizer, last_epoch)

    def get_lr(self):
        return [max(base_lr * self.gamma ** (self.last_epoch // self.step_size), self.min_lr)
                for base_lr in self.base_lrs]



def ensure_dir(path):
    """
        Creates the directory specigied by the input if it does not yet exist. 
    """
    if not os.path.exists(path):
        os.makedirs(path, mode=0o755)



def read_trajectory(filename, dim=4):
    """
    Function that reads a trajectory saved in the 3DMatch/Redwood format to a numpy array. 
    Format specification can be found at http://redwood-data.org/indoor/fileformat.html
    
    Args:
    filename (str): path to the '.txt' file containing the trajectory data
    dim (int): dimension of the transformation matrix (4x4 for 3D data)

    Returns:
    final_keys (dict): indices of pairs with more than 30% overlap (only this ones are included in the gt file)
    traj (numpy array): gt pairwise transformation matrices for n pairs[n,dim, dim] 
    """

    with open(filename) as f:
        lines = f.readlines()

        # Extract the point cloud pairs
        keys = lines[0::(dim+1)]
        temp_keys = []
        for i in range(len(keys)):
            temp_keys.append(keys[i].split('\t')[0:3])

        final_keys = []
        for i in range(len(temp_keys)):
            final_keys.append([temp_keys[i][0].strip(), temp_keys[i][1].strip(), temp_keys[i][2].strip()])


        traj = []
        for i in range(len(lines)):
            if i % 5 != 0:
                traj.append(lines[i].split('\t')[0:dim])

        traj = np.asarray(traj, dtype=np.float).reshape(-1,dim,dim)
        
        final_keys = np.asarray(final_keys)

        return final_keys, traj



def write_trajectory(traj,metadata, filename, dim=4):
    """
    Writes the trajectory into a '.txt' file in 3DMatch/Redwood format. 
    Format specification can be found at http://redwood-data.org/indoor/fileformat.html

    Args:
    traj (numpy array): trajectory for n pairs[n,dim, dim] 
    metadata (numpy array): file containing metadata about fragment numbers [n,3]
    filename (str): path where to save the '.txt' file containing trajectory data
    dim (int): dimension of the transformation matrix (4x4 for 3D data)
    """

    with open(filename, 'w') as f:
        for idx in range(traj.shape[0]):
            # Only save the transfromation parameters for which the overlap threshold was satisfied
            if metadata[idx][2]:
                p = traj[idx,:,:].tolist()
                f.write('\t'.join(map(str, metadata[idx])) + '\n')
                f.write('\n'.join('\t'.join(map('{0:.12f}'.format, p[i])) for i in range(dim)))
                f.write('\n')


def read_trajectory_info(filename, dim=6):
    """
    Function that reads the trajectory information saved in the 3DMatch/Redwood format to a numpy array.
    Information file contains the variance-covariance matrix of the transformation paramaters. 
    Format specification can be found at http://redwood-data.org/indoor/fileformat.html
    
    Args:
    filename (str): path to the '.txt' file containing the trajectory information data
    dim (int): dimension of the transformation matrix (4x4 for 3D data)

    Returns:
    n_frame (int): number of fragments in the scene
    cov_matrix (numpy array): covariance matrix of the transformation matrices for n pairs[n,dim, dim] 
    """

    with open(filename) as fid:
        contents = fid.readlines()
    n_pairs = len(contents) // 7
    assert (len(contents) == 7 * n_pairs)
    info_list = []
    n_frame = 0

    for i in range(n_pairs):
        frame_idx0, frame_idx1, n_frame = [int(item) for item in contents[i * 7].strip().split()]
        info_matrix = np.concatenate(
            [np.fromstring(item, sep='\t').reshape(1, -1) for item in contents[i * 7 + 1:i * 7 + 7]], axis=0)
        info_list.append(info_matrix)
    
    cov_matrix = np.asarray(info_list, dtype=np.float).reshape(-1,dim,dim)
    
    return n_frame, cov_matrix

def extract_corresponding_trajectors(est_pairs,gt_pairs, gt_traj):
    """
    Extract only those transformation matrices from the ground truth trajectory that are also in the estimated trajectory.
    
    Args:
    est_pairs (numpy array): indices of point cloud pairs with enough estimated overlap [m, 3]
    gt_pairs (numpy array): indices of gt overlaping point cloud pairs [n,3]
    gt_traj (numpy array): 3d array of the gt transformation parameters [n,4,4]

    Returns:
    ext_traj (numpy array): gt transformation parameters for the point cloud pairs from est_pairs [m,4,4] 
    """
    ext_traj = np.zeros((len(est_pairs), 4, 4))

    for est_idx, pair in enumerate(est_pairs):
        pair[2] = gt_pairs[0][2]
        gt_idx = np.where((gt_pairs == pair).all(axis=1))[0]
        
        ext_traj[est_idx,:,:] = gt_traj[gt_idx,:,:]

    return ext_traj

def computeTransformationErr(trans, info):
    """
    Computer the transformation error as an approximation of the RMSE of corresponding points.
    More informaiton at http://redwood-data.org/indoor/registration.html
    
    Args:
    trans (numpy array): transformation matrices [n,4,4]
    info (numpy array): covariance matrices of the gt transformation paramaters [n,4,4]

    Returns:
    p (float): transformation error
    """
    
    t = trans[:3, 3]
    r = trans[:3, :3]
    q = nq.mat2quat(r)
    er = np.concatenate([t, q[1:]], axis=0)
    p = er.reshape(1, 6) @ info @ er.reshape(6, 1) / info[0, 0]
    
    return p.item()


def evaluate_registration(num_fragment, result, result_pairs, gt_pairs, gt, gt_info, err2=0.2):
    """
    Evaluates the performance of the registration algorithm according to the evaluation protocol defined
    by the 3DMatch/Redwood datasets. The evaluation protocol can be found at http://redwood-data.org/indoor/registration.html
    
    Args:
    num_fragment (int): path to the '.txt' file containing the trajectory information data
    result (numpy array): estimated transformation matrices [n,4,4]
    result_pairs (numpy array): indices of the point cloud for which the transformation matrix was estimated (m,3)
    gt_pairs (numpy array): indices of the ground truth overlapping point cloud pairs (n,3)
    gt (numpy array): ground truth transformation matrices [n,4,4]
    gt_cov (numpy array): covariance matrix of the ground truth transfromation parameters [n,6,6]
    err2 (float): threshold for the RMSE of the gt correspondences (default: 0.2m)

    Returns:
    precision (float): mean registration precision over the scene (not so important because it can be increased see papers)
    recall (float): mean registration recall over the scene (deciding parameter for the performance of the algorithm)
    """

    err2 = err2 ** 2
    gt_mask = np.zeros((num_fragment, num_fragment), dtype=np.int)



    for idx in range(gt_pairs.shape[0]):
        i = int(gt_pairs[idx,0])
        j = int(gt_pairs[idx,1])

        # Only non consecutive pairs are tested
        if j - i > 1:
            gt_mask[i, j] = idx

    n_gt = np.sum(gt_mask > 0)

    good = 0
    n_res = 0
    for idx in range(result_pairs.shape[0]):
        i = int(result_pairs[idx,0])
        j = int(result_pairs[idx,1])
        pose = result[idx,:,:]

        if j - i > 1:
            n_res += 1
            if gt_mask[i, j] > 0:
                gt_idx = gt_mask[i, j]
                p = computeTransformationErr(np.linalg.inv(gt[gt_idx,:,:]) @ pose, gt_info[gt_idx,:,:])
                if p <= err2:
                    good += 1
    if n_res == 0:
        n_res += 1e6
    precision = good * 1.0 / n_res
    recall = good * 1.0 / n_gt

    return precision, recall




def do_single_pair_RANSAC_reg(xyz_i, xyz_j, pc_i, pc_j, voxel_size=0.025,method='3DMatch'):
    """
    Runs a RANSAC registration pipeline for a single pair of point clouds.
    
    Args:
    xyz_i (numpy array): coordinates of the correspondences from the first point cloud [n,3]
    xyz_j (numpy array): coordinates of the correspondences from the second point cloud [n,3]
    pc_i (numpy array): coordinates of all the points from the first point cloud [N,3]
    pc_j (numpy array): coordinates of all the points from the second point cloud [N,3]
    method (str): name of the method used for the overlap computation [3DMatch, FCGF]

    Returns:
    overlap_flag (bool): flag denoting if overlap of the point cloud after aplying the estimated trans paramaters if more than a threshold
    trans (numpy array): transformation parameters that trnasform point of point cloud 2 to the coordinate system of point cloud 1
    """

    trans = run_ransac(xyz_j, xyz_i)


    ratio = compute_overlap_ratio(pc_i, pc_j, trans, method, voxel_size)
    
    overlap_flag = True if ratio > 0.3 else False

    
    return [overlap_flag, trans]



def run_ransac(xyz_i, xyz_j):
    """
    Ransac based estimation of the transformation paramaters of the congurency transformation. Estimates the
    transformation parameters thtat map xyz0 to xyz1. Implementation is based on the open3d library
    (http://www.open3d.org/docs/release/python_api/open3d.registration.registration_ransac_based_on_correspondence.html)
    
    Args:
    xyz_i (numpy array): coordinates of the correspondences from the first point cloud [n,3]
    xyz_j (numpy array): coordinates of the correspondences from the second point cloud [n,3]

    Returns:
    trans_param (float): mean registration precision over the scene (not so important because it can be increased see papers)
    recall (float): mean registration recall over the scene (deciding parameter for the performance of the algorithm)
    """

    # Distance threshold as specificed by 3DMatch dataset
    distance_threshold = 0.05

    # Convert the point to an open3d PointCloud object
    xyz0 = o3d.geometry.PointCloud()
    xyz1 = o3d.geometry.PointCloud()
    
    xyz0.points = o3d.utility.Vector3dVector(xyz_i)
    xyz1.points = o3d.utility.Vector3dVector(xyz_j)

    # Correspondences are already sorted
    corr_idx = np.tile(np.expand_dims(np.arange(len(xyz0.points)),1),(1,2))
    corrs = o3d.utility.Vector2iVector(corr_idx)

    result_ransac = o3d.registration.registration_ransac_based_on_correspondence(
        source=xyz0, target=xyz1,corres=corrs, 
        max_correspondence_distance=distance_threshold,
        estimation_method=o3d.registration.TransformationEstimationPointToPoint(False),
        ransac_n=4,
        criteria=o3d.registration.RANSACConvergenceCriteria(50000, 1000))

    trans_param = result_ransac.transformation
    
    return trans_param

  

def compute_overlap_ratio(pc_i, pc_j, trans, method = '3DMatch', voxel_size=0.05):
    """
    Computes the overlap percentage of the two point clouds using the estimateted transformation paramaters and based on the selected method.
    Available methods are 3DMatch/Redwood as defined in the oficial dataset and the faster FCGF method that first downsamples the point clouds.
    Method 3DMatch slightly deviates from the original implementation such that we take the max of the overlaps to check if it is above the threshold
    where as in the original implementation only the overlap relative to PC1 is used. 

    Args:
    pc_i (numpy array): coordinates of all the points from the first point cloud [N,3]
    pc_j (numpy array): coordinates of all the points from the second point cloud [N,3]
    trans (numpy array): estimated transformation paramaters [4,4]
    method (str): name of the method for overlap computation to be used ['3DMatch', 'FCGF']
    voxel size (float): voxel size used to downsample the point clouds when 'FCGF' method is selected

    Returns:
    overlap (float): max of the computed overlap ratios relative to the PC1 and PC2
    """

    # Convert the point to an open3d PointCloud object
    pcd0 = o3d.geometry.PointCloud()
    pcd1 = o3d.geometry.PointCloud()
    pcd0.points = o3d.utility.Vector3dVector(pc_i)
    pcd1.points = o3d.utility.Vector3dVector(pc_j)

    if method == '3DMatch':
        matching01 = get_matching_indices(pcd0, pcd1, np.linalg.inv(trans), search_voxel_size = 0.05, K=1)
        matching10 = get_matching_indices(pcd0, pcd1, trans,
                                    search_voxel_size=0.05, K=1)
        overlap0 = len(matching01) / len(pcd0.points)
        overlap1 = len(matching10) / len(pcd1.points)

    elif method == 'FCGF':
        pcd0_down = pcd0.voxel_down_sample(voxel_size)
        pcd1_down = pcd1.voxel_down_sample(voxel_size)
        matching01 = get_matching_indices(pcd0_down, pcd1_down, np.linalg.inv(trans), search_voxel_size = 0.05, K=1)
        matching10 = get_matching_indices(pcd1_down, pcd0_down, trans,
                                    search_voxel_size = 0.05, K=1)
        overlap0 = len(matching01) / len(pcd0_down.points)
        overlap1 = len(matching10) / len(pcd1_down.points)

    else:
        logging.error("Wrong overlap computation method was selected.")

    
    return max(overlap0, overlap1)


def get_matching_indices(pc_i, pc_j, trans, search_voxel_size=0.025, K=None):   
    """
    Helper function for the point cloud overlap computation. Based on the estimated transformation parameters transforms the point cloud
    and searches for the neares neighbor in the other point cloud.

    Args:
    pc_i (numpy array): coordinates of all the points from the first point cloud [N,3]
    pc_j (numpy array): coordinates of all the points from the second point cloud [N,3]
    trans (numpy array): estimated transformation paramaters [4,4]
    search_voxel_size (float): threshold used to determine if a point has a correspondence given the estimated trans parameters
    K (int): number of nearest neighbors to be returned

    Returns:
    match_inds (list): indices of points that have a correspondence withing the search_voxel_size
    """

    pc_i_copy = copy.deepcopy(pc_i)
    pc_j_copy = copy.deepcopy(pc_j)
    pc_i_copy.transform(trans)
    pcd_tree = o3d.geometry.KDTreeFlann(pc_j_copy)

    match_inds = []
    for i, point in enumerate(pc_i_copy.points):
        [_, idx, _] = pcd_tree.search_radius_vector_3d(point, search_voxel_size)
        if K is not None:
            idx = idx[:K]
            
        for j in idx:
            match_inds.append((i, j))

    return match_inds




if __name__ == '__main__':

    pc_1 = np.random.rand(5000,3)
    pc_2 = np.random.rand(5000,3)

    xyz0 = o3d.geometry.PointCloud()
    xyz1 = o3d.geometry.PointCloud()
    
    xyz0.points = o3d.utility.Vector3dVector(pc_1)
    xyz1.points = o3d.utility.Vector3dVector(pc_2)
    
    run_ransac(xyz0, xyz1)