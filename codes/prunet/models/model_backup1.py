import os,sys,math,torch
import numpy as np
from tqdm import tqdm
import torch.nn as nn
import torch.nn.functional as F
from models.filter.ltfgc import LTFGCBlock
from models.filter.oanet import OANBlock
from models.procrustes import SVDHead
from models.pointnet2 import pointnet2_utils
from models.pointconv_util import index_points_gather,group_query,knn_point,index_points_group,square_distance
from lib.util import get_angle_deviation,mutual_selection,freeze_parameter,log_perm_to_correspondence
from models.filter.sinkhorn import log_optimal_transport
from models.set_aggre import MaxPooling,AveragePooling,AttentivePooling,NetVLADLoupe
from sklearn.metrics import precision_recall_fscore_support
from models import load_model
from models.feat_extract.multiplexgnn import MLP,AttentionalGNN

class KeypointEncoder(nn.Module):
    """ Joint encoding of visual appearance and location using MLPs"""
    def __init__(self, feature_dim, layers):
        super().__init__()
        self.encoder = MLP([3] + layers + [feature_dim])
        nn.init.constant_(self.encoder[-1].bias, 0.0)

    def forward(self, kpts):
        return self.encoder(kpts)


class PruneNet(nn.Module):
    def __init__(self, args):
        super(PruneNet, self).__init__()
        self.n_neighbors=args['model']['n_neighbors']
        self.n_clusters=args['model']['n_clusters']
        self.inlier_threshold=args['data']['inlier_threshold']
        self.n_sinkhorn=args['model']['n_sinkhorn']
        self.patch_inlier_threshold=args['model']['patch_inlier_threshold']
        self.patch_outlier_threshold=args['model']['patch_outlier_threshold']
        self.n_embed_dims=args['set_aggregation']['descriptor_dim']
        self.topk=args['model']['topk']

        ###################################
        # sampler and its temperature
        ###################################
        tau = torch.nn.Parameter(torch.tensor(args['model']['tau']),requires_grad=False) 
        self.register_parameter('tau', tau) # temperature
        self.cat_sampler=args['model']['sampler']

        # ###################################
        # # gnn 
        # ###################################
        # self.kenc = KeypointEncoder(self.n_embed_dims, args['superglue']['keypoint_encoder'])
        # self.gnn = AttentionalGNN(self.n_embed_dims, args['superglue']['GNN_layers']*args['superglue']['n_GNN_layers'])
        # self.final_proj = nn.Conv1d(self.n_embed_dims, self.n_embed_dims,kernel_size=1, bias=True)
        # bin_score = torch.nn.Parameter(torch.tensor(2.)) # the value from the pre-trained model is 2.3457
        # self.register_parameter('point_bin_score', bin_score)
        # self.match_threshold=args['superglue']['match_threshold']


        ###################################
        # filtering network, freeze the parameters
        ###################################
        if(args['model']['filtering']=='ltfgc'):
            net_channels = args['LTFGC']['net_channels']
            depth_each_stage = args['LTFGC']['net_depth'] // (
                args['LTFGC']['iter_num']+1)
            input_channel = 6 # [x1,y1,z1,x2,y2,z2]
            self.weights_init = LTFGCBlock(
                net_channels, input_channel, depth_each_stage)
            self.weights_iter = [LTFGCBlock(net_channels, 8, depth_each_stage)]
            self.weights_iter = nn.Sequential(*self.weights_iter)
            self.filtering_threshold=args['LTFGC']['threshold']

        elif(args['model']['filtering']=='oanet'):
            depth_each_stage = args['OANet']['net_depth']//(args['OANet']['iter_num']+1)

            self.weights_init = OANBlock(args['OANet']['net_channel'], 6,
                                    depth_each_stage, args['OANet']['clusters'], args['OANet']['normalize_weights'])
            
            self.weights_iter = [OANBlock(args['OANet']['net_channel'], 8, depth_each_stage, args['OANet']['clusters'], args['OANet']['normalize_weights'])]
            self.weights_iter = nn.Sequential(*self.weights_iter)
            self.filtering_threshold=args['OANet']['threshold']

        freeze_parameter(self.weights_init)
        freeze_parameter(self.weights_iter)

        ###################################
        # set aggregation
        ###################################
        self.set_aggregation_init=AttentivePooling(args)
        self.set_aggregation_iter=AttentivePooling(args)
        bin_score = torch.nn.Parameter(torch.tensor([2.]*2)) 
        self.register_parameter('patch_bin_score_list', bin_score)

        ###################################
        # pose estimation
        ###################################
        self.head = SVDHead(args=args)



    def _get_correspndence(self,src_pcd,tgt_pcd,src_embedding,tgt_embedding,temperature):
        """
        Find the correspondence from tgt_pcd for each point in src_pcd
        Input:
            src_pcd:        [B,3,N]
            tgt_pcd:        [B,3,N]
            src_embedding:  [B,C,N]
            tgt_embedding:  [B,C,N]
            temperature:    scalar
        Return:
            src_ref_pcd:    [B,3,N]
        """
        C=src_embedding.size(1)
        scores = torch.matmul(src_embedding.transpose(2, 1), tgt_embedding) / math.sqrt(C)
        temperature=temperature.repeat(scores.size())
        scores = F.softmax(scores / temperature, dim=2)

        if self.cat_sampler == 'gumbel_softmax': # one-hot encoding of soft-assignment
            #  gumbels = -torch.empty_like(scores, memory_format=torch.legacy_contiguous_format).exponential_().log() # ~Gumbel(0,1)
             index = scores.max(-1, keepdim=True)[1]
             y_hard = torch.zeros_like(scores,requires_grad=False).scatter_(-1, index, 1.0)
             scores = y_hard - scores.detach()+scores 

        src_ref_pcd = torch.matmul(tgt_pcd,scores.transpose(1,2))
        return src_ref_pcd

    
    def _estimate_pose(self,src_pcd,src_ref_pcd):
        """
        Use LTFGC to weight the outliers and then solve the 6DoF poses using SVD
        Input:
            src_pcd:        [B*topk,N,3]
            src_ref_pcd:    [B*topk,N,3]
        Return:
            rot_est:        [B,3,3]
            trans_est:      [B,3,1]
            logits:         [B,N]
            weights:        [B,N]
            src_pcd:        [B,N,3]
            src_ref_pcd:    [B,N,3]
        """
        correspondence = torch.cat((src_pcd.unsqueeze(3),src_ref_pcd.unsqueeze(3)),2).transpose(1,2)/4
        ############################################################
        #### Use filtering network to initialize weights
        logits,weights = self.weights_init(correspondence)
        _,_,residuals= self.head(src_pcd,src_ref_pcd,weights)

        ############################################################
        #### Use filtering network to refine weights
        new_correspondences = torch.cat((correspondence.detach(),residuals.detach().unsqueeze(1).unsqueeze(3),weights.detach().unsqueeze(1).unsqueeze(3)),dim=1)
        
        logits,weights = self.weights_iter(new_correspondences)
        rot_est,trans_est,new_residuals= self.head(src_pcd,src_ref_pcd.detach(),weights)


        return rot_est,trans_est,logits,weights,src_pcd,src_ref_pcd
    

    def _estimate_overlap(self,src_pcd,ref_pcd,rot_gt,trans_gt):
        """
        We get the patch-wise overlap ratios on the fly
        Input:
            src_pcd:    [Bxn_clusters**2, N_neighbor, 3]
            ref_pcd:    [Bxn_clusters**2, N_neighbor, 3]
            rot_gt:     [B,3,3]
            trans_gt:   [B,3,1]
        Return:
            overlaps:   [B, n_clusters, n_clusters]
        """
        n_neighbors=src_pcd.size(1)
        rot_gt=torch.repeat_interleave(rot_gt,self.n_clusters**2,0)
        trans_gt=torch.repeat_interleave(trans_gt,self.n_clusters**2,0)
        src_pcd_trans=(torch.matmul(rot_gt,src_pcd.transpose(1,2))+trans_gt).transpose(1,2)
        distances=square_distance(src_pcd_trans,ref_pcd)   #[BxN_sparse_pts**2, N_neighbor, N_neighbor]
        inliers_ratio=((distances<self.inlier_threshold**2).sum(2)>0).sum(1)*1.0/n_neighbors
        inliers_ratio=inliers_ratio.view(-1,self.n_clusters,self.n_clusters)

        return inliers_ratio

    def _get_permutation_matrix(self,overlap_matrix):
        """
        Given the overlap_matrix, we build patch-wise ground truth permutation matrix
        Input:  
            overlap_matrix:     [B,N,N]
        
        Return:
            permutation_matrix: [B,N+1,N+1]
        """
        mutuals=torch.from_numpy(mutual_selection(overlap_matrix)).to(overlap_matrix.device)  #[B,N,N]
        thresholding = overlap_matrix>self.inlier_threshold
        permutation_matrix = mutuals & thresholding
        permutation_matrix = F.pad(permutation_matrix,(0,1,0,1),'constant',0)  #[B,N+1,N+1]

        # assign outliers 
        overlap_matrix=F.pad(overlap_matrix,(0,1,0,1),'constant',-1e7)
        flag_row=(permutation_matrix.sum(2)==0.) & (overlap_matrix.max(2)[0]<=self.patch_outlier_threshold)
        flag_column=(permutation_matrix.sum(1)==0.) & (overlap_matrix.max(1)[0]<=self.patch_outlier_threshold)
        permutation_matrix[:,:,-1]=flag_row.float()
        permutation_matrix[:,-1,:]=flag_column.float()
        permutation_matrix[:,-1,-1]=0

        return permutation_matrix.float()

    def _fps_sample_and_cluster(self,pcd,embedding,n_clusters,n_neighbors):
        """
        Use FPS to sample centroids and build the cluster
        Input:
            pcd:            [B,N,3]
            embedding:      [B,N,C]
            n_clusters:     integer
            n_neighbors:    integer
        Output:
            grouped_pcd:        [B,n_clusters,n_neighbors,3]
            grouped_embedding:  [B,n_clusters,n_neighbors,C]
            knn_idx:            [B,n_clusters, n_neighbors]
        """
        # FPS sampling
        fps_idx = pointnet2_utils.furthest_point_sample(pcd, n_clusters)
        centroids = index_points_gather(pcd, fps_idx)                 # [B, n_clusters,3]

        # KNN search to build the cluster
        knn_idx = knn_point(n_neighbors, pcd, centroids)         # [B, n_clusters, n_neighbors]
        grouped_pcd = index_points_group(pcd, knn_idx)              # [B, n_clusters, n_neighbors, 3]
        grouped_embedding=index_points_group(embedding, knn_idx)    # [B, n_clusters, n_neighbors, C]

        return grouped_pcd,grouped_embedding,knn_idx


    def _mine_patch(self,confidence,grouped_src_pcd,grouped_src_embedding,grouped_tgt_pcd,grouped_tgt_embedding,knn_src_idx,knn_tgt_idx,topk):
        """
        Given the confidence estimation between patches, we determine topk pairs of patches to use 
        Input:
            confidence:             [N, n_clusters, n_clusters]
            grouped_src_pcd:        [B,n_clusters,n_neighbors,3]     
            grouped_src_embedding:  [B,n_clusters,n_neighbors,C]
            knn_src_idx:            [B,n_clusters,n_neighbors]
            topk:                   integer
        Return:
            topk_src_pcd:           [B, K, n_neighbors, 3]
            topk_src_embedding:     [B, k, n_neighbors, C]
            topk_knn_src_idx:          [B,K,n_neighbors]
        """
        _,n_clusters,N,C=grouped_src_embedding.size()

        confidence=confidence.view(-1,n_clusters**2)
        topk_confidence=torch.topk(confidence,k=topk,dim=-1,sorted=False)[1]
        topk_src=(topk_confidence // n_clusters).unsqueeze(-1).unsqueeze(-1)
        topk_tgt=(topk_confidence % n_clusters).unsqueeze(-1).unsqueeze(-1)

        topk_src_pcd=torch.gather(grouped_src_pcd,1,index=topk_src.repeat(1,1,N,3))
        topk_tgt_pcd=torch.gather(grouped_tgt_pcd,1,index=topk_tgt.repeat(1,1,N,3))
        topk_src_embedding=torch.gather(grouped_src_embedding,1,index=topk_src.repeat(1,1,N,C))
        topk_tgt_embedding=torch.gather(grouped_tgt_embedding,1,index=topk_tgt.repeat(1,1,N,C))

        topk_knn_src_idx=torch.gather(knn_src_idx,1,index=topk_src.squeeze(-1).repeat(1,1,N))
        topk_knn_tgt_idx=torch.gather(knn_tgt_idx,1,index=topk_tgt.squeeze(-1).repeat(1,1,N))

        return topk_src_pcd,topk_src_embedding,topk_tgt_pcd,topk_tgt_embedding,topk_knn_src_idx,topk_knn_tgt_idx

    
    def _get_precision_recall(self,rot_gt,trans_gt,src,src_ref,weights):
        """
        Get the feature match precision and recall
        Input:
            rot_gt:         [B,3,3]
            trans_gt:       [B,3,1]
            src:            [B,3,N]
            src_ref:        [B,3,N]
            weights:        [B,N]
        Return:
            precisons:      list
            recalls:        list
            inlier_ratios:  list
        """
        src_trans=torch.matmul(rot_gt,src)+trans_gt
        distance=torch.norm(src_trans-src_ref,dim=1) 
        label_gt=(distance<self.inlier_threshold).float()
        label_est=weights.round()
        precisions,recalls,inlier_ratios=[],[],[]
        for i in range(src_trans.size(0)):
            precision, recall, _, _ = precision_recall_fscore_support(label_gt[i].detach().cpu().numpy(),label_est[i].detach().cpu().numpy(), average='binary')
            precisions.append(round(precision,2))
            recalls.append(round(recall,2))
            inlier_ratios.append(round((label_gt[i].sum()/src.size(2)).item(),2))
        return precisions,recalls,inlier_ratios

    def _gather_pcd_embedding(self,pcd,embedding,idx,n_points):
        """
        The Idea is sampling a fixed number of points and associated embeddings given indice 
        Input:
            pcd:            [B,N,3]
            embedding:      [B,N,C]
            idx:            [B,n]
            n_points:       integer
        Return:
            pcd:            [B,n_points,3]
            embedding:      [B,n_points,C]
        """
        B,N,C=embedding.size()

        indice=[]
        for i in range(B):
            unique_idx=list(set(idx[i].tolist()))
            n_unique_idx=len(unique_idx)
            if(n_unique_idx>=n_points):
                c_idx=np.random.permutation(n_unique_idx)[:n_points]
            else:
                c_idx=np.random.choice(n_unique_idx,n_points)
            unique_idx=np.array(unique_idx)[c_idx]
            indice.append(unique_idx)
        indice=torch.from_numpy(np.array(indice)).unsqueeze(-1).to(pcd.device).long() 
        sampled_pcd=torch.gather(pcd,1,index=indice.repeat(1,1,3)).detach().contiguous()
        sampled_embedding=torch.gather(embedding,1,index=indice.repeat(1,1,C)).detach().contiguous()

        return sampled_pcd,sampled_embedding

    def _knn_search(self,pcd,embedding,centroids,n_neighbors):
        """
        Given centroids and neighborhood size, apply KNN search
        Input:
            pcd:                [B,N,3]
            centroid:           [B,k,3]
            embedding:          [B,N,C]
            n_neighbors:        integer
        Return:
            grouped_pcd:        [B,n_neighbor,k,3]
            grouped_embedding:  [B,n_neighbor,k,C]
        """
        knn_idx = knn_point(n_neighbors, pcd, centroids)         # [B, n_clusters, n_neighbors]
        grouped_pcd = index_points_group(pcd, knn_idx).detach()               # [B, n_clusters, n_neighbors, 3]
        grouped_embedding=index_points_group(embedding, knn_idx).detach()     # [B, n_clusters, n_neighbors, C]

        return grouped_pcd,grouped_embedding

    def _random_sampling(self,pcd,embedding,n_samples):
        """
        Randomly sample the n_samples given point clouds and embeddings
        Input:
            pcd:            [B,N,3]
            embedding:      [B,N,C]
            n_samples:      integer
        Output:
            pcd:            [B,n_samples,3]
            embedding:      [B,n_samples,C]
        """
        device=pcd.device
        batch_size,N,C=embedding.size()
        indice=torch.from_numpy(np.random.choice(N,n_samples,replace=False)).unsqueeze(0).unsqueeze(-1).to(device)
        pcd=torch.gather(pcd,1,index=indice.repeat(batch_size,1,3)).contiguous().to(device)
        embedding=torch.gather(embedding,1,index=indice.repeat(batch_size,1,C)).contiguous().to(device)
        return pcd,embedding

    
    def _superglue(self):
        # ###########################################################
        # # 4. Run SuperGlue to communicate point-wise features
        # topk_src_pcd,topk_tgt_pcd=topk_src_pcd.transpose(1,2),topk_tgt_pcd.transpose(1,2)
        # topk_src_embedding,topk_tgt_embedding=topk_src_embedding.transpose(1,2),topk_tgt_embedding.transpose(1,2)
        # topk_src_embedding = topk_src_embedding + self.kenc(topk_src_pcd/4) # normalize keypoints and encode it
        # topk_tgt_embedding = topk_tgt_embedding + self.kenc(topk_tgt_pcd/4)
        # topk_src_embedding, topk_tgt_embedding = self.gnn(topk_src_embedding, topk_tgt_embedding)
        # topk_src_embedding, topk_tgt_embedding = self.final_proj(topk_src_embedding), self.final_proj(topk_tgt_embedding)

        # scores = torch.einsum('bdn,bdm->bnm', topk_src_embedding, topk_tgt_embedding)
        # scores = scores / self.n_embed_dims**.5
        # log_permutation_matrix1 = log_optimal_transport(scores, self.bin_score,iters=self.n_sinkhorn) 

        # src_corr,weights1=log_perm_to_correspondence(log_permutation_matrix1,self.match_threshold,topk_tgt_pcd,threshold=False)
        # rot_est,trans_est,_ = self.head(topk_src_pcd, src_corr.transpose(1,2), weights1) 

        # topk_src_pcd,topk_tgt_pcd=topk_src_pcd.transpose(1,2),topk_tgt_pcd.transpose(1,2)
        # topk_src_embedding,topk_tgt_embedding=topk_src_embedding.transpose(1,2),topk_tgt_embedding.transpose(1,2)
        pass

    
    def forward(self, *input):
        """
        Input:
            src_pcd:        [B,N,3]
            src_embedding:  [B,N,C]
            rot_gt:         [B,3,3]
            trans_gt:       [B,3,1]
        """
        src_pcd,tgt_pcd=input[0],input[1]
        src_embedding,tgt_embedding=input[2],input[3]
        rot_gt,trans_gt=input[4],input[5]
        c_iter=input[6]
        batch_size=src_pcd.size(0)

        # Here we only train the set aggregation part
        list_est_permutation_matrix,list_gt_permutation_matrix=[],[]

        ##############################################
        # 1. cluster the source and target point clouds based on FPS sampling
        ##############################################
        grouped_src_pcd,grouped_src_embedding,knn_src_idx=self._fps_sample_and_cluster(src_pcd,src_embedding,self.n_clusters,self.n_neighbors)
        grouped_tgt_pcd,grouped_tgt_embedding,knn_tgt_idx=self._fps_sample_and_cluster(tgt_pcd,tgt_embedding,self.n_clusters,self.n_neighbors)
        
        ###################################
        #get ground truth permutation matrix and overlap ratios between patches
        overlap_matrix=torch.rand(batch_size,self.n_clusters,self.n_clusters)
        gt_permutation_matrix=torch.rand(batch_size,self.n_clusters+1,self.n_clusters+1)
        # c_grouped_src_pcd=torch.repeat_interleave(grouped_src_pcd,self.n_clusters,1).contiguous().view(-1,self.n_neighbors,3)
        # c_grouped_tgt_pcd=grouped_tgt_pcd.repeat(1,self.n_clusters,1,1).contiguous().view(-1,self.n_neighbors,3)
        # overlap_matrix=self._estimate_overlap(c_grouped_src_pcd,c_grouped_tgt_pcd,rot_gt,trans_gt)   
        # gt_permutation_matrix=self._get_permutation_matrix(overlap_matrix) 
        list_gt_permutation_matrix.append(gt_permutation_matrix)


        ###################################
        # 2. set aggregation
        ###################################
        src_patch_feats,tgt_patch_feats=self.set_aggregation_init(grouped_src_embedding,grouped_tgt_embedding)
        dim_feats=src_patch_feats.size(-1)

        ###################################
        # 3. run sinkhorn over patches
        ###################################
        scores=torch.matmul(src_patch_feats,tgt_patch_feats.transpose(1,2))/dim_feats**.5  #[B,n_clusters,n_clusters]
        log_permutation_matrix = log_optimal_transport(scores, self.patch_bin_score_list[0],iters=self.n_sinkhorn) 
        list_est_permutation_matrix.append(log_permutation_matrix)

        ###########################################################
        # 4. mine the topk confident pairs of patches
        confidence=log_permutation_matrix[:,:-1,:-1].exp() #[B,N_sparse,N_sparse]
        topk_src_pcd,topk_src_embedding,topk_tgt_pcd,topk_tgt_embedding,_,_ =self._mine_patch(confidence,grouped_src_pcd,grouped_src_embedding,grouped_tgt_pcd,grouped_tgt_embedding,knn_src_idx,knn_tgt_idx, self.topk)
        
        
        B,_,N,_=topk_src_pcd.size()
        topk_src_pcd,topk_tgt_pcd=topk_src_pcd.view(B,-1,3),topk_tgt_pcd.view(B,-1,3)
        topk_src_embedding,topk_tgt_embedding=topk_src_embedding.view(B,-1,self.n_embed_dims),topk_tgt_embedding.view(B,-1,self.n_embed_dims)
        
        src_ref = self._get_correspndence(topk_src_pcd.transpose(1,2),topk_tgt_pcd.transpose(1,2),topk_src_embedding.transpose(1,2),topk_tgt_embedding.transpose(1,2),self.tau).transpose(1,2)
        tgt_ref = self._get_correspndence(topk_tgt_pcd.transpose(1,2),topk_src_pcd.transpose(1,2),topk_tgt_embedding.transpose(1,2),topk_src_embedding.transpose(1,2),self.tau).transpose(1,2)
        c_source=torch.cat((topk_src_pcd,tgt_ref),1)
        c_target=torch.cat((src_ref,topk_tgt_pcd),1)
        rot_est,trans_est,logits,weights,c_source,c_target=self._estimate_pose(c_source,c_target)

        return list_est_permutation_matrix,list_gt_permutation_matrix,self.patch_bin_score_list[0],rot_est,trans_est,logits,weights,c_source,c_target


class Engine(nn.Module):
    def __init__(self,args):
        super(Engine,self).__init__()
        self.model=PruneNet(args)

        ###################################
        # checkpoint
        # ###################################
        if(args['checkpoint']['restore']): # restore the pre-trained model
            self.load(args['checkpoint']['checkpoint'])
        if torch.cuda.device_count() > 1: # data parallel
            self.model = nn.DataParallel(self.model)

    def forward(self,*input):
        return self.model(*input)

    def save(self, path):
        if torch.cuda.device_count() > 1:
            state={
                'state_dict':self.model.module.state_dict()
            }
            torch.save(state,path)
        else:
            state={
                'state_dict':self.model.state_dict()
            }
            torch.save(state, path)

    def load(self, path):
        checkpoint = torch.load(path)['state_dict']
        model_dict = self.model.state_dict()
        pretrained_dict = {k: v for k, v in checkpoint.items() if k in model_dict}
        model_dict.update(pretrained_dict)
        self.model.load_state_dict(model_dict)