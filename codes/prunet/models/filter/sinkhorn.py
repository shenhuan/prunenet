import torch
import torch.nn as nn

######################################
# Used in RPM-Net
#######################################
def sinkhorn(log_socre, n_iters: int = 5, slack: bool = True) -> torch.Tensor:
    """ Run sinkhorn iterations to generate a near doubly stochastic matrix, where each row or column sum to <=1
    Args:
        log_socre: log of positive matrix to apply sinkhorn normalization (B, J, K)
        n_iters (int): Number of normalization iterations
        slack (bool): Whether to include slack row and column

    Returns:
        log(perm_matrix): Doubly stochastic matrix (B, J+1, K+1)

    Modified from original source taken from:
        Learning Latent Permutations with Gumbel-Sinkhorn Networks
        https://github.com/HeddaCohenIndelman/Learning-Gumbel-Sinkhorn-Permutations-w-Pytorch
    """

    # pad zero to the logarithm
    zero_pad = nn.ZeroPad2d((0, 1, 0, 1))
    log_socre_padded = zero_pad(log_socre)

    for i in range(n_iters):
        # Row normalization
        log_socre_padded = torch.cat((log_socre_padded[:, :-1, :] - (torch.logsumexp(log_socre_padded[:, :-1, :], dim=2, keepdim=True)),log_socre_padded[:, -1, None, :]),dim=1)  # Don't normalize last row, because of the log_socre_padded[:,-1,-1]

        # Column normalization
        log_socre_padded = torch.cat((log_socre_padded[:, :, :-1] - (torch.logsumexp(log_socre_padded[:, :, :-1], dim=1, keepdim=True)),log_socre_padded[:, :, -1, None]),  dim=2) # Don't normalize last column
            
    return log_socre_padded



###################################
# Used in SuperGlue
###################################
def log_sinkhorn_iterations(Z, log_mu, log_nu, iters: int):
    """ Perform Sinkhorn Normalization in Log-space for stability"""
    u, v = torch.zeros_like(log_mu), torch.zeros_like(log_nu)
    for _ in range(iters):
        u = log_mu - torch.logsumexp(Z + v.unsqueeze(1), dim=2)
        v = log_nu - torch.logsumexp(Z + u.unsqueeze(2), dim=1)
    return Z + u.unsqueeze(2) + v.unsqueeze(1)


def log_optimal_transport(scores, alpha, iters: int):
    """ Perform Differentiable Optimal Transport in Log-space for stability"""
    b, m, n = scores.shape
    one = scores.new_tensor(1)
    ms, ns = (m*one).to(scores), (n*one).to(scores)

    bins0 = alpha.expand(b, m, 1)
    bins1 = alpha.expand(b, 1, n)
    alpha = alpha.expand(b, 1, 1)

    couplings = torch.cat([torch.cat([scores, bins0], -1),
                           torch.cat([bins1, alpha], -1)], 1)

    norm = - (ms + ns).log()
    log_mu = torch.cat([norm.expand(m), ns.log()[None] + norm])
    log_nu = torch.cat([norm.expand(n), ms.log()[None] + norm])
    log_mu, log_nu = log_mu[None].expand(b, -1), log_nu[None].expand(b, -1)

    Z = log_sinkhorn_iterations(couplings, log_mu, log_nu, iters)
    Z = Z - norm  # multiply probabilities by M+N
    return Z

