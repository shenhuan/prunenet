import torch
import torch.nn as nn

class PointCN(nn.Module):
    def __init__(self, channels, out_channels=None):
        nn.Module.__init__(self)
        if not out_channels:
           out_channels = channels
        self.shot_cut = None
        if out_channels != channels:
            self.shot_cut = nn.Conv2d(channels, out_channels, kernel_size=1)
        
        self.conv = nn.Sequential(
                nn.Conv2d(channels, out_channels, kernel_size=1),
                nn.InstanceNorm2d(channels),
                nn.BatchNorm2d(channels),
                nn.ReLU(),
                nn.Conv2d(out_channels, out_channels, kernel_size=1),
                nn.InstanceNorm2d(out_channels),
                nn.BatchNorm2d(out_channels),
                nn.ReLU()
                )
    
    def forward(self, x):
        out = self.conv(x)
        if self.shot_cut:
            out = out + self.shot_cut(x)
        else:
            out = out + x
        return out


class LTFGCBlock(nn.Module):
    '''
    return the logits & weights for each pair
    '''
    def __init__(self, net_channels, input_channel, depth):
        nn.Module.__init__(self)
        channels = net_channels
        self.layer_num = depth
        # print('channels:'+str(channels)+', layer_num:'+str(self.layer_num))

        # Initial MLP that maps from input dim to num of channels in the networks
        self.conv1 = nn.Conv2d(input_channel, channels, kernel_size=1)
        
        # Resnet blocks
        self.l1_1 = []
        for _ in range(self.layer_num):
            self.l1_1.append(PointCN(channels))

        self.l1_1 = nn.Sequential(*self.l1_1)

        # Last multilayer perceptron that maps from network channels to a single weight
        self.output = nn.Conv2d(channels, 1, kernel_size=1)


    def forward(self, data):
        '''
        Input:
            data:       [B,C,N,1]
        return: 
            logits:     [B,N] 
            weights:    [B,N]
        '''
        x1_1 = self.conv1(data)
        out = self.l1_1(x1_1)

        logits = self.output(out).squeeze(3).squeeze(1) # used for calculating cross entropy loss
        weights = torch.relu(torch.tanh(logits)) # between [0,1]

        return logits,weights