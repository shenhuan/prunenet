from __future__ import print_function
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import math


class MaxPooling(nn.Module):
    def __init__(self,args):
        super(MaxPooling,self).__init__()
    
    def forward(src_embedding,ref_embedding):
        return src_embedding.max(2)[0],ref_embedding.max(2)[0]

class AveragePooling(nn.Module):
    def __init__(self,args):
        super(AveragePooling,self).__init__()
    
    def forward(src_embedding,ref_embedding):
        return src_embedding.sum(2),ref_embedding.sum(2)

class AttentivePooling(nn.Module):
    def __init__(self,args):
        super(AttentivePooling,self).__init__()
        self.in_dim=args['set_aggregation']['descriptor_dim']
        self.out_dim=args['set_aggregation']['out_dim']

        self.fc=nn.Linear(32,1,bias=False)
        self.proj2=nn.Conv1d(32,64,kernel_size=1,bias=True)
        self.bn2=nn.BatchNorm1d(64)
        self.proj3=nn.Conv1d(64,128,kernel_size=1,bias=True)
        self.bn3=nn.BatchNorm1d(128)
        self.proj4=nn.Conv1d(128,256,kernel_size=1,bias=True)

    def forward(self,*input):
        """
        We we attentive pooling for set aggregation
        Input:
            src_embedding:      [B,N,n_neighbor,C]
            ref_embedding:      [B,N,n_neighbor,C]
            c_out:              scalar, output feature dimensions
        Return:
            src_feats:          [B,N,C1]
            ref_feats:          [B,N,C1]
        """
        src_embedding,ref_embedding=input[0],input[1]
        # src_embedding=src_embedding.transpose(1,3)
        # ref_embedding=ref_embedding.transpose(1,3)

        # # project the FCGF features before assign weights
        # src_feats=F.relu(self.bn1(self.proj1(src_embedding))).transpose(1,3)
        # ref_feats=F.relu(self.bn1(self.proj1(ref_embedding))).transpose(1,3)

        # get weights
        src_weights=F.softmax(self.fc(src_embedding),dim=2) #[B,N,n_neighbor,1]
        ref_weights=F.softmax(self.fc(ref_embedding),dim=2) 

        # attentive pooling
        src_weighted_feats=src_embedding*src_weights #[B,N,n_neighbor,C]
        ref_weighted_feats=ref_embedding*ref_weights

        src_feats=src_weighted_feats.sum(2).transpose(1,2) # [B,C,N]
        ref_feats=ref_weighted_feats.sum(2).transpose(1,2)

        # project the pooled features 
        src_feats=F.relu(self.bn2(self.proj2(src_feats)))
        ref_feats=F.relu(self.bn2(self.proj2(ref_feats)))

        src_feats=F.relu(self.bn3(self.proj3(src_feats)))
        ref_feats=F.relu(self.bn3(self.proj3(ref_feats)))

        src_feats=self.proj4(src_feats)
        ref_feats=self.proj4(ref_feats)

        return src_feats.transpose(1,2),ref_feats.transpose(1,2)


class NetVLADLoupe(nn.Module):
    def __init__(self, args):
        super(NetVLADLoupe, self).__init__()
        self.feature_size = args['set_aggregation']['descriptor_dim']
        self.max_samples = args['model']['n_neighbors']
        self.output_dim = args['set_aggregation']['out_dim']
        self.gating = True
        self.add_batch_norm = True
        self.cluster_size = args['set_aggregation']['n_clusters']
        self.softmax = nn.Softmax(dim=-1)
        self.cluster_weights = nn.Parameter(torch.randn(
            self.feature_size, self.cluster_size) * 1 / math.sqrt(self.feature_size))
        self.cluster_weights2 = nn.Parameter(torch.randn(
            1, self.feature_size, self.cluster_size) * 1 / math.sqrt(self.feature_size))
        self.hidden1_weights = nn.Parameter(
            torch.randn(self.cluster_size * self.feature_size, self.output_dim) * 1 / math.sqrt(self.feature_size))

        if self.add_batch_norm:
            self.cluster_biases = None
            self.bn1 = nn.BatchNorm1d(self.cluster_size)
        else:
            self.cluster_biases = nn.Parameter(torch.randn(
                self.cluster_size) * 1 / math.sqrt(self.feature_size))
            self.bn1 = None

        self.bn2 = nn.BatchNorm1d(self.output_dim)

        if self.gating:
            self.context_gating = GatingContext(
                self.output_dim, add_batch_norm=self.add_batch_norm)

    def forward(self, x):
        """
        x:      [B,1,N,C]
        """
        x = x.transpose(1, 3).contiguous()
        x = x.view((-1, self.max_samples, self.feature_size))
        activation = torch.matmul(x, self.cluster_weights)
        if self.add_batch_norm:
            # activation = activation.transpose(1,2).contiguous()
            activation = activation.view(-1, self.cluster_size)
            activation = self.bn1(activation)
            activation = activation.view(-1,
                                         self.max_samples, self.cluster_size)
            # activation = activation.transpose(1,2).contiguous()
        else:
            activation = activation + self.cluster_biases
        activation = self.softmax(activation)
        activation = activation.view((-1, self.max_samples, self.cluster_size))

        a_sum = activation.sum(-2, keepdim=True)
        a = a_sum * self.cluster_weights2

        activation = torch.transpose(activation, 2, 1)
        x = x.view((-1, self.max_samples, self.feature_size))
        vlad = torch.matmul(activation, x)
        vlad = torch.transpose(vlad, 2, 1).contiguous()
        vlad = vlad - a

        vlad = F.normalize(vlad, dim=1, p=2)
        vlad = vlad.view((-1, self.cluster_size * self.feature_size))
        vlad = F.normalize(vlad, dim=1, p=2)

        vlad = torch.matmul(vlad, self.hidden1_weights)

        vlad = self.bn2(vlad)

        if self.gating:
            vlad = self.context_gating(vlad)

        return vlad


class GatingContext(nn.Module):
    def __init__(self, dim, add_batch_norm=True):
        super(GatingContext, self).__init__()
        self.dim = dim
        self.add_batch_norm = add_batch_norm
        self.gating_weights = nn.Parameter(
            torch.randn(dim, dim) * 1 / math.sqrt(dim))
        self.sigmoid = nn.Sigmoid()

        if add_batch_norm:
            self.gating_biases = None
            self.bn1 = nn.BatchNorm1d(dim)
        else:
            self.gating_biases = nn.Parameter(
                torch.randn(dim) * 1 / math.sqrt(dim))
            self.bn1 = None

    def forward(self, x):
        gates = torch.matmul(x, self.gating_weights)

        if self.add_batch_norm:
            gates = self.bn1(gates)
        else:
            gates = gates + self.gating_biases

        gates = self.sigmoid(gates)

        activation = x * gates

        return activation