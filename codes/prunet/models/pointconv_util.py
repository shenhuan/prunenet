"""
PointConv util functions
Author: Wenxuan Wu
Date: May 2020
"""

import torch 
import torch.nn as nn 

import torch.nn.functional as F
from time import time
import numpy as np
from sklearn.neighbors.kde import KernelDensity
from models.pointnet2 import pointnet2_utils

LEAKY_RATE = 0.1
use_bn = False

def square_distance(src, dst):
    """
    Calculate Euclid distance between each two points.
    src^T * dst = xn * xm + yn * ym + zn * zm；
    sum(src^2, dim=-1) = xn*xn + yn*yn + zn*zn;
    sum(dst^2, dim=-1) = xm*xm + ym*ym + zm*zm;
    dist = (xn - xm)^2 + (yn - ym)^2 + (zn - zm)^2
         = sum(src**2,dim=-1)+sum(dst**2,dim=-1)-2*src^T*dst
    Input:
        src: source points, [B, N, C]
        dst: target points, [B, M, C]
    Output:
        dist: per-point square distance, [B, N, M]
    """
    B, N, _ = src.shape
    _, M, _ = dst.shape
    dist = -2 * torch.matmul(src, dst.permute(0, 2, 1))
    dist += torch.sum(src ** 2, -1).view(B, N, 1)
    dist += torch.sum(dst ** 2, -1).view(B, 1, M)
    return dist

def knn_point(topk, source_pcd, query_pcd):
    """
    For each point in query pcd, we find the topk closest neighbor in source_pcd
    Input:
        topk:           scalar
        source_pcd:     [B, N, 3]
        query_pcd:      [B, S, 3]
    Return:
        group_idx:      [B, S, topk]
    """
    sqrdists = square_distance(query_pcd, source_pcd)
    _, group_idx = torch.topk(sqrdists, topk, dim = -1, largest=False, sorted=False)
    return group_idx

def index_points_gather(points, fps_idx):
    """
    Batched gather the fps sampled points
    Input:
        points:     [B, N, C]
        idx:        [B, S]
    Return:
        new_points: [B, S, C]
    """

    points_flipped = points.permute(0, 2, 1).contiguous()
    new_points = pointnet2_utils.gather_operation(points_flipped, fps_idx)
    return new_points.permute(0, 2, 1).contiguous()

def index_points_group(points, knn_idx):
    """
    For each point, we gather its knn neighbors features
    Input:
        points:     [B, N, C]
        knn_idx:    [B, N, topk]
    Return:
        new_points: [B, N, topk, C]
    """
    points_flipped = points.permute(0, 2, 1).contiguous()
    new_points = pointnet2_utils.grouping_operation(points_flipped, knn_idx.int()).permute(0, 2, 3, 1)

    return new_points

def group(nsample, pcd, feats):
    """
    For eachpoint in pcd, gather features from knn graph
    Input:
        nsample:            scalar
        pcd:                [B, N, 3]
        feats:              [B, N, D]
    Return:
        new_feats:          [B,N,nsample,D+3]
        relative_coords:    [B,N,nsample,3]
    """
    B, N, C = pcd.shape
    new_pcd = pcd

    # for each point, get relative coords from nsample neighbors
    idx = knn_point(nsample, pcd, new_pcd)
    grouped_pcd = index_points_group(pcd, idx) # [B, N, nsample, C]
    relative_coords = grouped_pcd - new_pcd.view(B, N, 1, C)

    # for each point, get feature from nsample neighbors
    if feats is not None:
        grouped_feats = index_points_group(feats, idx) # [B,N,nsample,C]
        new_feats = torch.cat([relative_coords, grouped_feats], dim=-1) # [B, N, nsample, 3+D]
    else:
        new_feats = relative_coords

    return new_feats, relative_coords

def group_query(nsample, dense_pcd, query_pcd, dense_feats):
    """
    For eachpoint in query pcd, we gather feature from knn graph
    Input:
        nsample:            scalar
        dense_pcd:          [B, N, 3]
        dense_feats:        [B, N, D]
        query_pcd:          [B, S, 3]
    Return:
        new_points:         [B, S, nsample, 3+D]
        relative_coords:    [B, S, nsample, 3]
    """
    B, N, C = dense_pcd.shape
    S = query_pcd.shape[1]
    new_query_pcd = query_pcd
    idx = knn_point(nsample, dense_pcd, new_query_pcd)
    grouped_query_pcd = index_points_group(dense_pcd, idx) # [B, S, nsample, 3]
    relative_coords = grouped_query_pcd - new_query_pcd.view(B, S, 1, C)
    if dense_feats is not None:
        grouped_points = index_points_group(dense_feats, idx)
        new_points = torch.cat([relative_coords, grouped_points], dim=-1) # [B, S, nsample, 3+D]
    else:
        new_points = relative_coords

    return new_points, relative_coords


class Conv1d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, use_leaky=True, bn=use_bn):
        super(Conv1d, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        relu = nn.ReLU(inplace=True) if not use_leaky else nn.LeakyReLU(LEAKY_RATE, inplace=True)

        self.composed_module = nn.Sequential(
            nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding, bias=True),
            nn.BatchNorm1d(out_channels) if bn else nn.Identity(),
            relu
        )

    def forward(self, x):
        x = self.composed_module(x)
        return x


class WeightNet(nn.Module):
    def __init__(self, in_channel, out_channel, hidden_unit = [8, 8], bn = use_bn):
        super(WeightNet, self).__init__()

        self.bn = bn
        self.mlp_convs = nn.ModuleList()
        self.mlp_bns = nn.ModuleList()
        if hidden_unit is None or len(hidden_unit) == 0:
            self.mlp_convs.append(nn.Conv2d(in_channel, out_channel, 1))
            self.mlp_bns.append(nn.BatchNorm2d(out_channel))
        else:
            self.mlp_convs.append(nn.Conv2d(in_channel, hidden_unit[0], 1))
            self.mlp_bns.append(nn.BatchNorm2d(hidden_unit[0]))
            for i in range(1, len(hidden_unit)):
                self.mlp_convs.append(nn.Conv2d(hidden_unit[i - 1], hidden_unit[i], 1))
                self.mlp_bns.append(nn.BatchNorm2d(hidden_unit[i]))
            self.mlp_convs.append(nn.Conv2d(hidden_unit[-1], out_channel, 1))
            self.mlp_bns.append(nn.BatchNorm2d(out_channel))
        
    def forward(self, localized_xyz):
        #xyz : BxCxKxN

        weights = localized_xyz
        for i, conv in enumerate(self.mlp_convs):
            if self.bn:
                bn = self.mlp_bns[i]
                weights =  F.relu(bn(conv(weights)))
            else:
                weights = F.relu(conv(weights))

        return weights


class PointConv(nn.Module):
    def __init__(self, nsample, in_channel, out_channel, weightnet = 16, bn = use_bn, use_leaky = True):
        super(PointConv, self).__init__()
        self.bn = bn
        self.nsample = nsample
        self.weightnet = WeightNet(3, weightnet)
        self.linear = nn.Linear(weightnet * in_channel, out_channel)
        if bn:
            self.bn_linear = nn.BatchNorm1d(out_channel)

        self.relu = nn.ReLU(inplace=True) if not use_leaky else nn.LeakyReLU(LEAKY_RATE, inplace=True)


    def forward(self, xyz, points):
        """
        PointConv without strides size, i.e., the input and output have the same number of points.
        Input:
            xyz: input points position data, [B, C, N]
            points: input points data, [B, D, N]
        Return:
            new_xyz: sampled points position data, [B, C, S]
            new_points_concat: sample points feature data, [B, D', S]
        """
        B = xyz.shape[0]
        N = xyz.shape[2]
        xyz = xyz.permute(0, 2, 1)
        points = points.permute(0, 2, 1)

        new_points, grouped_xyz_norm = group(self.nsample, xyz, points)

        grouped_xyz = grouped_xyz_norm.permute(0, 3, 2, 1)
        weights = self.weightnet(grouped_xyz)
        new_points = torch.matmul(input=new_points.permute(0, 1, 3, 2), other = weights.permute(0, 3, 2, 1)).view(B, N, -1)
        new_points = self.linear(new_points)
        if self.bn:
            new_points = self.bn_linear(new_points.permute(0, 2, 1))
        else:
            new_points = new_points.permute(0, 2, 1)

        new_points = self.relu(new_points)

        return new_points


class PointConvD(nn.Module):
    def __init__(self, n_points, n_neighbor, in_channel, out_channel, weightnet = 16, bn = use_bn, use_leaky = True):
        super(PointConvD, self).__init__()
        self.n_points = n_points
        self.bn = bn
        self.n_neighbor = n_neighbor
        self.weightnet = WeightNet(3, weightnet)
        self.linear = nn.Linear(weightnet * in_channel, out_channel)
        if bn:
            self.bn_linear = nn.BatchNorm1d(out_channel)

        self.relu = nn.ReLU(inplace=True) if not use_leaky else nn.LeakyReLU(LEAKY_RATE, inplace=True)

    def forward(self, xyz, points):
        """
        PointConv with downsampling.
        Input:
            xyz: input points position data, [B, C, N]
            points: input points data, [B, D, N]
        Return:
            new_xyz: sampled points position data, [B, C, S]
            new_feats_concat: sample points feature data, [B, D', S]
            fpc_idx
        """
        #import ipdb; ipdb.set_trace()
        B,_,N = xyz.shape
        xyz = xyz.permute(0, 2, 1).contiguous()
        points = points.permute(0, 2, 1).contiguous()

        fps_idx = pointnet2_utils.furthest_point_sample(xyz, self.n_points)
        new_xyz = index_points_gather(xyz, fps_idx)

        new_feats, grouped_xyz_norm = group_query(self.n_neighbor, xyz, new_xyz, points)

        grouped_xyz = grouped_xyz_norm.permute(0, 3, 2, 1)
        weights = self.weightnet(grouped_xyz)
        new_feats = torch.matmul(input=new_feats.permute(0, 1, 3, 2), other = weights.permute(0, 3, 2, 1)).view(B, self.n_points, -1)
        new_feats = self.linear(new_feats)
        if self.bn:
            new_feats = self.bn_linear(new_feats.permute(0, 2, 1))
        else:
            new_feats = new_feats.permute(0, 2, 1)

        new_feats = self.relu(new_feats)

        return new_xyz.permute(0, 2, 1), new_feats, fps_idx