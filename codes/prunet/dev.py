    def _get_permutation_matrix(self,src_pcd,tgt_pcd,rot_gt,trans_gt):
        """
        We get the ground truth permutation matrix
        Input:
            src_pcd:    [B,N,3]
            tgt_pcd:    [B,N,3]
            rot_gt:     [B,3,3]
            trans_gt:   [B,3,1]
        """
        trans_src=(torhc.matmul(rot_gt,src_pcd.transpose(1,2))+trans_gt).transpose(1,2)
        distance=square_distance(trans_src,tgt_pcd)
        mutuals=torch.from_numpy(mutual_selection(-distance.numpy().cpu())).to(distance.device)
        thresholding=distance<self.inlier_threshold**2

        permutation_matrix = mutuals & thresholding
        permutation_matrix = F.pad(permutation_matrix,(0,1,0,1),'constant',0)  #[B,N+1,N+1]

        # assign outliers 
        distance=F.pad(distance,(0,1,0,1),'constant',-1e7)
        flag_row=(permutation_matrix.sum(2)==0.) & (distance.min(2)[0]>=self.inlier_threshold)
        flag_column=(permutation_matrix.sum(1)==0.) & (distance.min(1)[0]>=self.inlier_threshold)
        permutation_matrix[:,:,-1]=flag_row.float()
        permutation_matrix[:,-1,:]=flag_column.float()
        permutation_matrix[:,-1,-1]=0

        return permutation_matrix.float()