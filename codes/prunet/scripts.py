import os,sys,glob,torch,time
import numpy as np
# from lib.util import natural_key,mutual_selection
import open3d as o3d
from lib.util import jitter_pointcloud,natural_key
from copy import deepcopy
from sklearn.manifold import TSNE
from matplotlib import pyplot as plt
import copy
from tqdm import tqdm
# from timeit import default_timer as timer
# import teaserpp_python
# from tqdm import tqdm
# import lib.bench_utils as bench_utils
# from lib.benchmark import read_trajectory,read_pairs,write_trajectory,benchmark
# from models.pointnet2 import pointnet2_utils
# from models.pointconv_util import index_points_gather,group_query,knn_point,index_points_group

colors_info=np.array([[0,255,0],
[0,0,255],
[0,255,255],
[255,255,0],
[255,0,255],
[100,100,255],
[200,200,100],
[170,120,200],
[255,0,0],
[200,100,100],
[10,200,100],
[200,200,200],
[50,50,50]]).astype(np.float)


def split_pcd(pcd):
    mean_coords=np.mean(pcd,0)
    index_0=pcd<mean_coords
    index_1=pcd>=mean_coords

    up_left=pcd[index_0[:,0] & index_1[:,1]]
    up_right=pcd[index_1[:,0] & index_1[:,1]]
    down_left=pcd[index_0[:,0] & index_0[:,1]]
    down_right=pcd[index_1[:,0] & index_0[:,1]]

    return up_left,up_right,down_left,down_right

def split_pcd_embedding(pcd,embedding):
    mean_coords=np.mean(pcd,0)
    index_0=pcd<mean_coords
    index_1=pcd>=mean_coords

    parts=[]

    indice=index_0[:,0] & index_1[:,1]
    parts.append([pcd[indice],embedding[indice]])

    indice=index_1[:,0] & index_1[:,1]
    parts.append([pcd[indice],embedding[indice]])

    indice=index_0[:,0] & index_0[:,1]
    parts.append([pcd[indice],embedding[indice]])

    indice=index_1[:,0] & index_0[:,1]
    parts.append([pcd[indice],embedding[indice]])

    return parts

def get_r_t(path):
    f=open(path,'r')
    lines=f.readlines()[1:]
    rows=[]
    for eachline in lines:
        eles=eachline.split('	 ')
        rows.append([float(ele) for ele in eles])
    T=np.array(rows)
    r,t=T[:3,:3],T[:3,3]
    return r,t

def ss1():
    base_train='/net/pf-pc27/scratch2/shengyu/3DMatch/test'
    train_folders=os.listdir(base_train)

    low_thre=0.1
    high_thre=0.4
    n=0
    rots,trans=[],[]
    srcs,tgts=[],[]
    overlap_ratios=[]
    for eachfolder in train_folders:
        c_path=os.path.join(base_train,eachfolder,'overlap.npy')
        overlaps=np.load(c_path)
        c_path=os.path.join(base_train,eachfolder,'05_FCGF_feats_0.025/*.pth')
        samples=sorted(glob.glob(c_path),key=natural_key)
        c_path=os.path.join(base_train,eachfolder,'04_Info/*.txt')
        infos=sorted(glob.glob(c_path),key=natural_key)
        if len(infos)==len(samples):
            real_overlap=np.where((overlaps>=low_thre)&(overlaps<=high_thre))
            indice_0=real_overlap[0]
            indice_1=real_overlap[1]

            for i in range(indice_0.shape[0]):
                ind0,ind1=indice_0[i],indice_1[i]
                if(ind0<ind1): # process each pair
                    n+=1
                    src=samples[ind0].split('/3DMatch/')[1]
                    tgt=samples[ind1].split('/3DMatch/')[1]

                    r0,t0=get_r_t(infos[ind0])
                    r1,t1=get_r_t(infos[ind1])
                    r=r1.T.dot(r0)
                    t=r1.T.dot(np.expand_dims(t0-t1,1))

                    rots.append(r)
                    trans.append(t)
                    srcs.append(src)
                    tgts.append(tgt)
                    overlap_ratios.append(overlaps[ind0,ind1])
        else:
            pass
    np.savez(f'../../dataset/3DMatch/test/overlap_1_4.npz',src=srcs,tgt=tgts,rot=rots,trans=trans,overlap=overlap_ratios)
    print(n)

def ss2():
    '''
    We use the script to visually check 
    The overlap ratio is calculated on the source point cloud
    '''
    info=np.load('configs/3DMatch/test/overlap_1.npz')
    base_dir='/Users/stupidog/Documents/dataset/3DMatch'
    idx=np.random.permutation(len(info['rot']))[:10].tolist()
    for item in idx:
        path_src=os.path.join(base_dir,info['src'][item].replace('05_FCGF_feats_0.025','02_Fragments').replace('.pth','.ply'))
        path_ref=os.path.join(base_dir,info['tgt'][item].replace('05_FCGF_feats_0.025','02_Fragments').replace('.pth','.ply'))

        src=o3d.io.read_point_cloud(path_src)
        src.paint_uniform_color([0, 0.651, 0.929])
        ref=o3d.io.read_point_cloud(path_ref)
        ref.paint_uniform_color([1, 0.706, 0])

        tsfm=np.eye(4)
        tsfm[:3,:3]=info['rot'][item]
        tsfm[:3,3]=info['trans'][item][:,0]
        src.transform(tsfm)

        src=src.voxel_down_sample(voxel_size=0.025)
        ref=ref.voxel_down_sample(voxel_size=0.025)
        overlap=round(info['overlap'][item],3)
        o3d.visualization.draw_geometries([src,ref],window_name=f'overlap={overlap}')

def ss3():
    '''
    visually check the split(into 4 parts)
    '''
    info=np.load('configs/3DMatch/test/overlap_1.npz')
    base_dir='/Users/stupidog/Documents/dataset/3DMatch'
    idx=np.random.permutation(len(info['rot']))[:10].tolist()
    for item in idx:
        path_src=os.path.join(base_dir,info['src'][item].replace('05_FCGF_feats_0.025','02_Fragments').replace('.pth','.ply'))
        path_ref=os.path.join(base_dir,info['tgt'][item].replace('05_FCGF_feats_0.025','02_Fragments').replace('.pth','.ply'))

        src=o3d.io.read_point_cloud(path_src)
        src.paint_uniform_color([0, 0.651, 0.929])
        ref=o3d.io.read_point_cloud(path_ref)
        ref.paint_uniform_color([1, 0.706, 0])

        src_pcd=np.asarray(src.points)
        ref_pcd=np.asarray(ref.points)

        up_left,up_right,down_left,down_right=split_pcd(src_pcd)
        # print(up_left.shape,up_right.shape,down_left.shape,down_right.shape)

        up_left_pcd=o3d.geometry.PointCloud()
        up_left_pcd.points=o3d.utility.Vector3dVector(up_left)
        up_left_pcd.paint_uniform_color([1,0,0])

        up_right_pcd=o3d.geometry.PointCloud()
        up_right_pcd.points=o3d.utility.Vector3dVector(up_right)
        up_right_pcd.paint_uniform_color([0,1,0])

        down_left_pcd=o3d.geometry.PointCloud()
        down_left_pcd.points=o3d.utility.Vector3dVector(down_left)
        down_left_pcd.paint_uniform_color([0,0,1])

        down_right_pcd=o3d.geometry.PointCloud()
        down_right_pcd.points=o3d.utility.Vector3dVector(down_right)
        down_right_pcd.paint_uniform_color([0, 0.651, 0.929])
        o3d.visualization.draw_geometries([up_left_pcd,up_right_pcd,down_left_pcd,down_right_pcd])


def ss4():
    '''
    visually check the FPS sampling
    '''
    device=torch.device('cuda')
    info=np.load('configs/3DMatch/fcgf_0.025_test_benchmark.npz')
    base_dir='/net/pf-pc27/scratch2/shengyu/3DMatch'
    idx=np.random.permutation(len(info['rot']))[:10].tolist()
    n_input_points=2048
    n_sparse_points=4
    n_neighbor=350
    for ii,item in enumerate(idx):
        path_src=os.path.join(base_dir,info['src'][item])
        path_ref=os.path.join(base_dir,info['tgt'][item])

        src_data,ref_data=torch.load(path_src),torch.load(path_ref)
        src_pcd,src_embedding=src_data['coords'],src_data['feats']
        ref_pcd,ref_embedding=ref_data['coords'],ref_data['feats']

        # transform the source point cloud
        src_pcd=info['rot'][item].dot(src_pcd.T)+info['trans'][item]
        src_pcd=src_pcd.T

        if(src_pcd.shape[0]>n_input_points):
            src_permute=np.random.permutation(src_pcd.shape[0])[:n_input_points]
        else:
            src_permute=np.random.choice(src_pcd.shape[0],n_input_points)
        if(ref_pcd.shape[0]>n_input_points):
            ref_permute=np.random.permutation(ref_pcd.shape[0])[:n_input_points]
        else:
            ref_permute=np.random.choice(ref_pcd.shape[0],n_input_points)
        src_pcd,src_embedding = src_pcd[src_permute][None,:,:],src_embedding[src_permute][None,:,:]
        ref_pcd,ref_embedding = ref_pcd[ref_permute][None,:,:],ref_embedding[ref_permute][None,:,:]
        
        src_pcd,ref_pcd=torch.from_numpy(src_pcd).to(device).float(),torch.from_numpy(ref_pcd).to(device).float()
        src_embedding,ref_embedding=torch.from_numpy(src_embedding).to(device).float(),torch.from_numpy(ref_embedding).to(device).float()
        print(src_pcd.shape,ref_pcd.shape)
        


        # FPS sampling
        fps_idx = pointnet2_utils.furthest_point_sample(src_pcd, n_sparse_points)
        new_src_pcd = index_points_gather(src_pcd, fps_idx)

        # KNN search to build the cluster
        idx = knn_point(n_neighbor, src_pcd, new_src_pcd)
        grouped_query_pcd = index_points_group(src_pcd, idx) # [B, S, nsample, 3]

        # color code the cluster
        colors=colors_info[:n_sparse_points]/255
        colors=torch.from_numpy(colors).unsqueeze(1).repeat(1,n_neighbor,1)
        coords=grouped_query_pcd[0].view(-1,3).cpu().numpy()
        colors=colors.view(-1,3).cpu().numpy()

        pcd=o3d.geometry.PointCloud()
        pcd.colors=o3d.utility.Vector3dVector(colors)
        pcd.points=o3d.utility.Vector3dVector(coords)

        o3d.io.write_point_cloud(f'dump/{ii}_source.ply', pcd)


        # FPS sampling
        fps_idx = pointnet2_utils.furthest_point_sample(ref_pcd, n_sparse_points)
        new_ref_pcd = index_points_gather(ref_pcd, fps_idx)

        # KNN search to build the cluster
        idx = knn_point(n_neighbor, ref_pcd, new_ref_pcd)
        grouped_query_pcd = index_points_group(ref_pcd, idx) # [B, S, nsample, 3]

        # color code the cluster
        colors=colors_info[:n_sparse_points]/255
        colors=torch.from_numpy(colors).unsqueeze(1).repeat(1,n_neighbor,1)
        coords=grouped_query_pcd[0].view(-1,3).cpu().numpy()
        colors=colors.view(-1,3).cpu().numpy()

        pcd=o3d.geometry.PointCloud()
        pcd.colors=o3d.utility.Vector3dVector(colors)
        pcd.points=o3d.utility.Vector3dVector(coords)

        o3d.io.write_point_cloud(f'dump/{ii}_target.ply', pcd)
    

def ss5():
    '''
    show pruning results
    '''
    base_dir='/Users/stupidog/Downloads/top5/*.pth'
    files=sorted(glob.glob(base_dir),key=natural_key)
    in_src,in_tgt=[],[]
    left_src,left_tgt=[],[]
    rots,trans=[],[]
    left_src_easy,left_tgt_easy=[],[]
    for eachfile in files:
        data=torch.load(eachfile,map_location=torch.device('cpu'))
        B=data['rot'].size(0)

        src_pcd,tgt_pcd=data['input_src_pcd'],data['input_tgt_pcd']
        left_src_pcd,left_tgt_pcd=data['left_src_pcd'],data['left_tgt_pcd']
        rot,trans=data['rot'],data['trans']
        left_src_pcd_easy,left_tgt_pcd_easy=data['left_src_pcd_easy'],data['left_tgt_pcd_easy']

        input_src_trans=torch.matmul(rot,src_pcd.transpose(1,2))+trans
        left_src_trans=torch.matmul(rot,left_src_pcd.transpose(1,2))+trans
        left_src_easy_trans=torch.matmul(rot,left_src_pcd_easy.transpose(1,2))+trans

        src_1=input_src_trans.transpose(1,2).numpy()
        src_2=left_src_trans.transpose(1,2).numpy()
        src_3=left_src_easy_trans.transpose(1,2).numpy()
        tgt_1=tgt_pcd.numpy()
        tgt_2=left_tgt_pcd.numpy()
        tgt_3=left_tgt_pcd_easy.numpy()

        in_src.append(src_1)
        in_tgt.append(tgt_1)
        left_src.append(src_2)
        left_tgt.append(tgt_2)
        left_src_easy.append(src_3)
        left_tgt_easy.append(tgt_3)

    in_src=np.vstack(in_src)
    in_tgt=np.vstack(in_tgt)
    left_src=np.vstack(left_src)
    left_tgt=np.vstack(left_tgt)
    left_src_easy=np.vstack(left_src_easy)
    left_tgt_easy=np.vstack(left_tgt_easy)

    info=np.load('/Users/stupidog/Downloads/top5_1_ana.npz')
    rot_errors=info['rot']
    trans_errors=info['trans']
    idx_failed_1=rot_errors<10
    idx_failed_2=trans_errors<0.3

    idx_failed=idx_failed_1 & idx_failed_2
    
    in_src,in_tgt=in_src[idx_failed],in_tgt[idx_failed]
    left_src,left_tgt=left_src[idx_failed],left_tgt[idx_failed]
    left_src_easy,left_tgt_easy=left_src_easy[idx_failed],left_tgt_easy[idx_failed]

    n1,n2,n3=in_src.shape[1],left_src.shape[1],left_src_easy.shape[1]

    for i in range(idx_failed.sum()):
        idx=np.random.randint(0,idx_failed.sum(),1)[0]
        color1=torch.from_numpy(colors_info[2]/255).unsqueeze(0)
        color2=torch.from_numpy(colors_info[4]/255).unsqueeze(0)
        color3=torch.from_numpy(colors_info[0]/255).unsqueeze(0)
        color4=torch.from_numpy(colors_info[1]/255).unsqueeze(0)
        color5=torch.from_numpy(colors_info[0]/255).unsqueeze(0)
        color6=torch.from_numpy(colors_info[1]/255).unsqueeze(0)
        
        pcd1=o3d.geometry.PointCloud()
        pcd1.colors=o3d.utility.Vector3dVector(color1.repeat(n1,1).numpy())
        pcd1.points=o3d.utility.Vector3dVector(jitter_pointcloud(in_src[idx]))

        pcd2=o3d.geometry.PointCloud()
        pcd2.colors=o3d.utility.Vector3dVector(color2.repeat(n1,1).numpy())
        pcd2.points=o3d.utility.Vector3dVector(jitter_pointcloud(in_tgt[idx]))

        offset=[4,0,0]
        pcd3=o3d.geometry.PointCloud()
        pcd3.colors=o3d.utility.Vector3dVector(color3.repeat(n2,1).numpy())
        pcd3.points=o3d.utility.Vector3dVector(jitter_pointcloud(left_src[idx]+offset))

        pcd4=o3d.geometry.PointCloud()
        pcd4.colors=o3d.utility.Vector3dVector(color4.repeat(n2,1).numpy())
        pcd4.points=o3d.utility.Vector3dVector(jitter_pointcloud(left_tgt[idx]+offset))

        pcd5=o3d.geometry.PointCloud()
        pcd5.colors=o3d.utility.Vector3dVector(color5.repeat(n3,1).numpy())
        pcd5.points=o3d.utility.Vector3dVector(jitter_pointcloud(left_src_easy[idx]-offset))

        pcd6=o3d.geometry.PointCloud()
        pcd6.colors=o3d.utility.Vector3dVector(color6.repeat(n3,1).numpy())
        pcd6.points=o3d.utility.Vector3dVector(jitter_pointcloud(left_tgt_easy[idx]-offset))

        o3d.visualization.draw_geometries([pcd1,pcd2,pcd3,pcd4,pcd5,pcd6])
        

def ss6():
    '''
    We use the script to prepare the point clouds for pre
    '''
    for i in range(1,2):
        info=np.load(f'../../dataset/3DMatch/test/overlap_{i}.npz')
        base_dir='/Users/stupidog/Documents/dataset/3DMatch'
        overlaps=np.array(info['overlap'])
        # for idx in range(len(info['src'])):
        idx=overlaps.argmin()
        idx=0
        c_overlap=round(overlaps.min(),3)
        c_overlap=info['overlap'][idx]
        path_src=os.path.join(base_dir,info['src'][idx].replace('05_FCGF_feats_0.025','02_Fragments').replace('.pth','.ply'))
        path_ref=os.path.join(base_dir,info['tgt'][idx].replace('05_FCGF_feats_0.025','02_Fragments').replace('.pth','.ply'))


        src=o3d.io.read_point_cloud(path_src)
        src.paint_uniform_color([0, 0.651, 0.929]) #[0,166,237]
        ref=o3d.io.read_point_cloud(path_ref)
        ref.paint_uniform_color([1, 0.706, 0])  #[255,180,0]

        o3d.io.write_point_cloud(f'/Users/stupidog/Documents/MT_Pre/dump/{i}_{c_overlap}_src.ply',src)

        tsfm=np.eye(4)
        tsfm[:3,:3]=info['rot'][idx]
        tsfm[:3,3]=info['trans'][idx][:,0]
        src.transform(tsfm)

        # o3d.visualization.draw_geometries([src,ref])
    

        o3d.io.write_point_cloud(f'/Users/stupidog/Documents/MT_Pre/dump/{i}_{c_overlap}_src_trans.ply',src)
        o3d.io.write_point_cloud(f'/Users/stupidog/Documents/MT_Pre/dump/{i}_{c_overlap}_tgt.ply',ref)

def ss7():
    path='/Users/stupidog/Documents/dataset/3DMatch/test/7-scenes-redkitchen/02_Fragments/cloud_bin_1.ply'
    pcd=o3d.io.read_point_cloud(path)
    pcd.paint_uniform_color([0, 0.651, 0.929]) #[0,166,237]
    
    o3d.io.write_point_cloud(f'/Users/stupidog/Documents/MT_Pre/dump/raw.ply',pcd)
    print(len(pcd.points))

    pcd1=pcd.voxel_down_sample(voxel_size=0.025)
    print(len(pcd1.points))

    o3d.io.write_point_cloud(f'/Users/stupidog/Documents/MT_Pre/dump/0.025.ply',pcd1)

    pcd2=pcd.voxel_down_sample(voxel_size=0.05)
    print(len(pcd2.points))

    o3d.io.write_point_cloud(f'/Users/stupidog/Documents/MT_Pre/dump/0.05.ply',pcd2)

    pts=np.array(pcd1.points)
    idx=np.random.permutation(pts.shape[0])[:2048]
    pcd2.points=o3d.utility.Vector3dVector(pts[idx])
    pcd2.paint_uniform_color([0, 0.651, 0.929])
    o3d.io.write_point_cloud(f'/Users/stupidog/Documents/MT_Pre/dump/random_sample.ply',pcd2)


def ss8():
    '''
    show fps and knn clustering
    '''
    def custom_draw_geometry_with_rotation(pcd):
        def rotate_view(vis):
            ctr = vis.get_view_control()
            ctr.rotate(10.0, 0.0)
            return False

        o3d.visualization.draw_geometries_with_animation_callback([pcd],
                                                              rotate_view)
    base_dir='/Users/stupidog/Downloads/clusters/*.pth'
    files=sorted(glob.glob(base_dir),key=natural_key)
    in_src,in_tgt=[],[]
    left_src,left_tgt=[],[]
    rots,trans=[],[]
    for eachfile in files:
        data=torch.load(eachfile,map_location=torch.device('cpu'))
        B=data['rot'].size(0)

        src_pcd,tgt_pcd=data['input_src_pcd'],data['input_tgt_pcd']
        left_src_pcd,left_tgt_pcd=data['left_src_pcd'],data['left_tgt_pcd']
        rot,trans=data['rot'],data['trans']

        c_color=torch.from_numpy(colors_info[:10])/255
        c_color=torch.repeat_interleave(c_color,512,0).numpy()

        c_pcds=left_src_pcd[0].view(-1,3)

        pcd=o3d.geometry.PointCloud()
        pcd.colors=o3d.utility.Vector3dVector(c_color)
        pcd.points=o3d.utility.Vector3dVector(jitter_pointcloud(c_pcds))

        custom_draw_geometry_with_rotation(pcd)
    

def custom_draw_geometry_with_rotation(pcds):
    def rotate_view(vis):
        ctr = vis.get_view_control()
        ctr.rotate(10.0, 0.0)
        return False

    o3d.visualization.draw_geometries_with_animation_callback(pcds,
                                                              rotate_view)


    

def get_color_map(x):
    colours = plt.cm.Spectral(x)
    return colours[:, :3]


def mesh_sphere(pcd, voxel_size, sphere_size=0.6):
    # Create a mesh sphere
    spheres = o3d.geometry.TriangleMesh()
    s = o3d.geometry.TriangleMesh.create_sphere(radius=voxel_size * sphere_size)
    s.compute_vertex_normals()

    for i, p in enumerate(pcd.points):
        si = copy.deepcopy(s)
        trans = np.identity(4)
        trans[:3, 3] = p
        si.transform(trans)
        si.paint_uniform_color(pcd.colors[i])
        spheres += si
    return spheres


def get_colored_point_cloud_feature(pcd, feature, voxel_size):
    tsne_results = embed_tsne(feature)

    color = get_color_map(tsne_results)
    pcd.colors = o3d.utility.Vector3dVector(color)
    # spheres = mesh_sphere(pcd, voxel_size)

    return pcd


def embed_tsne(data):
    """
    N x D np.array data
    """
    tsne = TSNE(n_components=1, verbose=1, perplexity=40, n_iter=300, random_state=0)
    tsne_results = tsne.fit_transform(data)
    tsne_results = np.squeeze(tsne_results)
    tsne_min = np.min(tsne_results)
    tsne_max = np.max(tsne_results)
    return (tsne_results - tsne_min) / (tsne_max - tsne_min)

def ss10():
    """
    color-code the FCGF features
    """
    files=glob.glob('/Users/stupidog/Documents/dataset/3DMatch/test/7-scenes-redkitchen/05_FCGF_feats_0.025/*.pth')
    for eachfile in tqdm(files):
        data=torch.load(eachfile)

        coords,feature=data['coords'],data['feats']

        vis_pcd = o3d.geometry.PointCloud()
        vis_pcd.points = o3d.utility.Vector3dVector(coords)

        vis_pcd = get_colored_point_cloud_feature(vis_pcd,feature,0.025)

        new_path=eachfile.replace('05_FCGF_feats_0.025','tsne_feats').replace('pth','ply')

        o3d.io.write_point_cloud(new_path,vis_pcd)

def get_r_t(path):
    '''
    Read rotation and translation from log file
    '''
    f=open(path,'r')
    lines=f.readlines()[1:]
    rows=[]
    for eachline in lines:
        eles=eachline.split('	 ')
        rows.append([float(ele) for ele in eles])
    T=np.array(rows)
    r,t=T[:3,:3],T[:3,3]
    return r,t

def ss11():
    # colorize the point clouds
    data=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/cloud_bin_0_transformed.ply')
    data.paint_uniform_color([0, 0.651, 0.929]) #[0,166,237]
    o3d.io.write_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/cloud_bin_0_transformed.ply',data)

    data=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/cloud_bin_3_transformed.ply')
    data.paint_uniform_color([1, 0.706, 0]) #[0,166,237]
    o3d.io.write_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/cloud_bin_3_transformed.ply',data)

    # transform the point cloud
    data=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/color_cloud_bin_0.ply')
    r,t=get_r_t('/Users/stupidog/Documents/MT_Pre/dump/color_code/cloud_bin_0.info.txt')
    T=np.eye(4)
    T[:3,:3]=r
    T[:3,3]=t
    data.transform(T)
    o3d.io.write_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/color_cloud_bin_0.ply',data)

    data=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/color_cloud_bin_3.ply')
    r,t=get_r_t('/Users/stupidog/Documents/MT_Pre/dump/color_code/cloud_bin_3.info.txt')
    T=np.eye(4)
    T[:3,:3]=r
    T[:3,3]=t
    data.transform(T)
    o3d.io.write_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/color_cloud_bin_3.ply',data)

    #colorize again
    data=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/color_cloud_bin_0.ply')
    data.paint_uniform_color([0, 0.651, 0.929]) #[0,166,237]
    o3d.io.write_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/grey_color_cloud_bin_0.ply',data)

    data=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/color_cloud_bin_3.ply')
    data.paint_uniform_color([1, 0.706, 0]) #[0,166,237]
    o3d.io.write_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/grey_color_cloud_bin_3.ply',data)

    src=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/grey_color_cloud_bin_3.ply')
    tgt=o3d.io.read_point_cloud('/Users/stupidog/Documents/MT_Pre/dump/color_code/grey_color_cloud_bin_0.ply')
    custom_draw_geometry_with_rotation([src,tgt])


def mine_left(idx,pcd):
    """
    idx:    [K,N]
    pcd:    [2048,3]
    """
    idx=set(idx.view(-1).tolist())
    left_idx=[ele for ele in range(2048) if ele not in idx ]
    left_idx=np.array(left_idx)
    return pcd[left_idx].numpy()

def ss12():
    '''
    show pruning results
    '''
    base_dir='/Users/stupidog/Downloads/remove_left/*.pth'
    files=sorted(glob.glob(base_dir),key=natural_key)
    color1=torch.from_numpy(colors_info[2]/255).unsqueeze(0)
    color2=torch.from_numpy(colors_info[4]/255).unsqueeze(0)
    color4=torch.from_numpy(colors_info[0]/255).unsqueeze(0)
    color3=torch.from_numpy(colors_info[1]/255).unsqueeze(0)
    color5=torch.from_numpy(colors_info[0]/255).unsqueeze(0)
    color6=torch.from_numpy(colors_info[1]/255).unsqueeze(0)

    for eachfile in files:
        idx=1

        data=torch.load(eachfile,map_location=torch.device('cpu'))
        B=data['rot'].size(0)

        src_pcd,tgt_pcd=data['input_src_pcd'],data['input_tgt_pcd']
        left_src_pcd,left_tgt_pcd=data['left_src'],data['left_tgt']
        rot,trans=data['rot'],data['trans']
        idx_src,idx_tgt=data['idx_src'],data['idx_ref']
        B=left_src_pcd.size(0)
        left_src_pcd=left_src_pcd.view(B,-1,3)
        left_tgt_pcd=left_tgt_pcd.view(B,-1,3)

        src_pcd=(torch.matmul(rot,src_pcd.transpose(1,2))+trans).transpose(1,2)
        left_src_pcd=(torch.matmul(rot,left_src_pcd.transpose(1,2))+trans).transpose(1,2)

        removed_src=mine_left(idx_src[idx],src_pcd[idx])
        removed_tgt=mine_left(idx_tgt[idx],tgt_pcd[idx])

        left_src_pcd=jitter_pointcloud(left_src_pcd[idx].view(-1,3).numpy())
        left_tgt_pcd=jitter_pointcloud(left_tgt_pcd[idx].view(-1,3).numpy())


        pcd1=o3d.geometry.PointCloud()
        pcd1.colors=o3d.utility.Vector3dVector(color1.repeat(src_pcd.size(1),1).numpy())
        pcd1.points=o3d.utility.Vector3dVector(src_pcd[idx].numpy())

        pcd2=o3d.geometry.PointCloud()
        pcd2.colors=o3d.utility.Vector3dVector(color2.repeat(tgt_pcd.size(1),1).numpy())
        pcd2.points=o3d.utility.Vector3dVector(tgt_pcd[idx].numpy())

        offset=[0,3,0]

        pcd5=o3d.geometry.PointCloud()
        pcd5.colors=o3d.utility.Vector3dVector(color4.repeat(left_src_pcd.shape[0],1).numpy())
        pcd5.points=o3d.utility.Vector3dVector(left_src_pcd+offset)

        pcd6=o3d.geometry.PointCloud()
        pcd6.colors=o3d.utility.Vector3dVector(color4.repeat(left_tgt_pcd.shape[0],1).numpy())
        pcd6.points=o3d.utility.Vector3dVector(left_tgt_pcd+offset)

        pcd3=o3d.geometry.PointCloud()
        pcd3.colors=o3d.utility.Vector3dVector(color3.repeat(removed_src.shape[0],1).numpy())
        pcd3.points=o3d.utility.Vector3dVector(removed_src+offset)

        pcd4=o3d.geometry.PointCloud()
        pcd4.colors=o3d.utility.Vector3dVector(color3.repeat(removed_tgt.shape[0],1).numpy())
        pcd4.points=o3d.utility.Vector3dVector(removed_tgt+offset)

        custom_draw_geometry_with_rotation([pcd1,pcd2,pcd3,pcd4,pcd5,pcd6])

if __name__=='__main__':
    benchmark=np.load('/scratch/shengyu/masterthesis/dataset/3DMatch/test/overlap_0_4.npz')
    benchmark=dict(benchmark)
    for key, value in benchmark.items():
        benchmark[key]=value[::50]
    np.savez('/scratch/shengyu/masterthesis/dataset/3DMatch/test/overlap_vis.npz',src=benchmark['src'],tgt=benchmark['tgt'],rot=benchmark['rot'],trans=benchmark['trans'],overlap=benchmark['overlap'])
    