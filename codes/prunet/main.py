import os,sys,torch,time
import numpy as np
from lib.util import setup_seed,load_config
from lib.data_loader import get_train_val_loaders
from lib.trainer import PruneTrainer
from models.model import Engine


def _init_(args):
    setup_seed(cfg['misc']['seed'])
    torch.backends.cudnn.enabled = False
    if not os.path.exists('logs'):
        os.makedirs('logs')
    c_log=os.path.join('logs',args['exp_name'])
    if not os.path.exists(c_log):
        os.makedirs(c_log)
    args['exp_name']=c_log

    # backup files
    os.system('cp -r models %s' % c_log)
    os.system('cp -r lib %s' % c_log)
    if(not os.path.exists(args['dump_dir'])):
        os.makedirs(args['dump_dir'])


if __name__ == '__main__':
    #########################################
    # load configures
    cfg=load_config('configs/oanet.yaml')
    if(not os.path.exists(cfg['misc']['base_dir'])): # in case we run it on leonhard
        cfg['misc']['base_dir']='../../../3DMatch'

    #########################################
    # fix random seed
    _init_(cfg)
    net = Engine(cfg).cuda()
    train_loader,val_loader=get_train_val_loaders(cfg)
    trainer=PruneTrainer(cfg, net, train_loader, val_loader,val_loader)

    if(cfg['mode']=='train'):
        trainer.train()
    else:
        trainer.val()

    # n_sparse_points=[4,6,8,10,12,14]
    # for value_1 in n_sparse_points:
    #     cfg['model']['n_sparse_points']=value_1
    #     dump_dir=f'dump/2048_512_{value_1}'
    #     os.makedirs(dump_dir)
    #     cfg['dump']=dump_dir

    #     net = Engine(cfg).cuda()
    #     train_loader,val_loader=get_train_val_loaders(cfg)
    #     trainer=PruneTrainer(cfg, net, train_loader, val_loader,val_loader)

    #     trainer.train()

    # for overlap_info in range(10):
    #     cfg['model']['n_sparse_points']=14
    #     cfg['misc']['path_train_info']=f'../../dataset/3DMatch/test/overlap_{overlap_info}.npz'
    #     dump_dir=f'dump/2048_512_14_{overlap_info}'
    #     os.makedirs(dump_dir)
    #     cfg['dump']=dump_dir

    #     net = Engine(cfg).cuda()
    #     train_loader,val_loader=get_train_val_loaders(cfg)
    #     trainer=PruneTrainer(cfg, net, train_loader, val_loader,val_loader)

    #     trainer.train()