import open3d as o3d
import numpy as np
import sys,os,time
from tqdm import tqdm
from util import natural_key,mutual_selection
from benchmark import read_trajectory,read_pairs,write_trajectory,benchmark
import glob
import torch

def pose_estimation_ransac(src_pcd,ref_pcd,src_embedding,ref_embedding,mutual=True):
    """
    Ransac based estimation of the transformation paramaters of the congurency transformation. Estimates the
    transformation parameters thtat map xyz0 to xyz1. Implementation is based on the open3d library
    (http://www.open3d.org/docs/release/python_api/open3d.registration.registration_ransac_based_on_correspondence.html)
    
    Input:
        src_pcd:            [N,3]
        src_embedding:      [N,C]
    """

    # Distance threshold as specificed by 3DMatch dataset
    distance_threshold = 0.05
    print(src_pcd.shape)

    # mutual selection 
    scores=src_embedding.dot(ref_embedding.T)[None,:,:]
    if(mutual):
        selection=mutual_selection(scores)[0]
        src_selection,ref_selection=np.where(selection==1)
        
        src_pcd=src_pcd[src_selection]
        ref_pcd=ref_pcd[ref_selection]
    else:
        idx=scores.argmax(-1)[0]
        ref_pcd=ref_pcd[idx]
    
    print(src_pcd.shape)

    # Convert the point to an open3d PointCloud object
    xyz0 = o3d.geometry.PointCloud()
    xyz1 = o3d.geometry.PointCloud()
    xyz0.points = o3d.utility.Vector3dVector(src_pcd)
    xyz1.points = o3d.utility.Vector3dVector(ref_pcd)

    # Correspondences are already sorted
    corr_idx = np.tile(np.expand_dims(np.arange(len(xyz0.points)),1),(1,2))
    corrs = o3d.utility.Vector2iVector(corr_idx)

    result_ransac = o3d.registration.registration_ransac_based_on_correspondence(
        source=xyz0, target=xyz1,corres=corrs, 
        max_correspondence_distance=distance_threshold,
        estimation_method=o3d.registration.TransformationEstimationPointToPoint(False),
        ransac_n=4,
        criteria=o3d.registration.RANSACConvergenceCriteria(50000, 1000))

    trans_param = result_ransac.transformation
    
    return trans_param

def ss1():
    """
    Benchmark ransac, 5000 points + mutual selection
    """
    # Prepare TEASER++ Solver
    n_points=5000
    benchmark_dir='/scratch/shengyu/masterthesis/benchmark/3DMatch/gt_result'
    base_test='/net/pf-pc27/scratch2/shengyu/3DMatch/test'
    scene_names=sorted(os.listdir(benchmark_dir))
    exp_dir=os.path.join(benchmark_dir.split('/gt_result')[0],'ransac_5000')
    if(not os.path.exists(exp_dir)):
        os.makedirs(exp_dir)

    for scene_name in tqdm(scene_names):
        c_path=os.path.join(base_test,scene_name,'05_FCGF_feats_0.025/*.pth')
        samples=sorted(glob.glob(c_path),key=natural_key)

        c_path=os.path.join(base_test,scene_name,'overlap.npy')
        overlaps=np.load(c_path)

        gt_pairs, gt_traj = read_trajectory(os.path.join(benchmark_dir,scene_name,'gt.log'))
        est_traj = []
        for i in range(len(gt_pairs)):
            # prepare test pair
            ind0,ind1,n_fragments=int(gt_pairs[i][0]),int(gt_pairs[i][1]),int(gt_pairs[i][2])
            src_path,tgt_path=samples[ind1],samples[ind0]
            c_overlap = max(overlaps[ind0,ind1],overlaps[ind1,ind0])
            src_pcd,src_embedding,ref_pcd,ref_embedding=read_pairs(src_path,tgt_path,n_points)

            # get transformation estimation
            ts_est=pose_estimation_ransac(src_pcd,ref_pcd,src_embedding,ref_embedding,True)

            est_traj.append(ts_est)
            
        # # write the trajectory
        # c_directory=os.path.join(exp_dir,scene_name)
        # os.makedirs(c_directory)
        # write_trajectory(np.array(est_traj),gt_pairs,os.path.join(c_directory,'est.log'))
    benchmark('ransac_5000')

def ss2():
    base_dir='/net/pf-pc27/scratch2/shengyu/3DMatch'
    n_points=5000

    for i in range(0,4):
        path=f'/scratch/shengyu/masterthesis/dataset/3DMatch/test/overlap_{i}.npz'
        infos=np.load(path)
        pose_estimates=[]
        for item in tqdm(range(len(infos['src']))):
            # get overlap and transformation
            overlap=infos['overlap'][item]
            rot=infos['rot'][item]
            trans=infos['trans'][item]

            # get pointcloud
            src_path=os.path.join(base_dir,infos['src'][item])
            tgt_path=os.path.join(base_dir,infos['tgt'][item])
            src = torch.load(src_path)
            tgt = torch.load(tgt_path)

            src_pcd, src_embedding = src['coords'],src['feats']
            tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats']
            
            # permute and randomly select 2048/1024 points
            if(src_pcd.shape[0]>n_points):
                src_permute=np.random.permutation(src_pcd.shape[0])[:n_points]
            else:
                src_permute=np.random.choice(src_pcd.shape[0],n_points)
            if(tgt_pcd.shape[0]>n_points):
                tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:n_points]
            else:
                tgt_permute=np.random.choice(tgt_pcd.shape[0],n_points)
            src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
            tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]

            pose_est=pose_estimation_ransac(src_pcd,tgt_pcd,src_embedding,tgt_embedding,False)

            pose_estimates.append(pose_est)
    
        np.save(f'../dump/ransac/overlap_{i}_est',np.array(pose_estimates))

if __name__=='__main__':
    ss1()