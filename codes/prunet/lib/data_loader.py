import os, sys, glob, torch
import numpy as np
from torch.utils.data import DataLoader
from lib.dataset import ThreeDMatchFCGF,ThreeDMatchFCGFSampled

# def collate_fn(list_data):
#     src_pcd,tgt_pcd,src_embedding,tgt_embedding,rot,trans=list(zip(*list_data))
#     src_pcd,tgt_pcd,src_embedding,tgt_embedding=list(src_pcd),list(tgt_pcd),list(src_embedding),list(tgt_embedding)
#     rot=torch.stack(rot).float()
#     trans=torch.stack(trans).float()

#     data=dict()
#     data['src_pcd']= src_pcd
#     data['src_embedding'] = src_embedding
#     data['tgt_pcd']=tgt_pcd
#     data['tgt_embedding'] = tgt_embedding
#     data['rot']=rot
#     data['trans']=trans
#     return data

# def get_train_val_loaders(args):
#     '''
#     get train/val for FCGF features of 3DMatch 
#     '''
#     all_infos=np.load(args['misc']['path_test_info'])
#     info_val=dict()
#     for key,value in all_infos.items():
#         info_val[key]=value

#     all_infos=np.load(args['misc']['path_train_info'])
#     info_train=dict()
#     for key,value in all_infos.items():
#         info_train[key]=value

#     train_loader=DataLoader(ThreeDMatchFCGF(info_train,args,True),batch_size=args['train']['batch_size'],shuffle=True, drop_last=True, num_workers=args['misc']['workers'],collate_fn= collate_fn,pin_memory=args['misc']['pin_memory'])

#     val_loader=DataLoader(ThreeDMatchFCGF(info_val,args,False),batch_size=args['train']['batch_size'],shuffle=False, drop_last=False, num_workers=args['misc']['workers'],collate_fn= collate_fn,pin_memory=args['misc']['pin_memory'])

#     return train_loader,val_loader


def collate_fn(list_data):
    src_pcd,tgt_pcd,src_embedding,tgt_embedding,rot,trans=list(zip(*list_data))

    r_abs=torch.from_numpy(np.array(rot)).float()
    t_abs=torch.from_numpy(np.array(trans)).float()

    src_pcd = torch.from_numpy(np.array(src_pcd)).float()
    tgt_pcd = torch.from_numpy(np.array(tgt_pcd)).float()
    src_embedding = torch.from_numpy(np.array(src_embedding)).float()
    tgt_embedding = torch.from_numpy(np.array(tgt_embedding)).float()

    if(t_abs.dim()==2):
        t_abs=t_abs.unsqueeze(2)

    data=dict()
    data['src_pcd']= src_pcd
    data['src_embedding'] = src_embedding
    data['tgt_pcd']=tgt_pcd
    data['tgt_embedding'] = tgt_embedding
    data['rot']=r_abs
    data['trans']=t_abs

    return data


def get_train_val_loaders(args):
    '''
    get train/val for FCGF features of 3DMatch 
    '''
    all_infos=np.load(args['misc']['path_test_info'])
    info_val=dict()
    for key,value in all_infos.items():
        info_val[key]=value

    all_infos=np.load(args['misc']['path_train_info'])
    info_train=dict()
    for key,value in all_infos.items():
        info_train[key]=value
    # info_train,info_val=split_real_dataset(all_infos)

    train_loader=DataLoader(ThreeDMatchFCGFSampled(info_train,args,True),batch_size=args['train']['batch_size'],shuffle=True, drop_last=True, num_workers=args['misc']['workers'],collate_fn= collate_fn,pin_memory=args['misc']['pin_memory'])

    val_loader=DataLoader(ThreeDMatchFCGFSampled(info_val,args,False),batch_size=args['train']['batch_size'],shuffle=False, drop_last=False, num_workers=args['misc']['workers'],collate_fn= collate_fn,pin_memory=args['misc']['pin_memory'])

    return train_loader,val_loader
