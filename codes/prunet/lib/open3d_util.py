import open3d as o3d
import numpy as np
import torch

def square_distance(src, dst):
    """
    Calculate Euclid distance between each two points.
    src^T * dst = xn * xm + yn * ym + zn * zm；
    sum(src^2, dim=-1) = xn*xn + yn*yn + zn*zn;
    sum(dst^2, dim=-1) = xm*xm + ym*ym + zm*zm;
    dist = (xn - xm)^2 + (yn - ym)^2 + (zn - zm)^2
         = sum(src**2,dim=-1)+sum(dst**2,dim=-1)-2*src^T*dst
    Input:
        src: source points, [B, N, C]
        dst: target points, [B, M, C]
    Output:
        dist: per-point square distance, [B, N, M]
    """
    B, N, _ = src.shape
    _, M, _ = dst.shape
    dist = -2 * torch.matmul(src, dst.permute(0, 2, 1))
    dist += torch.sum(src ** 2, -1).view(B, N, 1)
    dist += torch.sum(dst ** 2, -1).view(B, 1, M)
    return dist

def get_blue():
    """
    Get color blue for rendering
    """
    return [0, 0.651, 0.929]

def get_yellow():
    """
    Get color yellow for rendering
    """
    return [1, 0.706, 0]


def to_pcd(points):
    """
    convert points to open3d point cloud
    Input:
        points: [N,3]
    """
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)
    return pcd

def custom_draw_geometry_with_rotation(pcd):
    """
    Visualize the point clouds while rotating it
    """
    def rotate_view(vis):
        ctr = vis.get_view_control()
        ctr.rotate(10.0, 0.0)
        return False

    o3d.visualization.draw_geometries_with_animation_callback([pcd],
                                                            rotate_view)

def plot_correspondence(src_pcd,tgt_pcd,src_feats,tgt_feats,rot,trans,shift=[0,3,0]):
    """
    Use argamx sampler to determine correspondence, then plot them
    Input:
        src_pcd:    [N,3]
        tgt_pcd:    [M,3]
        src_feats:  [N,C]
        tgt_feats:  [M,C]
        rot:        [3,3]
        trans:      [3,1]
    """
    scores=src_feats.dot(tgt_feats.T)
    tgt_indice=scores.argmax(1)
    src_indice=np.arange(0,src_pcd.shape[0])
    pairs=np.vstack([src_indice,tgt_indice]).T.tolist()
    pairs=[tuple(ele) for ele in pairs]

    src_pcd=(rot.dot(src_pcd.T)+trans).T
    src=to_pcd(src_pcd+shift)
    src.paint_uniform_color(get_blue())

    tgt=to_pcd(tgt_pcd)
    tgt.paint_uniform_color(get_yellow())

    src.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.3, max_nn=30))
    tgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.3, max_nn=30))

    line_set=o3d.geometry.LineSet.create_from_point_cloud_correspondences(src,tgt,pairs)
    o3d.visualization.draw_geometries([src,tgt,line_set])


def plot_classified_correspondence(src_pcd,tgt_pcd,src_feats,tgt_feats,rot,transshift=[0,3,0]):
    """
    Use argamx sampler to determine correspondence, then plot them with inlier/outlier mark
    Input:
        src_pcd:    [N,3]
        tgt_pcd:    [M,3]
        src_feats:  [N,C]
        tgt_feats:  [M,C]
        rot:        [3,3]
        trans:      [3,1]
    """
    # get correspondence
    scores=src_feats.dot(tgt_feats.T)
    tgt_indice=scores.argmax(1)
    src_indice=np.arange(0,src_pcd.shape[0])
    pairs=np.vstack([src_indice,tgt_indice]).T.tolist()
    pairs=[tuple(ele) for ele in pairs]

    src_pcd=(rot.dot(src_pcd.T)+trans).T
    src=to_pcd(src_pcd+shift)
    src.paint_uniform_color(get_blue())

    tgt=to_pcd(tgt_pcd)
    tgt.paint_uniform_color(get_yellow())

    src.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.3, max_nn=30))
    tgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.3, max_nn=30))

    # get inlier/outlier ground truth mark
    dist=square_distance(torch.Tensor(src_pcd).unsqueeze(0),torch.Tensor(tgt_pcd).unsqueeze(0))[0]
    distance=torch.gather(dist,1,index=torch.LongTensor(tgt_indice).unsqueeze(1)).squeeze(-1)
    flag=distance<0.1**2
    colors=torch.Tensor([1,0,0]).unsqueeze(0).repeat(dist.size(0),1)
    colors[flag]=torch.Tensor([0,1,0])

    line_set=o3d.geometry.LineSet.create_from_point_cloud_correspondences(src,tgt,pairs[::25])
    line_set.colors=o3d.utility.Vector3dVector(colors.numpy()[::25])
    o3d.visualization.draw_geometries([line_set,src,tgt])
                                                            