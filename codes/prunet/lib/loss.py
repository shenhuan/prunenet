import torch.nn.functional as F
import torch
import torch.nn as nn
from lib.util import transform_pcd,get_geometric_errors,get_angle_deviation
import numpy as np
from sklearn.metrics import precision_recall_fscore_support

class PruneNetLoss:
    def __init__(self,args):
        self.n_clusters=args['model']['n_clusters']
        self.inlier_threshold=args['data']['inlier_threshold']

    def init_stats(self):
        stats=dict()
        stats['inlier_ratio']=0.
        stats['precision']=0.
        stats['recall']=0.
        return stats

    def init_loss_terms(self):
        loss=dict()
        loss['total_loss']=0.
        loss['perm_loss']=0.
        loss['pos_loss']=0.
        loss['neg_loss']=0.
        return loss

    def _neg_log_likelihood(self,est_log_permutation_matrix,gt_permutation_matrix):
        '''
        negative log likelihood loss,here we weight the inlier / outlier 
        Input:
            est_log_permutation_matrix:  [B,N+1,N+1]
            gt_permutation_matrix:       [B,N+1,N+1]
        '''
        n_pos=torch.sum(gt_permutation_matrix[:,:-1,:-1]) # mutual
        n_neg=torch.sum(gt_permutation_matrix[:,-1,:-1])+torch.sum(gt_permutation_matrix[:,:-1,-1])
        w_pos=n_neg/(n_pos+n_neg)
        w_neg=1-w_pos

        # weighted negative log likelihood loss
        pos_loss=-est_log_permutation_matrix[:,:-1,:-1][gt_permutation_matrix[:,:-1,:-1].bool()].sum()
        neg_loss=-est_log_permutation_matrix[:,:-1,-1][gt_permutation_matrix[:,:-1,-1].bool()].sum()-est_log_permutation_matrix[:,-1,:-1][gt_permutation_matrix[:,-1,:-1].bool()].sum()

        pos_loss/=n_pos
        neg_loss/=n_neg

        weighted_loss=pos_loss*w_pos+neg_loss*w_neg
        unweighted_loss=pos_loss+neg_loss
        return weighted_loss,unweighted_loss,pos_loss,neg_loss



    def _class_loss(self, src, src_ref, logits, scores, rot, trans):
        """
        Binary classification loss per putative correspondence.
        Input:
            src:        [B,3,N]
            src_ref:    [B,3,N]
            logits:     [B,N] logits from filtering network
            scores:     [B,N] weights from filtering network
            rot:        [B,3,3]
            trans:      [B,3,1]        
        Return:
            w_class_loss: binary cross entropy loss [1]
            stats:        inlier_ratio / feature match recall / precision              

        precision: tp/(tp+fp)
        recall:    tp/(tp+fn)
        """
        ######################################
        # get unweighted BCE loss using logits
        src_ref_trans = transform_pcd(src, rot, trans) 

        dist = torch.norm(src_ref-src_ref_trans, dim=1).flatten()
        gt_labels = (dist<self.inlier_threshold).float()  

        loss = nn.BCELoss(reduction='none')
        sigmoid = nn.Sigmoid()
        class_loss = loss(sigmoid(logits).flatten(), gt_labels) # use logits to calculate the BCE loss

        #######################################
        # get weighted loss
        weights = torch.ones_like(gt_labels)
        w_negative = gt_labels.sum()/gt_labels.size(0)  # Number of positives/number of all
        w_positive = 1 - w_negative  # Number of negatives/number of all
        
        weights[gt_labels > 0.5] = w_positive
        weights[gt_labels < 0.5] = w_negative
        w_class_loss = torch.mean(weights * class_loss)

        #######################################
        # get precision and recall
        predicted_labels = scores.detach().round().flatten() # [0,1]

        inlier_ratio=w_negative
        precision, recall, f_measure, _ = precision_recall_fscore_support(gt_labels.cpu().numpy(),predicted_labels.cpu().numpy(), average='binary')
        
        # print('Inlier ratio: %.3f,   Recall:%.3f,  Precision:%.3f' % (inlier_ratio,recall,precision))

        stats=self.init_stats()
        stats['inlier_ratio']=inlier_ratio.item()
        stats['recall']=recall
        stats['precision']=precision

        return w_class_loss,stats


    def _rigid_motion_loss(self, rot_est, rot_gt, trans_est, trans_gt, method='Frobenius_norm'):
        """
        Frobenius norm of rotation matrix, L2 norm of translation vectors

        Input:  
            rot_est:    [B,3,3]
            trans_est:  [B,3,1]
        """
        diff_rot = (rot_est-rot_gt).view(rot_est.size(0), -1).contiguous()
        diff_trans = trans_est-trans_gt
        t_loss = torch.mean(torch.norm(diff_trans, dim=1))
        r_loss = torch.mean(torch.norm(diff_rot, dim=1))
        return r_loss + t_loss



    def evaluate(self,*inputs):
        log_perm_mat,gt_perm_mat=inputs

        loss=self.init_loss_terms()

        loss['perm_loss'],_,loss['pos_loss'],loss['neg_loss']=self._neg_log_likelihood(log_perm_mat,gt_perm_mat)
        loss['total_loss']=loss['perm_loss']

        return loss

