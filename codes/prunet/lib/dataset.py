#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys,glob,torch
import numpy as np
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset
import open3d as o3d
import MinkowskiEngine as ME
from lib.util import jitter_pointcloud,get_distant_point,farthest_subsample_points,get_permutation_matrix,get_inliers

class ThreeDMatchFCGF(Dataset):
    '''
    load FCGF features of 3DMatch
    '''
    def __init__(self,infos,args,data_augmentation=True):
        super(ThreeDMatchFCGF,self).__init__()
        self.infos = infos
        self.base_dir = args['misc']['base_dir']
        self.n_points = args['model']['n_input_points']
        self.rot_factor=args['data']['rot_factor']
        self.inlier_threshold=args['data']['inlier_threshold']
        self.data_augmentation=data_augmentation
        self.outlier_threshold=args['data']['outlier_threshold']

    def __len__(self):
        return len(self.infos['rot'])

    def __getitem__(self,item): 
        # get overlap and transformation
        rot=self.infos['rot'][item]
        trans=self.infos['trans'][item]

        # get pointcloud
        src_path=os.path.join(self.base_dir,self.infos['src'][item])
        tgt_path=os.path.join(self.base_dir,self.infos['tgt'][item])
        src = torch.load(src_path)
        tgt = torch.load(tgt_path)

        src_pcd, src_embedding = src['coords'],src['feats']
        tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats'] 

        # add gaussian noise
        if self.data_augmentation:            
            # rotate the point cloud
            euler_ab=np.random.rand(3)*np.pi*2/self.rot_factor # anglez, angley, anglex
            rot_ab= Rotation.from_euler('zyx', euler_ab).as_matrix()
            if(np.random.rand(1)[0]>0.5):
                src_pcd=np.matmul(rot_ab,src_pcd.T).T
                rot=np.matmul(rot,rot_ab.T)
            else:
                tgt_pcd=np.matmul(rot_ab,tgt_pcd.T).T
                rot=np.matmul(rot_ab,rot)
                trans=np.matmul(rot_ab,trans)

        src_pcd=torch.Tensor(src_pcd).float()
        tgt_pcd=torch.Tensor(tgt_pcd).float()
        src_embedding=torch.Tensor(src_embedding).float()
        tgt_embedding=torch.Tensor(tgt_embedding).float()
        rot=torch.Tensor(rot).float()
        trans=torch.Tensor(trans).float()

        if(trans.dim()==1):
            trans=trans.unsqueeze(-1)

        return src_pcd,tgt_pcd,src_embedding,tgt_embedding,rot,trans



class ThreeDMatchFCGFSampled(Dataset):
    '''
    load FCGF features of 3DMatch
    '''
    def __init__(self,infos,args,data_augmentation=True):
        super(ThreeDMatchFCGFSampled,self).__init__()
        self.infos = infos
        self.base_dir = args['misc']['base_dir']
        self.n_points = args['model']['n_input_points']
        self.rot_factor=args['data']['rot_factor']
        self.data_augmentation=data_augmentation

    def __len__(self):
        return len(self.infos['rot'])

    def __getitem__(self,item): 
        # get overlap and transformation
        rot=self.infos['rot'][item]
        trans=self.infos['trans'][item]

        # get pointcloud
        src_path=os.path.join(self.base_dir,self.infos['src'][item])
        tgt_path=os.path.join(self.base_dir,self.infos['tgt'][item])
        src = torch.load(src_path)
        tgt = torch.load(tgt_path)

        src_pcd, src_embedding = src['coords'],src['feats']
        tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats']
        
        # permute and randomly select 2048/1024 points
        if(src_pcd.shape[0]>self.n_points):
            src_permute=np.random.permutation(src_pcd.shape[0])[:self.n_points]
        else:
            src_permute=np.random.choice(src_pcd.shape[0],self.n_points)
        if(tgt_pcd.shape[0]>self.n_points):
            tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:self.n_points]
        else:
            tgt_permute=np.random.choice(tgt_pcd.shape[0],self.n_points)
        src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
        tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]        

        # add gaussian noise
        if self.data_augmentation:            
            # rotate the point cloud
            euler_ab=np.random.rand(3)*np.pi*2/self.rot_factor # anglez, angley, anglex
            rot_ab= Rotation.from_euler('zyx', euler_ab).as_matrix()
            if(np.random.rand(1)[0]>0.5):
                src_pcd=np.matmul(rot_ab,src_pcd.T).T
                rot=np.matmul(rot,rot_ab.T)
            else:
                tgt_pcd=np.matmul(rot_ab,tgt_pcd.T).T
                rot=np.matmul(rot_ab,rot)
                trans=np.matmul(rot_ab,trans)

        
        return src_pcd.T,tgt_pcd.T,src_embedding.T,tgt_embedding.T,rot.astype('float32'),trans.astype('float32')



