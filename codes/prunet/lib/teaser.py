import os,sys,glob,torch,time,math,copy
import numpy as np
from lib.util import natural_key,mutual_selection,setup_seed,load_config
import open3d as o3d
import teaserpp_python
from tqdm import tqdm
from lib.benchmark import read_trajectory,read_pairs,write_trajectory,benchmark
from models.model_non_diff import Engine
from models.pointconv_util import square_distance

NOISE_BOUND = 0.05

def pose_estimation_ransac(src_pcd,ref_pcd,src_embedding,ref_embedding,mutual=True):
    """
    Ransac based estimation of the transformation paramaters of the congurency transformation. Estimates the
    transformation parameters thtat map xyz0 to xyz1. Implementation is based on the open3d library
    (http://www.open3d.org/docs/release/python_api/open3d.registration.registration_ransac_based_on_correspondence.html)
    
    Input:
        src_pcd:            [N,3]
        src_embedding:      [N,C]
    """

    # Distance threshold as specificed by 3DMatch dataset
    distance_threshold = 0.05

    # mutual selection 
    scores=src_embedding.dot(ref_embedding.T)[None,:,:]
    if(mutual):
        selection=mutual_selection(scores)[0]
        src_selection,ref_selection=np.where(selection==1)
        
        src_pcd=src_pcd[src_selection]
        ref_pcd=ref_pcd[ref_selection]
    else:
        idx=scores.argmax(-1)[0]
        ref_pcd=ref_pcd[idx]

    
    # Convert the point to an open3d PointCloud object
    xyz0 = o3d.geometry.PointCloud()
    xyz1 = o3d.geometry.PointCloud()
    xyz0.points = o3d.utility.Vector3dVector(src_pcd)
    xyz1.points = o3d.utility.Vector3dVector(ref_pcd)

    # Correspondences are already sorted
    corr_idx = np.tile(np.expand_dims(np.arange(len(xyz0.points)),1),(1,2))
    corrs = o3d.utility.Vector2iVector(corr_idx)

    result_ransac = o3d.registration.registration_ransac_based_on_correspondence(
        source=xyz0, target=xyz1,corres=corrs, 
        max_correspondence_distance=distance_threshold,
        estimation_method=o3d.registration.TransformationEstimationPointToPoint(False),
        ransac_n=4,
        criteria=o3d.registration.RANSACConvergenceCriteria(50000, 1000))

    trans_param = result_ransac.transformation
    
    return trans_param

def load_all_gt_pairs(gt_log_path):
    """
    Load all possible pairs from GT
    """
    with open(gt_log_path) as f:
        content = f.readlines()

    gt_data = {}
    for i in range(len(content)):
        tokens = [k.strip() for k in content[i].split(" ")]
        if len(tokens) == 3:
            frag1 = int(tokens[0])
            frag2 = int(tokens[1])

            def line_to_list(line):
                return [float(k.strip()) for k in line.split("\t")[:4]]

            gt_mat = np.array([line_to_list(content[i+1]),
                               line_to_list(content[i+2]), 
                               line_to_list(content[i+3]), 
                               line_to_list(content[i+4])])

            gt_data[(frag1, frag2)] = gt_mat

    return gt_data

def load_gt_transformation(fragment1_idx, fragment2_idx, gt_log_path):
    """
    Load gt transformation
    """
    with open(gt_log_path) as f:
        content = f.readlines()

    for i in range(len(content)):
        tokens = [k.strip() for k in content[i].split(" ")]
        if tokens[0] == str(fragment1_idx) and tokens[1] == str(fragment2_idx):

            def line_to_list(line):
                return [float(k.strip()) for k in line.split("\t")[:4]]

            gt_mat = np.array([line_to_list(content[i+1]),
                               line_to_list(content[i+2]), 
                               line_to_list(content[i+3]), 
                               line_to_list(content[i+4])])
            return gt_mat
    return None
    
def get_gt_inliers(fragment1_points, fragment2_points, gt_mat):
    """
    Get GT inliers
    """
    # gt transformed source
    gt_fragment1 = o3d.geometry.PointCloud()
    gt_fragment1.points = o3d.utility.Vector3dVector(fragment2_points)
    gt_fragment1.transform(gt_mat)

    # Count inliers and outliers
    inliers_set = set()
    num_inliers = 0
    total = 0
    for i in range(fragment1_points.shape[0]):
        total += 1

        p1 = np.asarray(gt_fragment1.points)[i, :]
        p2 = fragment1_points[i, :]

        dist = np.linalg.norm(p1 - p2)
        if dist <= NOISE_BOUND:
            inliers_set.add(i)
            num_inliers += 1

    # GT Inliers
    print("GT Inliers count:", num_inliers)
    print("GT Inliers:", inliers_set)
    return inliers_set

def compose_mat4_from_teaserpp_solution(solution):
    """
    Compose a 4-by-4 matrix from teaserpp solution
    """
    s = solution.scale
    rotR = solution.rotation
    t = solution.translation
    T = np.eye(4)
    T[0:3, 3] = t
    R = np.eye(4)
    R[0:3, 0:3] = rotR
    M = T.dot(R)

    if s == 1:
        M = T.dot(R)
    else:
        S = np.eye(4)
        S[0:3, 0:3] = np.diag([s, s, s])
        M = T.dot(R).dot(S)

    return M

def get_angular_error(R_gt, R_est):
    """
    Get angular error
    """
    try:
        A = (np.trace(np.dot(R_gt.T, R_est))-1) / 2.0
        if A < -1:
            A = -1
        if A > 1:
            A = 1
        rotError = math.fabs(math.acos(A));
        return math.degrees(rotError)
    except ValueError:
        import pdb; pdb.set_trace()
        return 99999

def compute_transformation_diff(est_mat, gt_mat):
    """
    Compute difference between two 4-by-4 SE3 transformation matrix
    """
    R_gt = gt_mat[:3,:3]
    R_est = est_mat[:3,:3]
    rot_error = get_angular_error(R_gt, R_est)

    t_gt = gt_mat[:,-1]
    t_est = est_mat[:,-1]
    trans_error = np.linalg.norm(t_gt - t_est)

    return rot_error, trans_error


def get_r_t(path):
    '''
    Read rotation and translation from log file
    '''
    f=open(path,'r')
    lines=f.readlines()[1:]
    rows=[]
    for eachline in lines:
        eles=eachline.split('	 ')
        rows.append([float(ele) for ele in eles])
    T=np.array(rows)
    r,t=T[:3,:3],T[:3,3]
    return r,t


def get_teaser_config():
    """
    Here we use the default configs taken from here: https://github.com/MIT-SPARK/TEASER-plusplus/tree/master/examples/teaser_python_3dsmooth

    """
    NOISE_BOUND = 0.05
    solver_params = teaserpp_python.RobustRegistrationSolver.Params()
    solver_params.cbar2 = 1
    solver_params.noise_bound = NOISE_BOUND
    solver_params.estimate_scaling = False
    solver_params.rotation_estimation_algorithm = (
        teaserpp_python.RobustRegistrationSolver.ROTATION_ESTIMATION_ALGORITHM.GNC_TLS
    )
    solver_params.rotation_gnc_factor = 1.4
    solver_params.rotation_max_iterations = 100
    solver_params.rotation_cost_threshold = 1e-12

    return solver_params

def pose_estimation_teaser(src_pcd,ref_pcd,src_embedding,ref_embedding,solver_params,mutual=True):
    """
    Given input point clouds and embeddings, run feature match firts, then use teaser++ to estimate the pose
    """
    teaserpp_solver = teaserpp_python.RobustRegistrationSolver(solver_params)

    # mutual selection 
    scores=src_embedding.dot(ref_embedding.T)[None,:,:]
    if(mutual):
        selection=mutual_selection(scores)[0]
        src_selection,ref_selection=np.where(selection==1)
        
        src_pcd=src_pcd[src_selection]
        ref_pcd=ref_pcd[ref_selection]
    else:
        idx=scores.argmax(-1)[0]
        ref_pcd=ref_pcd[idx]

    teaserpp_solver.solve(src_pcd.T,ref_pcd.T)
    est_solution = teaserpp_solver.getSolution()
    est_mat = compose_mat4_from_teaserpp_solution(est_solution)
    return est_mat


def ss1():
    """
    Benchmark teaser, 5000 points + mutual selection
    """
    # Prepare TEASER++ Solver
    n_points=2000
    benchmark_dir='/scratch/shengyu/masterthesis/benchmark/3DMatch/gt_result'
    base_test='/net/pf-pc27/scratch2/shengyu/3DMatch/test'
    scene_names=sorted(os.listdir(benchmark_dir))
    exp_dir=os.path.join(benchmark_dir.split('/gt_result')[0],'teaser_5000')
    # os.makedirs(exp_dir)
    solver_params=get_teaser_config()

    for scene_name in tqdm(scene_names):
        c_path=os.path.join(base_test,scene_name,'05_FCGF_feats_0.025/*.pth')
        samples=sorted(glob.glob(c_path),key=natural_key)

        c_path=os.path.join(base_test,scene_name,'overlap.npy')
        overlaps=np.load(c_path)

        gt_pairs, gt_traj = read_trajectory(os.path.join(benchmark_dir,scene_name,'gt.log'))
        est_traj = []
        for i in range(len(gt_pairs)):
            # prepare test pair
            ind0,ind1,n_fragments=int(gt_pairs[i][0]),int(gt_pairs[i][1]),int(gt_pairs[i][2])
            src_path,tgt_path=samples[ind1],samples[ind0]
            c_overlap = max(overlaps[ind0,ind1],overlaps[ind1,ind0])
            src_pcd,src_embedding,ref_pcd,ref_embedding=read_pairs(src_path,tgt_path,n_points)

            # get transformation estimation
            tsfm=gt_traj[i]
            ts_est=pose_estimation_teaser(src_pcd,ref_pcd,src_embedding,ref_embedding,solver_params,False)

            est_traj.append(ts_est)
            
        # write the trajectory
        c_directory=os.path.join(exp_dir,scene_name)
        os.makedirs(c_directory)
        write_trajectory(np.array(est_traj),gt_pairs,os.path.join(c_directory,'est.log'))

def ss2():
    base_dir='/net/pf-pc27/scratch2/shengyu/3DMatch'
    n_points=2000
    solver_params=get_teaser_config()

    for i in range(0,4):
        path=f'/scratch/shengyu/masterthesis/dataset/3DMatch/test/overlap_{i}.npz'
        infos=np.load(path)
        pose_estimates=[]
        for item in tqdm(range(len(infos['src']))):
            # get overlap and transformation
            overlap=infos['overlap'][item]
            rot=infos['rot'][item]
            trans=infos['trans'][item]

            # get pointcloud
            src_path=os.path.join(base_dir,infos['src'][item])
            tgt_path=os.path.join(base_dir,infos['tgt'][item])
            src = torch.load(src_path)
            tgt = torch.load(tgt_path)

            src_pcd, src_embedding = src['coords'],src['feats']
            tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats']
            
            # permute and randomly select 2048/1024 points
            if(src_pcd.shape[0]>n_points):
                src_permute=np.random.permutation(src_pcd.shape[0])[:n_points]
            else:
                src_permute=np.random.choice(src_pcd.shape[0],n_points)
            if(tgt_pcd.shape[0]>n_points):
                tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:n_points]
            else:
                tgt_permute=np.random.choice(tgt_pcd.shape[0],n_points)
            src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
            tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]

            pose_est=pose_estimation_teaser(src_pcd,tgt_pcd,src_embedding,tgt_embedding,solver_params,False)

            pose_estimates.append(pose_est)
    
        np.save(f'dump/teaser/overlap_{i}_est',np.array(pose_estimates))

def ss3():
    """
    Run TEASER++ on the pruned point cloud
    """
    base_dir='/net/pf-pc27/scratch2/shengyu/3DMatch'
    n_points=4096
    solver_params=get_teaser_config()

    cfg=load_config('configs/config.yaml')

    #########################################
    # fix random seed
    setup_seed(cfg['misc']['seed'])
    torch.backends.cudnn.enabled = False

    net = Engine(cfg).cuda()
    net.eval()
    device=torch.device('cuda')

    for i in range(2,4):
        path=f'/scratch/shengyu/masterthesis/dataset/3DMatch/test/overlap_{i}.npz'
        infos=np.load(path)
        pose_estimates_teaser=[]
        pose_estimates_ransac=[]
        for item in tqdm(range(len(infos['src']))):
            # get overlap and transformation
            overlap=infos['overlap'][item]
            rot=infos['rot'][item]
            trans=infos['trans'][item]

            # get pointcloud
            src_path=os.path.join(base_dir,infos['src'][item])
            tgt_path=os.path.join(base_dir,infos['tgt'][item])
            src = torch.load(src_path)
            tgt = torch.load(tgt_path)

            src_pcd, src_embedding = src['coords'],src['feats']
            tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats']

            
            # permute and randomly select 4096 points
            if(src_pcd.shape[0]>n_points):
                src_permute=np.random.permutation(src_pcd.shape[0])[:n_points]
            else:
                src_permute=np.random.choice(src_pcd.shape[0],n_points)
            if(tgt_pcd.shape[0]>n_points):
                tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:n_points]
            else:
                tgt_permute=np.random.choice(tgt_pcd.shape[0],n_points)
            src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
            tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]

            src_pcd=torch.from_numpy(src_pcd).unsqueeze(0).to(device).float()
            tgt_pcd=torch.from_numpy(tgt_pcd).unsqueeze(0).to(device).float()
            src_embedding=torch.from_numpy(src_embedding).unsqueeze(0).to(device).float()
            tgt_embedding=torch.from_numpy(tgt_embedding).unsqueeze(0).to(device).float()
            rot=torch.from_numpy(rot).unsqueeze(0).to(device)
            trans=torch.from_numpy(trans).unsqueeze(0).to(device)


            # prune the point cloud
            src_idx,tgt_idx=net.forward(src_pcd.contiguous(),tgt_pcd.contiguous(),src_embedding.contiguous(),tgt_embedding.contiguous())

            src_idx=list(set(src_idx.view(-1).cpu().tolist()))
            src_idx=np.array(src_idx)

            tgt_idx=list(set(tgt_idx.view(-1).cpu().tolist()))
            tgt_idx=np.array(tgt_idx)


            src_pcd=src_pcd[0].cpu().numpy()[src_idx]
            tgt_pcd=tgt_pcd[0].cpu().numpy()[tgt_idx]
            src_embedding=src_embedding[0].cpu().numpy()[src_idx]
            tgt_embedding=tgt_embedding[0].cpu().numpy()[tgt_idx]

            pose_est=pose_estimation_ransac(src_pcd,tgt_pcd,src_embedding,tgt_embedding,False)
            pose_estimates_ransac.append(pose_est)

            # # permute and randomly select 4096 points
            # nn=2000
            # if(src_pcd.shape[0]>nn):
            #     src_permute=np.random.permutation(src_pcd.shape[0])[:nn]
            # else:
            #     src_permute=np.random.choice(src_pcd.shape[0],nn)
            # if(tgt_pcd.shape[0]>nn):
            #     tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:nn]
            # else:
            #     tgt_permute=np.random.choice(tgt_pcd.shape[0],nn)
            # src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
            # tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]

            # pose_est=pose_estimation_teaser(src_pcd,tgt_pcd,src_embedding,tgt_embedding,solver_params,False)
            # pose_estimates_teaser.append(pose_est)


    
        # np.save(f'dump/teaser_prune/overlap_{i}_est',np.array(pose_estimates_teaser))
        np.save(f'dump/ransac_prune/overlap_{i}_est',np.array(pose_estimates_ransac))

if __name__=='__main__':
    """
    Run TEASER++ on the pruned point cloud
    """
    base_dir='/net/pf-pc27/scratch2/shengyu/3DMatch'
    n_points=2048
    solver_params=get_teaser_config()

    cfg=load_config('configs/ltfgc.yaml')

    #########################################
    # fix random seed
    setup_seed(cfg['misc']['seed'])
    torch.backends.cudnn.enabled = False

    net = Engine(cfg).cuda()
    net.eval()
    device=torch.device('cuda')

    pre_overlap=[]
    after_overlap=[]

    for i in range(0,4):
        path=f'/scratch/shengyu/masterthesis/dataset/3DMatch/test/overlap_{i}.npz'
        infos=np.load(path)


        for item in tqdm(range(len(infos['src']))):
            # get overlap and transformation
            overlap=infos['overlap'][item]
            rot=infos['rot'][item]
            trans=infos['trans'][item]

            # get pointcloud
            src_path=os.path.join(base_dir,infos['src'][item])
            tgt_path=os.path.join(base_dir,infos['tgt'][item])
            src = torch.load(src_path)
            tgt = torch.load(tgt_path)

            src_pcd, src_embedding = src['coords'],src['feats']
            tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats']

            
            # permute and randomly select 4096 points
            if(src_pcd.shape[0]>n_points):
                src_permute=np.random.permutation(src_pcd.shape[0])[:n_points]
            else:
                src_permute=np.random.choice(src_pcd.shape[0],n_points)
            if(tgt_pcd.shape[0]>n_points):
                tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:n_points]
            else:
                tgt_permute=np.random.choice(tgt_pcd.shape[0],n_points)
            src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
            tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]


            # pre-compute the overlap
            src_pcd_trans=(rot.dot(src_pcd.T)+trans).T
            src_pcd_trans=torch.from_numpy(src_pcd_trans).unsqueeze(0).float()
            tgt_pcd=torch.from_numpy(tgt_pcd).unsqueeze(0).float()
            distances=square_distance(src_pcd_trans,tgt_pcd)[0].numpy()
            distances=distances<0.1**2
            n=distances.shape[0]
            overlap=(distances.sum(1)>0).sum()/n
            pre_overlap.append(overlap)

            src_pcd=torch.from_numpy(src_pcd).unsqueeze(0).to(device).float()
            tgt_pcd=tgt_pcd.to(device)
            src_embedding=torch.from_numpy(src_embedding).unsqueeze(0).to(device).float()
            tgt_embedding=torch.from_numpy(tgt_embedding).unsqueeze(0).to(device).float()


            # prune the point cloud
            src_idx,tgt_idx=net.forward(src_pcd.contiguous(),tgt_pcd.contiguous(),src_embedding.contiguous(),tgt_embedding.contiguous())

            src_idx=list(set(src_idx.view(-1).cpu().tolist()))
            src_idx=np.array(src_idx)

            tgt_idx=list(set(tgt_idx.view(-1).cpu().tolist()))
            tgt_idx=np.array(tgt_idx)


            src_pcd=src_pcd[0].cpu().numpy()[src_idx]
            tgt_pcd=tgt_pcd[0].cpu().numpy()[tgt_idx]

            # transform the src
            src_pcd_trans=(rot.dot(src_pcd.T)+trans).T
            src_pcd_trans=torch.from_numpy(src_pcd_trans).unsqueeze(0).float()
            tgt_pcd=torch.from_numpy(tgt_pcd).unsqueeze(0).float()


            distances=square_distance(src_pcd_trans,tgt_pcd)[0].numpy()
            distances=distances<0.1**2
            n=distances.shape[0]
            overlap=(distances.sum(1)>0).sum()/n

            after_overlap.append(overlap)
    
    np.savez('prune_ana',before=np.array(pre_overlap),after=np.array(after_overlap))



            

    
        # # 
        # np.save(f'dump/ransac_prune/overlap_{i}_est',np.array(pose_estimates_ransac))
    