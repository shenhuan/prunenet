import torch
import os,sys,gc,time,glob
import numpy as np
import torch.nn.functional as F
from lib.util import Logger,get_geometric_errors,validate_gradient
from tqdm import tqdm
from torch.optim.lr_scheduler import MultiStepLR
import torch.optim as optim
import open3d as o3d
from lib.loss import PruneNetLoss

from tensorboardX import SummaryWriter


class PruneTrainer:
    '''
    PruneTrainer
    '''
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        self.loader,self.n_iter={},{}
        self.loader['train']=train_loader
        self.loader['val']=val_loader
        self.loader['test']=test_loader
        self.loss_evaluator=PruneNetLoss(args)

        self.n_iter['train'] = int(
            len(train_loader.dataset)/args['train']['batch_size'])
        self.n_iter['val']= int(len(val_loader.dataset) /
                              args['train']['batch_size'])
        self.n_iter['test']= int(len(test_loader.dataset) /
                              args['train']['batch_size'])

        self.epochs = args['train']['epochs']
        self.model = net
        self.device=torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.exp_name = args['exp_name']
        self.writer = SummaryWriter(logdir=self.exp_name)
        self.logger=Logger(args)

        self.n_points=args['model']['n_input_points']
        self.logger.write('#parameters %.3f M\n' % (sum([x.nelement() for x in self.model.parameters()])/1e6))
        self.inlier_threshold = args['data']['inlier_threshold']
        self.dump_dir=args['dump_dir']

        ###################################
        # optimiser
        ###################################
        if args['optimizer']['use_sgd']:
            self.opt = optim.SGD(self.model.parameters(
            ), lr=args['optimiser']['lr'] * 100, momentum=args['optimizer']['momentum'], weight_decay=1e-4)
        else:
            self.opt = optim.Adam(
                net.parameters(), lr=args['optimizer']['lr'], weight_decay=1e-4)

        epoch_factor = args['train']['epochs'] / 100.0

        ###################################
        # learning rate scheduler
        ###################################
        self.scheduler = MultiStepLR(self.opt, milestones=[int(
            20*epoch_factor), int(50*epoch_factor),int(80*epoch_factor)], gamma=0.1)
    
    def _add_scalers(self, c_iter, c_loss, phase):
        '''
        update the tensorboard
        '''
        for key, value in c_loss.items():
            self.writer.add_scalar(
                '%s/%s' % (phase, key), value, c_iter)
    

    def _inference_one_epoch(self, epoch, phase):
        '''
        inference one epoch, return loss terms and errors
        for each batch, the loss terms are updated to tensorboard
        '''
        assert phase in ['train', 'val','test']

        ###################################
        # Initialize and set data loader
        ###################################
        rotations_gt = []
        translations_gt = []
        rotations_est = []
        translations_est = []
        retrieved=[]

        total_loss = self.loss_evaluator.init_loss_terms()
        n_batches = 0
        c_loader=self.loader[phase]
        
        ###################################
        #### loop the data loader
        ###################################
        for data in tqdm(c_loader): # here data is a dictionary
            n_batches += 1

            ###########################################
            # update for each batch
            ###########################################
            c_iter = n_batches + epoch * self.n_iter[phase]            
            batch_loss,rot_est,trans_est,nn_correct=self._inference_one_batch(data,phase,c_iter)
            torch.cuda.empty_cache()

            for key, value in batch_loss.items():
                total_loss[key] += value
            
            ## update to tensorboard
            self._add_scalers(c_iter,batch_loss,phase) 
 
            rotations_gt.append(data['rot'].numpy())
            translations_gt.append(data['trans'].numpy())
            rotations_est.append(rot_est.cpu().numpy())
            translations_est.append(trans_est.cpu().numpy())
            retrieved.append(nn_correct)
        
        # retrieved=np.array(retrieved)
        # np.save('dump/thesis/patch_recall.npy',retrieved)

        ###########################################
        # get rotation and translation errors
        ###########################################
        total_errors = get_geometric_errors(rotations_gt,rotations_est,translations_gt,translations_est)

        ##########################################
        # update log
        ##########################################
        for key, value in total_loss.items():
            new_value=value/n_batches
            total_loss[key]=new_value
            self.writer.add_scalar('g_%s/%s' % (phase,key),new_value,epoch)
        
        info = {'epoch': epoch,
                'stage': phase,
                'errors': total_errors,
                'loss': total_loss
                }
        self.logger.write(str(info)+'\n')
        return info

    def _to_device(self,data):
        """
        Put the elements of a list to CUDA
        """
        for i in range(len(data)):
            data[i]=data[i].to(self.device)
        return data

    def _inference_one_batch(self, data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        list, Nx3
            src_embedding:  list, NxC
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_ab_est:     B x 3 x 3
            trans_ab_est:   B x 3 x 1
        '''
        # src,tgt=data['src_pcd'],data['tgt_pcd']
        # src_embedding,tgt_embedding = data['src_embedding'],data['tgt_embedding']
        # rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)

        # src,tgt=self._to_device(src),self._to_device(tgt)
        # src_embedding,tgt_embedding=self._to_device(src_embedding),self._to_device(tgt_embedding)

        src_pcd,tgt_pcd=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        src_embedding,tgt_embedding=data['src_embedding'].to(self.device),data['tgt_embedding'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)

        ######################################################################
        # estimate current rotation and translation
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad() # zero_gradient the model

            # make predictions
            rot_est,trans_est,log_perm_matrix,gt_perm_matrix = self.model.forward(src_pcd.transpose(1,2).contiguous(),tgt_pcd.transpose(1,2).contiguous(),src_embedding.transpose(1,2).contiguous(),tgt_embedding.transpose(1,2).contiguous(),rot_gt,trans_gt)

            # get current loss
            c_loss = self.loss_evaluator.evaluate(log_perm_matrix,gt_perm_matrix)

            c_loss['total_loss'].backward()
            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')
            

        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions  
                rot_est,trans_est,log_perm_matrix,gt_perm_matrix,nn_correct = self.model.forward(src_pcd.transpose(1,2).contiguous(),tgt_pcd.transpose(1,2).contiguous(),src_embedding.transpose(1,2).contiguous(),tgt_embedding.transpose(1,2).contiguous(),rot_gt,trans_gt)

                # get current loss
                c_loss = self.loss_evaluator.evaluate(log_perm_matrix,gt_perm_matrix)
        
        for key,value in c_loss.items():
            c_loss[key]=value.item()

        return c_loss,rot_est.detach(),trans_est.detach(),nn_correct

    def val(self):
        self._inference_one_epoch(0, 'val')

    def train(self):
        '''
        Logics of training procedure
        '''
        info_test_best = None
        for epoch in range(self.epochs):
            # train and evaluate
            info_train = self._inference_one_epoch(epoch, 'train')
            info_test = self._inference_one_epoch(epoch, 'val')
            self.scheduler.step()

            # update checkpoints
            if info_test_best is None or info_test_best['loss']['total_loss'] > info_test['loss']['total_loss']:
                info_test_best = info_test
                info_test_best['stage'] = 'best_test'
                self.model.save('%s/model.best.pth' % self.exp_name)
                self.logger.write('Better model!\n')
            gc.collect()