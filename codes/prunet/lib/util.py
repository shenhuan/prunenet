#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
import os,re,sys,json,yaml,random
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from scipy.spatial.transform import Rotation

from sklearn.neighbors import NearestNeighbors
from scipy.spatial.distance import minkowski
_EPS = 1e-7  # To prevent division by zero

def load_config(path):
    """
    Loads config file:

    Args:
        path (str): path to the config file

    Returns: 
        config (dict): dictionary of the configuration parameters

    """
    with open(path,'r') as f:
        cfg = yaml.safe_load(f)

    return cfg


def setup_seed(seed):
    """
    fix random seed for deterministic training
    """
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    torch.backends.cudnn.deterministic = True

def to_tensor(array):
    """
    Convert array to tensor
    """
    if(not isinstance(array,torch.Tensor)):
        return torch.from_numpy(array)
    else:
        return array

def to_array(tensor):
    """
    Conver tensor to array
    """
    if(not isinstance(tensor,np.ndarray)):
        return tensor.cpu().numpy()
    else:
        return tensor

def mutual_selection(score_mat):
    """
    Return a {0,1} matrix, the element is 1 if and only if it's maximum along both row and column
    
    Input: np.array()
        score_mat:  [B,N,N]
    Return:
        mutuals:    [B,N,N] 
    """
    score_mat=to_array(score_mat)
    if(score_mat.ndim==2):
        score_mat=score_mat[None,:,:]
    
    mutuals=np.zeros_like(score_mat)
    for i in range(score_mat.shape[0]): # loop through the batch
        c_mat=score_mat[i]
        flag_row=np.zeros_like(c_mat)
        flag_column=np.zeros_like(c_mat)

        max_along_row=np.argmax(c_mat,1)[:,None]
        max_along_column=np.argmax(c_mat,0)[None,:]
        np.put_along_axis(flag_row,max_along_row,1,1)
        np.put_along_axis(flag_column,max_along_column,1,0)
        mutuals[i]=(flag_row.astype(np.bool)) & (flag_column.astype(np.bool))
    return mutuals.astype(np.bool)  

def get_inliers(src_pcd,tgt_pcd,rot,trans,inlier_threshold):
    """
    Given two point cloudd, get all the ground truth pairs
    Input: (np.ndarray)
        src_pcd:    [N, 3]
        tgt_pcd:    [N, 3]
        rot:        [3, 3]
        trans:      [3, 1]
        inlier_threshold:  float
    return:
        inlier_matrix:  [N+1,N+1] {0,1} matrix, 1 if its distance is within the threshold
    """
    # get L2 distance
    src_ref=(rot.dot(src_pcd.T)+trans).T
    dist = -2 * src_ref.dot(tgt_pcd.T)
    dist+= np.sum(src_ref**2,1)[:,None]
    dist+= np.sum(tgt_pcd**2,1)[None,:]
    dist = np.sqrt(dist)

    inlier_matrix=(dist<inlier_threshold).astype(np.float)
    inlier_matrix=np.pad(inlier_matrix,((0,1),(0,1)),'constant',constant_values=(0,0))
    flag_row = np.sum(inlier_matrix,1)==0.
    flag_column=np.sum(inlier_matrix,0)==0.
    inlier_matrix[:,-1]=flag_row.astype(np.float32)
    inlier_matrix[-1,:]=flag_column.astype(np.float32)
    inlier_matrix[-1,-1]=0

    return inlier_matrix
    


def get_permutation_matrix(src_pcd,tgt_pcd,rot,trans,inlier_threshold,outlier_threshold):
    """
    Given two point clouds, get permutation matrix based on L2 distance
    Input: (np.array)
        src_pcd:    [N, 3]
        tgt_pcd:    [N, 3]
        rot:        [3, 3]
        trans:      [3, 1]
        inlier_threshold:  float
        outlier_threshold: float
    return:
        permutation matrix: N+1,N+1, the last column is 1 if there's no correspondence
    """
    n_points=src_pcd.shape[0]

    # get L2 distance
    src_ref=(rot.dot(src_pcd.T)+trans).T
    dist = -2 * src_ref.dot(tgt_pcd.T)
    dist+= np.sum(src_ref**2,1)[:,None]
    dist+= np.sum(tgt_pcd**2,1)[None,:]
    dist = np.sqrt(dist)

    # treat the minimum along both row and column as correspondence(mutual selection)
    mutuals=mutual_selection(-dist)[0]
    thresholding=dist<inlier_threshold
    perm_mat=mutuals & thresholding
    perm_mat=np.pad(perm_mat,((0,1),(0,1)),'constant',constant_values=(0,0))

    # number of total inliers
    n_postive_samples=perm_mat.sum()

    # assign outliers 
    dist=np.pad(dist,((0,1),(0,1)),'constant',constant_values=(1e7,1e7))
    flag_row = (np.sum(perm_mat,1)==0.) & (np.min(dist,1)>= outlier_threshold)
    flag_column=(np.sum(perm_mat,0)==0.) & (np.min(dist,0)>= outlier_threshold)
    perm_mat[:,-1]=flag_row.astype(np.float32)
    perm_mat[-1,:]=flag_column.astype(np.float32)
    perm_mat[-1,-1]=0

    n_nagative_samples=perm_mat.sum()-n_postive_samples
    n_ambiguity=2*n_points-n_postive_samples*2-n_nagative_samples

    # print('Inlier ratio: %.2f; Outlier ratio: %.2f; Ambiguity ratio: %.2f' % (n_postive_samples/n_points,n_nagative_samples/n_points/2,n_ambiguity/n_points/2))

    return perm_mat

def arange_like(x, dim: int):
    return x.new_ones(x.shape[dim]).cumsum(0) - 1  # traceable in 1.1

def log_perm_to_correspondence(log_perm_matrix,match_threshold,xyz_ref,threshold=True):
    """
    Get correspondence from permutation matrix in log space
    Input:
        log_perm_matrix:    [B, N+1 , N+1], the last column is dustbin
        match_threshold:    float
        xyz_ref:            [B, 3, N]
    return:
        correspondence:     [B, 3, N]
        mscores:            [B, N]    use 0 to mask all the outliers in case of threshold=True
    """
    if(threshold): # use mutual selection and match_threshold to select one-to-one match
        max0, max1 = log_perm_matrix[:, :-1, :-1].max(2), log_perm_matrix[:, :-1, :-1].max(1) # [B,N]
        indices0, indices1 = max0.indices, max1.indices
        mutual0 = arange_like(indices0, 1)[None] == indices1.gather(1, indices0) #[B,N] bool
        above_threshold = max0.values.exp()>match_threshold 
        valid0 = mutual0 & above_threshold

        mscores0 = torch.where(valid0, max0.values.exp(), log_perm_matrix.new_tensor(0)) # B,N used as weights
        indices0 = torch.where(valid0, indices0, indices0.new_tensor(0))  # B,N, use 0 for invalid match
        indices0 = indices0.unsqueeze(1).repeat(1,3,1)
        correspondence = torch.gather(xyz_ref,dim=2,index=indices0)
        weights = mscores0
    else: # weighted correspondence
        perm_matrix = torch.exp(log_perm_matrix[:,:-1,:-1])
        correspondence = perm_matrix @ xyz_ref.transpose(1,2) / (torch.sum(perm_matrix, dim=2, keepdim=True) + _EPS)
        correspondence=correspondence.transpose(1,2)
        weights=torch.sum(perm_matrix, dim=2)
    return correspondence,weights

def jitter_pointcloud(pointcloud, sigma=0.01, clip=0.05):
    """
    Add Gaussian noise to the point cloud
    Input:
        pointcloud:     [N,3]
    """
    N, C = pointcloud.shape
    pointcloud += np.clip(sigma * np.random.randn(N, C), -1 * clip, clip)
    return pointcloud

def get_distant_point():
    """
    generate two distant point
    """
    random_p1=np.random.randint(500,1000,3).reshape(1,-1).astype('float32')
    random_p2=np.random.randint(500,1000,3).reshape(1,-1).astype('float32')
    # random_p1 = np.random.random(size=(1, 3)) + np.array([[500, 500, 500]]) * np.random.choice([1, -1, 1, -1])
    # random_p2=random_p1
    return random_p1,random_p2

def farthest_subsample_points(pcd_1, pcd_2,n_subsampled_points_1,n_subsampled_points_2):
    """
    We generate a random point and sample the closest N points to it as the partial representation
    Input:
        pcd_1:                      [N,3]
        n_ssubsampled_points_1:     int
    """
    pcd_1=pcd_1.astype('float32')
    pcd_2=pcd_2.astype('float32')

    random_p1,random_p2=get_distant_point()
    
    nbrs1 = NearestNeighbors(n_neighbors=n_subsampled_points_1, algorithm='auto',
                             metric=lambda x, y: minkowski(x, y)).fit(pcd_1)
    idx1 = nbrs1.kneighbors(random_p1, return_distance=False).reshape((n_subsampled_points_1,))

    nbrs2 = NearestNeighbors(n_neighbors=n_subsampled_points_2, algorithm='auto',
                             metric=lambda x, y: minkowski(x, y)).fit(pcd_2)

    idx2 = nbrs2.kneighbors(random_p2, return_distance=False).reshape((n_subsampled_points_2,))

    return pcd_1[idx1, :], pcd_2[idx2, :]

def square_distance(src, dst):
    """
    Calculate Euclid distance between each two points.
    Args:
        src: source points, [B, N, C]
        dst: target points, [B, M, C]
    Returns:
        dist: per-point square distance, [B, N, M]
    """
    B, N, _ = src.shape
    _, M, _ = dst.shape
    dist = -2 * torch.matmul(src, dst.permute(0, 2, 1))
    dist += torch.sum(src ** 2, dim=-1)[:, :, None]
    dist += torch.sum(dst ** 2, dim=-1)[:, None, :]
    return dist

def angle_difference(src, dst):
    """
    Calculate angle between each pair of vectors.Assumes points are l2-normalized to unit length.
    Input:
        src: source points, [B, N, C]
        dst: target points, [B, M, C]
    Output:
        dist: per-point square distance, [B, N, M]
    """
    B, N, _ = src.shape
    _, M, _ = dst.shape
    dist = torch.matmul(src, dst.permute(0, 2, 1))
    dist = torch.acos(dist)

    return dist
        
def sparse_tensor_to_normal_tensor(stensor,n_points,voxel_size):
    """
    Convert sparse tensor to batched normal torch tensor, we sample n_points 
    Input:
        stensor:    MinkowskiEngine sparse tensor
        n_points:   int, 
        voxel_size: float
    return:
        coords: [B,3,N]
        feats:  [B,C,N]
    """
    coords, feats = [], []
    batch_size = len(stensor.decomposed_coordinates)
    for ii in range(batch_size):
        fea = stensor.features_at(ii)
        coo = stensor.coordinates_at(ii)

        # randomly sample fixed number of points
        if(coo.size(0)>n_points):
            idx = np.random.permutation(coo.size(0))[:n_points]
        else:
            idx=np.random.choice(coo.size(0),n_points)
        fea = fea[idx].transpose(1, 0)
        coo = coo[idx].transpose(1, 0).float() * voxel_size  # transform back

        coords.append(coo)
        feats.append(fea)

    coords = torch.stack(coords, dim=0).cuda()
    feats = torch.stack(feats, dim=0).cuda()

    return coords, feats

def transform_sparse_tensor(pcds,lens,rot,trans):
    """
    Apply transformation to the point clouds
    Input:
        pcds:   [N, 4], the first column is batch index
        lens:   list of size B
        rot:    [B,3,3]
        trans:  [B,3,1]
    """
    batch_size=len(lens)
    c_ptr=0
    for i in range(batch_size):
        c_pcd=pcds[c_ptr:c_ptr+lens[i],1:] # Nx3
        c_rot,c_trans=rot[i],trans[i]

        # update the point clouds
        new_pcd=torch.matmul(c_rot,c_pcd.T)+c_trans
        pcds[c_ptr:c_ptr+lens[i],1:]=new_pcd.T
        c_ptr+=lens[i]
    return pcds

def transform_pcd(pcd, rotation, translation):
    """
    Apply transformation to pcd
    Input: 
        pcd: [B, 3, N]
        rotation: Eu angel [B, 3, 3]    
        translation: Translation [B, 3, 1]
    return:
        pcd: [B,3,N]
    """
    return torch.matmul(rotation, pcd) + translation


def get_angle_deviation(R_pred,R_gt):
    """
    Calculate the angle deviation between two rotaion matrice
    The rotation error is between [0,180]
    Input:
        R_pred: [B,3,3]
        R_gt  : [B,3,3]
    Return: 
        degs:   [B]path=f'/scratch/shengyu/masterthesis/dataset/3DMatch/test/overlap_{i}.npz'
    """
    R=np.matmul(R_pred,R_gt.transpose(0,2,1))
    tr=np.trace(R,0,1,2) 
    rads=np.arccos(np.clip((tr-1)/2,-1,1))  # clip to valid range
    degs=rads/np.pi*180

    return degs

def quat2mat(quat):
    """
    convert quaternion to rotation matrix ([x, y, z, w] to follow scipy
    :param quat: four quaternion of rotation
    :return: rotation matrix [B, 3, 3]
    """
    x, y, z, w = quat[:, 0], quat[:, 1], quat[:, 2], quat[:, 3]

    B = quat.size(0)

    w2, x2, y2, z2 = w.pow(2), x.pow(2), y.pow(2), z.pow(2)
    wx, wy, wz = w*x, w*y, w*z
    xy, xz, yz = x*y, x*z, y*z

    rotMat = torch.stack([w2 + x2 - y2 - z2, 2*xy - 2*wz, 2*wy + 2*xz,
                          2*wz + 2*xy, w2 - x2 + y2 - z2, 2*yz - 2*wx,
                          2*xz - 2*wy, 2*wx + 2*yz, w2 - x2 - y2 + z2], dim=1).reshape(B, 3, 3)
    return rotMat



def freeze_parameter(model):
    """
    Freeze the pre-trained model
    """
    for param in model.parameters():
        param.requires_grad = False


def validate_gradient(model):
    """
    Confirm all the gradients are non-nan
    """
    for name, param in model.named_parameters():
        if param.grad is not None:
            if torch.any(torch.isnan(param.grad)):
                return False
    return True

def get_geometric_errors(rotations_gt,rotations_est,translations_gt,translations_est,rot_threshold=10,trans_threshold=0.3):
    """
    Get rotaion, translation erros and registration recall

    Input:
        rotations_gt:       list [B,3,3]
        translations_gt:    list [B,3,1]
        
    Return:
        error_dict
        r_mean:     float
        r_median:   float
        r_rmse:     float
        r_medse:    float
        precision:  float
    """
    rotations_gt = np.concatenate(rotations_gt, axis=0)
    translations_gt = np.concatenate(translations_gt, axis=0).squeeze()
    rotations_est = np.concatenate(rotations_est, axis=0)
    translations_est = np.concatenate(translations_est, axis=0).squeeze()

    r_deviation = get_angle_deviation(rotations_est, rotations_gt)
    translation_errors = np.linalg.norm(translations_est-translations_gt,axis=-1)

    flag_1=r_deviation<rot_threshold
    flag_2=translation_errors<trans_threshold
    correct=(flag_1 & flag_2).sum()
    precision=correct/rotations_gt.shape[0]

    errors=dict()
    errors['rot_mean']=round(np.mean(r_deviation),3)
    errors['rot_median']=round(np.median(r_deviation),3)
    errors['trans_rmse'] = round(np.sqrt(np.mean((translations_gt-translations_est)**2)),3)
    errors['trans_rmedse']=round(np.sqrt(np.median((translations_gt-translations_est)**2)),3)
    errors['precision']=round(precision,3)

    return errors


def natural_key(string_):
    """
    Sort strings by numbers in the name
    """
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]


class Logger:
    def __init__(self, args):
        self.path = args['exp_name']
        self.fw = open(self.path+'/log', 'a')
        yaml.dump(args,open(os.path.join(self.path, 'config.yaml'), 'w'))

    def write(self, text):
        self.fw.write(text)
        self.fw.flush()

    def close(self):
        self.fw.close()

def images_to_gif(folder,path):
    """
    Convert all the images to a gif file
    Input:
        folder: folder contains all the images  e.g., /path/*.jpg
        path:   destination to save the created gif
    """
    import imageio
    images = []
    filenames=sorted(glob.glob(folder))
    for filename in filenames:
        images.append(imageio.imread(filename))
    imageio.mimsave(path, images)

