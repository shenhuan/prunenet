# End-to-end point cloud registration

## Usage
All three models use FCGF features(voxel_size=2.5cm) as front-end

For PRNet, run
```shell
python main.py ./configs/prnet.yaml
```
For RPM-Net, run
```shell
python main.py ./configs/rpmnet.yaml
```
For SuperGlue, run
```shell
python main.py ./configs/superglue.yaml
```
For LTFGC, run
```shell
python main.py ./configs/ltfgc.yaml
```
For Hierarchical Point Cloud Registeration, run
```shell
python main.py ./configs/hpr.yaml
```

## Performance:
Here we pre-compute the FCGF features for each point cloud with voxel_size=2.5cm, then we randomly sample fixed number of points and associated features online. Data augmentation including randomly rotate the point clouds.

### OANet 
Performance for Zan
```shell
04/16 21:53:49 Scene    ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦
04/16 21:53:49 Kitchen  ¦ 0.969 ¦ 0.969 ¦ 1.691 ¦ 0.041 ¦
04/16 21:53:49 Home 1   ¦ 0.972 ¦ 0.972 ¦ 1.546 ¦ 0.048 ¦
04/16 21:53:49 Home 2   ¦ 0.723 ¦ 0.723 ¦ 2.254 ¦ 0.069 ¦
04/16 21:53:49 Hotel 1  ¦ 0.956 ¦ 0.956 ¦ 1.551 ¦ 0.053 ¦
04/16 21:53:49 Hotel 2  ¦ 0.910 ¦ 0.910 ¦ 1.548 ¦ 0.056 ¦
04/16 21:53:49 Hotel 3  ¦ 0.846 ¦ 0.846 ¦ 1.589 ¦ 0.041 ¦
04/16 21:53:50 Study    ¦ 0.868 ¦ 0.868 ¦ 1.900 ¦ 0.071 ¦
04/16 21:53:50 MIT Lab  ¦ 0.867 ¦ 0.867 ¦ 1.555 ¦ 0.067 ¦
Mean precision 0.889 +- 0.078
Mean precision 0.889 +- 0.078
```

### LTFGC
- Architecture: FCGF + Argamx sampler + LTFGC + binary classification loss
- Parameters: 0.075/0.05 [m] as inlier threshold, 5000/2048 points per pair, batch_size=8/12, trained for 700k/40k iterations
- Zan's training code
```shell
Scene   ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦
Kitchen ¦ 0.911 ¦ 0.911 ¦ 1.754 ¦ 0.043 ¦
Home 1  ¦ 0.915 ¦ 0.915 ¦ 1.436 ¦ 0.049 ¦
Home 2  ¦ 0.616 ¦ 0.616 ¦ 2.517 ¦ 0.067 ¦
Hotel 1 ¦ 0.912 ¦ 0.912 ¦ 1.575 ¦ 0.055 ¦
Hotel 2 ¦ 0.846 ¦ 0.846 ¦ 1.615 ¦ 0.062 ¦
Hotel 3 ¦ 0.885 ¦ 0.885 ¦ 1.726 ¦ 0.042 ¦
Study   ¦ 0.731 ¦ 0.731 ¦ 2.377 ¦ 0.086 ¦
MIT Lab ¦ 0.711 ¦ 0.711 ¦ 2.043 ¦ 0.062 ¦
Mean precision 0.816 +- 0.107
Mean recall 0.816 +- 0.107
```

- Shengyu's training code
![](assets/ltfgc_weights.gif)

```shell
Scene   ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦
Kitchen ¦ 0.898 ¦ 0.898 ¦ 1.858 ¦ 0.048 ¦
Home 1  ¦ 0.896 ¦ 0.896 ¦ 1.654 ¦ 0.052 ¦
Home 2  ¦ 0.686 ¦ 0.686 ¦ 2.416 ¦ 0.070 ¦
Hotel 1 ¦ 0.890 ¦ 0.890 ¦ 1.776 ¦ 0.058 ¦
Hotel 2 ¦ 0.859 ¦ 0.859 ¦ 1.911 ¦ 0.060 ¦
Hotel 3 ¦ 0.769 ¦ 0.769 ¦ 1.874 ¦ 0.044 ¦
Study   ¦ 0.701 ¦ 0.701 ¦ 2.558 ¦ 0.086 ¦
MIT Lab ¦ 0.689 ¦ 0.689 ¦ 2.397 ¦ 0.075 ¦
Mean precision 0.798 +- 0.091
Mean recall 0.798 +- 0.091
```

- Simple Trick, 2048 points, 10 clusters, 512 points each cluster, take the pose estimation with most confidence prediction
```shell
Scene	¦ prec.	¦ rec.	¦ re	¦ te	¦ samples	¦
Kitchen	¦ 0.935	¦ 0.935	¦ 2.318	¦ 0.064	¦ 449¦
Home 1	¦ 0.896	¦ 0.896	¦ 1.864	¦ 0.064	¦ 106¦
Home 2	¦ 0.717	¦ 0.717	¦ 3.365	¦ 0.114	¦ 159¦
Hotel 1	¦ 0.934	¦ 0.934	¦ 2.001	¦ 0.070	¦ 182¦
Hotel 2	¦ 0.846	¦ 0.846	¦ 2.190	¦ 0.073	¦  78¦
Hotel 3	¦ 0.846	¦ 0.846	¦ 2.016	¦ 0.043	¦  26¦
Study	¦ 0.842	¦ 0.842	¦ 2.504	¦ 0.098	¦ 234¦
MIT Lab	¦ 0.644	¦ 0.644	¦ 3.052	¦ 0.136	¦  45¦
Mean precision 0.833 +- 0.096
Mean recall 0.833 +- 0.096
```

### SuperGlue
- FCGF + positional encoding + self & cross attention + Sinkhorn + negative log-likelihood loss
- Parameters: 0.075/0.1 [m] as inlier/outlier ratio, 0.4 as matching score threshold, 1024 points per pair, batch_size=8, n_sinkhorn_iters=50 
```shell
Scene   ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦ samples ¦
Kitchen ¦ 0.679 ¦ 0.679 ¦ 3.948 ¦ 0.104 ¦ 449¦
Home 1  ¦ 0.689 ¦ 0.689 ¦ 2.540 ¦ 0.085 ¦ 106¦
Home 2  ¦ 0.447 ¦ 0.447 ¦ 8.334 ¦ 0.207 ¦ 159¦
Hotel 1 ¦ 0.687 ¦ 0.687 ¦ 3.983 ¦ 0.117 ¦ 182¦
Hotel 2 ¦ 0.654 ¦ 0.654 ¦ 3.175 ¦ 0.120 ¦  78¦
Hotel 3 ¦ 0.577 ¦ 0.577 ¦ 3.248 ¦ 0.096 ¦  26¦
Study   ¦ 0.577 ¦ 0.577 ¦ 5.149 ¦ 0.162 ¦ 234¦
MIT Lab ¦ 0.533 ¦ 0.533 ¦ 3.500 ¦ 0.116 ¦  45¦
Mean precision 0.605 +- 0.082
Mean recall 0.605 +- 0.082
```

### RPMNet
- Architecture: FCGF + RPM + Sinkhorn + svd loss + permutation matrix loss($\lambda=0.01$)
- Parameters: 2048 points per pair, batch_size=12, n_sinkhorn_iters=15 
- Note: When adding the Transformer, it decreases the transformation loss, but the evaluation results degrade
- Without Transformer:
```shell
Scene   ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦
Kitchen ¦ 0.650 ¦ 0.650 ¦ 4.597 ¦ 0.120 ¦
Home 1  ¦ 0.623 ¦ 0.623 ¦ 3.783 ¦ 0.112 ¦
Home 2  ¦ 0.547 ¦ 0.547 ¦ 5.232 ¦ 0.170 ¦
Hotel 1 ¦ 0.654 ¦ 0.654 ¦ 5.184 ¦ 0.134 ¦
Hotel 2 ¦ 0.590 ¦ 0.590 ¦ 5.251 ¦ 0.151 ¦
Hotel 3 ¦ 0.500 ¦ 0.500 ¦ 5.815 ¦ 0.126 ¦
Study   ¦ 0.321 ¦ 0.321 ¦ 14.505¦ 0.430 ¦
MIT Lab ¦ 0.511 ¦ 0.511 ¦ 5.193 ¦ 0.200 ¦
Mean precision 0.549 +- 0.103
Mean recall 0.549 +- 0.103
```
- With Transformer:
```shell
Scene   ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦
Kitchen ¦ 0.325 ¦ 0.325 ¦ 14.741¦ 0.423 ¦
Home 1  ¦ 0.575 ¦ 0.575 ¦ 8.382 ¦ 0.221 ¦
Home 2  ¦ 0.428 ¦ 0.428 ¦ 13.871¦ 0.348 ¦
Hotel 1 ¦ 0.462 ¦ 0.462 ¦ 9.922 ¦ 0.274 ¦
Hotel 2 ¦ 0.423 ¦ 0.423 ¦ 10.037¦ 0.292 ¦
Hotel 3 ¦ 0.385 ¦ 0.385 ¦ 12.788¦ 0.298 ¦
Study   ¦ 0.197 ¦ 0.197 ¦ 17.987¦ 0.621 ¦
MIT Lab ¦ 0.289 ¦ 0.289 ¦ 13.524¦ 0.545 ¦
Mean precision 0.385 +- 0.108
Mean recall 0.385 +- 0.108
```

### TEASER_FCGF
- 5000 pairs, FCGF features, mutual selection
```shell
Scene   ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦ samples ¦
Kitchen ¦ 0.962 ¦ 0.962 ¦ 1.676 ¦ 0.042 ¦ 449¦
Home 1  ¦ 0.925 ¦ 0.925 ¦ 1.484 ¦ 0.048 ¦ 106¦
Home 2  ¦ 0.736 ¦ 0.736 ¦ 2.214 ¦ 0.067 ¦ 159¦
Hotel 1 ¦ 0.940 ¦ 0.940 ¦ 1.597 ¦ 0.051 ¦ 182¦
Hotel 2 ¦ 0.859 ¦ 0.859 ¦ 1.568 ¦ 0.054 ¦  78¦
Hotel 3 ¦ 0.808 ¦ 0.808 ¦ 1.561 ¦ 0.038 ¦  26¦
Study   ¦ 0.855 ¦ 0.855 ¦ 1.975 ¦ 0.076 ¦ 234¦
MIT Lab ¦ 0.711 ¦ 0.711 ¦ 1.677 ¦ 0.067 ¦  45¦
Mean precision 0.849 +- 0.087
Mean recall 0.849 +- 0.087
```

### RANSAC_FCGF
- 5000 pairs, FCGF features, mutual selection
```
Scene   ¦ prec. ¦ rec.  ¦ re    ¦ te    ¦ samples       ¦
Kitchen ¦ 0.953 ¦ 0.953 ¦ 1.972 ¦ 0.047 ¦ 449¦
Home 1  ¦ 0.934 ¦ 0.934 ¦ 1.861 ¦ 0.054 ¦ 106¦
Home 2  ¦ 0.742 ¦ 0.742 ¦ 2.428 ¦ 0.076 ¦ 159¦
Hotel 1 ¦ 0.962 ¦ 0.962 ¦ 1.759 ¦ 0.056 ¦ 182¦
Hotel 2 ¦ 0.872 ¦ 0.872 ¦ 1.891 ¦ 0.070 ¦  78¦
Hotel 3 ¦ 0.808 ¦ 0.808 ¦ 1.765 ¦ 0.042 ¦  26¦
Study   ¦ 0.889 ¦ 0.889 ¦ 2.361 ¦ 0.086 ¦ 234¦
MIT Lab ¦ 0.711 ¦ 0.711 ¦ 1.999 ¦ 0.067 ¦  45¦
Mean precision 0.859 +- 0.090
Mean recall 0.859 +- 0.090
```