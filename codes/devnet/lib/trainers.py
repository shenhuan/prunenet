import torch
import numpy as np
import gc
import torch.nn.functional as F
from lib.util import get_angle_deviation, Logger,transform_pcd,get_geometric_errors,sparse_tensor_to_normal_tensor,validate_gradient
from tqdm import tqdm
from torch.optim.lr_scheduler import MultiStepLR
import torch.optim as optim

from tensorboardX import SummaryWriter
from lib.loss import LTFGCLoss, RPMNetLoss,SuperGlueLoss,MixNetLoss,PRNetLoss,HPRLoss,RegNetLoss,OANetLoss
from lib.base_trainer import BaseTrainer

#########################################################################################
#########################################################################################
class OANetTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = OANetLoss(args)

    def _inference_one_batch(self, data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:  B, C, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_ab_est:     B x 3 x 3
            trans_ab_est:   B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        src_embedding,tgt_embedding = data['src_embedding'].to(self.device),data['tgt_embedding'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        batch_size=src.size(0)

        ######################################################################
        # estimate current rotation and translation
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad() # zero_gradient the model

            # make predictions
            res_rot,res_trans,res_logits,res_weights,source,source_ref = self.model.forward(src,tgt,src_embedding,tgt_embedding)

            # get current loss
            c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,source,source_ref,rot_gt,trans_gt)

            c_loss['total_loss'].backward()
            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')

        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions
                res_rot,res_trans,res_logits,res_weights,source,source_ref= self.model.forward(src,tgt,src_embedding,tgt_embedding)
                # get current loss
                c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,source,source_ref,rot_gt,trans_gt)
            
        ####################################################################################
        # update stast to the tensorboard
        # self._add_scalers(c_iter, c_loss, phase)
        for key,value in detailed_loss[0].items():
            self.writer.add_scalar('%s/init_%s' % (phase,key), value, c_iter)
        for key,value in detailed_loss[1].items():
            self.writer.add_scalar('%s/iter_%s' % (phase,key), value, c_iter)

        self._add_scalers(c_iter,c_stats,phase) # track inlier_ratio / precision /recall
        for key,value in c_loss.items():
            c_loss[key]=value.item()
        
        return c_loss,res_rot[-1][:batch_size].detach(),res_trans[-1][:batch_size].detach()


#########################################################################################
#########################################################################################
class LTFGCTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = LTFGCLoss(args)

    def _inference_one_batch(self, data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:  B, C, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_ab_est:     B x 3 x 3
            trans_ab_est:   B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        src_embedding,tgt_embedding = data['src_embedding'].to(self.device),data['tgt_embedding'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        batch_size=src.size(0)

        ######################################################################
        # estimate current rotation and translation
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad() # zero_gradient the model

            # make predictions
            res_rot,res_trans,res_logits,res_weights,source,source_ref = self.model.forward(src,tgt,src_embedding,tgt_embedding)

            # get current loss
            c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,source,source_ref,rot_gt,trans_gt)

            c_loss['total_loss'].backward()
            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')

        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions
                res_rot,res_trans,res_logits,res_weights,source,source_ref= self.model.forward(src,tgt,src_embedding,tgt_embedding)
                # get current loss
                c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,source,source_ref,rot_gt,trans_gt)
            
        ####################################################################################
        # update stast to the tensorboard
        # self._add_scalers(c_iter, c_loss, phase)
        for key,value in detailed_loss[0].items():
            self.writer.add_scalar('%s/init_%s' % (phase,key), value, c_iter)
        for key,value in detailed_loss[1].items():
            self.writer.add_scalar('%s/iter_%s' % (phase,key), value, c_iter)

        self._add_scalers(c_iter,c_stats,phase) # track inlier_ratio / precision /recall
        for key,value in c_loss.items():
            c_loss[key]=value.item()
        
        return c_loss,res_rot[-1][:batch_size].detach(),res_trans[-1][:batch_size].detach()

#########################################################################################
#########################################################################################
class PRNetTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = PRNetLoss(args)

    def _inference_one_batch(self, data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:  B, C, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_ab_est:     B x 3 x 3
            trans_ab_est:   B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        src_embedding,tgt_embedding = data['src_embedding'].to(self.device),data['tgt_embedding'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        batch_size=src.size(0)

        ######################################################################
        # estimate current rotation and translation
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad() # zero_gradient the model

            # make predictions
            res_rot,res_trans,res_logits,res_weights,source,source_ref,feature_disparity,temperature = self.model.forward(src,tgt,src_embedding,tgt_embedding)

            # get current loss
            c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,source,source_ref,feature_disparity,rot_gt,trans_gt)

            c_loss['total_loss'].backward()
            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')

        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions
                res_rot,res_trans,res_logits,res_weights,source,source_ref,feature_disparity,temperature = self.model.forward(src,tgt,src_embedding,tgt_embedding)

                # get current loss
                c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,source,source_ref,feature_disparity,rot_gt,trans_gt)
            
        # ####################################################################################
        # update stast to the tensorboard
        # self._add_scalers(c_iter, c_loss, phase)
        for key,value in detailed_loss[0].items():
            self.writer.add_scalar('%s/init_%s' % (phase,key), value, c_iter)
        for key,value in detailed_loss[1].items():
            self.writer.add_scalar('%s/iter_%s' % (phase,key), value, c_iter)

        self._add_scalers(c_iter,c_stats,phase) # track inlier_ratio / precision /recall
        # self.writer.add_scalar('%s/temperature' % phase, torch.mean(temperature), c_iter)
        for key,value in c_loss.items():
            c_loss[key]=value.item()
        

        return c_loss,res_rot[-1][:batch_size].detach(),res_trans[-1][:batch_size].detach()
   


#########################################################################################
#########################################################################################
class SuperGlueTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = SuperGlueLoss(args)

    def _inference_one_batch(self,data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:  B, C, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_est:     B x 3 x 3
            trans_est:   B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        src_embedding,tgt_embedding = data['src_embedding'].to(self.device),data['tgt_embedding'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        ###################################
        # estimate current rotation and translation
        ###################################
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad()

            # make predictions
            rot_est,trans_est,log_perm_mat,bin_score= self.model.forward(src,tgt,src_embedding,tgt_embedding)

            # get current loss
            c_loss = self.loss_evaluator.evaluate(rot_est,trans_est,rot_gt,trans_gt,log_perm_mat,data['perm_mat'].to(self.device))

            c_loss['total_loss'].backward()
            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')
            
        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions
                rot_est,trans_est,log_perm_mat,bin_score= self.model.forward(src,tgt,src_embedding,tgt_embedding)

                # get current loss
                c_loss = self.loss_evaluator.evaluate(rot_est,trans_est,rot_gt,trans_gt,log_perm_mat,data['perm_mat'].to(self.device))


            
        ###########################################
        # update loss within the batch
        ###########################################
        for key,value in c_loss.items():
            c_loss[key]=value.item()
        
        self.writer.add_scalar('%s/bin_score' % phase, bin_score.item(), c_iter)

        return c_loss,rot_est.detach(),trans_est.detach()


#########################################################################################
#########################################################################################
class RPMNetTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = RPMNetLoss(args)

    def _inference_one_batch(self,data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:   B, c, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_ab_est:     B x 3 x 3
            trans_ab_est:   B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        src_embedding,tgt_embedding = data['src_embedding'].to(self.device),data['tgt_embedding'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        ################################################################
        #### initialise the rotation and translation
        ################################################################
        local_rot_gt = rot_gt
        local_trans_gt = trans_gt
        batch_loss = self.loss_evaluator.init_loss_terms()
        batch_size = rot_gt.size(0)
        rot_est = torch.eye(3, device=self.device, dtype=torch.float32).unsqueeze(0).repeat(batch_size, 1, 1)
        trans_est = torch.zeros((batch_size,3,1), device=self.device, dtype=torch.float32)

        ################################################################
        #### iterative refinement
        ################################################################
        alphas,betas=0.,0.
        for i_iter in range(self.num_iters):
            c_discount_factor = self.discount_factor**i_iter  # discount factor for the loss term

            ###################################
            # transform the src first
            ###################################
            c_src = transform_pcd(src,rot_est,trans_est)

            ###################################
            # estimate current rotation and translation
            ###################################
            if(phase == 'train'):
                self.model.train()
                self.opt.zero_grad()

                # make predictions
                c_log_perm_matrix,rot_ab_est,trans_ab_est,alpha,beta = self.model.forward(c_src.transpose(1,2),tgt.transpose(1,2),src_embedding.transpose(1,2),tgt_embedding.transpose(1,2))

                # get current loss
                c_loss = self.loss_evaluator.evaluate(c_src,c_log_perm_matrix,data['perm_mat'].to(self.device),local_rot_gt,rot_ab_est,local_trans_gt,trans_ab_est,c_discount_factor)
                c_loss['total_loss'].backward()

                gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
                if(gradient_valid):
                    self.opt.step()
                else:
                    print('Skip this iteration')

            elif(phase in ['val','test']):
                self.model.eval()
                with torch.no_grad():
                    # make predictions
                    c_log_perm_matrix,rot_ab_est,trans_ab_est,alpha,beta= self.model.forward(c_src.transpose(1,2),tgt.transpose(1,2),src_embedding.transpose(1,2),tgt_embedding.transpose(1,2))

                    # get current loss
                    c_loss = self.loss_evaluator.evaluate(c_src,c_log_perm_matrix,data['perm_mat'].to(self.device),local_rot_gt,rot_ab_est,local_trans_gt,trans_ab_est,c_discount_factor)

            
            ###########################################
            # update estimation and local ground truth
            # formulas according to PRNet ,Eq. 5 & Eq. 6
            ###########################################
            c_rot_est=rot_ab_est.detach()
            c_trans_est=trans_ab_est.detach()
            rot_est = torch.matmul(c_rot_est, rot_est)
            trans_est = torch.matmul(c_rot_est, trans_est) + c_trans_est

            local_rot_gt = torch.matmul(rot_gt,rot_est.transpose(1,2))
            local_trans_gt = trans_gt - torch.matmul(local_rot_gt, trans_est)

            ###########################################
            # update loss within the batch
            ###########################################
            self.writer.add_scalar('%s/trans_loss_%d' % (phase,i_iter),c_loss['trans_loss'],c_iter)
            self.writer.add_scalar('%s/perm_loss_%d' % (phase,i_iter),c_loss['perm_loss'],c_iter)
            alphas += alpha
            betas += beta

            for key,value in c_loss.items():
                batch_loss[key]+=value.item()

        # get rotation and translation deviation
        r_deviation=get_angle_deviation(rot_est.cpu().numpy(),rot_gt.cpu().numpy())
        r_mean = np.mean(r_deviation)
        r_median=np.median(r_deviation)
        t_deviation=(trans_gt.cpu().numpy()-trans_est.cpu().numpy())**2
        t_rmse = np.sqrt(np.mean(t_deviation))
        t_rmedian=np.sqrt(np.median(t_deviation))

        # print('R_error: %.2f / %.2f;  T_error: %.2f / %.2f' % (r_mean,r_median,t_rmse,t_rmedian))

        self.writer.add_scalar('%s/r_median' % phase, r_median, c_iter)
        self.writer.add_scalar('%s/t_median' % phase, t_rmedian, c_iter)
        self.writer.add_scalar('%s/r_mean' % phase, r_mean, c_iter)
        self.writer.add_scalar('%s/t_rmse' % phase, t_rmse, c_iter)
        self.writer.add_scalar('%s/alpha' % phase,alphas/self.num_iters,c_iter)
        self.writer.add_scalar('%s/beta' % phase,betas/self.num_iters,c_iter)   
        

        return batch_loss,rot_est,trans_est


#########################################################################################
#########################################################################################
class MixNetTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = MixNetLoss(args)

    def _inference_one_batch(self,data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:  B, C, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_est:        B x 3 x 3
            trans_est:      B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        src_embedding,tgt_embedding = data['src_embedding'].to(self.device),data['tgt_embedding'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        ###################################
        # estimate current rotation and translation
        ###################################
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad()

            # make predictions
            res_rot,res_trans,res_logits,res_weights,score,correspondence= self.model.forward(src,tgt,src_embedding,tgt_embedding)

            # get current loss
            c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,rot_gt,trans_gt,data['inlier_mat'].to(self.device),score,correspondence)

            c_loss['total_loss'].backward()
            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')
            
        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions
                res_rot,res_trans,res_logits,res_weights,score,correspondence= self.model.forward(src,tgt,src_embedding,tgt_embedding)

                # get current loss
                c_loss,c_stats,detailed_loss = self.loss_evaluator.evaluate(res_rot,res_trans,res_logits,res_weights,rot_gt,trans_gt,data['inlier_mat'].to(self.device),score,correspondence)
        
        ###########################################
        # update loss within the batch
        ###########################################
        for key,value in c_loss.items():
            c_loss[key]=value.item()
        
        ####################################################################################
        # update stast to the tensorboard
        for key,value in detailed_loss[0].items():
            self.writer.add_scalar('%s/init_%s' % (phase,key), value, c_iter)
        for key,value in detailed_loss[1].items():
            self.writer.add_scalar('%s/iter_%s' % (phase,key), value, c_iter)
        self._add_scalers(c_iter,c_stats,phase) # track inlier_ratio / precision /recall

        return c_loss,res_rot[-1].detach(),res_trans[-1].detach()


#########################################################################################
#########################################################################################
class HPRTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = HPRLoss(args)

    def _inference_one_batch(self, data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:  B, C, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_ab_est:     B x 3 x 3
            trans_ab_est:   B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        ######################################################################
        # estimate current rotation and translation
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad() # zero_gradient the model

            # make predictions
            confidences, fps_src_pcd_idxs,rots,trans=self.model.forward(src,tgt,rot_gt,trans_gt)
            c_loss,motion_stats = self.loss_evaluator.evaluate(data['flag_src'].to(self.device),confidences,fps_src_pcd_idxs,rot_gt,trans_gt,rots,trans)

            # torch.save(src,'dump/hpr/src.pth')
            # torch.save(ref,'dump/hpr/ref.pth')
            # torch.save(data,'dump/hpr/data.pth')
            # raise Exception('shit happens')

            c_loss['total_loss'].backward()
            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')

        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions
                confidences, fps_src_pcd_idxs,rots,trans=self.model.forward(src,tgt,rot_gt,trans_gt)
                c_loss,motion_stats = self.loss_evaluator.evaluate(data['flag_src'].to(self.device),confidences,fps_src_pcd_idxs,rot_gt,trans_gt,rots,trans)
            
        ####################################################################################
        for key,value in c_loss.items():
            c_loss[key]=value.item()

        self._add_scalers(c_iter,motion_stats,phase)
        return c_loss,rots[0].detach(),trans[0].detach()


#########################################################################################
#########################################################################################
class RegNetTrainer(BaseTrainer):
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        BaseTrainer.__init__(self, args, net, train_loader, val_loader, test_loader)
        self.loss_evaluator = RegNetLoss(args)

    def _inference_one_batch(self, data, phase,c_iter):
        '''
        Iteratively inference one batch
        Input: 
            src_pcd:        B, 3, N
            src_embedding:  B, C, N
            rot_gt:         B x 3 x 3       rotation from src to tgt
            trans_gt:       B x 3 x 1       translation from src to tgt
        return:
            c_loss:         dictionary
            rot_ab_est:     B x 3 x 3
            trans_ab_est:   B x 3 x 1
        '''
        src,tgt=data['src_pcd'].to(self.device),data['tgt_pcd'].to(self.device)
        rot_gt,trans_gt = data['rot'].to(self.device),data['trans'].to(self.device)
        ######################################################################
        # estimate current rotation and translation
        if(phase == 'train'):
            self.model.train()
            self.opt.zero_grad() # zero_gradient the model

            # make predictions
            overlaps = self.model.forward(src,tgt)

            # get loss
            c_loss=self.loss_evaluator.evaluate(overlaps,data['overlap'].to(self.device))


            gradient_valid = validate_gradient(self.model) # check whether or not the gradient is valid
            if(gradient_valid):
                self.opt.step()
            else:
                print('Skip this iteration')

        elif(phase in ['val','test']):
            self.model.eval()
            with torch.no_grad():
                # make predictions
                overlaps = self.model.forward(src,tgt)

                # get loss
                c_loss=self.loss_evaluator.evaluate(overlaps,data['overlap'].to(self.device))
            
        ##################################################################################
        for key,value in c_loss.items():
            c_loss[key]=value.item()
        
        print(c_loss)
        return c_loss,rot_gt,trans_gt

def get_trainer(args, net, train_loader, val_loader, test_loader):
    if(args['method']=='ltfgc'):
        return LTFGCTrainer(args, net, train_loader, val_loader, test_loader)
    elif(args['method']=='oanet'):
        return OANetTrainer(args,net,train_loader,val_loader,test_loader)
    elif(args['method']=='rpmnet'):
        return RPMNetTrainer(args, net, train_loader, val_loader, test_loader)
    elif(args['method']=='superglue'):
        return SuperGlueTrainer(args, net, train_loader, val_loader, test_loader)
    elif(args['method']=='mixnet'):
        return MixNetTrainer(args, net, train_loader, val_loader, test_loader)
    elif(args['method']=='prnet'):
        return PRNetTrainer(args,net,train_loader,val_loader,test_loader)
    elif(args['method']=='hpr'):
        return HPRTrainer(args,net,train_loader,val_loader,test_loader)
    elif(args['method']=='overlapreg'):
        return RegNetTrainer(args,net,train_loader,val_loader,test_loader)
    raise NotImplementedError