#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob
import h5py
import numpy as np
import torch
from torch.utils.data import DataLoader
from lib.dataset import Dataset3DMatchFCGF,Dataset3DMatchRaw,Dataset3DMatchSimulatedFix,Dataset3DMatchSimulatedSparse

def split_simulated(base_dir):
    '''
    use 70% for training, 30% for testing
    '''
    files=sorted(glob.glob(base_dir))
    n_samples=len(files)
    n_train=int(n_samples*0.7)
    permute=np.random.permutation(n_samples)
    train_samples=np.array(files)[permute[:n_train]].tolist()
    test_samples=np.array(files)[permute[n_train:]].tolist()
    return train_samples,test_samples


def split_real_dataset(infos):
    '''
    use 70% for training, 30% for testing
    '''
    n_samples=infos['rot'].shape[0]
    n_train=int(n_samples*0.8)
    permute=np.random.permutation(n_samples)
    info_train={
        'src':infos['src'][permute[:n_train]],
        'tgt':infos['tgt'][permute[:n_train]],
        'rot':infos['rot'][permute[:n_train]],
        'trans':infos['trans'][permute[:n_train]],
        'overlap':infos['overlap'][permute[:n_train]]
    }
    info_val={
        'src':infos['src'][permute[n_train:]],
        'tgt':infos['tgt'][permute[n_train:]],
        'rot':infos['rot'][permute[n_train:]],
        'trans':infos['trans'][permute[n_train:]],
        'overlap':infos['overlap'][permute[n_train:]]
    }

    return info_train,info_val


def collate_pcd(pcds):
    '''
    collate point clouds as ME required
    '''
    lens=[len(ele) for ele in pcds]
    num_tot_pts=sum(lens)
    coords=torch.zeros(num_tot_pts,4,dtype=torch.int32)
    curr_ptr=0

    for batch_id in range(len(pcds)):
        coords[curr_ptr:curr_ptr+lens[batch_id],1:]=\
            torch.from_numpy(pcds[batch_id].astype(np.int32))
        coords[curr_ptr:curr_ptr+lens[batch_id],0]=batch_id
        curr_ptr+=lens[batch_id]
    return coords

def collate_fn(list_data):
    '''
    return a dictionary, [rot and trans] transform src to tgt
        src: N x 4
        tgt: N x 4
        r_abs: B x 3 x 3  
        t_abs: B x 3 x 1
    '''
    pcd1s,pcd2s,r_abs,t_abs=list(zip(*list_data))
    assert len(pcd1s)==len(pcd2s)
    coords1=collate_pcd(pcd1s)
    coords2=collate_pcd(pcd2s)
    r_abs=torch.from_numpy(np.array(r_abs)).float()
    t_abs=torch.from_numpy(np.array(t_abs)).float()
    if(t_abs.dim()==2):
        t_abs=t_abs.unsqueeze(2)

    data=dict()
    data['src']=coords1
    data['tgt']=coords2
    data['rot']=r_abs
    data['trans']=t_abs

    return data

def collate_fn_raw(list_data):
    src_pcd,tgt_pcd,rot,trans,overlap,flag_src,flag_tgt,inlier_matrix=list(zip(*list_data))
    src_pcd=torch.from_numpy(np.array(src_pcd)).float()
    tgt_pcd=torch.from_numpy(np.array(tgt_pcd)).float()
    rot=torch.from_numpy(np.array(rot)).float()
    trans=torch.from_numpy(np.array(trans)).float()
    overlap=torch.from_numpy(np.array(overlap)).float()
    flag_src=torch.from_numpy(np.array(flag_src)).bool()
    flag_tgt=torch.from_numpy(np.array(flag_tgt)).bool()
    inlier_mat=torch.from_numpy(np.array(inlier_matrix)).bool()
    
    data=dict()
    data['src_pcd']= src_pcd
    data['tgt_pcd']=tgt_pcd
    data['rot']=rot
    data['trans']=trans
    data['overlap']=overlap
    data['flag_src']=flag_src
    data['flag_tgt']=flag_tgt
    data['inlier_mat']=inlier_mat
    
    return data

def collate_fn_fcgf(list_data):
    src_pcd,tgt_pcd,src_embedding,tgt_embedding,rot,trans,perms,overlaps,inlier_matrice=list(zip(*list_data))

    r_abs=torch.from_numpy(np.array(rot)).float()
    t_abs=torch.from_numpy(np.array(trans)).float()
    perms = torch.from_numpy(np.array(perms)).float()
    overlaps=torch.from_numpy(np.array(overlaps)).float()
    inlier_matrice=torch.from_numpy(np.array(inlier_matrice)).float()


    src_pcd = torch.from_numpy(np.array(src_pcd)).float()
    tgt_pcd = torch.from_numpy(np.array(tgt_pcd)).float()
    src_embedding = torch.from_numpy(np.array(src_embedding)).float()
    tgt_embedding = torch.from_numpy(np.array(tgt_embedding)).float()

    if(t_abs.dim()==2):
        t_abs=t_abs.unsqueeze(2)

    data=dict()
    data['src_pcd']= src_pcd
    data['src_embedding'] = src_embedding
    data['tgt_pcd']=tgt_pcd
    data['tgt_embedding'] = tgt_embedding
    data['rot']=r_abs
    data['trans']=t_abs
    data['perm_mat']=perms
    data['overlaps']=overlaps
    data['inlier_mat']=inlier_matrice

    return data


def get_train_val_loaders_sparse(args):
    '''
    get train/val for simulated 3DMatch for FCGF
    '''
    train_files,test_files=split_simulated(args['misc']['data_dir'])

    train_loader = DataLoader(Dataset3DMatchSimulatedSparse(train_files,\
        voxel_size=args['data']['voxel_size'],rot_factor=args['data']['rot_factor']),batch_size=args['train']['batch_size'],\
            shuffle=True, drop_last=True, num_workers=args['misc']['workers'],collate_fn= collate_fn)

    test_loader = DataLoader(Dataset3DMatchSimulatedSparse(test_files,\
        voxel_size=args['data']['voxel_size'],rot_factor=args['data']['rot_factor']),batch_size=args['train']['batch_size'],\
            shuffle=False, drop_last=False, num_workers=args['misc']['workers'],collate_fn=collate_fn)

    return train_loader,test_loader

def get_train_val_loaders_fix(args):
    '''
    get fps samples of simulated 3DMatch
    '''
    train_files,test_files=split_simulated(args['misc']['data_dir'])

    train_loader = DataLoader(Dataset3DMatchSimulatedFix(train_files,args['model']['n_input_points'],args['data']['rot_factor'],True),\
        batch_size=args['train']['batch_size'],\
            shuffle=True, drop_last=True, num_workers=args['misc']['workers'])

    test_loader = DataLoader(Dataset3DMatchSimulatedFix(test_files,args['model']['n_input_points'],args['data']['rot_factor'],False),\
        batch_size=args['train']['batch_size'],\
            shuffle=False, drop_last=False, num_workers=args['misc']['workers'])

    return train_loader,test_loader

def get_train_val_loaders_raw(args):
    '''
    get train/val for real 3DMatch 
    '''
    all_infos=np.load(args['misc']['path_test_info_raw'])
    info_val=dict()
    for key,value in all_infos.items():
        info_val[key]=value

    all_infos=np.load(args['misc']['path_train_info_raw'])
    info_train=dict()
    for key,value in all_infos.items():
        info_train[key]=value

    train_loader=DataLoader(Dataset3DMatchRaw(info_train,args,data_augmentation=True),batch_size=args['train']['batch_size'],shuffle=True, drop_last=True, num_workers=args['misc']['workers'],collate_fn= collate_fn_raw)

    val_loader=DataLoader(Dataset3DMatchRaw(info_val,args,data_augmentation=False),batch_size=args['train']['batch_size'],shuffle=False, drop_last=False, num_workers=args['misc']['workers'],collate_fn= collate_fn_raw)

    return train_loader,val_loader

def get_test_loader_real(args):
    '''
    test loader for 3DMatch for FCGF
    '''
    all_infos=np.load(args['misc']['path_test_info'])
    infos=dict()
    for key,value in all_infos.items():
        infos[key]=value

    test_loader=DataLoader(Dataset3DMatchRaw(infos,args,data_augmentation=False),batch_size=args['train']['batch_size'],shuffle=False, drop_last=False, num_workers=args['misc']['workers'],collate_fn= collate_fn)

    return test_loader



def get_train_val_loaders_fcgf(args):
    '''
    get train/val for FCGF features of 3DMatch 
    '''
    all_infos=np.load(args['misc']['path_test_info'])
    info_val=dict()
    for key,value in all_infos.items():
        info_val[key]=value

    all_infos=np.load(args['misc']['path_train_info'])
    info_train=dict()
    for key,value in all_infos.items():
        info_train[key]=value
    # info_train,info_val=split_real_dataset(all_infos)

    train_loader=DataLoader(Dataset3DMatchFCGF(info_train,args,True),batch_size=args['train']['batch_size'],shuffle=True, drop_last=True, num_workers=args['misc']['workers'],collate_fn= collate_fn_fcgf,pin_memory=args['misc']['pin_memory'])

    val_loader=DataLoader(Dataset3DMatchFCGF(info_val,args,False),batch_size=args['train']['batch_size'],shuffle=False, drop_last=False, num_workers=args['misc']['workers'],collate_fn= collate_fn_fcgf,pin_memory=args['misc']['pin_memory'])

    return train_loader,val_loader

def get_test_loader_fcgf(args):
    all_infos=np.load(args['misc']['path_test_info'])
    infos=dict()
    for key,value in all_infos.items():
        infos[key]=value

    test_loader=DataLoader(Dataset3DMatchFCGF(infos,args,False),batch_size=args['train']['batch_size'],shuffle=False, drop_last=True, num_workers=args['misc']['workers'],collate_fn= collate_fn_fcgf,pin_memory=args['misc']['pin_memory'])

    return test_loader

    

def get_data_loaders(args):
    '''
    general dataloader
    '''
    if(args['mode']=='train'):
        if(args['data']['dataset']=='simulated'):
            if(args['model']['emb_nn']=='FCGF'):
                return get_train_val_loaders_sparse(args)
            elif(args['model']['emb_nn']=='DGCNN'):
                return get_train_val_loaders_fix(args)
        elif(args['data']['dataset']=='real'):
            return get_train_val_loaders_raw(args)
        elif(args['data']['dataset']=='fcgf_feats'):
            return get_train_val_loaders_fcgf(args)
    elif(args['mode'] in ['benchmark','test']):
        if(args['data']['dataset']=='real'):
            return get_test_loader_real(args)
        elif(args['data']['dataset']=='fcgf_feats'):
            return get_test_loader_fcgf(args)