import torch.nn.functional as F
import torch
import torch.nn as nn
from lib.util import transform_pcd
import numpy as np
from lib.base_loss import BaseLoss
from sklearn.metrics import precision_recall_fscore_support

class RegNetLoss(BaseLoss):
    def __init__(self,args):
        pass

    def init_loss_terms(self):
        loss=dict()
        loss['total_loss']=0.
        return loss
    
    def evaluate(self,pred,gt):
        loss=self.init_loss_terms()
        loss['total_loss']=torch.mean((pred-gt)**2)
        return loss

class HPRLoss(BaseLoss):
    def __init__(self,args):
        self.weights=args['model']['weights']

    def init_loss_terms(self):
        total_loss=dict()
        total_loss['total_loss']=0.
        total_loss['conf_loss_0']=0.
        total_loss['conf_loss_1']=0.
        total_loss['conf_loss_2']=0.
        total_loss['conf_loss_3']=0.
        return total_loss
    
    def motion_stats(self):
        stats=dict()
        stats['trans_loss_0']=0.
        stats['trans_loss_1']=0.
        stats['trans_loss_2']=0.
        return stats
    
    def _confidence_loss(self,gt,estimation):
        '''
        Binary Cross Entropy loss for confidence estimation,
        Here we use the weighted mean
        Input:
            gt:         [B,N]
            estimation: [B,N]
        '''
        gt_labels=gt.flatten().float()
        estimation=estimation.flatten().float()
        sigmoid=nn.Sigmoid()
        estimation=sigmoid(estimation) # apply sigmoid to transform to (0,1)

        loss = nn.BCELoss(reduction='none')
        class_loss = loss(estimation,gt_labels) # use logits to calculate the BCE loss

        #######################################
        # get precision and recall
        predicted_labels = estimation.detach().round() # [0,1]
        precision, recall, f_measure, _ = precision_recall_fscore_support(gt_labels.cpu().numpy(),predicted_labels.cpu().numpy(), average='binary')
        
        print('Recall:%.3f,  Precision:%.3f' % (recall,precision))

        #######################################
        # get weighted loss
        weights = torch.ones_like(gt_labels)
        w_negative = gt_labels.sum()/gt_labels.size(0)  # Number of positives/number of all
        w_positive = 1 - w_negative  # Number of negatives/number of all
        
        weights[gt_labels > 0.5] = w_positive
        weights[gt_labels < 0.5] = w_negative
        w_class_loss = torch.mean(weights * class_loss)
        return w_class_loss

    
    def evaluate(self,*input):
        '''
        Here we only calculate the binary cross entropy loss for the source point cloud
        Input:
            src_flag:       [B,N] flag of being within the overlap region
            confidence:     list, each element of shape [B,1,N_i]
            src_index:      list, each element of shape [B,N_i]
            rot_est:        list, [B,3,3]
            trans_est:      list, [B,3,1]
        '''
        src_flag=input[0]
        confidence=input[1]
        src_index=input[2]
        rot_gt,trans_gt=input[3],input[4]
        rot_est,trans_est=input[5],input[6]

        total_loss=self.init_loss_terms()
        loss_0=self._confidence_loss(src_flag,confidence[0].squeeze(1).contiguous())
        total_loss['conf_loss_0']=loss_0

        c_gt=src_flag
        for i in range(len(src_index)):
            c_gt=torch.gather(c_gt,-1,src_index[i].long())
            c_est=confidence[i+1].squeeze(1).contiguous()
            c_loss=self._confidence_loss(c_gt,c_est)
            total_loss['conf_loss_%d' % (i+1)]=c_loss
        
        # total_loss['total_loss']=total_loss['conf_loss_0']
        for key,value in total_loss.items():
            if(key != 'total_loss'):
                total_loss['total_loss'] += value
        
        motion_stats=self.motion_stats()
        # here we only record the transformation loss values(detached)
        for i in range(len(rot_est)):
            rot_loss,trans_loss=self._rigid_motion_loss(rot_est[i],rot_gt,trans_est[i],trans_gt)
            motion_stats['trans_loss_%d' % i]=rot_loss+trans_loss
        return total_loss,motion_stats

class PRNetLoss(BaseLoss):
    '''
    # 1. rigid motion loss
    # 2. feature alignment loss
    # 3. cycle_consistency loss
    # 4. classification loss
    '''

    def __init__(self, args):
        BaseLoss.__init__(self, args)
        self.w_feature_alignment = args['loss']['feature_alignment_loss']
        self.w_cycle_consistency = args['loss']['cycle_consistency_loss']
        self.w_class = args['loss']['class_loss']
        self.w_trans = args['loss']['trans_loss']
        self.inlier_threshold = args['data']['inlier_threshold']
    
    def losses(self):
        total_loss = dict()
        total_loss['rot_loss'] = 0.
        total_loss['trans_loss'] = 0.
        total_loss['class_loss'] = 0.
        return total_loss

    def init_loss_terms(self):
        total_loss = dict()
        total_loss['total_loss'] = 0.
        total_loss['rot_loss'] = 0.
        total_loss['trans_loss'] = 0.
        total_loss['feat_align_loss'] = 0.
        total_loss['cyclic_loss'] = 0.
        total_loss['class_loss'] = 0.
        return total_loss

    def _cycle_consistency(self, rot_ab, trans_ab, rot_ba, trans_ba):
        '''
        A-B and B-A
        '''
        batch_size = rot_ab.size(0)
        identity = torch.eye(3, device=rot_ab.device).unsqueeze(
            0).repeat(batch_size, 1, 1)
        return F.mse_loss(torch.matmul(rot_ab, rot_ba), identity)
        # return F.mse_loss(torch.matmul(rot_ab, rot_ba), identity) + F.mse_loss(trans_ab, -trans_ba)

        
    def evaluate(self, *input):
        '''
        res_rot,res_trans,res_logits,res_weights,source,corr,feature_disparity
        we calculate the cyclic error: A ————> B and B ————> A
        '''
        res_rot = input[0]
        res_trans = input[1]
        res_logits = input[2]
        res_weights = input[3]
        src = input[4] # [2B,3,N]
        src_ref = input[5]  # [2B,3,N]
        feature_disparity = input[6]
        rot_ab_gt = input[7] #   [B,3,3]
        trans_ab_gt = input[8] # [B,3,1]

        # get rot_gt from target to source
        rot_ba_gt = rot_ab_gt.transpose(1, 2)
        trans_ba_gt = -torch.matmul(rot_ba_gt,trans_ab_gt)
        rot_gt = torch.cat((rot_ab_gt,rot_ba_gt),0)
        trans_gt = torch.cat((trans_ab_gt,trans_ba_gt),0)

        # get loss
        loss = self.init_loss_terms()
        batch_size=int(res_rot[0].size(0)/2)
        detailed_loss=dict()
        detailed_loss[0],detailed_loss[1]=self.losses(),self.losses()
        for ii in range(len(res_rot)):
            # rigid motion loss
            rot_loss,trans_loss = self._rigid_motion_loss(res_rot[ii],rot_gt,res_trans[ii],trans_gt)

            # cycle_consistency_loss
            cycle_consistency_loss = self._cycle_consistency(res_rot[ii][:batch_size],res_trans[ii][:batch_size],res_rot[ii][batch_size:],res_trans[ii][batch_size:])
            
            # inlier/outlier classification loss
            class_loss,stats = self._class_loss(src,src_ref,res_logits[ii],res_weights[ii],rot_gt,trans_gt)

            detailed_loss[ii]['rot_loss']=rot_loss.item()
            detailed_loss[ii]['trans_loss']=trans_loss.item()
            detailed_loss[ii]['class_loss']=class_loss.item()

            loss['rot_loss'] += rot_loss * self.w_trans
            loss['trans_loss']+= trans_loss * self.w_trans
            loss['class_loss'] += class_loss * self.w_class
            loss['cyclic_loss'] += cycle_consistency_loss * self.w_cycle_consistency
        
        # features alignment loss
        feature_alignment_loss = feature_disparity.mean()
        loss['feat_align_loss'] += feature_alignment_loss * self.w_feature_alignment

        # get total loss
        for key,value in loss.items():
            if(key != 'total_loss'):
                loss['total_loss'] += value

        return loss,stats,detailed_loss

###############################################################
class LTFGCLoss(BaseLoss):
    '''
    # 1. rigid motion loss
    # 2. feature alignment loss
    # 3. cycle_consistency loss
    # 4. classification loss
    '''

    def __init__(self, args):
        BaseLoss.__init__(self, args)
        self.w_class = args['loss']['class_loss']
        self.w_trans = args['loss']['trans_loss']
        self.inlier_threshold = args['data']['inlier_threshold']
    
    def losses(self):
        total_loss = dict()
        total_loss['rot_loss'] = 0.
        total_loss['trans_loss'] = 0.
        total_loss['class_loss'] = 0.
        return total_loss

    def init_loss_terms(self):
        total_loss = dict()
        total_loss['total_loss'] = 0.
        total_loss['rot_loss'] = 0.
        total_loss['trans_loss'] = 0.
        total_loss['class_loss'] = 0.
        return total_loss

        
    def evaluate(self, *input):
        '''
        res_rot,res_trans,res_logits,res_weights,source,corr,feature_disparity
        we calculate the cyclic error: A ————> B and B ————> A
        '''
        res_rot = input[0]
        res_trans = input[1]
        res_logits = input[2]
        res_weights = input[3]
        src = input[4] # [2B,3,N]
        src_ref = input[5]  # [2B,3,N]
        rot_ab_gt = input[6] #   [B,3,3]
        trans_ab_gt = input[7] # [B,3,1]

        # get rot_gt from target to source
        rot_ba_gt = rot_ab_gt.transpose(1, 2)
        trans_ba_gt = -torch.matmul(rot_ba_gt,trans_ab_gt)
        rot_gt = torch.cat((rot_ab_gt,rot_ba_gt),0)
        trans_gt = torch.cat((trans_ab_gt,trans_ba_gt),0)

        # get loss
        loss = self.init_loss_terms()
        batch_size=int(res_rot[0].size(0)/2)
        detailed_loss=dict()
        detailed_loss[0],detailed_loss[1]=self.losses(),self.losses()
        for ii in range(len(res_rot)):
            # rigid motion loss
            rot_loss,trans_loss = self._rigid_motion_loss(res_rot[ii],rot_gt,res_trans[ii],trans_gt)
            
            # inlier/outlier classification loss
            class_loss,stats = self._class_loss(src,src_ref,res_logits[ii],res_weights[ii],rot_gt,trans_gt)

            detailed_loss[ii]['rot_loss']=rot_loss.item()
            detailed_loss[ii]['trans_loss']=trans_loss.item()
            detailed_loss[ii]['class_loss']=class_loss.item()

            loss['rot_loss'] += rot_loss * self.w_trans
            loss['trans_loss']+= trans_loss * self.w_trans
            loss['class_loss'] += class_loss * self.w_class

        # get total loss
        for key,value in loss.items():
            if(key != 'total_loss'):
                loss['total_loss'] += value

        return loss,stats,detailed_loss


###############################################################
class SuperGlueLoss(BaseLoss):
    def __init__(self,args):
        BaseLoss.__init__(self, args)
        self.w_trans=args['loss']['w_trans']
        self.w_neg_likelihood=args['loss']['w_neg_likelihood']

    def init_loss_terms(self):
        loss=dict()
        loss['trans_loss']=0.
        loss['rot_loss']=0.
        loss['total_loss']=0.
        loss['nll_loss']=0.
        loss['pos_loss']=0.
        loss['neg_loss']=0.
        return loss
    

    def evaluate(self,*input):
        rot_est = input[0]  # Bx3x3
        trans_est = input[1]  # Bx3x1
        rot_gt = input[2] 
        trans_gt = input[3]
        log_perm_pred = input[4] #B x (N+1) x (N+1)
        perm_mat = input[5] # B X (N+1) X (N+1), ground truth

        # rigid motion loss 
        rot_loss, trans_loss = self._rigid_motion_loss(rot_est,rot_gt,trans_est,trans_gt)
        nll_loss,_,pos_loss,neg_loss = self._neg_log_likelihood(log_perm_pred,perm_mat)

        loss=self.init_loss_terms()
        loss['rot_loss']=rot_loss
        loss['trans_loss']=trans_loss
        loss['nll_loss']=nll_loss
        loss['pos_loss']=pos_loss
        loss['neg_loss']=neg_loss
        loss['total_loss']= nll_loss*self.w_neg_likelihood
        return loss


###############################################################
class RPMNetLoss(BaseLoss):
    def __init__(self,args):
        BaseLoss.__init__(self, args)
        self.wt_inliers = args['loss']['wt_inliers']
        self.w_trans=args['loss']['w_trans']
        self.w_neg_likelihood=args['loss']['w_neg_likelihood']

    def init_loss_terms(self):
        loss=dict()
        loss['trans_loss']=0.
        loss['perm_loss']=0.
        loss['total_loss']=0.
        return loss


    def _geometric_loss(self,src,rot_gt,rot_est,trans_gt,trans_est):
        '''
        get the mae loss 
        '''
        src_1=transform_pcd(src,rot_gt,trans_gt)
        src_2=transform_pcd(src,rot_est,trans_est)
        criterion = nn.L1Loss(reduction='mean')
        loss = criterion(src_1,src_2)
        return loss

    def _permutation_matrix_loss(self,log_perm_matrices):
        ref_outliers_strength = (1.0 - torch.sum(log_perm_matrices, dim=1)) * self.wt_inliers
        src_outliers_strength = (1.0 - torch.sum(log_perm_matrices, dim=2)) * self.wt_inliers
        
        loss = torch.mean(ref_outliers_strength) + torch.mean(src_outliers_strength)
        return loss

    def evaluate(self,*input):
        '''
        Input:
            src:        B,3,N
            perm_matrix:B,N,N
            rot_gt:     B,3,3
            t_gt:       B,3,1
            rot_est:    B,3,3
            t_gt:       B,3,1

        '''
        src,log_perm_matrices=input[0],input[1]
        perm_mat_gt=input[2]
        rot_gt,rot_est = input[3],input[4]
        t_gt,t_est=input[5],input[6]
        c_discount_factor=input[7]

        loss=dict()
        loss['trans_loss']=self._geometric_loss(src,rot_gt,rot_est,t_gt,t_est)*self.w_trans
        # loss['perm_loss']=self._permutation_matrix_loss(log_perm_matrices)
        w_loss,_=self._neg_log_likelihood(log_perm_matrices,perm_mat_gt)
        loss['perm_loss']=w_loss*self.w_neg_likelihood
        loss['total_loss']=(loss['trans_loss']+loss['perm_loss'])*c_discount_factor
        return loss

###############################################################
class MixNetLoss(BaseLoss):
    '''
    # 1. rigid motion loss
    # 2. classification loss
    # 3. negative log likelihood loss
    '''

    def __init__(self, args):
        BaseLoss.__init__(self, args)
        self.w_class = args['loss']['w_class']
        self.w_trans = args['loss']['w_trans']
        self.w_score=args['loss']['w_score']
        self.inlier_threshold = args['data']['inlier_threshold']
        self.topk=args['model']['topk']
    
    def losses(self):
        total_loss = dict()
        total_loss['trans_loss'] = 0.
        total_loss['class_loss'] = 0.
        return total_loss

    def init_loss_terms(self):
        total_loss = dict()
        total_loss['total_loss'] = 0.
        total_loss['trans_loss'] = 0.
        total_loss['class_loss'] = 0.
        total_loss['score_loss']=0.
        return total_loss

    def _score_matrix_loss(self,inlier_mat,score_mat):
        '''
        Here the values of score_mat is between [-1,1]
        Input:
            inlier_mat:     [B,N+1,N+1]
            score_mat:      [B,N,N]
        '''
        # transform to [0,1]
        score_mat=((score_mat+1)/2).flatten()

        # apply the BCE loss
        gt_labels=inlier_mat[:,:-1,:-1].contiguous().flatten()
        loss = nn.BCELoss(reduction='none')
        class_loss = loss(score_mat, gt_labels) 

        #######################################
        # get weighted loss
        weights = torch.ones_like(gt_labels)
        w_negative = gt_labels.sum()/gt_labels.size(0)  # Number of positives/number of all
        w_positive = 1 - w_negative  # Number of negatives/number of all
        
        weights[gt_labels > 0.5] = w_positive
        weights[gt_labels < 0.5] = w_negative
        w_class_loss = torch.sum(weights * class_loss)
        return w_class_loss

        
    def evaluate(self, *input):
        '''
        res_rot,res_trans,res_logits,res_weights,source,corr,feature_disparity
        we calculate the cyclic error: A ————> B and B ————> A
        '''
        res_rot = input[0]
        res_trans = input[1]
        res_logits = input[2]
        res_weights = input[3]
        rot_gt = input[4] # [B,3,3]
        trans_gt = input[5] # [B,3,1]
        inlier_mat = input[6] # [B,N+1,N+1]
        score_mat =input[7] # [B,N,N]
        correspondence=input[8]
        src=correspondence[:,:3,:,0]
        src_ref=correspondence[:,3:,:,0]

        # get loss
        loss = self.init_loss_terms()
        detailed_loss=dict()
        detailed_loss[0],detailed_loss[1]=self.losses(),self.losses()
        for ii in range(len(res_rot)):
            # rigid motion loss
            rot_loss,trans_loss = self._rigid_motion_loss(res_rot[ii],rot_gt,res_trans[ii],trans_gt)
            
            # inlier/outlier classification loss
            class_loss,stats = self._class_loss(src,src_ref,res_logits[ii],res_weights[ii],rot_gt,trans_gt)

            detailed_loss[ii]['trans_loss']=trans_loss.item()+rot_loss.item()
            detailed_loss[ii]['class_loss']=class_loss.item()

            loss['trans_loss']+= (trans_loss+rot_loss) * self.w_trans
            loss['class_loss'] += class_loss * self.w_class
        
        # loss from permutation matrix
        score_loss=self._score_matrix_loss(inlier_mat,score_mat)
        loss['score_loss']=score_loss * self.w_score

        # get total loss
        for key,value in loss.items():
            if(key != 'total_loss'):
                loss['total_loss'] += value

        return loss,stats,detailed_loss


###############################################################
class OANetLoss(BaseLoss):
    '''
    # 1. rigid motion loss
    # 2. feature alignment loss
    # 3. cycle_consistency loss
    # 4. classification loss
    '''

    def __init__(self, args):
        BaseLoss.__init__(self, args)
        self.w_class = args['loss']['class_loss']
        self.w_trans = args['loss']['trans_loss']
        self.inlier_threshold = args['data']['inlier_threshold']
    
    def losses(self):
        total_loss = dict()
        total_loss['rot_loss'] = 0.
        total_loss['trans_loss'] = 0.
        total_loss['class_loss'] = 0.
        return total_loss

    def init_loss_terms(self):
        total_loss = dict()
        total_loss['total_loss'] = 0.
        total_loss['rot_loss'] = 0.
        total_loss['trans_loss'] = 0.
        total_loss['class_loss'] = 0.
        return total_loss

        
    def evaluate(self, *input):
        '''
        res_rot,res_trans,res_logits,res_weights,source,corr,feature_disparity
        we calculate the cyclic error: A ————> B and B ————> A
        '''
        res_rot = input[0]
        res_trans = input[1]
        res_logits = input[2]
        res_weights = input[3]
        src = input[4] # [2B,3,N]
        src_ref = input[5]  # [2B,3,N]
        rot_ab_gt = input[6] #   [B,3,3]
        trans_ab_gt = input[7] # [B,3,1]

        # get rot_gt from target to source
        rot_ba_gt = rot_ab_gt.transpose(1, 2)
        trans_ba_gt = -torch.matmul(rot_ba_gt,trans_ab_gt)
        rot_gt = torch.cat((rot_ab_gt,rot_ba_gt),0)
        trans_gt = torch.cat((trans_ab_gt,trans_ba_gt),0)

        # get loss
        loss = self.init_loss_terms()
        batch_size=int(res_rot[0].size(0)/2)
        detailed_loss=dict()
        detailed_loss[0],detailed_loss[1]=self.losses(),self.losses()
        for ii in range(len(res_rot)):
            # rigid motion loss
            rot_loss,trans_loss = self._rigid_motion_loss(res_rot[ii],rot_gt,res_trans[ii],trans_gt)
            
            # inlier/outlier classification loss
            class_loss,stats = self._class_loss(src,src_ref,res_logits[ii],res_weights[ii],rot_gt,trans_gt)

            detailed_loss[ii]['rot_loss']=rot_loss.item()
            detailed_loss[ii]['trans_loss']=trans_loss.item()
            detailed_loss[ii]['class_loss']=class_loss.item()

            loss['rot_loss'] += rot_loss * self.w_trans
            loss['trans_loss']+= trans_loss * self.w_trans
            loss['class_loss'] += class_loss * self.w_class

        # get total loss
        for key,value in loss.items():
            if(key != 'total_loss'):
                loss['total_loss'] += value

        return loss,stats,detailed_loss