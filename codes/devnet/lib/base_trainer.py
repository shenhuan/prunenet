import torch
import os,sys,gc,time,glob
import numpy as np
import torch.nn.functional as F
from lib.util import Logger,get_geometric_errors,natural_key,get_permutation_matrix
from lib.benchmark import read_trajectory,read_pairs,write_trajectory,benchmark
from tqdm import tqdm
from torch.optim.lr_scheduler import MultiStepLR
import torch.optim as optim
import open3d as o3d

from tensorboardX import SummaryWriter


class BaseTrainer:
    '''
    BaseTrainer
    '''
    def __init__(self, args, net, train_loader, val_loader, test_loader):
        self.loader,self.n_iter={},{}
        self.loader['train']=train_loader
        self.loader['val']=val_loader
        self.loader['test']=test_loader

        self.n_iter['train'] = int(
            len(train_loader.dataset)/args['train']['batch_size'])
        self.n_iter['val']= int(len(val_loader.dataset) /
                              args['train']['batch_size'])
        self.n_iter['test']= int(len(test_loader.dataset) /
                              args['train']['batch_size'])

        self.epochs = args['train']['epochs']
        self.model = net
        self.device=torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.sparse = args['data']['sparse']

        self.exp_name = args['exp_name']
        self.writer = SummaryWriter(logdir=self.exp_name)
        self.logger=Logger(args)

        self.num_iters = args['train']['n_iters'] # iterative refine
        self.n_points=args['model']['n_input_points']
        self.discount_factor = args['loss']['discount_factor']
        self.logger.write('#parameters %.3f M\n' % (sum([x.nelement() for x in self.model.parameters()])/1e6))
        self.benchmark_dir=args['misc']['benchmark_dir']
        self.base_test = args['misc']['base_test']
        self.inlier_threshold = args['data']['inlier_threshold']


        ###################################
        # optimiser
        ###################################
        if args['optimizer']['use_sgd']:
            self.opt = optim.SGD(self.model.parameters(
            ), lr=args['optimiser']['lr'] * 100, momentum=args['optimizer']['momentum'], weight_decay=1e-4)
        else:
            self.opt = optim.Adam(
                net.parameters(), lr=args['optimizer']['lr'], weight_decay=1e-4)

        epoch_factor = args['train']['epochs'] / 100.0

        ###################################
        # learning rate scheduler
        ###################################
        self.scheduler = MultiStepLR(self.opt, milestones=[int(
            20*epoch_factor), int(50*epoch_factor),int(80*epoch_factor)], gamma=0.1)
    
    def _add_scalers(self, c_iter, c_loss, phase):
        '''
        update the tensorboard
        '''
        for key, value in c_loss.items():
            self.writer.add_scalar(
                '%s/%s' % (phase, key), value, c_iter)
    

    def _inference_one_epoch(self, epoch, phase):
        '''
        inference one epoch, return loss terms and errors
        for each batch, the loss terms are updated to tensorboard
        '''
        assert phase in ['train', 'val','test']

        ###################################
        # Initialize and set data loader
        ###################################
        rotations_gt = []
        translations_gt = []
        rotations_est = []
        translations_est = []

        total_loss = self.loss_evaluator.init_loss_terms()
        n_batches = 0
        c_loader=self.loader[phase]

        ###################################
        #### loop the data loader
        ###################################
        for data in tqdm(c_loader): # here data is a dictionary
            n_batches += 1

            ###########################################
            # update for each batch
            ###########################################
            c_iter = n_batches + epoch * self.n_iter[phase]            
            batch_loss,rot_est,trans_est=self._inference_one_batch(data,phase,c_iter)
            torch.cuda.empty_cache()

            for key, value in batch_loss.items():
                total_loss[key] += value
            
            ## update to tensorboard
            self._add_scalers(c_iter,batch_loss,phase) 
 
            rotations_gt.append(data['rot'].numpy())
            translations_gt.append(data['trans'].numpy())
            rotations_est.append(rot_est.cpu().numpy())
            translations_est.append(trans_est.cpu().numpy())

        ###########################################
        # get rotation and translation errors
        ###########################################
        total_errors = get_geometric_errors(rotations_gt,rotations_est,translations_gt,translations_est)

        ###########################################
        # update log
        ###########################################
        for key, value in total_loss.items():
            new_value=value/n_batches
            total_loss[key]=new_value
            self.writer.add_scalar('g_%s/%s' % (phase,key),new_value,epoch)
        
        info = {'epoch': epoch,
                'stage': phase,
                'errors': total_errors,
                'loss': total_loss
                }
        self.logger.write(str(info)+'\n')
        return info

    def train(self):
        '''
        Logics of training procedure
        '''
        info_test_best = None
        for epoch in range(self.epochs):
            # train and evaluate
            info_train = self._inference_one_epoch(epoch, 'train')
            info_test = self._inference_one_epoch(epoch, 'val')
            self.scheduler.step()

            # update checkpoints
            if info_test_best is None or info_test_best['loss']['total_loss'] > info_test['loss']['total_loss']:
                info_test_best = info_test
                info_test_best['stage'] = 'best_test'
                self.model.save('%s/model.best.pth' % self.exp_name)
                self.logger.write('Better model!\n')
            gc.collect()
            
    def test(self):
        '''
        Logics of training procedure
        '''
        self._inference_one_epoch(0, 'val')
        gc.collect()

    def benchmark(self):
        '''
        Benchmark on the 3DMatch test set
        '''
        scene_names=sorted(os.listdir(self.benchmark_dir))
        c_stamp=time.strftime("%d_%H:%M:%S",time.localtime())
        exp_dir=os.path.join(self.benchmark_dir.split('/gt_result')[0],c_stamp)
        os.makedirs(exp_dir)
        pcds,colors=[],[]
        for scene_name in tqdm(scene_names):
            c_path=os.path.join(self.base_test,scene_name,'05_FCGF_feats_0.025/*.pth')
            samples=sorted(glob.glob(c_path),key=natural_key)

            c_path=os.path.join(self.base_test,scene_name,'overlap.npy')
            overlaps=np.load(c_path)

            gt_pairs, gt_traj = read_trajectory(os.path.join(self.benchmark_dir,scene_name,'gt.log'))
            est_traj = []
            # get pair list
            for i in range(len(gt_pairs)):
                # prepare test pair
                ind0,ind1,n_fragments=int(gt_pairs[i][0]),int(gt_pairs[i][1]),int(gt_pairs[i][2])
                src_path,tgt_path=samples[ind1],samples[ind0]
                c_overlap = max(overlaps[ind0,ind1],overlaps[ind1,ind0])
                data=read_pairs(src_path,tgt_path,self.n_points)
                data['rot']=torch.from_numpy(gt_traj[i][None,:3,:3]).float()
                data['trans']=torch.from_numpy(gt_traj[i][None,:3,3,None]).float()
 
                data['perm_mat']=torch.eye(self.n_points+1)[None,:,:]
                data['inlier_mat']=data['perm_mat']

                # get transformation estimation
                _,rot_est,trans_est=self._inference_one_batch(data,'test',i)
            
                c_trans=np.eye(4)
                c_trans[:3,:3]=rot_est[0].cpu().numpy()
                c_trans[:3,3]=trans_est[0].cpu().numpy().flatten()
                est_traj.append(c_trans)
            
            # write the trajectory
            c_directory=os.path.join(exp_dir,scene_name)
            os.makedirs(c_directory)
            write_trajectory(np.array(est_traj),gt_pairs,os.path.join(c_directory,'est.log'))
        benchmark(c_stamp)


