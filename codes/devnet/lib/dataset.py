#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys,glob,h5py,torch
import numpy as np
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset
import open3d as o3d
import MinkowskiEngine as ME
from lib.util import jitter_pointcloud,get_distant_point,farthest_subsample_points,get_permutation_matrix,get_inliers

MAX_NUM_PTS=6500

class Dataset3DMatchRaw(Dataset):
    '''
    Load the raw point clouds
    '''
    def __init__(self,infos,args,data_augmentation=True):
        super(Dataset3DMatchRaw,self).__init__()
        self.infos = infos
        self.base_dir = args['misc']['base_dir']
        self.n_points = args['model']['n_input_points']
        self.rot_factor=args['data']['rot_factor']
        self.data_augmentation=data_augmentation
        self.voxel_size=args['data']['voxel_size']
        self.inlier_threshold=args['data']['inlier_threshold']

    def __len__(self):
        return len(self.infos['rot'])

    def __getitem__(self,item): 
        # get overlap and transformation
        overlap=self.infos['overlap'][item]
        rot=self.infos['rot'][item]
        trans=self.infos['trans'][item]

        # get point cloud
        src_path=os.path.join(self.base_dir,self.infos['src'][item])
        tgt_path=os.path.join(self.base_dir,self.infos['tgt'][item])
        src_pcd = np.asarray(o3d.io.read_point_cloud(src_path).points)
        tgt_pcd = np.asarray(o3d.io.read_point_cloud(tgt_path).points)
        
        # # voxelization
        # src=ME.utils.sparse_quantize(src,quantization_size=self.voxel_size) # N x 3
        # tgt=ME.utils.sparse_quantize(tgt,quantization_size=self.voxel_size) 

        # permute and randomly select # points
        if(src_pcd.shape[0]>self.n_points):
            src_permute=np.random.permutation(src_pcd.shape[0])[:self.n_points]
        else:
            src_permute=np.random.choice(src_pcd.shape[0],self.n_points)
        if(tgt_pcd.shape[0]>self.n_points):
            tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:self.n_points]
        else:
            tgt_permute=np.random.choice(tgt_pcd.shape[0],self.n_points)
        src_pcd,tgt_pcd=src_pcd[src_permute],tgt_pcd[tgt_permute]     

        # add gaussian noise
        if self.data_augmentation:
            src_pcd = jitter_pointcloud(src_pcd)
            tgt_pcd = jitter_pointcloud(tgt_pcd)
            
            # rotate the point cloud
            euler_ab=np.random.rand(3)*np.pi*2/self.rot_factor # anglez, angley, anglex
            rot_ab= Rotation.from_euler('zyx', euler_ab).as_matrix()
            if(np.random.rand(1)[0]>0.5):
                src_pcd=np.matmul(rot_ab,src_pcd.T).T
                rot=np.matmul(rot,rot_ab.T)
            else:
                tgt_pcd=np.matmul(rot_ab,tgt_pcd.T).T
                rot=np.matmul(rot_ab,rot)
                trans=np.matmul(rot_ab,trans)
        
        # get the matches
        inlier_matrix = get_inliers(src_pcd,tgt_pcd,rot,trans,self.inlier_threshold)[:-1,:-1]
        flag_src = np.sum(inlier_matrix,1)>0.
        flag_tgt=np.sum(inlier_matrix,0)>0.

        return src_pcd.T,tgt_pcd.T,rot.astype('float32'),trans.astype('float32'),overlap,flag_src,flag_tgt,inlier_matrix


class Dataset3DMatchFCGF(Dataset):
    '''
    load FCGF features of 3DMatch
    '''
    def __init__(self,infos,args,data_augmentation=True):
        super(Dataset3DMatchFCGF,self).__init__()
        self.infos = infos
        self.base_dir = args['misc']['base_dir']
        self.n_points = args['model']['n_input_points']
        self.rot_factor=args['data']['rot_factor']
        self.inlier_threshold=args['data']['inlier_threshold']
        self.data_augmentation=data_augmentation
        self.outlier_threshold=args['data']['outlier_threshold']

    def __len__(self):
        return len(self.infos['rot'])

    def __getitem__(self,item): 
        # get overlap and transformation
        overlap=self.infos['overlap'][item]
        rot=self.infos['rot'][item]
        trans=self.infos['trans'][item]

        # get pointcloud
        src_path=os.path.join(self.base_dir,self.infos['src'][item])
        tgt_path=os.path.join(self.base_dir,self.infos['tgt'][item])
        src = torch.load(src_path)
        tgt = torch.load(tgt_path)

        src_pcd, src_embedding = src['coords'],src['feats']
        tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats']
        
        # permute and randomly select 2048/1024 points
        if(src_pcd.shape[0]>self.n_points):
            src_permute=np.random.permutation(src_pcd.shape[0])[:self.n_points]
        else:
            src_permute=np.random.choice(src_pcd.shape[0],self.n_points)
        if(tgt_pcd.shape[0]>self.n_points):
            tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:self.n_points]
        else:
            tgt_permute=np.random.choice(tgt_pcd.shape[0],self.n_points)
        src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
        tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]        

        # add gaussian noise
        if self.data_augmentation:            
            # rotate the point cloud
            euler_ab=np.random.rand(3)*np.pi*2/self.rot_factor # anglez, angley, anglex
            rot_ab= Rotation.from_euler('zyx', euler_ab).as_matrix()
            if(np.random.rand(1)[0]>0.5):
                src_pcd=np.matmul(rot_ab,src_pcd.T).T
                rot=np.matmul(rot,rot_ab.T)
            else:
                tgt_pcd=np.matmul(rot_ab,tgt_pcd.T).T
                rot=np.matmul(rot_ab,rot)
                trans=np.matmul(rot_ab,trans)

        
        # ground truth permutation matrix after mutual close selection and distance thresholding
        perm_mat=get_permutation_matrix(src_pcd,tgt_pcd,rot,trans,self.inlier_threshold,self.outlier_threshold)

        # inlier matrix after distance thresholding
        # inlier_matrix = get_inliers(src_pcd,tgt_pcd,rot,trans,self.inlier_threshold)

        # perm_mat=np.zeros((self.n_points+1,self.n_points+1)).astype('float32')
        inlier_matrix=np.zeros((self.n_points,self.n_points)).astype('float32')

        return src_pcd.T,tgt_pcd.T,src_embedding.T,tgt_embedding.T,rot.astype('float32'),trans.astype('float32'),perm_mat,overlap,inlier_matrix

        # return src_pcd.T,tgt_pcd.T,overlap



class Dataset3DMatchSimulatedFix(Dataset):
    '''
    dataload for dgcnn
    '''
    def __init__(self, files, n_points, rot_factor,data_augmentation=False):
        super(Dataset3DMatchSimulatedFix, self).__init__()
        self.files=files
        self.data_augmentation = data_augmentation
        self.n_points=n_points
        self.rot_factor=rot_factor

    def __getitem__(self, item):
        # get pointcloud
        pointcloud = torch.load(self.files[item]).numpy()  # N x 3

        euler_ab=np.random.rand(3)*np.pi*2/self.rot_factor # anglez, angley, anglex
        euler_ba=-euler_ab[::-1]
        rot_ab= Rotation.from_euler('zyx', euler_ab).as_matrix()
        rot_ba=rot_ab.T
        t_ab=np.random.rand(3)-0.5
        trans_ba=-rot_ba.dot(t_ab)

        # rotate and translate
        pcd1=pointcloud
        pcd2=np.matmul(rot_ab,pcd1.T)+np.expand_dims(t_ab,axis=1) 
        pcd2=pcd2.T    

        # permute
        pcd1=np.random.permutation(pcd1)
        pcd2=np.random.permutation(pcd2)

        # add gaussian noise
        if self.data_augmentation:
            pcd1 = jitter_pointcloud(pcd1)
            pcd2 = jitter_pointcloud(pcd2)

        # sample partial representation online
        pcd1,pcd2=farthest_subsample_points(pcd1,pcd2,self.n_points,self.n_points)
        pcd1=pcd1.T.astype('float32')  # 3 X N
        pcd2=pcd2.T.astype('float32')  # 3 X N

        return pcd1,pcd2,rot_ab.astype('float32'),t_ab.astype('float32')
            

    def __len__(self):
        return len(self.files)


class Dataset3DMatchSimulatedSparse(Dataset):
    '''
    data loader for Sparse Tensor
    '''
    def __init__(self, files, voxel_size, rot_factor, subsample_ratio=0.7,data_augmentation=True):
        super(Dataset3DMatchSimulatedSparse, self).__init__()
        self.files=files
        self.voxel_size=voxel_size
        self.data_augmentation = data_augmentation
        self.subsample_ratio=subsample_ratio
        self.rot_factor=rot_factor  # restrict rotation angle

    def __getitem__(self, item):
        # get pointcloud
        pointcloud = np.array(o3d.io.read_point_cloud(self.files[item]).points)

        euler_ab=np.random.rand(3)*np.pi*2 /self.rot_factor # anglez, angley, anglex
        euler_ba=-euler_ab[::-1]
        rot_ab= Rotation.from_euler('zyx', euler_ab).as_matrix()
        rot_ba=rot_ab.T
        t_ab=np.random.rand(3)-0.5
        trans_ba=-rot_ba.dot(t_ab)

        # rotate and translate
        pcd1=pointcloud
        pcd2=np.matmul(rot_ab,pcd1.T)+np.expand_dims(t_ab,axis=1) # 3xN
        pcd2=pcd2.T

        # permute
        pcd1=np.random.permutation(pcd1)
        pcd2=np.random.permutation(pcd2)

        # add gaussian noise
        if self.data_augmentation:
            pcd1 = jitter_pointcloud(pcd1)
            pcd2 = jitter_pointcloud(pcd2)

        # voxelization
        pcd1=ME.utils.sparse_quantize(pcd1,quantization_size=self.voxel_size) # N x 3
        pcd2=ME.utils.sparse_quantize(pcd2,quantization_size=self.voxel_size) 

        # sample partial representation online
        num_subsampled_points_1=int(pcd1.shape[0]*self.subsample_ratio)
        num_subsampled_points_2=int(pcd2.shape[0]*self.subsample_ratio)
        pcd1,pcd2=farthest_subsample_points(pcd1,pcd2,num_subsampled_points_1,num_subsampled_points_2)

        # for each pcd, the maximal number of points are MAX_NUM_PTS
        if(pcd1.shape[0]>MAX_NUM_PTS):
            idx = np.random.permutation(pcd1.shape[0])[:MAX_NUM_PTS]
            pcd1=pcd1[idx]
        
        if(pcd2.shape[0]>MAX_NUM_PTS):
            idx = np.random.permutation(pcd2.shape[0])[:MAX_NUM_PTS]
            pcd2=pcd2[idx]

        return pcd1,pcd2,rot_ab.astype('float32'),t_ab.astype('float32')


    def __len__(self):
        return len(self.files)