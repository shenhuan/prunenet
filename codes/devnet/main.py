#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
import os,sys
import torch
import argparse

from configs import config
from lib.util import setup_seed
from lib.data_loader import get_data_loaders
from lib.trainers import get_trainer
from models.engine import Engine
import time
import numpy as np

def _init_(args):
    setup_seed(cfg['misc']['seed'])
    torch.backends.cudnn.enabled = False
    if not os.path.exists('logs'):
        os.makedirs('logs')
    c_log=os.path.join('logs',args['method'],args['exp_name'])
    if not os.path.exists(c_log):
        os.makedirs(c_log)
    args['exp_name']=c_log

    # backup files
    os.system('cp -r models %s' % c_log)
    os.system('cp -r lib %s' % c_log)


if __name__ == '__main__':
    #########################################
    # load configures
    parser = argparse.ArgumentParser()
    parser.add_argument('config', type=str, help= 'Path to the config file.')
    args = parser.parse_args()
    cfg=config.load_config(args.config)
    common_cfg=config.load_config('./configs/common.yaml')
    cfg.update(common_cfg)
    if(not os.path.exists(cfg['misc']['base_dir'])):
        cfg['misc']['base_dir']='../../../3DMatch'

    #########################################
    # fix random seed
    _init_(cfg)
    

    # make model
    net = Engine(cfg).cuda()
    if(cfg['mode']=='train'):  # used for train
        # make dataloader
        train_loader,val_loader=get_data_loaders(cfg)
        # train
        trainer=get_trainer(cfg, net, train_loader, val_loader,val_loader)
        trainer.train()
    elif(cfg['mode']=='test'):  # used to get some validation results
        test_loader = get_data_loaders(cfg)
        trainer = get_trainer(cfg, net, test_loader,test_loader,test_loader)
        trainer.test()
    elif(cfg['mode']=='benchmark'):  # used for benchmark 
        test_loader = get_data_loaders(cfg)
        trainer = get_trainer(cfg, net, test_loader,test_loader,test_loader)
        trainer.benchmark()
    else:
        raise Exception('Not Implemented')
        