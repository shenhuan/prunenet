import os,sys,glob
import torch
import numpy as np
import open3d as o3d
from models import load_model
from scipy.spatial.transform import Rotation
from tqdm import tqdm
import open3d as o3d
import MinkowskiEngine as ME
from lib.dataset import farthest_subsample_points
from lib.util import get_angle_deviation,get_permutation_matrix,mutual_selection
from configs import config
from lib.data_loader import get_data_loaders
import numpy.matlib
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from models.engine import Engine

def jitter_pointcloud(pointcloud, sigma=0.01, clip=0.05):
    N, C = pointcloud.shape
    pointcloud += np.clip(sigma * np.random.randn(N, C), -1 * clip, clip)
    return pointcloud


def extract_feats():
    voxel_size=0.025
    # load model
    Model=load_model('ResUNetBN2C')
    emb_nn=Model(1,32,bn_momentum=0.05,\
            normalize_feature=True,conv1_kernel_size=7,D=3)
    emb_nn.load_state_dict(torch.load('model_zoo/FCGF_%s.pth' % voxel_size)['state_dict'])
    emb_nn.eval()

    # prepare data
    for phase in ['train','test']:
        base_dir='/net/pf-pc27/scratch2/shengyu/3DMatch/%s' % phase
        folders=os.listdir(base_dir)
        with torch.no_grad():
            for eachfolder in tqdm(folders):
                c_folder=os.path.join(base_dir,eachfolder)
                feats_dir=os.path.join(c_folder,'05_FCGF_feats_%s' % voxel_size)
                if(not os.path.exists(feats_dir)):
                    os.makedirs(feats_dir)
                files=glob.glob(c_folder +'/02_Fragments/*.ply')
                for eachfile in files:
                    pcd=o3d.io.read_point_cloud(eachfile)
                    pcd=np.asarray(pcd.points)

                    # voxelization
                    sel=ME.utils.sparse_quantize(pcd/voxel_size,return_index=True)
                    xyz=torch.from_numpy(np.floor(pcd[sel]/voxel_size)).int()
                    feats=torch.ones((xyz.size()[0],1))
                    indice=torch.zeros((xyz.size()[0],1)).int()
                    coords=torch.cat((indice,xyz),1)

                    # predict
                    stensor=ME.SparseTensor(feats,coords=coords)
                    
                    embeddings=emb_nn(stensor).F.cpu().numpy()
                    data=dict()
                    data['coords']=pcd[sel]
                    data['feats']=embeddings

                    assert data['coords'].shape[0]==data['feats'].shape[0]

                    path=eachfile.split('Fragments/')[1].split('.')[0]+'.pth'
                    torch.save(data,os.path.join(feats_dir,path))


def ss1():
    src=torch.load('src.pth').cpu()
    tgt=torch.load('tgt.pth').cpu()
    feats=torch.ones((src.size()[0],1),dtype=torch.float32)
    src_stensor=ME.SparseTensor(feats.cpu(),coords=src)
    feats=torch.ones((tgt.size()[0],1),dtype=torch.float32)
    tgt_stensor=ME.SparseTensor(feats.cpu(),coords=tgt)
    
    Model=load_model('ResUNetBN2C')
    emb_nn=Model(1,32,bn_momentum=0.05,\
            normalize_feature=True,conv1_kernel_size=7,D=3)
    emb_nn.load_state_dict(torch.load('models/FCGF_0.05.pth')['state_dict'])
    emb_nn.eval()

    embeddings=emb_nn(src_stensor)
    coords, feats = embeddings.decomposed_coordinates_and_features
    print(len(coords))
    for i in range(8):
        print(embeddings.features_at(i))
        print(embeddings.coordinates_at(i).float())

def ss2():
    base_dir='../../dataset/3DMatch/train/*/02_Fragments/*.ply'
    files=glob.glob(base_dir)
    pointcloud = np.array(o3d.io.read_point_cloud(files[0]).points)
    euler_ab=np.random.rand(3)*np.pi*2 # anglez, angley, anglex
    euler_ba=-euler_ab[::-1]
    R_ab= Rotation.from_euler('zyx', euler_ab).as_matrix()
    R_ba=R_ab.T
    translation_ab=np.random.rand(3)-0.5
    translation_ba=-R_ba.dot(translation_ab)

    # rotate and translate
    pcd1=pointcloud.T # 3xN
    pcd2=np.matmul(R_ab,pcd1)+np.expand_dims(translation_ab,axis=1) # 3xN

    # permute
    pcd1=np.random.permutation(pcd1.T).T
    pcd2=np.random.permutation(pcd2.T).T

    #add gaussian noise
    pcd1 = jitter_pointcloud(pcd1)
    pcd2 = jitter_pointcloud(pcd2)


    # sample partial representation online
    pcd1,pcd2=farthest_subsample_points(pcd1,pcd2,subsample_ratio=0.75)

    pcd3=np.matmul(R_ab,pcd1.T)+np.expand_dims(translation_ab,axis=1)
    np.savetxt('assets/frag1_1.txt',pcd1)
    np.savetxt('assets/frag1_2.txt',pcd2)
    np.savetxt('assets/frag1_3.txt',pcd3.T)

    # voxelization
    pcd1=ME.utils.sparse_quantize(pcd1,quantization_size=0.05).astype('float32')
    pcd2=ME.utils.sparse_quantize(pcd2,quantization_size=0.05).astype('float32')

    pcd3=np.matmul(R_ab,pcd1.T)+np.expand_dims(translation_ab,axis=1)

    np.savetxt('assets/frag2_1.txt',pcd1)
    np.savetxt('assets/frag2_2.txt',pcd2)
    np.savetxt('assets/frag2_3.txt',pcd3.T)


def batch_sparse_tensor_to_coords_feat(stensor):
    '''
    convert sparse tensor to batched normal torch tensor
    '''
    coords, feats = [], []
    batch_size = len(stensor.decomposed_coordinates)
    for ii in range(batch_size):
        fea = stensor.features_at(ii)
        coo = stensor.coordinates_at(ii)

        # randomly sample fixed number of points
        idx = np.random.choice(coo.size(0), 1536)
        fea = fea[idx].transpose(1, 0)
        coo = coo[idx].transpose(1, 0).float() * 0.05  # transform back

        coords.append(coo)
        feats.append(fea)

    coords = torch.stack(coords, dim=0)
    feats = torch.stack(feats, dim=0)

    return coords, feats

def get_matching_count(source,target,threshold=0.05):
    pcd_tree = o3d.geometry.KDTreeFlann(target)
    
    match_count=0
    for i, point in enumerate(source.points):
        [count, _, _] = pcd_tree.search_radius_vector_3d(point, threshold)
        if(count!=0):
            match_count+=1
    return match_count

def ss3():
    '''
    check overlap ratio of the simulated dataset
    '''
    args=config.load_config('./configs/FCGF.yaml')
    # make dataloader
    train_loader,val_loader=get_data_loaders(args)
    pcd_files=glob.glob('../../dataset/3DMatch/train/*/02_Fragments/*.ply')
    pcd1=o3d.io.read_point_cloud(pcd_files[0])
    pcd2=o3d.io.read_point_cloud(pcd_files[1])
    pcd1.points=o3d.utility.Vector3dVector()
    pcd1.colors=o3d.utility.Vector3dVector()
    pcd2.points=o3d.utility.Vector3dVector()
    pcd2.colors=o3d.utility.Vector3dVector()

    overlaps=[]
    for data in tqdm(train_loader):  
        src, tgt, rot_ab_gt, trans_ab_gt = [d for d in data]
        feats = torch.ones((src.size()[0], 1), dtype=torch.float32)
        src_stensor = ME.SparseTensor(feats, coords=src)
        feats = torch.ones((tgt.size()[0], 1), dtype=torch.float32)
        tgt_stensor = ME.SparseTensor(feats, coords=tgt)
    
        src,_=batch_sparse_tensor_to_coords_feat(src_stensor)
        tgt,_=batch_sparse_tensor_to_coords_feat(tgt_stensor)

        # get overlap ratio
        src_transformed=torch.matmul(rot_ab_gt,src)+trans_ab_gt.unsqueeze(2)

        # get overlap ratios
        for i in range(src.size(0)):
            c_src=src_transformed[i].numpy().T #3 x 1536
            c_target=tgt[i].numpy().T

            pcd1.points=o3d.utility.Vector3dVector(c_src)
            pcd2.points=o3d.utility.Vector3dVector(c_target)

            overlap=get_matching_count(pcd1,pcd2)/1536
            print(overlap)

            overlaps.append(overlap)
    
    print(np.mean(overlaps),np.median(overlaps))
    overlaps=np.array(overlaps)
    np.save('overlap.npy',overlaps)



def ss4():
    '''
    determine epsilon for voxel size=0.05
    epsilon=0.05
    '''
    args=config.load_config('./configs/FCGF.yaml')
    # make dataloader
    train_loader,val_loader=get_data_loaders(args)
    pcd_files=glob.glob('../../dataset/3DMatch/train/*/02_Fragments/*.ply')
    pcd=o3d.io.read_point_cloud(pcd_files[0])
    pcd.points=o3d.utility.Vector3dVector()
    pcd.colors=o3d.utility.Vector3dVector()
    
    etas=[]
    for data in tqdm(val_loader):  
        src, tgt, rot_ab_gt, trans_ab_gt = [d for d in data]
        feats = torch.ones((src.size()[0], 1), dtype=torch.float32)
        src_stensor = ME.SparseTensor(feats, coords=src)

        src,_=batch_sparse_tensor_to_coords_feat(src_stensor)

        # get epsilon
        for ii in range(src.size(0)):
            c_src=src[ii].numpy().T
            pcd.points=o3d.utility.Vector3dVector(c_src)
            pcd_tree = o3d.geometry.KDTreeFlann(pcd)
            distances=[]
            for i, point in enumerate(pcd.points):
                [count,vec1, vec2] = pcd_tree.search_knn_vector_3d(point,2)
                distances.append(np.sqrt(vec2[1]))
            etai=np.median(distances)
            etas.append(etai)
            print(etai)
    print(max(etas),min(etas))

def ss5():
    # get baseline
    data=np.load('../../dataset/3DMatch/raw_all_test_0.1.npz')
    gt=data['rot']
    pred=np.repeat(np.expand_dims(np.eye(3),0),gt.shape[0],axis=0)
    deviation=get_angle_deviation(pred,gt)
    print(np.mean(deviation),np.median(deviation))


def ss6():
    '''
    check the data set
    the coordinates, rot and trans are absolutely correct
    '''
    red=np.matlib.repmat(np.array([255,0,0]),2048,1)
    green=np.matlib.repmat(np.array([0,255,0]),2048,1)   
    blue=np.matlib.repmat(np.array([0,0,255]),2048,1)

    data=torch.load('dump/sample_1.pth')
    src,tgt=data['src_pcd'],data['tgt_pcd']
    src_embedding,tgt_embedding = data['src_embedding'],data['tgt_embedding']
    rot_gt,trans_gt = data['rot'],data['trans']
    src_transform = torch.matmul(rot_gt,src)+trans_gt
    batch_size = rot_gt.size(0)
    for i in range(batch_size):
        c_src = src[i].transpose(0,1).numpy()
        c_tgt = tgt[i].transpose(0,1).numpy()
        c_src_trans = src_transform[i].transpose(0,1).numpy()
        np.savetxt('dump/src_%d.txt' % i,np.concatenate([c_src,red],1))
        np.savetxt('dump/tgt_%d.txt' % i,np.concatenate([c_tgt,green],1))
        np.savetxt('dump/trans_%d.txt' % i,np.concatenate([c_src_trans,blue],1))

def ss7():
    '''
    debug benchmark script
    '''
    red=np.matlib.repmat(np.array([255,0,0]),2048,1)
    green=np.matlib.repmat(np.array([0,255,0]),2048,1)   
    blue=np.matlib.repmat(np.array([0,0,255]),2048,1)


    data=torch.load('shit.pth')
    src=data['src_pcd']
    tgt=data['tgt_pcd']
    rot=data['rot']
    trans=data['trans']
    
    src_trans=torch.matmul(rot,src)+trans

    np.savetxt('dump/xx_src.txt',np.concatenate([src[0].numpy().T,red],1))
    np.savetxt('dump/xx_tgt.txt',np.concatenate([tgt[0].numpy().T,green],1))
    np.savetxt('dump/xx_trans.txt',np.concatenate([src_trans[0].numpy().T,blue],1))


class Dataset3DMatchFCGF(Dataset):
    '''
    load FCGF features of 3DMatch
    '''
    def __init__(self,infos,args,data_augmentation=True):
        super(Dataset3DMatchFCGF,self).__init__()
        self.infos = infos
        self.base_dir = args['misc']['base_dir']
        self.num_points = args['model']['n_input_points']
        self.rot_factor=args['data']['rot_factor']
        self.inlier_threshold=args['data']['inlier_threshold']
        self.data_augmentation=data_augmentation
        self.outlier_threshold=args['data']['outlier_threshold']

    def __len__(self):
        return len(self.infos['rot'])

    def __getitem__(self,item): 
        # get overlap and transformation
        overlap=self.infos['overlap'][item]
        rot=self.infos['rot'][item]
        trans=self.infos['trans'][item]

        # get pointcloud
        src_path=os.path.join(self.base_dir,self.infos['src'][item])
        tgt_path=os.path.join(self.base_dir,self.infos['tgt'][item])
        src = torch.load(src_path)
        tgt = torch.load(tgt_path)

        src_pcd, src_embedding = src['coords'],src['feats']
        tgt_pcd, tgt_embedding = tgt['coords'], tgt['feats']
        
        # permute and randomly select 2048/1024 points
        if(src_pcd.shape[0]>self.num_points):
            src_permute=np.random.permutation(src_pcd.shape[0])[:self.num_points]
        else:
            src_permute=np.random.choice(src_pcd.shape[0],self.num_points)
        if(tgt_pcd.shape[0]>self.num_points):
            tgt_permute=np.random.permutation(tgt_pcd.shape[0])[:self.num_points]
        else:
            tgt_permute=np.random.choice(tgt_pcd.shape[0],self.num_points)
        src_pcd,src_embedding = src_pcd[src_permute],src_embedding[src_permute]
        tgt_pcd,tgt_embedding = tgt_pcd[tgt_permute],tgt_embedding[tgt_permute]        

        # ground truth permutation matrix
        perm_mat_gt=get_permutation_matrix(src_pcd,tgt_pcd,rot,trans,self.inlier_threshold,self.
        outlier_threshold)
    

        return src_pcd.T,tgt_pcd.T,src_embedding.T,tgt_embedding.T,rot.astype('float32'),trans.astype('float32'),perm_mat_gt,overlap


def collate_fn_fcgf(list_data):
    src_pcd,tgt_pcd,src_embedding,tgt_embedding,rot,trans,perms,overlaps=list(zip(*list_data))
    r_abs=torch.from_numpy(np.array(rot)).float()
    t_abs=torch.from_numpy(np.array(trans)).float()
    perms = torch.from_numpy(np.array(perms)).float()
    overlaps=torch.from_numpy(np.array(overlaps)).float()


    src_pcd = torch.from_numpy(np.array(src_pcd)).float()
    tgt_pcd = torch.from_numpy(np.array(tgt_pcd)).float()
    src_embedding = torch.from_numpy(np.array(src_embedding)).float()
    tgt_embedding = torch.from_numpy(np.array(tgt_embedding)).float()

    if(t_abs.dim()==2):
        t_abs=t_abs.unsqueeze(2)

    data=dict()
    data['src_pcd']= src_pcd
    data['src_embedding'] = src_embedding
    data['tgt_pcd']=tgt_pcd
    data['tgt_embedding'] = tgt_embedding
    data['rot']=r_abs
    data['trans']=t_abs
    data['perm_mat']=perms
    data['overlaps']=overlaps

    return data


def get_train_val_loaders_fcgf(args):
    '''
    get train/val for FCGF features of 3DMatch 
    '''
    all_infos=np.load(args['misc']['path_test_info'])
    info_val=dict()
    for key,value in all_infos.items():
        info_val[key]=value

    all_infos=np.load(args['misc']['path_train_info'])
    info_train=dict()
    for key,value in all_infos.items():
        info_train[key]=value

    train_loader=DataLoader(Dataset3DMatchFCGF(info_train,args,True),batch_size=args['train']['batch_size'],shuffle=True, drop_last=True, num_workers=args['misc']['workers'],pin_memory=args['misc']['pin_memory'],collate_fn=collate_fn_fcgf)

    val_loader=DataLoader(Dataset3DMatchFCGF(info_val,args,False),batch_size=args['train']['batch_size'],shuffle=False, drop_last=True, num_workers=args['misc']['workers'],pin_memory=args['misc']['pin_memory'],collate_fn=collate_fn_fcgf)

    return train_loader,val_loader

def ss8():
    cfg=config.load_config('./configs/superglue.yaml')
    common_cfg=config.load_config('./configs/common.yaml')
    cfg.update(common_cfg)
    if(not os.path.exists(cfg['misc']['base_dir'])):
        cfg['misc']['base_dir']='../../../3DMatch'
    
    train_loader,val_loader=get_train_val_loaders_fcgf(cfg)
    model = Engine(cfg).cuda()
    model.eval()
    device=torch.device('cuda')
    recalls_fcgf=[]
    recalls_glued=[]
    recalls_sinkhorn=[]
    overlaps=[]
    with torch.no_grad():
        for data in tqdm(val_loader):
            src,tgt=data['src_pcd'].to(device),data['tgt_pcd'].to(device)
            src_embedding,tgt_embedding = data['src_embedding'].to(device),data['tgt_embedding'].to(device)
            rot_gt,trans_gt = data['rot'].to(device),data['trans'].to(device)  

            # make predictions
            rot_est,trans_est,log_perm_mat,bin_score,scores= model.forward(src,tgt,src_embedding,tgt_embedding)
            score_fcgf=torch.matmul(src_embedding.transpose(1,2),tgt_embedding)

            
            perm_mat_gt=data['perm_mat'][:,:-1,:-1].numpy()
            perm_mat_sinkhorn=log_perm_mat.exp()[:,:-1,:-1].cpu().numpy()
            perm_mat_sinkhorn=mutual_selection(perm_mat_sinkhorn)
            perm_mat_fcgf=mutual_selection(score_fcgf)
            perm_mat_glued=mutual_selection(scores)

            for i in range(perm_mat_gt.shape[0]):
                n_matches=perm_mat_gt[i].sum()
                mask=perm_mat_gt[i].astype(np.bool)
                n=(perm_mat_gt[i]==perm_mat_sinkhorn[i])[mask].sum()
                recalls_sinkhorn.append(n/n_matches)
                n=(perm_mat_gt[i]==perm_mat_fcgf[i])[mask].sum()
                recalls_fcgf.append(n/n_matches)
                n=(perm_mat_gt[i]==perm_mat_glued[i])[mask].sum()
                recalls_glued.append(n/n_matches)
            overlaps.extend(data['overlaps'].numpy().tolist())
    
    np.savez('dump/superglue',overlap=overlaps,fcgf=recalls_fcgf,glued=recalls_glued,sinkhorn=recalls_sinkhorn)

def ss9():
    cfg=config.load_config('./configs/rpmnet.yaml')
    common_cfg=config.load_config('./configs/common.yaml')
    cfg.update(common_cfg)
    if(not os.path.exists(cfg['misc']['base_dir'])):
        cfg['misc']['base_dir']='../../../3DMatch'
    
    train_loader,val_loader=get_train_val_loaders_fcgf(cfg)
    model = Engine(cfg).cuda()
    model.eval()
    device=torch.device('cuda')
    recalls_fcgf=[]
    recalls_sinkhorn=[]
    overlaps=[]
    with torch.no_grad():
        for data in tqdm(val_loader):
            src,tgt=data['src_pcd'].to(device),data['tgt_pcd'].to(device)
            src_embedding,tgt_embedding = data['src_embedding'].to(device),data['tgt_embedding'].to(device)
            rot_gt,trans_gt = data['rot'].to(device),data['trans'].to(device)  

            # make predictions
            c_perm_matrix,rot_ab_est,trans_ab_est,alpha,beta = model.forward(src.transpose(1,2),tgt.transpose(1,2),src_embedding.transpose(1,2),tgt_embedding.transpose(1,2))
            score_fcgf=torch.matmul(src_embedding.transpose(1,2),tgt_embedding)

            perm_mat_gt=data['perm_mat'][:,:-1,:-1].numpy()
            perm_mat_sinkhorn=mutual_selection(c_perm_matrix)
            perm_mat_fcgf=mutual_selection(score_fcgf)

            for i in range(perm_mat_gt.shape[0]):
                n_matches=perm_mat_gt[i].sum()
                mask=perm_mat_gt[i].astype(np.bool)
                n=(perm_mat_gt[i]==perm_mat_sinkhorn[i])[mask].sum()
                recalls_sinkhorn.append(n/n_matches)
                n=(perm_mat_gt[i]==perm_mat_fcgf[i])[mask].sum()
                recalls_fcgf.append(n/n_matches)
            overlaps.extend(data['overlaps'].numpy().tolist())
    
    np.savez('dump/rpm',overlap=overlaps,fcgf=recalls_fcgf,sinkhorn=recalls_sinkhorn)

def ss10():
    '''
    Compare 3 recalls from FCGF, FCGF+Transformer, FCGF+Transformer+Sinkhorn
    based on ground truth permutation matrix
    '''
    cfg=config.load_config('./configs/mixnet.yaml')
    common_cfg=config.load_config('./configs/common.yaml')
    cfg.update(common_cfg)
    if(not os.path.exists(cfg['misc']['base_dir'])):
        cfg['misc']['base_dir']='../../../3DMatch'
    
    train_loader,val_loader=get_data_loaders(cfg)
    model = Engine(cfg).cuda()
    model.eval()
    device=torch.device('cuda')
    recalls_fcgf=[]
    recalls_sinkhorn=[]
    recalls_superglue=[]
    for i in range(6):
        recalls_fcgf.append([])
        recalls_sinkhorn.append([])
        recalls_superglue.append([])

    with torch.no_grad():
        for data in tqdm(val_loader):
            src,tgt=data['src_pcd'].to(device),data['tgt_pcd'].to(device)
            src_embedding,tgt_embedding = data['src_embedding'].to(device),data['tgt_embedding'].to(device)
            rot_gt,trans_gt = data['rot'].to(device),data['trans'].to(device)  

            # make predictions
            scores_fcgf,scores_superglue,scores_sinkhorn= model.forward(src,tgt,src_embedding,tgt_embedding)

            scores_sinkhorn=scores_sinkhorn.exp()[:,:-1,:-1]
            # build ground truth inlier indice
            for idx,k in enumerate([1,2,3,4,5,10]):
                gt_indice=torch.argmax(data['perm_mat'].to(device),-1,keepdim=True)[:,:-1,:] #[B,N,1]
                gt_indice=gt_indice.repeat(1,1,k) #[B,N,K]

                # indice from fcgf
                topk=torch.topk(scores_fcgf,k,-1,sorted=False)
                top_fcgf=topk[1]  # [B,N,K]

                # indice from superglue features
                topk=torch.topk(scores_superglue,k,-1,sorted=False)
                top_superglue=topk[1]

                # indice from sinkhorn
                topk=torch.topk(scores_sinkhorn,k,-1,sorted=False)
                top_sinkhorn=topk[1]

                # get recalls
                n=data['perm_mat'].to(device)[:,:-1,:-1].sum([1,2])
                n1=(gt_indice==top_sinkhorn).sum([1,2])
                n2=(gt_indice==top_fcgf).sum([1,2])
                n3=(gt_indice==top_superglue).sum([1,2])

                recalls_fcgf[idx].extend((n2/n).tolist())
                recalls_sinkhorn[idx].extend((n1/n).tolist())
                recalls_superglue[idx].extend((n3/n).tolist())

    np.savez('dump/recall_analysis',fcgf=recalls_fcgf,sinkhorn=recalls_sinkhorn,superglue=recalls_superglue)

def ss11():
    '''
    compare inliers from permutation matrix and naive distance thresholding using training data
    '''
    cfg=config.load_config('./configs/mixnet.yaml')
    common_cfg=config.load_config('./configs/common.yaml')
    cfg.update(common_cfg)
    if(not os.path.exists(cfg['misc']['base_dir'])):
        cfg['misc']['base_dir']='../../../3DMatch'
    
    train_loader,val_loader=get_data_loaders(cfg)
    model = Engine(cfg).cuda()
    model.eval()
    device=torch.device('cuda')

    n_inliers_1=[]
    n_inliers_2=[]
    with torch.no_grad():
        for data in tqdm(train_loader):
            src,tgt=data['src_pcd'].to(device),data['tgt_pcd'].to(device)
            src_embedding,tgt_embedding = data['src_embedding'].to(device),data['tgt_embedding'].to(device)
            rot_gt,trans_gt = data['rot'].to(device),data['trans'].to(device)  

            # compare the ground truth
            inlier_matrix=data['inlier_mat'][:,:-1,:-1]
            perm_mat=data['perm_mat'][:,:-1,:-1]
            n_inliers_1.extend(inlier_matrix.sum([1,2]).tolist())
            n_inliers_2.extend(perm_mat.sum([1,2]).tolist())

    np.savez('dump/comp_inliers_train',perm_mat=n_inliers_2,inlier_mat=n_inliers_1)


def ss12():
    '''
    Detailed analysis of how many inliers one point could have 
    '''
    cfg=config.load_config('./configs/mixnet.yaml')
    common_cfg=config.load_config('./configs/common.yaml')
    cfg.update(common_cfg)
    if(not os.path.exists(cfg['misc']['base_dir'])):
        cfg['misc']['base_dir']='../../../3DMatch'
    
    train_loader,val_loader=get_data_loaders(cfg)
    model = Engine(cfg).cuda()
    model.eval()
    device=torch.device('cuda')
    recalls_fcgf=[]
    recalls_sinkhorn=[]
    recalls_superglue=[]
    for i in range(6):
        recalls_fcgf.append([])
        recalls_sinkhorn.append([])
        recalls_superglue.append([])

    n_inliers_1=[]
    n_inliers_2=[]
    nn=[]
    k=5
    with torch.no_grad():
        for data in tqdm(val_loader):
            src,tgt=data['src_pcd'].to(device),data['tgt_pcd'].to(device)
            src_embedding,tgt_embedding = data['src_embedding'].to(device),data['tgt_embedding'].to(device)
            rot_gt,trans_gt = data['rot'].to(device),data['trans'].to(device)  

            # compare the ground truth
            inlier_matrix=data['inlier_mat'][:,:-1,:-1]
            perm_mat=data['perm_mat'][:,:-1,:-1]

            nn.extend(inlier_matrix.sum(-1).cpu().numpy().flatten().tolist())
    
    np.savez('dump/detailed_match',nn=nn)

def ss13():
    '''
    Sanity check of the datasetRaw
    '''
    data=torch.load('data.pth')
    src=data['src_pcd']
    tgt=data['tgt_pcd']
    src_trans=torch.matmul(data['rot'],src)+data['trans']

    for i in range(4):
        src_trans_pcd = o3d.geometry.PointCloud()
        tgt_pcd=o3d.geometry.PointCloud()

        src_trans_pcd.points=o3d.utility.Vector3dVector(src_trans[i].cpu().numpy().T)
        tgt_pcd.points=o3d.utility.Vector3dVector(tgt[i].cpu().numpy().T)

        src_trans_pcd.paint_uniform_color([0, 0.651, 0.929])
        tgt_pcd.paint_uniform_color([1, 0.706, 0])

        o3d.visualization.draw_geometries([src_trans_pcd,tgt_pcd],window_name='prediction')


if __name__=='__main__':
    voxel_size=0.025
    # load model
    Model=load_model('ResUNetBN2C')
    emb_nn=Model(1,32,bn_momentum=0.05,\
            normalize_feature=True,conv1_kernel_size=7,D=3)
    emb_nn.load_state_dict(torch.load('model_zoo/FCGF_%s.pth' % voxel_size)['state_dict'])
    emb_nn.eval()

    with torch.no_grad():
        eachfile='/scratch/shengyu/masterthesis/codes/devnet/dump/2/tgt_prune.ply'
        pcd=o3d.io.read_point_cloud(eachfile)
        pcd=np.asarray(pcd.points)

        # voxelization
        sel=ME.utils.sparse_quantize(pcd/voxel_size,return_index=True)
        xyz=torch.from_numpy(np.floor(pcd[sel]/voxel_size)).int()
        feats=torch.ones((xyz.size()[0],1))
        indice=torch.zeros((xyz.size()[0],1)).int()
        coords=torch.cat((indice,xyz),1)

        # predict
        stensor=ME.SparseTensor(feats,coords=coords)
        
        embeddings=emb_nn(stensor).F.cpu().numpy()
        data=dict()
        data['coords']=pcd[sel]
        data['feats']=embeddings

        assert data['coords'].shape[0]==data['feats'].shape[0]

        torch.save(data,'dump/2/tgt_prune.pth')