###############################################################
class SuperGlueLoss(BaseLoss):
    def __init__(self,args):
        BaseLoss.__init__(self, args)
        self.w_trans=args['loss']['w_trans']
        self.w_neg_likelihood=args['loss']['w_neg_likelihood']

    def init_loss_terms(self):
        loss=dict()
        loss['trans_loss']=0.
        loss['rot_loss']=0.
        loss['total_loss']=0.
        loss['nll_loss']=0.
        loss['pos_loss']=0.
        loss['neg_loss']=0.
        return loss
    

    def evaluate(self,*input):
        rot_est = input[0]  # Bx3x3
        trans_est = input[1]  # Bx3x1
        rot_gt = input[2] 
        trans_gt = input[3]
        log_perm_pred = input[4] #B x (N+1) x (N+1)
        perm_mat = input[5] # B X (N+1) X (N+1), ground truth

        # rigid motion loss 
        rot_loss, trans_loss = self._rigid_motion_loss(rot_est,rot_gt,trans_est,trans_gt)
        nll_loss,_,pos_loss,neg_loss = self._neg_log_likelihood(log_perm_pred,perm_mat)

        loss=self.init_loss_terms()
        loss['rot_loss']=rot_loss
        loss['trans_loss']=trans_loss
        loss['nll_loss']=nll_loss
        loss['pos_loss']=pos_loss
        loss['neg_loss']=neg_loss
        loss['total_loss']= (rot_loss+trans_loss)*self.w_trans+nll_loss*self.w_neg_likelihood
        return loss