import torch
# from gesvd import GESVD
# svd = GESVD()
import torch.nn as nn
from lib.util import quat2mat,_EPS
import math
import numpy as np

class MLPHead(nn.Module):
    def __init__(self, args):
        super(MLPHead, self).__init__()
        n_emb_dims = args.n_emb_dims
        self.n_emb_dims = n_emb_dims
        self.nn = nn.Sequential(nn.Linear(n_emb_dims*2, n_emb_dims//2),
                                nn.BatchNorm1d(n_emb_dims//2),
                                nn.ReLU(),
                                nn.Linear(n_emb_dims//2, n_emb_dims//4),
                                nn.BatchNorm1d(n_emb_dims//4),
                                nn.ReLU(),
                                nn.Linear(n_emb_dims//4, n_emb_dims//8),
                                nn.BatchNorm1d(n_emb_dims//8),
                                nn.ReLU())
        self.proj_rot = nn.Linear(n_emb_dims//8, 4)
        self.proj_trans = nn.Linear(n_emb_dims//8, 3)

    def forward(self, *input):
        src_embedding = input[0]
        tgt_embedding = input[1]
        embedding = torch.cat((src_embedding, tgt_embedding), dim=1)
        embedding = self.nn(embedding.max(dim=-1)[0])
        rotation = self.proj_rot(embedding)
        rotation = rotation / torch.norm(rotation, p=2, dim=1, keepdim=True)
        translation = self.proj_trans(embedding)
        return quat2mat(rotation), translation



class SVDHead(nn.Module):
    """
    Torch differentiable implementation of the weighted Kabsch algorithm (https://en.wikipedia.org/wiki/Kabsch_algorithm). Based on the correspondences and weights calculates
    the optimal rotation matrix in the sense of the Frobenius norm (RMSD), based on the estimate rotation matrix is then estimates the translation vector hence solving
    the Procrustes problem. This implementation supports batch inputs.

    Args:
        src            (torch array): points of the first point cloud [b,n,3]
        src_corr       (torch array): correspondences for the PC1 established in the feature space [b,n,3]
        weights        (torch array): weights denoting if the coorespondence is an inlier (~1) or an outlier (~0) [b,n]

    Returns:
        rot_matrices  (torch array): estimated rotation matrices [b,3,3]
        trans_vectors (torch array): estimated translation vectors [b,3,1]
        res           (torch array): pointwise residuals (Eucledean distance) [b,n]
        valid_gradient (bool): Flag denoting if the SVD computation converged (gradient is valid)
    """

    def __init__(self, args):
        super(SVDHead, self).__init__()
        self.svd=args['model']['svd']

    def _transformation_residuals(self,src, src_corr, R, t):
        """
        Computer the pointwise residuals based on the estimated transformation paramaters
        
        Args:
            src  (torch array): points of the first point cloud [b,n,3]
            src_corr  (torch array): points of the second point cloud [b,n,3]
            R   (torch array): estimated rotation matrice [b,3,3]
            t   (torch array): estimated translation vectors [b,3,1]
        Returns:
            res (torch array): pointwise residuals (Eucledean distance) [b,n]
        """
        src_corr_reconstruct = torch.matmul(R, src.transpose(1, 2)) + t 

        res = torch.norm(src_corr_reconstruct.transpose(1, 2) - src_corr, dim=2)

        return res
        
    def forward(self, *input):
        src=input[0]
        src_corr=input[1]
        weights=input[2]

        
        weights = weights.unsqueeze(2)
        src_mean = torch.matmul(weights.transpose(1,2), src) / (torch.sum(weights, dim=1).unsqueeze(1)+_EPS) # numerical safety
        src_corr_mean = torch.matmul(weights.transpose(1,2), src_corr) / (torch.sum(weights, dim=1).unsqueeze(1)+_EPS)

        src_centered = src - src_mean
        src_corr_centered = src_corr - src_corr_mean

        weight_matrix = torch.diag_embed(weights.squeeze(2))

        cov_mat = torch.matmul(src_centered.transpose(1, 2),torch.matmul(weight_matrix, src_corr_centered))
        u,v=[],[]
        n_samples=cov_mat.size(0)
        n_invalid = 0
        cuu,cvv=torch.eye(3,device='cuda'),torch.eye(3,device='cuda')
        for i in range(n_samples): # use for loop to avoid consider the entire batch as invalid
            try:
                cu, _, cv = torch.svd(cov_mat[i])
                u.append(cu)
                v.append(cv)
            except Exception as e: # SVD not converge error
                n_invalid+=1
                u.append(cuu)
                v.append(cvv)
            
        u=torch.stack(u,dim=0).cuda()
        v=torch.stack(v,dim=0).cuda()

        tm_determinant = torch.det(torch.matmul(v.transpose(1, 2), u.transpose(1, 2)))

        determinant_matrix = torch.diag_embed(torch.cat((torch.ones((tm_determinant.shape[0],2),device='cuda'), tm_determinant.unsqueeze(1)), 1))

        rotation_matrix = torch.matmul(v,torch.matmul(determinant_matrix,u.transpose(1,2)))

        # translation vector
        translation_matrix = src_corr_mean.transpose(1,2) - torch.matmul(rotation_matrix,src_mean.transpose(1,2))

        # Residuals
        res = self._transformation_residuals(src, src_corr, rotation_matrix, translation_matrix)

        if(n_invalid>0):
            print('%d/%d' %(n_invalid,n_samples))

        return rotation_matrix, translation_matrix, res