#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os,sys
import math
import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from models.feat_extract.transformer import Transformer, Identity
from models.feat_extract.dgcnn import DGCNN
from models.procrustes import SVDHead, MLPHead
from models.filter.ltfgc import LTFGCBlock
from lib.util import transform_pcd
# torch.autograd.set_detect_anomaly(True)


class PositionEncoder(nn.Module):
    """
    Explicit positional encoding
    Conv+IN+ReLU except the last conv
    """
    def __init__(self):
        super(PositionEncoder,self).__init__()

        self.conv = nn.Sequential(
                nn.Conv2d(3, 32, kernel_size=1),
                nn.InstanceNorm2d(32),
                nn.ReLU(),
                nn.Conv2d(32, 64, kernel_size=1),
                nn.InstanceNorm2d(64),
                nn.ReLU(),
                nn.Conv2d(64, 128, kernel_size=1),
                nn.InstanceNorm2d(128),
                nn.ReLU(),
                nn.Conv2d(128, 64, kernel_size=1),
                nn.InstanceNorm2d(64),
                nn.ReLU(),
                nn.Conv2d(64, 32, kernel_size=1)
                )

    def forward(self, kpts, normalise_feat=True):
        '''
        kpts: B x 3 x N x 1
        return: B x 32 x N
        '''
        # center the point cloud 
        center_kpts=torch.mean(kpts,2,keepdim=True)
        kpts = kpts - center_kpts

        feats=torch.squeeze(self.conv(kpts),3)

        if(normalise_feat):# normalize the features
            feats=feats / torch.norm(feats,dim=1,keepdim=True)

        return feats

class TemperatureNet(nn.Module):
    '''
    apply MLP on abs(src_embedding.mean()-tgt_embedding.mean())
    return
        temperature [B,1]
        abs(src_embedding.mean()-tgt_embedding.mean())
    '''

    def __init__(self, args):
        super(TemperatureNet, self).__init__()
        self.n_emb_dims = args['model']['n_emb_dims']
        self.temp_factor = args['model']['temp_factor']
        self.nn = nn.Sequential(nn.Linear(self.n_emb_dims, 128),
                                nn.BatchNorm1d(128),
                                nn.ReLU(),
                                nn.Linear(128, 128),
                                nn.BatchNorm1d(128),
                                nn.ReLU(),
                                nn.Linear(128, 128),
                                nn.BatchNorm1d(128),
                                nn.ReLU(),
                                nn.Linear(128, 1),
                                nn.ReLU())
        self.feature_disparity = None

    def forward(self, *input):
        src_embedding = input[0]
        tgt_embedding = input[1]
        src_embedding = src_embedding.mean(dim=2)
        tgt_embedding = tgt_embedding.mean(dim=2)
        residual = torch.abs(src_embedding-tgt_embedding)

        self.feature_disparity = residual
        temperature=torch.clamp(self.nn(residual), 1.0/self.temp_factor, 1.0*self.temp_factor)
        temperature= torch.ones_like(temperature,requires_grad=False).to(temperature.device)*1e1
        return temperature, residual


class KeyPointNet(nn.Module):
    '''
    based on L2 norm of embedding, pick K keypoints for each fragment
    return
        detected keypoints and their embeddings
    '''

    def __init__(self, num_keypoints):
        super(KeyPointNet, self).__init__()
        self.num_keypoints = num_keypoints

    def forward(self, *input):
        src = input[0]
        src_embedding = input[1]

        _, num_dims, _ = src_embedding.size()
        src_norm = torch.norm(src_embedding, dim=1, keepdim=True)
        src_topk_idx = torch.topk(
            src_norm, k=self.num_keypoints, dim=2, sorted=False)[1]
        src_keypoints_idx = src_topk_idx.repeat(1, 3, 1)
        src_embedding_idx = src_topk_idx.repeat(1, num_dims, 1)

        src_keypoints = torch.gather(src, dim=2, index=src_keypoints_idx)
        src_embedding = torch.gather(
            src_embedding, dim=2, index=src_embedding_idx)

        return src_keypoints,src_embedding


class PRNet(nn.Module):
    '''
    1. Source ——> Feature Embedding ——> Detect Keypoints ——> determine temperature ——> correspondence assignment ——>(filtering network)——> SVD head

    2. estimate transformation from source ——> target and target ——> source

    3. the GPU memory bottleneck is the Transformer module
    '''

    def __init__(self, args):
        super(PRNet, self).__init__()
        self.n_emb_dims = args['model']['n_emb_dims']
        self.num_keypoints = args['model']['n_keypoints']  # 512
        self.cat_sampler = args['model']['cat_sampler']
        self.add_pos_encoder=args['model']['add_pos_encoder']
        self.thred=args['data']['inlier_threshold']

        ###################################
        # co-contexture learning
        ###################################
        if args['model']['attention'] == 'identity':
            self.attention = Identity()
        elif args['model']['attention'] == 'transformer':
            self.attention = Transformer(args=args)
        else:
            raise NotImplementedError

        ###################################
        # positional encoding
        ###################################
        if(self.add_pos_encoder != False):
            self.posencoder = PositionEncoder()
            w_pos_encoding = torch.nn.Parameter(torch.tensor(1.),requires_grad=True)
            self.register_parameter('w_pos_encoding', w_pos_encoding)

        ###################################
        # keypoint detection
        ###################################
        self.keypointnet = KeyPointNet(num_keypoints=self.num_keypoints)
        self.add_keypointnet=args['model']['keypoint_detection']

        ###################################
        # temperature in Gumbel-Softmax
        # initialize temperature with a super small value(1e-3), it could quickly drop to negative values, so we just fix it.  
        ###################################
        self.temp_net = TemperatureNet(args)
        tau = torch.nn.Parameter(torch.tensor(args['model']['tau']),requires_grad=False) 
        self.register_parameter('tau', tau)
        self.temp_factor=args['model']['temp_factor'] # clamp temeprature within [1/temp_factor,temp_factor]

        ###################################
        # filtering network, freeze the parameters
        ###################################
        if(args['model']['filtering_net'] == 'LTFGC'):
            net_channels = args['LTFGC']['net_channels']
            depth_each_stage = args['LTFGC']['net_depth'] // (
                args['LTFGC']['iter_num']+1)
            input_channel = 6 # [x1,y1,z1,x2,y2,z2]
            self.weights_init = LTFGCBlock(
                net_channels, input_channel, depth_each_stage)
            self.weights_iter = [LTFGCBlock(net_channels, 8, depth_each_stage)]
            self.weights_iter = nn.Sequential(*self.weights_iter)
            # for param in self.weights_iter.parameters():  
            #     param.requires_grad = False
            # for param in self.weights_init.parameters():
            #     param.requires_grad = False
        else:
            raise NotImplementedError


        ###################################
        # pose estimation
        ###################################
        if args['model']['head'] == 'mlp':
            self.head = MLPHead(args=args)
        elif args['model']['head'] == 'svd':
            self.head = SVDHead(args=args)
        else:
            raise NotImplementedError

    def _get_correspndence(self,source,target,src_embedding,tgt_embedding,temperature):
        '''
        get correspondence for src and tgt respectively, use softmax / gumbel_softmax for soft/hard assignment 
        '''
        temperature = torch.clamp(temperature, 1.0/self.temp_factor, 1.0*self.temp_factor) # clamp to a safe region
        batch_size, C , num_points = src_embedding.size()
        scores = torch.matmul(src_embedding.transpose(2, 1), tgt_embedding) / math.sqrt(C)
        temperature=temperature.repeat(scores.size())
        scores = F.softmax(scores / temperature, dim=2)

        if self.cat_sampler == 'gumbel_softmax': # one-hot encoding of soft-assignment
             gumbels = -torch.empty_like(scores, memory_format=torch.legacy_contiguous_format).exponential_().log() # ~Gumbel(0,1)
             index = scores.max(-1, keepdim=True)[1]
             y_hard = torch.zeros_like(scores,requires_grad=False).scatter_(-1, index, 1.0)
             scores = y_hard - scores.detach()+scores 

        source_ref = torch.matmul(target,scores.transpose(1,2))
        return source_ref


    def forward(self, *input):
        '''
        Input: 
            src:            B x 3 x N, 
            tgt:            B x 3 x N,
            src_embedding:  B x C x N
            tgt_embedding:   B x C x N

        '''
        src,tgt=input[0],input[1]
        src_embedding, tgt_embedding= input[2],input[3]
        
        batch_size=src.size(0) 
        ##########################################################
        ### explicit positional encoding
        if(self.add_pos_encoder!=False):
            print('Postional encoder added!')
            coords=torch.cat((src,tgt),0).unsqueeze(3) # 2B x 3 x N x 1
            pos_embedding=self.posencoder(coords/4)
            src_embedding_p,tgt_embedding_p=pos_embedding[:batch_size],pos_embedding[batch_size:]

            if(self.add_pos_encoder =='cat'): # two modes, catenate the positional encoding or add it
                src_embedding=torch.cat((src_embedding,src_embedding_p),1)
                tgt_embedding=torch.cat((tgt_embedding,tgt_embedding_p),1)
            elif(self.add_pos_encoder == 'add'):
                src_embedding = src_embedding + src_embedding_p * self.w_pos_encoding
                tgt_embedding = tgt_embedding + tgt_embedding_p * self.w_pos_encoding
            else:
                raise NotImplementedError

        ##########################################################
        ### asymmetric co-contextual learning
        src_embedding_p, tgt_embedding_p = self.attention(
            src_embedding.transpose(1,2), tgt_embedding.transpose(1,2))

        src_embedding = src_embedding + src_embedding_p.transpose(1,2)
        tgt_embedding = tgt_embedding + tgt_embedding_p.transpose(1,2)

        ##########################################################
        ### detect keypoints
        if(self.add_keypointnet):
            print('Keypoint encoder added!')
            src,src_embedding = self.keypointnet(src,src_embedding)
            tgt,tgt_embedding = self.keypointnet(tgt,tgt_embedding)

        ###########################################################
        #### get feature disparity
        feature_disparity = torch.abs(src_embedding.mean(dim=2)-tgt_embedding.mean(dim=2))
        

        ###########################################################
        #### get correspondence using Softmax / Gumbel_softmax
        source=torch.cat((src,tgt),0) 
        source_embedding = torch.cat((src_embedding, tgt_embedding),0)
        target = torch.cat((tgt,src),0)
        target_embedding = torch.cat((tgt_embedding,src_embedding),0)
        source_ref = self._get_correspndence(source,target,source_embedding,target_embedding,self.tau)


        res_rot,res_trans,res_logits,res_weights = [],[],[],[]
        ############################################################
        #### Use LTFGC to initialize weights
        correspondence = torch.cat((source.unsqueeze(3),source_ref.unsqueeze(3)),1) # normalise the coordinates
        logits,weights = self.weights_init(correspondence)
        rot,trans,residuals= self.head(source.transpose(1,2),source_ref.transpose(1,2),weights)
        res_logits.append(logits),res_weights.append(weights)
        res_rot.append(rot),res_trans.append(trans)


        ############################################################
        #### Use LTFGC to refine weights, here we detach everything to train self.weight_iter model only
        new_correspondences = torch.cat((correspondence.detach(),residuals.detach().unsqueeze(1).unsqueeze(3),weights.detach().unsqueeze(1).unsqueeze(3)),dim=1)
        
        new_logits,new_weights = self.weights_iter(new_correspondences)
        new_rot,new_trans,new_residuals= self.head(source.transpose(1,2),source_ref.detach().transpose(1,2),new_weights)
        res_logits.append(new_logits),res_weights.append(new_weights)
        res_rot.append(new_rot),res_trans.append(new_trans)
    
        return res_rot,res_trans,res_logits,res_weights,source,source_ref,feature_disparity,self.tau