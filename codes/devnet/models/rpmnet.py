import argparse
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math

from lib.util import square_distance, angle_difference, log_perm_to_correspondence
from models.filter.sinkhorn import sinkhorn
from models.procrustes import SVDHead, MLPHead
from models.feat_extract.transformer import Transformer, Identity


def match_features(src_embedding, tgt_embedding, metric='l2'):
    """ Compute pairwise distance between features

    Args:
        src_embedding: (B, J, C)
        tgt_embedding: (B, K, C)
        metric: either 'angle' or 'l2' (squared euclidean)

    Returns:
        Matching matrix (B, J, K). i'th row describes how well the i'th point
         in the src agrees with every point in the ref.
    """
    assert src_embedding.shape[-1] == tgt_embedding.shape[-1]

    if metric == 'l2':
        dist_matrix = square_distance(src_embedding, tgt_embedding)
    elif metric == 'angle':
        src_embedding_norm = src_embedding / (torch.norm(src_embedding, dim=-1, keepdim=True) + _EPS)
        tgt_embedding_norm = tgt_embedding / (torch.norm(tgt_embedding, dim=-1, keepdim=True) + _EPS)

        dist_matrix = angle_difference(src_embedding_norm, tgt_embedding_norm)
    else:
        raise NotImplementedError

    return dist_matrix


class ParameterPredictionNet(nn.Module):
    def __init__(self, weights_dim):
        """PointNet based Parameter prediction network

        Args:
            weights_dim: Number of weights to predict (excluding beta), should be something like[3], or [64, 3], for 3 types of features
        """

        super().__init__()

        self.weights_dim = weights_dim

        # Pointnet
        self.prepool = nn.Sequential(
            nn.Conv1d(4, 64, 1),
            nn.GroupNorm(8, 64),
            nn.ReLU(),

            nn.Conv1d(64, 64, 1),
            nn.GroupNorm(8, 64),
            nn.ReLU(),

            nn.Conv1d(64, 64, 1),
            nn.GroupNorm(8, 64),
            nn.ReLU(),

            nn.Conv1d(64, 128, 1),
            nn.GroupNorm(8, 128),
            nn.ReLU(),

            nn.Conv1d(128, 1024, 1),
            nn.GroupNorm(16, 1024),
            nn.ReLU(),
        )
        self.pooling = nn.AdaptiveMaxPool1d(1)
        self.postpool = nn.Sequential(
            nn.Linear(1024, 512),
            nn.GroupNorm(16, 512),
            nn.ReLU(),

            nn.Linear(512, 256),
            nn.GroupNorm(16, 256),
            nn.ReLU(),

            nn.Linear(256, 2 + np.prod(weights_dim)),
        )


    def forward(self, x):
        """ Returns alpha, beta, and gating_weights (if needed)

        Args:
            x: List containing two point clouds, x[0] = src (B, J, 3), x[1] = ref (B, K, 3)

        Returns:
            beta, alpha, weightings
        """

        src_padded = F.pad(x[0], (0, 1), mode='constant', value=0)
        ref_padded = F.pad(x[1], (0, 1), mode='constant', value=1)
        concatenated = torch.cat([src_padded, ref_padded], dim=1)

        prepool_feat = self.prepool(concatenated.permute(0, 2, 1))
        pooled = torch.flatten(self.pooling(prepool_feat), start_dim=-2)
        raw_weights = self.postpool(pooled)

        beta = F.softplus(raw_weights[:, 0])
        alpha = F.softplus(raw_weights[:, 1])

        return beta, alpha

class RPMNet(nn.Module):
    def __init__(self, args):
        super().__init__()

        self.add_slack = args['model']['add_slack']
        self.num_sk_iter = args['model']['num_sk_iter']
        self.weights_net = ParameterPredictionNet(weights_dim=[0])

        ###################################
        # co-contexture learning
        ###################################
        self.add_attention = args['model']['add_attention']
        if args['model']['attention'] == 'identity':
            self.attention = Identity()
        elif args['model']['attention'] == 'transformer':
            self.attention = Transformer(args=args)
        else:
            raise NotImplementedError
        self.method = args['model']['method']   # cat or add the co-contextual from Transformer

        ###################################
        # pose estimation
        ###################################
        if args['model']['head'] == 'mlp':
            self.head = MLPHead(args=args)
        elif args['model']['head'] == 'svd':
            self.head = SVDHead(args=args)
        else:
            raise Exception('Not implemented')

    def compute_affinity(self, beta, feat_distance, alpha=0.5):
        """Compute logarithm of Initial match matrix values, i.e. log(m_jk)"""
        if isinstance(alpha, float):
            hybrid_affinity = -beta[:, None, None] * (feat_distance - alpha)
        else:
            hybrid_affinity = -beta[:, None, None] * (feat_distance - alpha[:, None, None])
        return hybrid_affinity

    def forward(self, *input):
        """Forward pass for RPMNet
        """
        xyz_src,xyz_ref = input[0],input[1] # B,N,3
        src_embedding, tgt_embedding= input[2],input[3]  # B,N,C

        ##############################################################
        # parameter prediction
        beta, alpha = self.weights_net([xyz_src/4, xyz_ref/4]) # [B],[B]
        print(beta,alpha)

        ##############################################################
        # co-contextual learning and normalize features
        if(self.add_attention):
            src_embedding_p, tgt_embedding_p = self.attention(src_embedding,tgt_embedding)

            if(self.method == 'cat'):
                src_embedding=torch.cat((src_embedding,src_embedding_p),dim=-1)
                tgt_embedding = torch.cat((tgt_embedding,tgt_embedding_p),dim=-1)
            elif(self.method == 'add'):
                src_embedding = src_embedding + src_embedding_p
                tgt_embedding = tgt_embedding + tgt_embedding_p
            else:
                raise NotImplementedError

            src_embedding = src_embedding / torch.norm(src_embedding, dim=-1, keepdim=True)
            tgt_embedding = tgt_embedding / torch.norm(tgt_embedding, dim=-1, keepdim=True)

        ##############################################################     
        # match features, apply sinkhorn
        feat_distance = match_features(src_embedding, tgt_embedding)  # L2 norm distance
        torch.save(feat_distance,'dump/rpmnet/feats.pth')
        affinity = self.compute_affinity(beta, feat_distance, alpha=alpha) # scaled by alpha and beta
        torch.save(affinity,'dump/rpmnet/affinity.pth')
        log_perm_matrix = sinkhorn(affinity, n_iters=self.num_sk_iter, slack=self.add_slack) # sinkhorn in log-space for stability
        perm_matrix = torch.exp(log_perm_matrix[:,:-1,:-1])
        torch.save(perm_matrix,'dump/rpmnet/perm_mat.pth')
        raise Exception('shit')

        ##############################################################
        # get correspondence
        src_corr,weights=log_perm_to_correspondence(log_perm_matrix,0,xyz_ref.transpose(1,2),False)

        ##############################################################
        # weighted SVD for pose estimation
        rotation, translation, _  = self.head(xyz_src, src_corr.transpose(1,2), weights) 

        return log_perm_matrix,rotation,translation,alpha.mean(),beta.mean()