"""
PointPWC-Net model and losses
Author: Wenxuan Wu
Date: May 2020
"""

import torch.nn as nn
import torch
import numpy as np
import torch.nn.functional as F
from models.pointconv_util import PointConv, PointConvD, index_points_group, Conv1d
from models.pointconv_util import index_points_gather as index_points,knn_point
import time
import copy
from models.procrustes import SVDHead


class CostVolume(nn.Module):
    def __init__(self, topk, in_channel, mlp, bn = False, use_leaky = True,LEAKY_RATE=0.1):
        super(CostVolume, self).__init__()
        self.topk = topk
        self.bn = bn
        self.mlp_convs = nn.ModuleList()
        if bn:
            self.mlp_bns = nn.ModuleList()
        last_channel = in_channel
        for out_channel in mlp:
            self.mlp_convs.append(nn.Conv2d(last_channel, out_channel, 1))
            if bn:
                self.mlp_bns.append(nn.BatchNorm2d(out_channel))
            last_channel = out_channel

        self.relu = nn.ReLU(inplace=True) if not use_leaky else nn.LeakyReLU(LEAKY_RATE, inplace=True)


    def forward(self, src_pcd, ref_pcd, src_feats, ref_feats):
        """
        Distance-based selection definitely doesn't work
        Cost Volume layer for confidence Estimation, apply a MLP on feats and diff_coords, reweight with directional vector
        Input:
            src_pcd: input points position data, [B, C, N1]
            ref_pcd: input points position data, [B, C, N2]
            src_feats: input points data, [B, D, N1]
            ref_feats: input points data, [B, D, N2]
        Return:
            new_points: upsample points feature data, [B, D', N1]
        """
        B, C, N1 = src_pcd.shape
        _, _, N2 = ref_pcd.shape
        _, D1, _ = src_feats.shape
        _, D2, _ = ref_feats.shape
        src_pcd = src_pcd.permute(0, 2, 1)
        ref_pcd = ref_pcd.permute(0, 2, 1)
        src_feats = src_feats.permute(0, 2, 1)
        ref_feats = ref_feats.permute(0, 2, 1)

        # for each point in src_pcd, we find the topk neighbor in ref_pcd
        knn_idx = knn_point(self.topk, ref_pcd, src_pcd) 
        neighbor_xyz = index_points_group(ref_pcd, knn_idx)
        direction_xyz = neighbor_xyz - src_pcd.view(B, N1, 1, C)

        grouped_ref_feats = index_points_group(ref_feats, knn_idx) # B, N1, topk, D2
        grouped_src_feats = src_feats.view(B, N1, 1, D1).repeat(1, 1, self.topk, 1)
        new_points = torch.cat([grouped_src_feats, grouped_ref_feats, direction_xyz], dim = -1) # B, N1, topk, D1+D2+3
        new_points = new_points.permute(0, 3, 2, 1) # [B, D1+D2+3, topk, N1]
        for i, conv in enumerate(self.mlp_convs):
            if self.bn:
                bn = self.mlp_bns[i]
                new_points =  self.relu(bn(conv(new_points)))
            else:
                new_points =  self.relu(conv(new_points))

        # weighted sum
        dist = torch.norm(direction_xyz, dim = 3).clamp(min = 1e-10) # B N1 topk
        norm = torch.sum(1.0/dist, dim = 2, keepdim = True) # B N1 1
        weights = (1.0/dist) / norm # B N1 topk

        costVolume = torch.sum(weights.unsqueeze(-1).permute(0, 3, 2, 1) * new_points, dim = 2) # B C N
        return costVolume

class UpsampleConfidence(nn.Module):
    def forward(self, dense_pcd, coarse_pcd, coarse_feats):
        '''
        Upsample the scene feats based on knn search and interpolation
        Input:
            dense_pcd:       dense coords
            coarse_pcd:     coarse coords
            coarse_feats:   coarse feats / coarse feats
        '''
        B, C, N = dense_pcd.shape
        _, _, S = coarse_pcd.shape

        dense_pcd = dense_pcd.permute(0, 2, 1) # B N 3
        coarse_pcd = coarse_pcd.permute(0, 2, 1) # B S 3
        coarse_feats = coarse_feats.permute(0, 2, 1) # B S 3
        knn_idx = knn_point(3, coarse_pcd, dense_pcd)
        grouped_dense_pcd_norm = index_points_group(coarse_pcd, knn_idx) - dense_pcd.view(B, N, 1, C)
        dist = torch.norm(grouped_dense_pcd_norm, dim = 3).clamp(min = 1e-10)
        norm = torch.sum(1.0 / dist, dim = 2, keepdim = True)
        weight = (1.0 / dist) / norm 

        grouped_feats = index_points_group(coarse_feats, knn_idx)
        dense_feats = torch.sum(weight.view(B, N, 3, 1) * grouped_feats, dim = 2).permute(0, 2, 1)
        return dense_feats 


class WarpPoints(nn.Module):
    def __init__(self,args):
        super(WarpPoints, self).__init__()
        self.head = SVDHead(args)

    def forward(self, src_pcd, ref_pcd, src_feats, ref_feats, src_confidence):
        '''
        Get the score matrix from embeddings, then use softmax to get correspondence, finally together with confidence, we run svd to get the pose, then apply the transformation to coordinates.
        Input:  
            src_pcd:        [B,3,N]
            tgt_pcd:        [B,3,N]
            src_feats:      [B,C,N]
            ref_feats:      [B,C,N]
            src_confidence: [B,1,N]
        '''

        # Get correspondence
        scores=torch.matmul(src_feats.transpose(1,2),ref_feats)
        scores = F.softmax(scores, dim=2)

        index = scores.max(-1, keepdim=True)[1]
        y_hard = torch.zeros_like(scores,requires_grad=False).scatter_(-1, index, 1.0)

        src_pcd_ref = torch.matmul(ref_pcd,y_hard.transpose(1,2))

        # Estimate the pose
        weights=src_confidence.squeeze(1)
        rot,trans,new_residuals=self.head(src_pcd.transpose(1,2),src_pcd_ref.transpose(1,2),weights)

        # get warped src_pcd
        src_pcd_trans=torch.matmul(rot,src_pcd)+trans

        # return src_pcd_trans,rot.detach(),trans.detach()

        return src_pcd,rot.detach(),trans.detach()


class EstimateOverlapConfidence(nn.Module):
    def __init__(self, feat_channel, cost_channel, confidence_channel = 1, pconv = [128, 128], mlp = [128, 64], neighbors = 9, use_leaky = True):
        super(EstimateOverlapConfidence, self).__init__()
        self.use_leaky = use_leaky
        self.pointconv_list = nn.ModuleList()
        last_channel = feat_channel + cost_channel + confidence_channel

        for _, ch_out in enumerate(pconv):
            pointconv = PointConv(neighbors, last_channel + 3, ch_out, bn = True, use_leaky = True)
            self.pointconv_list.append(pointconv)
            last_channel = ch_out 
        
        self.mlp_convs = nn.ModuleList()
        for _, ch_out in enumerate(mlp):
            self.mlp_convs.append(Conv1d(last_channel, ch_out))
            last_channel = ch_out

        # transform to be within [0,1]
        self.fc =nn.Conv1d(last_channel, 1, 1)


    def forward(self, xyz, feats, cost_volume, confidence = None):
        '''
        Given the input coordinates, features, cost_volumn, and confidence of being overlapped, 
        Estimate new confidence and features
        Input:
            xyz:            [B,3,N]  
            feats:          [B,C1,N]
            cost_volume:    [B,C2,N]
            confidence:     [B,1,N]
        Return:
            new_feats:      [B,C3,N]
            confidence:     [B,1,N]
        '''
        if confidence is None:
            new_feats = torch.cat([feats, cost_volume], dim = 1)
        else:
            new_feats = torch.cat([feats, cost_volume, confidence], dim = 1)

        for _, pointconv in enumerate(self.pointconv_list):
            new_feats = pointconv(xyz, new_feats)

        for conv in self.mlp_convs:
            new_feats = conv(new_feats)

        # confidence = torch.relu(torch.tanh(self.fc(new_feats)))
        confidence = self.fc(new_feats)
        return new_feats, confidence



class PointConvRegistration(nn.Module):
    def __init__(self,args):
        super(PointConvRegistration, self).__init__()
        self.reg_neighbor = args['model']['reg_neighbor']
        self.feat_neighbor = args['model']['feat_neighbor']
        self.n_points=args['model']['n_points']

        #l0: 8192
        self.level0 = Conv1d(3, 32)
        self.level0_1 = Conv1d(32, 32)
        self.cost0 = CostVolume(self.reg_neighbor, 32 + 32 + 32 + 32 + 3, [32, 32])
        self.confidence0 = EstimateOverlapConfidence(32 + 64, 32)
        self.level0_2 = Conv1d(32, 64)

        #l1: 2048
        self.level1 = PointConvD(self.n_points[0], self.feat_neighbor, 64 + 3, 64)
        self.cost1 = CostVolume(self.reg_neighbor, 64 + 32 + 64 + 32 + 3, [64, 64])
        self.confidence1 = EstimateOverlapConfidence(64 + 64, 64)
        self.level1_0 = Conv1d(64, 64)
        self.level1_1 = Conv1d(64, 128)

        #l2: 512
        self.level2 = PointConvD(self.n_points[1], self.feat_neighbor, 128 + 3, 128)
        self.cost2 = CostVolume(self.reg_neighbor, 128 + 64 + 128 + 64 + 3, [128, 128])
        self.confidence2 = EstimateOverlapConfidence(128 + 64, 128)
        self.level2_0 = Conv1d(128, 128)
        self.level2_1 = Conv1d(128, 256)

        #l3: 256
        self.level3 = PointConvD(self.n_points[2], self.feat_neighbor, 256 + 3, 256)
        self.cost3 = CostVolume(self.reg_neighbor, 256 + 64 + 256 + 64 + 3, [256, 256])
        self.confidence3 = EstimateOverlapConfidence(256, 256, confidence_channel=0)
        self.level3_0 = Conv1d(256, 256)
        self.level3_1 = Conv1d(256, 512)

        #l4: 64
        self.level4 = PointConvD(self.n_points[3], self.feat_neighbor, 512 + 3, 256)

        #deconv
        self.deconv4_3 = Conv1d(256, 64)
        self.deconv3_2 = Conv1d(256, 64)
        self.deconv2_1 = Conv1d(128, 32)
        self.deconv1_0 = Conv1d(64, 32)

        #warping
        self.warping = WarpPoints(args)

        #upsample
        self.upsample = UpsampleConfidence()

    def forward(self, src_pcd_l0, ref_pcd_l0,rot,trans):
        """
        Input:  
            src_pcd_l0:   [B,3,N]
            ref_pcd_l0:   [B,3,N]   
            rot:          [B,3,3]
            trans:        [B,3,1]
        """
        src_feat=copy.deepcopy(src_pcd_l0)
        ref_feat=copy.deepcopy(ref_pcd_l0)

        ##########################################
        # Down sample
        ##########################################
        #l0   8192 
        # 1D Conv: 3 -- 32 -- 32 -- 64
        src_feat_l0 = self.level0(src_feat)
        src_feat_l0 = self.level0_1(src_feat_l0)
        src_feat_l0_1 = self.level0_2(src_feat_l0)

        ref_feat_l0 = self.level0(ref_feat)
        ref_feat_l0 = self.level0_1(ref_feat_l0)
        ref_feat_l0_1 = self.level0_2(ref_feat_l0)

        # l1 2048
        # PointConv followed by two 1D conv
        src_pcd_l1, src_feat_l1, fps_src_pcd_l1 = self.level1(src_pcd_l0, src_feat_l0_1)
        src_feat_l1_2 = self.level1_0(src_feat_l1)
        src_feat_l1_2 = self.level1_1(src_feat_l1_2)

        ref_pcd_l1, ref_feat_l1, fps_ref_pcd_l1 = self.level1(ref_pcd_l0, ref_feat_l0_1)
        ref_feat_l1_2 = self.level1_0(ref_feat_l1)
        ref_feat_l1_2 = self.level1_1(ref_feat_l1_2)

        #l2 512
        # PointConv followed by two 1D conv
        src_pcd_l2, src_feat_l2, fps_src_pcd_l2 = self.level2(src_pcd_l1, src_feat_l1_2)
        src_feat_l2_3 = self.level2_0(src_feat_l2)
        src_feat_l2_3 = self.level2_1(src_feat_l2_3)

        ref_pcd_l2, ref_feat_l2, fps_ref_pcd_l2 = self.level2(ref_pcd_l1, ref_feat_l1_2)
        ref_feat_l2_3 = self.level2_0(ref_feat_l2)
        ref_feat_l2_3 = self.level2_1(ref_feat_l2_3)

        #l3 256
        # PointConv followed by two 1D conv
        src_pcd_l3, src_feat_l3, fps_src_pcd_l3 = self.level3(src_pcd_l2, src_feat_l2_3)
        src_feat_l3_4 = self.level3_0(src_feat_l3)
        src_feat_l3_4 = self.level3_1(src_feat_l3_4)

        ref_pcd_l3, ref_feat_l3, fps_ref_pcd_l3 = self.level3(ref_pcd_l2, ref_feat_l2_3)
        ref_feat_l3_4 = self.level3_0(ref_feat_l3)
        ref_feat_l3_4 = self.level3_1(ref_feat_l3_4)

        #l4 64
        # PointConv, then upsamples the feats to l3
        src_pcd_l4, src_feat_l4, _ = self.level4(src_pcd_l3, src_feat_l3_4)
        ref_pcd_l4, ref_feat_l4, _ = self.level4(ref_pcd_l3, ref_feat_l3_4)

        ##########################################################
        # upsample from l4 to l3, start with zero confidence estimation
        ##########################################################
        src_feat_l4_3 = self.upsample(src_pcd_l3, src_pcd_l4, src_feat_l4)
        src_feat_l4_3 = self.deconv4_3(src_feat_l4_3)
        ref_feat_l4_3 = self.upsample(ref_pcd_l3, ref_pcd_l4, ref_feat_l4)
        ref_feat_l4_3 = self.deconv4_3(ref_feat_l4_3)

        c_src_feat_l3 = torch.cat([src_feat_l3, src_feat_l4_3], dim = 1)
        c_ref_feat_l3 = torch.cat([ref_feat_l3, ref_feat_l4_3], dim = 1)

        # cost3=self.cost3(src_pcd_l3, ref_pcd_l3, c_src_feat_l3, c_ref_feat_l3)
        cost3 = self.cost3(torch.matmul(rot,src_pcd_l3)+trans, ref_pcd_l3, c_src_feat_l3, c_ref_feat_l3)
        feat3, confidence3 = self.confidence3(src_pcd_l3, src_feat_l3, cost3)


        ##########################################################
        # upsample from l3 to l2, upsample both features and confidences,
        # also warp the source point cloud
        ##########################################################
        src_feat_l3_2 = self.upsample(src_pcd_l2, src_pcd_l3, src_feat_l3)
        src_feat_l3_2 = self.deconv3_2(src_feat_l3_2)
        ref_feat_l3_2 = self.upsample(ref_pcd_l2, ref_pcd_l3, ref_feat_l3)
        ref_feat_l3_2 = self.deconv3_2(ref_feat_l3_2)

        c_src_feat_l2 = torch.cat([src_feat_l2, src_feat_l3_2], dim = 1)
        c_ref_feat_l2 = torch.cat([ref_feat_l2, ref_feat_l3_2], dim = 1)

        up_confidence2 = self.upsample(src_pcd_l2, src_pcd_l3,  confidence3)
        src_pcd_l2_warp,rot2,trans2 = self.warping(src_pcd_l2, ref_pcd_l2, c_src_feat_l2,c_ref_feat_l2,up_confidence2)
        # cost2 = self.cost2(src_pcd_l2_warp, ref_pcd_l2, c_src_feat_l2, c_ref_feat_l2)
        cost2=self.cost2(torch.matmul(rot,src_pcd_l2)+trans,ref_pcd_l2, c_src_feat_l2,c_ref_feat_l2)

        feat3_up = self.upsample(src_pcd_l2, src_pcd_l3, feat3)
        new_src_feat_l2 = torch.cat([src_feat_l2, feat3_up], dim = 1)
        feat2, confidence2 = self.confidence2(src_pcd_l2, new_src_feat_l2, cost2, up_confidence2)


        ##########################################################
        # upsample from l2 to l1, upsample both features and confidences,
        # also warp the source point cloud
        ##########################################################
        src_feat_l2_1 = self.upsample(src_pcd_l1, src_pcd_l2, src_feat_l2)
        src_feat_l2_1 = self.deconv2_1(src_feat_l2_1)
        ref_feat_l2_1 = self.upsample(ref_pcd_l1, ref_pcd_l2, ref_feat_l2)
        ref_feat_l2_1 = self.deconv2_1(ref_feat_l2_1)

        c_src_feat_l1 = torch.cat([src_feat_l1, src_feat_l2_1], dim = 1)
        c_ref_feat_l1 = torch.cat([ref_feat_l1, ref_feat_l2_1], dim = 1)

        up_confidence1 = self.upsample(src_pcd_l1, src_pcd_l2,  confidence2)
        src_pcd_l1_warp,rot1,trans1 = self.warping(src_pcd_l1, ref_pcd_l1, c_src_feat_l1,c_ref_feat_l1,up_confidence1)
        # cost1 = self.cost1(src_pcd_l1_warp, ref_pcd_l1, c_src_feat_l1, c_ref_feat_l1)
        cost1=self.cost1(torch.matmul(rot,src_pcd_l1)+trans,ref_pcd_l1, c_src_feat_l1, c_ref_feat_l1)

        feat2_up = self.upsample(src_pcd_l1, src_pcd_l2, feat2)
        new_src_feat_l1 = torch.cat([src_feat_l1, feat2_up], dim = 1)
        feat1, confidence1 = self.confidence1(src_pcd_l1, new_src_feat_l1, cost1, up_confidence1)

        ##########################################################
        # upsample from l1 to l0, upsample both features and confidences,
        # also warp the source point cloud
        ##########################################################
        src_feat_l1_0 = self.upsample(src_pcd_l0, src_pcd_l1, src_feat_l1)
        src_feat_l1_0 = self.deconv1_0(src_feat_l1_0)
        ref_feat_l1_0 = self.upsample(ref_pcd_l0, ref_pcd_l1, ref_feat_l1)
        ref_feat_l1_0 = self.deconv1_0(ref_feat_l1_0)

        c_src_feat_l0 = torch.cat([src_feat_l0, src_feat_l1_0], dim = 1)
        c_ref_feat_l0 = torch.cat([ref_feat_l0, ref_feat_l1_0], dim = 1)

        up_confidence0 = self.upsample(src_pcd_l0, src_pcd_l1,  confidence1)
        src_pcd_l0_warp,rot0,trans0 = self.warping(src_pcd_l0, ref_pcd_l0, c_src_feat_l0,c_ref_feat_l0,up_confidence0)
        # cost0 = self.cost0(src_pcd_l0_warp, ref_pcd_l0, c_src_feat_l0, c_ref_feat_l0)
        cost0 = self.cost0(torch.matmul(rot,src_pcd_l0)+trans,ref_pcd_l0, c_src_feat_l0, c_ref_feat_l0)

        feat1_up = self.upsample(src_pcd_l0, src_pcd_l1, feat1)
        new_src_feat_l0 = torch.cat([src_feat_l0, feat1_up], dim = 1)
        feat0, confidence0 = self.confidence0(src_pcd_l0, new_src_feat_l0, cost0, up_confidence0)


        # conclude
        confidences = [confidence0, confidence1, confidence2, confidence3]
        fps_src_pcd_idxs = [fps_src_pcd_l1, fps_src_pcd_l2, fps_src_pcd_l3]
        rotations=[rot0,rot1,rot2]
        translations=[trans0,trans1,trans2]
        src_pcd = [src_pcd_l0, src_pcd_l1, src_pcd_l2, src_pcd_l3]
        ref_pcd = [ref_pcd_l0, ref_pcd_l1, ref_pcd_l2, ref_pcd_l3]

        fps_ref_pcd_idxs = [fps_ref_pcd_l1, fps_ref_pcd_l2, fps_ref_pcd_l3]

        return confidences, fps_src_pcd_idxs,rotations,translations
