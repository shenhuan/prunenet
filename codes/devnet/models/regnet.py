import torch
import torch.nn as nn
import torch.nn.functional as F

class PointNet(nn.Module):
    def __init__(self):
        '''
        Overlap regression network
        '''
        super().__init__()

        # Pointnet
        self.conv1 = nn.Conv1d(3, 64, kernel_size=1, bias=False)
        self.conv2 = nn.Conv1d(64, 64, kernel_size=1, bias=False)
        self.conv3 = nn.Conv1d(64, 64, kernel_size=1, bias=False)
        self.conv4 = nn.Conv1d(64, 128, kernel_size=1, bias=False)
        self.conv5 = nn.Conv1d(128, 1024, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(64)
        self.bn3 = nn.BatchNorm1d(64)
        self.bn4 = nn.BatchNorm1d(128)
        self.bn5 = nn.BatchNorm1d(1024)
        self.pooling = nn.AdaptiveMaxPool1d(1)

    def forward(self,pcd):
        output=F.relu(self.bn1(self.conv1(pcd)))
        output=F.relu(self.bn2(self.conv2(output)))
        output=F.relu(self.bn3(self.conv3(output)))
        output=F.relu(self.bn4(self.conv4(output)))
        output=F.relu(self.bn5(self.conv5(output)))

        return self.pooling(output)

class OverlapRegression(nn.Module):
    def __init__(self,args):
        super().__init__()

        self.pointnet=PointNet()

        self.fc1 = nn.Linear(2048,1024)
        self.fc2 = nn.Linear(1024,1024)
        self.fc3 = nn.Linear(1024,512)
        self.fc4 = nn.Linear(512,512)
        self.fc5 = nn.Linear(512,256)
        self.fc6 = nn.Linear(256,1)

        self.bn1= nn.BatchNorm1d(1024)
        self.bn2= nn.BatchNorm1d(1024)
        self.bn3= nn.BatchNorm1d(512)
        self.bn4= nn.BatchNorm1d(512)
        self.bn5= nn.BatchNorm1d(256)

    
    def forward(self, pcd1, pcd2):
        feat1=self.pointnet(pcd1)
        feat2=self.pointnet(pcd2)
        feats=torch.cat((feat1,feat2),dim=1).squeeze(2)
        output=F.relu(self.bn1(self.fc1(feats)))
        output=F.relu(self.bn2(self.fc2(output)))
        output=F.relu(self.bn3(self.fc3(output)))
        output=F.relu(self.bn4(self.fc4(output)))
        output=F.relu(self.bn5(self.fc5(output)))

        return torch.relu(torch.tanh(self.fc6(output)))