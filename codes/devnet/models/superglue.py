# %BANNER_BEGIN%
# ---------------------------------------------------------------------
# %COPYRIGHT_BEGIN%
#
#  Magic Leap, Inc. ("COMPANY") CONFIDENTIAL
#
#  Unpublished Copyright (c) 2020
#  Magic Leap, Inc., All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains the property
# of COMPANY. The intellectual and technical concepts contained herein
# are proprietary to COMPANY and may be covered by U.S. and Foreign
# Patents, patents in process, and are protected by trade secret or
# copyright law.  Dissemination of this information or reproduction of
# this material is strictly forbidden unless prior written permission is
# obtained from COMPANY.  Access to the source code contained herein is
# hereby forbidden to anyone except current COMPANY employees, managers
# or contractors who have executed Confidentiality and Non-disclosure
# agreements explicitly covering such access.
#
# The copyright notice above does not evidence any actual or intended
# publication or disclosure  of  this source code, which includes
# information that is confidential and/or proprietary, and is a trade
# secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION,
# PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS
# SOURCE CODE  WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS
# STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
# INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE
# CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
# TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE,
# USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
#
# %COPYRIGHT_END%
# ----------------------------------------------------------------------
# %AUTHORS_BEGIN%
#
#  Originating Authors: Paul-Edouard Sarlin
#
# %AUTHORS_END%
# --------------------------------------------------------------------*/
# %BANNER_END%
import torch
from torch import nn
from models.feat_extract.multiplexgnn import MLP,AttentionalGNN
from models.filter.sinkhorn import log_optimal_transport
from models.procrustes import SVDHead,MLPHead
from lib.util import log_perm_to_correspondence
import glob
torch.autograd.set_detect_anomaly(True)



class KeypointEncoder(nn.Module):
    """ Joint encoding of visual appearance and location using MLPs"""
    def __init__(self, feature_dim, layers):
        super().__init__()
        self.encoder = MLP([3] + layers + [feature_dim])
        nn.init.constant_(self.encoder[-1].bias, 0.0)

    def forward(self, kpts):
        return self.encoder(kpts)


class SuperGlue(nn.Module):
    """SuperGlue feature matching middle-end

    Given two sets of keypoints and locations, we determine the
    correspondences by:
      1. Keypoint Encoding (normalization + visual feature and location fusion)
      2. Graph Neural Network with multiple self and cross-attention layers
      3. Final projection layer
      4. Optimal Transport Layer (a differentiable Hungarian matching algorithm)
      5. Thresholding matrix based on mutual exclusivity and a match_threshold

    The correspondence ids use -1 to indicate non-matching points.

    Paul-Edouard Sarlin, Daniel DeTone, Tomasz Malisiewicz, and Andrew
    Rabinovich. SuperGlue: Learning Feature Matching with Graph Neural
    Networks. In CVPR, 2020. https://arxiv.org/abs/1911.11763

    """

    def __init__(self, args):
        super().__init__()
        self.n_emb_dims=args['model']['descriptor_dim']
        self.n_sinkhorn=args['model']['sinkhorn_iterations']
        self.match_threshold = args['model']['match_threshold']

        self.kenc = KeypointEncoder(self.n_emb_dims, args['model']['keypoint_encoder'])

        self.gnn = AttentionalGNN(self.n_emb_dims, args['model']['GNN_layers']*args['model']['n_GNN_layers'])

        self.final_proj = nn.Conv1d(self.n_emb_dims, self.n_emb_dims,kernel_size=1, bias=True)

        bin_score = torch.nn.Parameter(torch.tensor(2.)) # the value from the pre-trained model is 2.3457
        self.register_parameter('bin_score', bin_score)

        if args['model']['head'] == 'mlp':
            self.head = MLPHead(args=args)
        elif args['model']['head'] == 'svd':
            self.head = SVDHead(args=args)
        else:
            raise Exception('Not implemented')

    def forward(self, *inputs):
        '''
        Input:
            xyz_src:        [B,3,N]
            src_embedding:  [B,C,N]
        '''
        """Run SuperGlue on a pair of keypoints and descriptors"""
        xyz_src,xyz_ref = inputs[0],inputs[1] # B,3,N
        src_embedding, tgt_embedding= inputs[2],inputs[3]  # B,C,N

        # scores1=torch.matmul(src_embedding.transpose(1,2),tgt_embedding)

        #######################################
        # Keypoint MLP encoder.
        src_embedding = src_embedding + self.kenc(xyz_src/4) # normalize keypoints and encode it
        tgt_embedding = tgt_embedding + self.kenc(xyz_ref/4)

        #######################################
        # Multi-layer Transformer network.
        src_embedding, tgt_embedding = self.gnn(src_embedding, tgt_embedding)
    
        #######################################
        # Final MLP projection to combine features from 4 heads
        src_embedding, tgt_embedding = self.final_proj(src_embedding), self.final_proj(tgt_embedding)

        #######################################
        # match and run optimal transport 
        scores = torch.einsum('bdn,bdm->bnm', src_embedding, tgt_embedding)
        scores = scores / self.n_emb_dims**.5

        # scores2=scores
        log_perm_matrix = log_optimal_transport(scores, self.bin_score,iters=self.n_sinkhorn) 

        # scores3=log_perm_matrix.exp()

        # data=dict()
        # data['fcgf']=scores1.cpu().detach()
        # data['gnn']=scores2.cpu().detach()
        # data['sinkhorn']=scores3.cpu().detach()
        # nn=len(glob.glob('dump/eval_feats/*.pth'))
        # torch.save(data,f'dump/eval_feats/{nn}.pth')


        #######################################
        # get correspondence and weights
        src_corr,weights=log_perm_to_correspondence(log_perm_matrix,self.match_threshold,xyz_ref,threshold=True)

        ########################################
        # weighted SVD for pose estimation
        rotation, translation, _  = self.head(xyz_src.transpose(1,2), src_corr.transpose(1,2), weights) 

        return rotation,translation,log_perm_matrix,self.bin_score
