import os,sys
import math
import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from models import load_model
from models.prnet import PRNet
from models.rpmnet import RPMNet
from models.superglue import SuperGlue
from models.mixnet import MixNet
from models.ltfgc import LTFGC
from models.oanet import OANet
from models.hpr import PointConvRegistration
from models.regnet import OverlapRegression
import MinkowskiEngine as ME

class Engine(nn.Module):
    '''
    A wrapper of PRNet/RPMNet/SuperGlue
    '''
    def __init__(self, args):
        super(Engine, self).__init__()
        self.voxel_size = args['data']['voxel_size']
        self.n_points = args['model']['n_input_points'] # 1536


        #################################
        # primary method
        #################################
        if(args['method']=='prnet'): 
            self.model = PRNet(args)
        elif(args['method']=='rpmnet'):
            self.model=RPMNet(args)
        elif(args['method']=='superglue'):
            self.model=SuperGlue(args)
        elif(args['method']=='mixnet'):
            self.model=MixNet(args)
        elif(args['method']=='ltfgc'):
            self.model=LTFGC(args)
        elif(args['method']=='oanet'):
            self.model=OANet(args)
        elif(args['method']=='hpr'):
            self.model=PointConvRegistration(args)
        elif(args['method']=='overlapreg'):
            self.model=OverlapRegression(args)
        else:
            raise NotImplementedError
    

        if(args['checkpoint']['restore']): # restore the pre-trained model
            self.load(args['checkpoint']['checkpoint'])
        if torch.cuda.device_count() > 1: # data parallel
            self.model = nn.DataParallel(self.model)
            
        #################################
        # feature embedding, here we use pre-trained FCGF and freeze the parameters
        #################################
        # self.embedding = args['model']['emb_nn']
        # if(self.embedding == 'FCGF'): 
        #     Model = load_model('ResUNetBN2C')
        #     self.emb_nn = Model(1, 32, bn_momentum=0.05,
        #                         normalize_feature=True, conv1_kernel_size=7, D=3)
        #     self.emb_nn.load_state_dict(torch.load(
        #         args['misc']['FCGF_checkpoint'])['state_dict'])
        #     for param in self.emb_nn.parameters():  
        #         param.requires_grad = False
        # elif(self.embedding == 'DGCNN'):
        #     self.emb_nn = DGCNN(n_emb_dims=self.n_emb_dims)
        # else:
        #     raise Exception('Not implemented')


    def forward(self, *input):
        return self.model(*input)

    def embed(self,src,tgt,device):
        # '''
        # Use FCGF to embed point-wise features
        # Input:
        #     src: [M,4]
        #     tgt: [N,4]
        # Reture:
        #     src:            [b,3,self.n_points]
        #     src_embedding:  [B,C,self.n_points]
        # '''
        # feats = torch.ones((src.size(0), 1), dtype=torch.float32)
        # src_stensor = ME.SparseTensor(feats, coords=src).to(device)
        # feats = torch.ones((tgt.size(0), 1), dtype=torch.float32)
        # tgt_stensor = ME.SparseTensor(feats, coords=tgt).to(self.device)

        # src_embedding = self.emb_nn(src_stensor)
        # tgt_embedding = self.emb_nn(tgt_stensor)

        # # convert sparse tensor back to normal tensor
        # src, src_embedding = sparse_tensor_to_normal_tensor(src_embedding,self.n_points,self.voxel_size)
        # tgt, tgt_embedding = sparse_tensor_to_normal_tensor(tgt_embedding,self.n_points,self.voxel_size)
        # return src,src_embedding,tgt,tgt_embedding
        pass

    def save(self, path):
        if torch.cuda.device_count() > 1:
            state={
                'state_dict':self.model.module.state_dict()
            }
            torch.save(state,path)
        else:
            state={
                'state_dict':self.model.state_dict()
            }
            torch.save(state, path)

    def load(self, path):
        checkpoint = torch.load(path)['state_dict']
        model_dict = self.model.state_dict()
        pretrained_dict = {k: v for k, v in checkpoint.items() if k in model_dict}
        model_dict.update(pretrained_dict)
        self.model.load_state_dict(model_dict)
