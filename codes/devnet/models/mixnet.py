import torch
from torch import nn
from models.feat_extract.multiplexgnn import MLP,AttentionalGNN
from models.filter.sinkhorn import log_optimal_transport
from models.procrustes import SVDHead,MLPHead
from lib.util import log_perm_to_correspondence,freeze_parameter
from models.filter.ltfgc import LTFGCBlock
import torch.nn.functional as F



class KeypointEncoder(nn.Module):
    """ Joint encoding of visual appearance and location using MLPs"""
    def __init__(self, feature_dim, layers):
        super().__init__()
        self.encoder = MLP([3] + layers + [feature_dim])
        nn.init.constant_(self.encoder[-1].bias, 0.0)

    def forward(self, kpts):
        return self.encoder(kpts)


class MixNet(nn.Module):

    def __init__(self, args):
        super().__init__()
        self.n_emb_dims=args['model']['descriptor_dim']
        self.n_sinkhorn=args['model']['sinkhorn_iterations']
        self.match_threshold = args['model']['match_threshold']
        self.topk=args['model']['topk']

        #######################################
        # Keypoint Encoder
        #######################################
        self.kenc = KeypointEncoder(self.n_emb_dims, args['model']['keypoint_encoder'])

        #######################################
        # Multiplex GNN
        #######################################
        self.gnn = AttentionalGNN(self.n_emb_dims, args['model']['GNN_layers']*args['model']['n_GNN_layers'])

        self.final_proj = nn.Conv1d(self.n_emb_dims, self.n_emb_dims,kernel_size=1, bias=True)

        #######################################
        # Dustbin column
        #######################################
        bin_score = torch.nn.Parameter(torch.tensor(2.)) # the value from the pre-trained model is 2.3457
        self.register_parameter('bin_score', bin_score)

        ###################################
        # filtering network, freeze the parameters
        ###################################
        if(args['model']['filtering_net'] == 'LTFGC'):
            net_channels = args['LTFGC']['net_channels']
            depth_each_stage = args['LTFGC']['net_depth'] // (
                args['LTFGC']['iter_num']+1)
            input_channel = 6 # [x1,y1,z1,x2,y2,z2]
            self.weights_init = LTFGCBlock(
                net_channels, input_channel, depth_each_stage)
            self.weights_iter = [LTFGCBlock(net_channels, 8, depth_each_stage)]
            self.weights_iter = nn.Sequential(*self.weights_iter)
        else:
            raise NotImplementedError
        # freeze_parameter(self.weights_iter)
        # freeze_parameter(self.weights_init)

        #######################################
        # SVD head
        #######################################
        if args['model']['head'] == 'mlp':
            self.head = MLPHead(args=args)
        elif args['model']['head'] == 'svd':
            self.head = SVDHead(args=args)
        else:
            raise Exception('Not implemented')


    def _get_correspndence(self,scores,src_ref):
        '''
        Use topk to get sharp correspondence
        Input:
            scores:     [B,N,N]
            src_ref:    [B,3,N]
        Return:
            src_corr:   [B,3,KN]
            weights:    [B,KN,1]
        '''

        scores=scores.detach() 
        batch_size=scores.size(0)
        n_samples=scores.size(1)
        topk=torch.topk(scores,self.topk,-1,sorted=False)
        top_indice=topk[1].view(batch_size,-1,1) # [B,kN,1]
        # top_weights=F.softmax(topk[0],dim=-1).view(batch_size,-1,1) # [B,kN,1]
        top_weights=topk[0].view(batch_size,-1,1)
        top_one_hot=torch.zeros((batch_size,self.topk*n_samples,n_samples),requires_grad=False).to(scores.device).scatter_(-1,top_indice, 1.0)
        src_corr = torch.matmul(src_ref,top_one_hot.transpose(1,2))

        return src_corr,top_weights


    def forward(self, *inputs):
        '''
        Input:
            src_pcd:        [B,3,N]
            src_embedding:  [B,C,N]
        '''
        """Run SuperGlue on a pair of keypoints and descriptors"""
        src_pcd,ref_xyz = inputs[0],inputs[1] # B,3,N
        src_embedding, tgt_embedding= inputs[2],inputs[3]  # B,C,N
        
        # scores = torch.einsum('bdn,bdm->bnm', src_embedding, tgt_embedding)
        # torch.save(scores.detach().cpu(),'dump/fcgf_score.pth')

        #######################################
        # 1. Keypoint MLP encoder.
        src_embedding = src_embedding + self.kenc(src_pcd/4) # normalize keypoints and encode it
        tgt_embedding = tgt_embedding + self.kenc(ref_xyz/4)

        #######################################
        # 2. Multi-layer Transformer network.
        src_embedding, tgt_embedding = self.gnn(src_embedding, tgt_embedding)
    
        #######################################
        # 3. Final MLP projection
        src_embedding, tgt_embedding = self.final_proj(src_embedding), self.final_proj(tgt_embedding)

        #######################################
        # 4. Normalise the features
        src_embedding = src_embedding / torch.norm(src_embedding, dim=1, keepdim=True)
        tgt_embedding = tgt_embedding / torch.norm(tgt_embedding, dim=1, keepdim=True)

        #######################################
        # 5. Match, for normalized featrues, the maximum inner-product is between [-1,1]
        scores = torch.einsum('bdn,bdm->bnm', src_embedding, tgt_embedding)
        # torch.save(scores.detach().cpu(),'dump/gnn_score.pth')
        # raise Exception('stop')
       
        #######################################
        # 6. run optimal transport 
        # scores = scores / self.n_emb_dims**.5
        # log_perm_matrix = log_optimal_transport(scores, self.bin_score,iters=self.n_sinkhorn) 
        # scores=log_perm_matrix.exp()[:,:-1,:-1].contiguous().detach()       

        #######################################
        # get correspondence and weights
        src_corr,weights=self._get_correspndence(scores,ref_xyz)
        src_pcd = torch.repeat_interleave(src_pcd,repeats=self.topk,dim=-1)

        ############################################################
        #### Select top N pairs based on weights
        correspondence = torch.cat((src_pcd.unsqueeze(3),src_corr.unsqueeze(3)),1)  #[B,6,N,1] 
        top_weights=torch.topk(weights,1024,1,sorted=False)[1]
        top_weights=top_weights.unsqueeze(1).repeat(1,6,1,1)
        correspondence=torch.gather(correspondence,dim=2,index=top_weights)
        src_pcd=correspondence[:,:3,:,0]
        src_corr=correspondence[:,3:,:,0]

        res_rot,res_trans,res_logits,res_weights = [],[],[],[]
        ############################################################
        #### Use LTFGC to initialize weights
        logits,weights = self.weights_init(correspondence)
        rot,trans,residuals= self.head(src_pcd.transpose(1,2),src_corr.transpose(1,2),weights)
        res_logits.append(logits),res_weights.append(weights)
        res_rot.append(rot),res_trans.append(trans)

        ############################################################
        #### Use LTFGC to refine weights, here we detach gradients to train self.weights_iter model only
        new_correspondences = torch.cat((correspondence,residuals.detach().unsqueeze(1).unsqueeze(3),weights.detach().unsqueeze(1).unsqueeze(3)),dim=1)
        
        new_logits,new_weights = self.weights_iter(new_correspondences)
        new_rot,new_trans,new_residuals= self.head(src_pcd.transpose(1,2),src_corr.transpose(1,2),new_weights)
        res_logits.append(new_logits),res_weights.append(new_weights)
        res_rot.append(new_rot),res_trans.append(new_trans)

        return res_rot,res_trans,res_logits,res_weights,scores,correspondence
