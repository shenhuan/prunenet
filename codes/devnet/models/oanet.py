import os,sys
import math
import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from models.feat_extract.transformer import Transformer, Identity
from models.feat_extract.dgcnn import DGCNN
from models.procrustes import SVDHead, MLPHead
from models.filter.oanet import OANBlock
from lib.util import transform_pcd
# torch.autograd.set_detect_anomaly(True)


class OANet(nn.Module):
    '''
    1. Source ——> Feature Embedding ——> Detect Keypoints ——> determine temperature ——> correspondence assignment ——>(filtering network)——> SVD head

    2. estimate transformation from source ——> target and target ——> source

    3. the GPU memory bottleneck is the Transformer module
    '''

    def __init__(self, args):
        super(OANet, self).__init__()

        ###################################
        # temperature in Gumbel-Softmax
        # initialize temperature with a super small value(1e-3), it could quickly drop to negative values, so we just fix it.  
        ###################################
        tau = torch.nn.Parameter(torch.tensor(args['model']['tau']),requires_grad=False) 
        self.register_parameter('tau', tau)
        self.temp_factor=args['model']['temp_factor'] # clamp temeprature within [1/temp_factor,temp_factor]

        ###################################
        # filtering network, freeze the parameters
        ###################################
        depth_each_stage = args['OANet']['net_depth']//(args['OANet']['iter_num']+1)
        self.weights_init = OANBlock(args['OANet']['net_channel'], 6,depth_each_stage, args['OANet']['clusters'], args['OANet']['normalize_weights'])
        self.weights_iter = [OANBlock(args['OANet']['net_channel'], 8, depth_each_stage, args['OANet']['clusters'], args['OANet']['normalize_weights'])]
        self.weights_iter = nn.Sequential(*self.weights_iter)

        self.cat_sampler=args['model']['cat_sampler']


        ###################################
        # pose estimation
        ###################################
        if args['model']['head'] == 'mlp':
            self.head = MLPHead(args=args)
        elif args['model']['head'] == 'svd':
            self.head = SVDHead(args=args)
        else:
            raise NotImplementedError

    def _get_correspndence(self,source,target,src_embedding,tgt_embedding,temperature):
        '''
        get correspondence for src and tgt respectively, use softmax / gumbel_softmax for soft/hard assignment 
        '''
        temperature = torch.clamp(temperature, 1.0/self.temp_factor, 1.0*self.temp_factor) # clamp to a safe region
        batch_size, C , num_points = src_embedding.size()
        scores = torch.matmul(src_embedding.transpose(2, 1), tgt_embedding) / math.sqrt(C)
        temperature=temperature.repeat(scores.size())
        scores = F.softmax(scores / temperature, dim=2)

        if self.cat_sampler == 'gumbel_softmax': # one-hot encoding of soft-assignment
            #  gumbels = -torch.empty_like(scores, memory_format=torch.legacy_contiguous_format).exponential_().log() # ~Gumbel(0,1)
             index = scores.max(-1, keepdim=True)[1]
             y_hard = torch.zeros_like(scores,requires_grad=False).scatter_(-1, index, 1.0)
             scores = y_hard - scores.detach()+scores 

        source_ref = torch.matmul(target,scores.transpose(1,2))
        return source_ref


    def forward(self, *input):
        '''
        Input: 
            src:            B x 3 x N, 
            tgt:            B x 3 x N,
            src_embedding:  B x C x N
            tgt_embedding:  B x C x N

        '''
        src,tgt=input[0],input[1]
        src_embedding, tgt_embedding= input[2],input[3]
        
        batch_size=src.size(0)
        
        ###########################################################
        #### get correspondence using Softmax / Gumbel_softmax
        source=torch.cat((src,tgt),0) 
        source_embedding = torch.cat((src_embedding, tgt_embedding),0)
        target = torch.cat((tgt,src),0)
        target_embedding = torch.cat((tgt_embedding,src_embedding),0)
        source_ref = self._get_correspndence(source,target,source_embedding,target_embedding,self.tau)


        res_rot,res_trans,res_logits,res_weights = [],[],[],[]
        ############################################################
        #### Use OANet to initialize weights
        correspondence = torch.cat((source.unsqueeze(3),source_ref.unsqueeze(3)),1)/4 # normalise the coordinates
        logits,weights = self.weights_init(correspondence)
        rot,trans,residuals= self.head(source.transpose(1,2),source_ref.transpose(1,2),weights)
        res_logits.append(logits),res_weights.append(weights)
        res_rot.append(rot),res_trans.append(trans)


        ############################################################
        #### Use OANet to refine weights, here we detach everything to train self.weight_iter model only
        new_correspondences = torch.cat((correspondence.detach(),residuals.detach().unsqueeze(1).unsqueeze(3),weights.detach().unsqueeze(1).unsqueeze(3)),dim=1)
        
        new_logits,new_weights = self.weights_iter(new_correspondences)
        new_rot,new_trans,new_residuals= self.head(source.transpose(1,2),source_ref.detach().transpose(1,2),new_weights)
        res_logits.append(new_logits),res_weights.append(new_weights)
        res_rot.append(new_rot),res_trans.append(new_trans)
    
        return res_rot,res_trans,res_logits,res_weights,source,source_ref