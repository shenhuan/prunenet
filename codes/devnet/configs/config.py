import yaml

def load_config(path):
    """
    Loads config file:

    Args:
        path (str): path to the config file

    Returns: 
        config (dict): dictionary of the configuration parameters

    """
    with open(path,'r') as f:
        cfg = yaml.safe_load(f)

    return cfg
