import random


#random.shuffle(x, random)
validation_scenes = ['sun3d-brown_bm_4-brown_bm_4','sun3d-harvard_c11-hv_c11_2', '7-scenes-heads',
                    'rgbd-scenes-v2-scene_10', 'bundlefusion-office0', 'analysis-by-synthesis-apt2-kitchen']
files = []
val_indices = []

subset_names = open('./configs/3DMatch/3DMatch_all.txt').read().split()


for name in subset_names:
    files.append(name)

all_indices = list(range(len(files)))

for idx, file_name in enumerate(files):
    if file_name.split('\\')[0] in validation_scenes:
        val_indices.append(idx)

train_indices = list(set(all_indices)-set(val_indices))


with open('3DMatch_all_train.txt', 'w') as filehandle:
    for idx in train_indices:
        filehandle.write('%s\n' % files[idx])


with open('3DMatch_all_valid.txt', 'w') as filehandle:
    for idx in val_indices:
        filehandle.write('%s\n' % files[idx])

