## Implementation of the recently proposed outlier filtering networks
- **LTFGC**: *Learning to find good correspondences* Yi et al. (CVPR 2018) 
- **OANet**: *Learning Two-View Correspondences and Geometry Using Order-Aware Network* Zhang et al. (ICCV 2019)
- **ACNE**: *Attentive Context Normalization
for Robust Permutation-Equivariant Learning* Sun et al. (CVPR 2020)

## Network parameters: 
The network paramaters can be controled with the "*.yaml" files located in ```./configs/* ```. The training or test of the selected network architecture e.g. ACNE can be run as:
```python main.py ./configs/ACNE.yaml ```, where all the paramaters are then controled in the ```./configs/ACNE.yaml``` file.

## Training data:
The loading of the training data follows the style of the FCGF code. We have *config_files* e.g. ```./configs/3DMatch/3DMatch_filtered.txt``` that contain the path to the training files that should be used. Which config file should be used can be controled in the ```./lib/data_loader.py```

The data can be downloaded from [3DMatch (~65GB)](https://share.phys.ethz.ch/~gsg/Multiview_Reg_PAMI/filtering_networks/3DMatch_FCGF.rar) and should then be extracted to ``` ./datasets/```

There are 8 keys for each npz file:
- data['R'] is the ground truth rotation matrix [3,3]
- data['t] is the ground truth translation [3,]
- data['y] is |p-Rq-t|, [5000,]
- data['x'] is source and target point cloud [5000,6], 5000 pairs are 
- data['overlap1'] overlap ratio 1
- data['overlap2'] overlap ratio 2
- data['ratios'] ratio test [5000,]
- data['mutuals'] flag of mutual selection based on Euclidean distance between FCGF faetures


## TODO:
- Perform the data split currently 
- Implement the test/evaluation routine
- Implement the validation step
- Train and compare the networks
  


