# Code is heavily borrowed from https://github.com/vcg-uvic/learned-correspondence-release
# Author: Jiahui Zhang
# Date: 2019/09/03
# E-mail: jiahui-z15@mails.tsinghua.edu.cn
import sys
import logging
import torch
import time
import argparse


from configs import config
from lib.data_loader import make_data_loader
from lib.trainer import get_trainer


ch = logging.StreamHandler(sys.stdout)
logging.getLogger().setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d %H:%M:%S', handlers=[ch])

torch.manual_seed(41)
torch.cuda.manual_seed(41)

logging.basicConfig(level=logging.INFO, format="")


def main(cfg):
    """
    Main function of this software. Prepares the data loaders and the trainer and starts the train or eval process
    Args:
        cfg (dict): current configuration paramaters
    """

    train_loader = make_data_loader(cfg, phase='train')

    val_loader = make_data_loader(cfg, phase='val')

    Trainer = get_trainer(cfg['misc']['trainer'])

    trainer = Trainer(cfg, train_loader, val_loader)

    trainer.train()


if __name__ == "__main__":
    logger = logging.getLogger

    # Argument: path to the config file
    print('Torch version: {}'.format(torch.__version__))
    parser = argparse.ArgumentParser()
    parser.add_argument('config', type=str, help= 'Path to the config file.')
    args = parser.parse_args()
    cfg = config.load_config(args.config)

    main(cfg)