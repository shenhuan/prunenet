import torch
import torch.nn as nn
import numpy as np
import math
from sklearn.metrics import precision_recall_fscore_support
from lib.utils import rotation_error, kabsch_transformation_estimation, transformation_residuals



class CombinedLoss(object):
    """
    Combined loss class. Creates a CombinedLoss object that is used to train the netowrk. The loss is a combination of the
    transformation loss and classification loss.
    """

    def __init__(self, config):  
        self.trans_loss_type = config['loss']['trans_loss_type']
        self.trans_loss_iter = config['loss']['loss_trans_iter']
        self.w_trans = config['loss']['loss_trans']
        self.w_class = config['loss']['loss_class']
        self.device = torch.device('cuda' if (torch.cuda.is_available() and config['misc']['use_gpu']) else 'cpu')
        self.method = config['method']
        
        if self.method == 'ACNENet':
            self.acne_output = config['misc']['acne_output']

        """
        self.loss_r_init_iter = config.loss_r_init_iter
        self.distance_weighted_class = config.distance_weighted_class
        self.rotation_loss_type = config.rotation_loss_type
        self.conf_loss_beta = config.conf_loss_beta
        self.conf_loss_offset = config.conf_loss_offset
        self.loss_conf_init_iter = config.loss_conf_init_iter
        self.loss_conf = config.loss_conf
        self.conf_loss_type = config.conf_loss_type
        """

    def trans_loss(self, x_in, rot_est, trans_est, gt_rot_mat, gt_t_vec):
        """
        Loss function on the transformation parameter. Based on the selected type of the loss computes either:
        0 - Vector distance between the point reconstructed using the EST transformation paramaters and the putative correspondence
        1 - Frobenius norm on the rotation matrix and L2 norm on the trasnlation vector
        2 - Vector distnace between the points reconctructed using the estimated and GT transofrmation paramaters

        Args: 
            x_in (torch tensor): coordinates of the input point [b,1,n,6]
            rot_est (torch tesnor): currently estimated rotation matrices [b,3,3]
            trans_est (torch tesnor): currently estimated translation vectors [b,3,1]
            gt_rot_mat (torch tesnor): ground truth rotation matrices [b,3,3]
            gt_t_vec (torch tesnor): ground truth translation vectors [b,3,1]

        Return:
            r_loss (torch tensor): transformation loss if type 0 or 2 else frobenius norm of the rotation matrices
            t_loss (torch tensor): 0 if type 0 or 2 else L2 norm of the translation vectors
        """
        if self.trans_loss_type == 0:

            x2_reconstruct = torch.matmul(rot_est, x_in[:, 0, :, 0:3].transpose(1, 2)) + trans_est
            r_loss = torch.mean(torch.mean(torch.norm(x2_reconstruct.transpose(1,2) - x_in[:, :, :, 3:6], dim=(1)), dim=1))
            t_loss = 0

        elif self.trans_loss_type == 1:
            r_loss = torch.mean(torch.norm(gt_rot_mat - rot_est, dim=(1, 2)))
            t_loss = torch.mean(torch.norm(trans_est - gt_t_vec,dim=1))  # Torch norm already does sqrt (p=1 for no sqrt)
        else:
            x2_reconstruct_estimated = torch.matmul(rot_est, x_in[:, 0, :, 0:3].transpose(1, 2)) + trans_est
            x2_reconstruct_gt = torch.matmul(gt_rot_mat, x_in[:, 0, :, 0:3].transpose(1, 2)) + gt_t_vec

            r_loss = torch.mean(torch.mean(torch.norm(x2_reconstruct_estimated - x2_reconstruct_gt, dim=1), dim=1))
            t_loss = 0

        return r_loss, t_loss


    def class_loss(self, predicted, target, method):
        """
        Binary classification loss per putative correspondence.

        Args: 
            predicted (torch tensor): predicted weight per correspondence [b,n,1]
            target (torch tensor): ground truth label per correspondence (0 - outlier, 1 - inlier) [b,n,1]

        Return:
            class_loss (torch tensor): binary cross entropy loss [1]
        """

        loss = nn.BCELoss(reduction='none')  # Binary Cross Entropy loss, expects that the input was passed through the sigmoid
        sigmoid = nn.Sigmoid()


        if method in ['OANet', 'LTFGCNet']:
            predicted_labels = sigmoid(predicted).flatten()
        else:
            if self.acne_output:
                predicted_labels = predicted.flatten()
            else:
                predicted_labels = sigmoid(predicted).flatten()

        class_loss = loss(predicted_labels, target.flatten())

        # Computing weights for compensating the class inbalance
        weights = torch.ones_like(target.flatten())
        w_negative = torch.where(target.flatten() > 0.5)[0].shape[0]/weights.shape[0] # Number of positives/number of all
        w_positive = torch.where(target.flatten() < 0.5)[0].shape[0]/weights.shape[0] # Number of negatives/number of all

        weights[target.flatten() > 0.5] = w_positive
        weights[target.flatten() < 0.5] = w_negative

        print("Number of predicted positive examples: {}".format(torch.where(predicted_labels > 0.5)[0].shape[0]))
        print("Number of actual positive examples: {}".format(torch.where(target.flatten() > 0.5)[0].shape[0]))

        w_class_loss = torch.mean(weights * class_loss)

        return w_class_loss

    def evaluate(self, global_step, data, scores, logits, rot_est, trans_est):
        """
        Evaluates the combied loss function based on the current values

        Args: 
            global_step (int): current training iteration (used for controling which parts of the loss are used in the current iter) [1]
            data (dict): input data of the current batch 
            scores (torch tensor): infered weights/scores of the current iteration (if LTFGC or OANet 1 = inlier, 0 = outlier, in ACNE they summ to 1) [b,n]
            logits (torch tesnor): logits for the BCE loss (if LTFGC or OANet the output of the network before tanh+ReLU, o.w. local attention of the last ACNE block) [b,n,1]
            rot_est (torch tensor): rotation matrices estimated based on the current scores [b,3,3]
            trans_est  (torch tensor): translation vectors estimated based on the current scores [b,3,1]
        
        Return:
            loss (torch tensor): mean combined loss of the current iteration over the batch [1]
            weighted_trans_loss (int): mean transformation loss of the current iteration over the batch [1]
            weighted_class_loss (int): mean classificaiton loss of the current iteration over the batch [1]
            precision (int): mean precision of the inlier class over the batch [1] 
            recall (int): mean recall of the inlier class over the batch [1]
        
        """
        # Extract the current data  
        x_in, gt_R, gt_t, y_in = data['xs'].to(self.device), data['R'].to(self.device), data['t'].to(self.device), data['ys'].to(self.device)

        # Compute the transformation loss 
        r_loss, t_loss = self.trans_loss(x_in, rot_est, trans_est, gt_R, gt_t)


        # Classification loss
        class_loss = self.class_loss(logits, y_in, self.method)

        # Compute precision and recall (depending on the method use wither the tanh+Relu output or the local weights (ACNE))
        if self.method in ['OANet', 'LTFGCNet']:
            y_predicted = scores.detach().cpu().numpy().reshape(-1)
        else:
            if self.acne_output:
                y_predicted = logits.detach().cpu().numpy().reshape(-1)
            else:
                y_predicted = scores.detach().cpu().numpy().reshape(-1)
                
        # Compute the evaluation metric such as precision, recall, translation errors, and rotation errors
        y_gt = y_in.detach().cpu().numpy().reshape(-1)
        precision, recall, f_measure, _ = precision_recall_fscore_support(y_gt, y_predicted.round(), average='binary')

        rotation_err = rotation_error(rot_est,gt_R)
        translation_err = torch.norm((gt_t-trans_est).squeeze(2), dim=1)

        print('\nPrecision: {}'.format(precision))
        print('Recall: {}'.format(recall))
        print('Max Rotation error: {}'.format(torch.max(rotation_err).item()))
        print('Min Rotation error: {}'.format(torch.min(rotation_err).item()))
        print('Mean Rotation error: {}'.format(torch.mean(rotation_err).item()))
        print('Mean translation error: {}'.format(torch.mean(translation_err).item()))

        loss = 0
        # Check global_step and add essential loss
        if self.w_trans > 0 and global_step >= self.trans_loss_iter:
            loss += self.w_trans * r_loss + self.w_trans * t_loss
        if self.w_class > 0:
            loss += self.w_class * class_loss

        weighted_trans_loss = (self.w_trans * r_loss).item()
        weighted_class_loss = (self.w_class * class_loss).item()

        return loss, weighted_trans_loss, weighted_class_loss, precision, recall