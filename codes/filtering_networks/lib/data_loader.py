import numpy as np
import glob
import logging
import os
import torch.utils.data as data
import torch 

def collate_fn(batch):
    data = {}

    def to_tensor(x):
        if isinstance(x, torch.Tensor):
            return x
        elif isinstance(x, np.ndarray):
            return torch.from_numpy(x).float()
        else:
            raise ValueError(f'Can not convert to torch tensor, {x}')


    for key in batch[0]:
        data[key] = []

    for sample in batch:
        for key in sample:
            data[key].append(to_tensor(sample[key]))

    for key in data:
        data[key] = torch.stack(data[key])

    return data


class IndoorDataset(data.Dataset):
    def __init__(self, phase, config):
        
        self.files = []
        self.random_shuffle = config['data']['shuffle_examples']
        self.root = config['data']['root']
        self.config = config
        self.data = None
        self.randng = np.random.RandomState()
        self.use_ratio = config['misc']['use_ratio']
        self.use_ratio_tf = config['misc']['use_ratio_th']
        self.use_mutuals = config['misc']['use_mutuals']
        self.dist_th = config['misc']['dist_th']
        self.max_num_points = config['misc']['max_num_points']

        self.device = torch.device('cuda' if (torch.cuda.is_available() and config['misc']['use_gpu']) else 'cpu') 

        logging.info("Loading the subset {} from {}!".format(phase,self.root))


        subset_names = open(self.DATA_FILES[phase]).read().split()

        for name in subset_names:
            self.files.append(name)

    def __getitem__(self, idx):

        file = os.path.join(self.root,self.files[idx])
        data = np.load(file)

        file_name = file.replace(os.sep,'/').split('/')[-1]

        xs = data['x']
        ys = data['y']
        Rs = data['R']
        ts = data['t']
        ratios = data['ratios']
        mutuals = data['mutuals']


        # Shuffle the examples
        if self.random_shuffle:
            if xs.shape[0] >= self.max_num_points:
                sample_idx = np.random.choice(xs.shape[0], self.max_num_points, replace=False)
            else:
                sample_idx = np.concatenate((np.arange(xs.shape[0]),
                                np.random.choice(xs.shape[0], self.max_num_points-xs.shape[0], replace=True)),axis=-1)

            xs = xs[sample_idx,:]
            ys = ys[sample_idx]
            ratios = ratios[sample_idx]
            mutuals = mutuals[sample_idx]

        # Check if the the mutuals or the ratios should be used
        side = []
        if self.use_ratio == 0 and self.use_mutuals == 0:
            pass
        elif self.use_ratio == 1 and self.use_mutuals == 0:
            mask = ratios.reshape(-1)  < self.use_ratio_tf
            xs = xs[mask,:]
            ys = ys[mask]
        elif self.use_ratio == 0 and self.use_mutuals == 1:
            mask = mutuals.reshape(-1).astype(bool)
            xs = xs[mask,:]
            ys = ys[mask]
        elif self.use_ratio == 2 and self.use_mutuals == 2:
            side.append(ratios.reshape(-1,1)) 
            side.append(mutuals.reshape(-1,1))
            side = np.concatenate(side,axis=-1)
        else:
            raise NotImplementedError

        # Threshold ys based on the distance threshol
        ys_binary = (ys < self.dist_th).astype(xs.dtype)

        if not side:
            side = np.array([0])

        test = np.sqrt(np.sum(np.square(np.transpose(np.matmul(Rs, np.transpose(xs[:,0:3])) + ts.reshape(-1,1)) - xs[:,3:]),1))

        return {'R': Rs, 
                't': np.expand_dims(ts, -1),
                'xs': np.expand_dims(xs, 0),
                'ys': np.expand_dims(ys_binary, -1),
                'side': side}


    def __len__(self):
        return len(self.files)

    def reset_seed(self,seed=41):
        logging.info('Resetting the data loader seed to {}'.format(seed))
        self.randng.seed(seed)


class Dataset3DMatch(IndoorDataset):
    # 3D Match dataset all files
    DATA_FILES = {
        'train': './configs/3DMatch/3DMatch_all_train.txt',
        'val': './configs/3DMatch/3DMatch_all_valid.txt',
        'test': './configs/3DMatch/test_all.txt'
    }

class Dataset3DMatchFiltered(IndoorDataset):
    # 3D Match dataset without outliers and without examples with less then 5% inliers (see dataset readme for more info)
    DATA_FILES = {
        'train': './configs/3DMatch/3DMatch_filtered_train.txt',
        'val': './configs/3DMatch/3DMatch_filtered_valid.txt',
        'test': './configs/3DMatch/test_filtered.txt'
    }


# Map the datasets to string names
ALL_DATASETS = [Dataset3DMatch, Dataset3DMatchFiltered]
dataset_str_mapping = {d.__name__: d for d in ALL_DATASETS}


def make_data_loader(config, phase, shuffle_dataset=None):
    """
    Defines the data loader based on the parameters specified in the config file
    Args:
        config (dict): dictionary of the arguments
        phase (str): phase for which the data loader should be initialized in [train,val,test]
        shuffle_dataset (bool): shuffle the dataset or not

    Returns:
        loader (torch data loader): data loader that handles loading the data to the model
    """

    assert config['misc']['run_mode'] in ['train','val','test']

    if shuffle_dataset is None:
        shuffle_dataset = shuffle_dataset != 'test'

    # Select the defined dataset
    Dataset = dataset_str_mapping[config['data']['dataset']]

    dset = Dataset(phase, config=config)

    loader = torch.utils.data.DataLoader(
        dset,
        batch_size=config[phase]['batch_size'],
        shuffle=shuffle_dataset,
        num_workers=config[phase]['num_workers'],
        collate_fn=collate_fn,
        pin_memory=False,
        drop_last=True
    )

    return loader
