import torch 
import torch.utils.data as data
import numpy as np
import torch.optim as optim
import yaml
import os 
import gc
from lib.timer import Timer, AverageMeter
import logging
from tensorboardX import SummaryWriter
from models import load_model
from lib.utils import ClippedStepLR, augment_data
from lib.loss import CombinedLoss

def get_trainer(trainer_type):
    """
    Return the trainer class based on the config parameter
    
    Args:
        trainer_type (str): name of the trainer class

    Returns:
        trainer (class object): selected trainer
    """

    if trainer_type == 'TransformationTrainer':
        return TransformationTrainer

    else:
        raise ValueError('Trainer {} not found!'.format(trainer_type))



class DefaultTrainer:
    def __init__(self, config, train_loader, val_loader = None):

        # Model initialization
        print('Using the method: {}'.format(config['method']))
        Model = load_model(config['method'])
        model = Model(config)

        # If there are weights to load
        if config['train']['resume'] and config['train']['pretrained_weights'] != 'None':
            config['train']['pretrained_weights'] = '/home/zgojcic/Documents/flownet3d/logs/ClusterNet_after_Leonhard_bs_1_continue_3/best_val_checkpoint.pth'
            checkpoint = torch.load(config['train']['pretrained_weights'])
            model_dict = model.state_dict()
            model_dict.update(checkpoint['state_dict'])
            model.load_state_dict(model_dict)

        logging.info(model)

        self.config = config
        self.model = model
        self.max_epoch = config['train']['max_epoch']
        self.save_freq = config['train']['save_intv']
        self.val_epoch_freq = config['val']['val_intv']
        self.best_val_epoch = np.inf
        self.best_val = np.inf
        self.best_val_metric = config['misc']['best_validation_metric']

        # If cuda was selected but there is no cuda enabled GPU
        if config['misc']['use_gpu'] and not torch.cuda.is_available():
            logging.warning('Warning: There\'s no CUDA support on this machine, '
                            'training is performed on CPU.')
            raise ValueError('GPU not available, but cuda flag set')

        self.device = torch.device('cuda' if (torch.cuda.is_available() and config['misc']['use_gpu']) else 'cpu') 


        self.optimizer = getattr(optim, config['optimizer']['alg'])(
            model.parameters(),
            lr=config['optimizer']['learning_rate'],
            weight_decay=config['optimizer']['weight_decay'])

        # LR Scheduling (Implement support for other methods)
        if config['optimizer']['lr_scheduling'] == 'exponential':
            self.scheduler = optim.lr_scheduler.ExponentialLR(self.optimizer, config['optimizer']['exp_gamma'])
        else:
            self.scheduler = ClippedStepLR(self.optimizer, 10, 0.00001, 0.7)

        # Initialize and create the output folder where the weights and the logs will be saved
        self.start_epoch = 1
        self.checkpoint_dir = config['misc']['out_dir']

        if not os.path.exists(self.checkpoint_dir):
            os.mkdir(self.checkpoint_dir)

        # Save the selected configuration parameters
        yaml.dump(
            config,
            open(os.path.join(self.checkpoint_dir, 'config.yaml'), 'w'))

        self.batch_size = train_loader.batch_size
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.accumulate_iter_size = config['train']['iter_size']

        # Check if the validation should be performed or not
        self.test_valid = True if self.val_loader is not None else False
        
        # Push the model to cuda and initialize the writer for tensorboard
        self.model = self.model.to(self.device)
        self.writer = SummaryWriter(logdir=self.checkpoint_dir)


    def train(self):
        """
        Full training logic
        """

        for epoch in range(self.start_epoch, self.max_epoch + 1):
            lr = self.scheduler.get_lr()
            logging.info(f" Epoch: {epoch}, LR: {lr}")
            self._train_epoch(epoch)
            self._save_checkpoint(epoch)
            self.scheduler.step()

            # Perform validation and save the model if the result is improved
            if self.test_valid and epoch % self.val_epoch_freq == 0:
                val_dict = self._valid_epoch()
                for k, v in val_dict.items():
                    self.writer.add_scalar(f'val/{k}', v, epoch)

                # If the validation metric is loss check if current loss lower else check if metric higher
                if self.best_val_metric == 'loss':
                    improved_flag = self.best_val > val_dict[self.best_val_metric]

                else:
                    improved_flag = -self.best_val < -val_dict[self.best_val_metric]

                if improved_flag:
                    logging.info(
                        f'Saving the best val model with {self.best_val_metric}: {val_dict[self.best_val_metric]}'
                    )
                    self.best_val = val_dict[self.best_val_metric]
                    self.best_val_epoch = epoch
                    self._save_checkpoint(epoch, 'best_val_checkpoint')
                else:
                    logging.info(
                        f'Current best val model with {self.best_val_metric}: {self.best_val} at epoch {self.best_val_epoch}'
                    )

    def _save_checkpoint(self, epoch, filename='checkpoint'):
        state = {
            'epoch': epoch,
            'state_dict': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict(),
            'scheduler': self.scheduler.state_dict(),
            'config': self.config,
            'best_val': self.best_val,
            'best_val_epoch': self.best_val_epoch,
            'best_val_metric': self.best_val_metric
        }
        filename = os.path.join(self.checkpoint_dir, f'{filename}.pth')
        logging.info("Saving checkpoint: {} ...".format(filename))
        torch.save(state, filename)

########################################################################################################################

class TransformationTrainer(DefaultTrainer):
    def __init__(self, config, train_loader, val_loader=None):

        DefaultTrainer.__init__(self, config, train_loader, val_loader)

        # Copy self config
        self.config = config

        # Initialize the method (ClusterNet, HPLFlowNet, WeightNet)
        self.method = config['method']

        # Initialize the loss class based on the input paramaters
        self.CombinedLoss = CombinedLoss(config)

        # Initialize data augmentation paramaters
        self.loss_trans_iter = config['loss']['loss_trans_iter']

        # Initialize data augmentation paramaters
        self.data_aug_iter = config['train']['data_aug_iter']

    def _train_epoch(self, epoch):
        gc.collect()
        self.model.train()

        # Epoch starts from 1
        total_loss = 0
        total_num = 0.0

        train_loader = self.train_loader
        train_loader_iter = self.train_loader.__iter__()
        accumulate_iter_size = self.accumulate_iter_size
        start_iter = (epoch - 1) * (len(train_loader)// accumulate_iter_size)

        # Structures to store results and time the performance
        data_meter, data_timer, total_timer, loss_timer = AverageMeter(), Timer(), Timer(), Timer()

        # Main training
        for curr_iter in range(len(train_loader) // accumulate_iter_size):
            self.optimizer.zero_grad()
            skip_iteration = 0 # Flag to skip this iteration if the gradient is NaN due to the problems in SVD
            batch_loss = 0

            data_time = 0
            total_timer.tic()

            for iter_idx in range(accumulate_iter_size):
                data_timer.tic()
                data = train_loader_iter.next()
                data_time += data_timer.toc(average=False)

                # Data augmentation 
                if start_iter + curr_iter > self.data_aug_iter:
                    data = augment_data(data,  start_iter + curr_iter, self.config)

                # Run the inference 
                logits, scores, rot_est, trans_est, gradient_flag = self.model(data)
                
                loss = 0
                loss_val = []


                for i in range(len(logits)):
                    loss_i, r_loss, cla_loss, _, _ = self.CombinedLoss.evaluate( start_iter + curr_iter, data, scores[i], logits[i],rot_est[i], trans_est[i])
                    loss += loss_i
                    loss_val += [r_loss, cla_loss]


                # Backprop the loss
                loss.backward()

                # To accumulate gradient, zero gradients only at the begining of iter_size
                batch_loss += loss.item()

            for name, param in self.model.named_parameters():
                if param.grad is not None:
                    if torch.any(torch.isnan(param.grad)):
                        print('skip because nan')
                        skip_iteration = 1
            
            # If there are no NaN in the gradient make a step otherwise skip this batch
            if not (skip_iteration and (gradient_flag and start_iter + curr_iter >= self.loss_trans_iter)):
                self.optimizer.step()

                torch.cuda.empty_cache()

                total_loss += batch_loss
                total_num += 1.0
                total_timer.toc()
                data_meter.update(data_time)

                # Print logs
                if curr_iter % self.config['misc']['stat_intv'] == 0:
                    self.writer.add_scalar('train/loss', batch_loss, start_iter + curr_iter)
                    self.writer.add_scalar('train/trans_loss_init', loss_val[0], start_iter + curr_iter)
                    self.writer.add_scalar('train/class_loss_init', loss_val[1], start_iter + curr_iter)
                    for i in range(self.config['misc']['iter_num']):
                        self.writer.add_scalar('train/trans_loss_iter_{}'.format(i+1), loss_val[i*2 + 2], start_iter + curr_iter)
                        self.writer.add_scalar('train/class_loss_iter_{}'.format(i+1), loss_val[i*2 + 3], start_iter + curr_iter)

                    logging.info(
                        "Train Epoch: {} [{}/{}], Current Loss: {:.3f}"
                        .format(epoch, curr_iter,
                                len(self.train_loader) //
                                accumulate_iter_size, batch_loss) +
                        "\tData time: {:.4f}, Train time: {:.4f}, Iter time: {:.4f}".format(
                            data_meter.avg, total_timer.avg - data_meter.avg, total_timer.avg))
                    data_meter.reset()
                    total_timer.reset()



    def _valid_epoch(self):
        # Change the network to evaluation mode
        self.model.eval()
        torch.no_grad()

        #self.val_loader.dataset.reset_seed(0)
        num_data = 0

        loss_meter, precision_meter, recall_meter, loss_class_meter, loss_trans_meter = \
            AverageMeter(), AverageMeter(), AverageMeter(), AverageMeter(), AverageMeter()



        data_timer, infer_timer,loss_timer = Timer(), Timer(), Timer()
        tot_num_data = len(self.val_loader.dataset)

        data_loader_iter = self.val_loader.__iter__()

        with torch.no_grad():
            for batch_idx in range(len(data_loader_iter)):
                data_timer.tic()
                val_data = data_loader_iter.next()
                data_timer.toc()

                infer_timer.tic()

                # Run the inference 
                val_logits, val_scores, val_rot_est, val_trans_est, _ = self.model(val_data)
                
                loss = 0
                trans_loss = 0
                cla_loss = 0
                loss_val = []


                for i in range(len(val_logits)):
                    loss_i, r_loss_i, cla_loss_i, precision, recall = self.CombinedLoss.evaluate(100000, val_data, val_scores[i], val_logits[i],
                                                                                           val_rot_est[i], val_trans_est[i])
                    loss += loss_i
                    trans_loss += r_loss_i
                    cla_loss += cla_loss_i

                loss_meter.update(loss)
                loss_class_meter.update(cla_loss)
                loss_trans_meter.update(trans_loss)
                precision_meter.update(precision)
                recall_meter.update(recall)

                torch.cuda.empty_cache()
                gc.collect()
                num_data += 1


                if batch_idx % 100 == 0 and batch_idx > 0:
                    logging.info(' '.join([
                        f"Validation iter {num_data} / {len(data_loader_iter)} : Data Loading Time: {data_timer.avg:.3f},",
                        f"Inference Time: {infer_timer.avg:.3f}",
                        f"Loss: {loss_meter.avg:.3f}, Precision: {precision_meter.avg:.3f}, Recall: {recall_meter.avg:.3f},"
                    ]))
                    data_timer.reset()

        logging.info(' '.join([
            f"Final Loss: {loss_meter.avg:.3f}, Classification loss: {loss_class_meter.avg:.3f}, Transformation loss: {loss_trans_meter.avg:.3f},",
            f"Precision: {precision_meter.avg:.3f}, Recall: {recall_meter.avg:.3f}"
        ]))


        return {
            "loss": loss_meter.avg,
            "classification_loss": loss_class_meter.avg,
            "transformation_loss": loss_trans_meter.avg,
            "precision": precision_meter.avg,
            "recall": recall_meter.avg,
        }