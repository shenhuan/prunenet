import math
import torch
from torch import optim
import numpy as np
import torch.nn.functional as F
from scipy.linalg import expm, norm
import sys
#sys.path.append('/cluster/work/igp_psr/zgojcic/gesvd/')
#from gesvd import GESVD

def rotation_error(R1, R2):
    """
    Torch batch implementation of the rotation error between the estimated and the ground truth rotatiom matrix. 
    Rotation error is defined as r_e = \arccos(\frac{Trace(\mathbf{R}_{ij}^{T}\mathbf{R}_{ij}^{\mathrm{GT}) - 1}{2})

    Args: 
        R1 (torch tensor): Estimated rotation matrices [b,3,3]
        R2 (torch tensor): Ground truth rotation matrices [b,3,3]

    Returns:
        ae (torch tensor): Rotation error in angular degreees [b,1]

    """
    R_ = torch.matmul(R1.transpose(1,2), R2)
    e = torch.stack([(torch.trace(R_[_, :, :]) - 1) / 2 for _ in range(R_.shape[0])], dim=0).unsqueeze(1)

    # Clamp the errors to the valid range (otherwise torch.acos() is nan)
    e = torch.clamp(e, -1, 1, out=None)

    ae = torch.acos(e)
    pi = torch.Tensor([math.pi])
    ae = 180. * ae / pi.to(ae.device).type(ae.dtype)

    return ae


def kabsch_transformation_estimation(x1, x2, weights):
    """
    Torch differentiable implementation of the weighted Kabsch algorithm (https://en.wikipedia.org/wiki/Kabsch_algorithm). Based on the correspondences and weights calculates
    the optimal rotation matrix in the sense of the Frobenius norm (RMSD), based on the estimate rotation matrix is then estimates the translation vector hence solving
    the Procrustes problem. This implementation supports batch inputs.

    Args:
        x1            (torch array): points of the first point cloud [b,n,3]
        x2            (torch array): correspondences for the PC1 established in the feature space [b,n,3]
        weights       (torch array): weights denoting if the coorespondence is an inlier (~1) or an outlier (~0) [b,n]

    Returns:
        rot_matrices  (torch array): estimated rotation matrices [b,3,3]
        trans_vectors (torch array): estimated translation vectors [b,3,1]
        res           (torch array): pointwise residuals (Eucledean distance) [b,n]
        valid_gradient (bool): Flag denoting if the SVD computation converged (gradient is valid)

    """
    
    weights = weights.unsqueeze(2)
    x1_mean = torch.matmul(weights.transpose(1,2), x1) / torch.sum(weights, dim=1).unsqueeze(1)
    x2_mean = torch.matmul(weights.transpose(1,2), x2) / torch.sum(weights, dim=1).unsqueeze(1)

    x1_centered = x1 - x1_mean
    x2_centered = x2 - x2_mean

    weight_matrix = torch.diag_embed(weights.squeeze(2))

    cov_mat = torch.matmul(x1_centered.transpose(1, 2),
                           torch.matmul(weight_matrix, x2_centered))

    #print('Initialized GESVD')
    #svd = GESVD()
    #print('Initialized GESVD')
    try:
        u, s, v = torch.svd(cov_mat)
        #print('Trying to perform svd on a tensor {}'.format(cov_mat.shape))
        #u, s, v = svd(cov_mat)
        #print(u)
        #print(u.dtype)
    except Exception as e:
        print(e)
        r = torch.eye(3,device='cuda')
        r = r.repeat(x1_mean.shape[0],1,1)
        t = torch.zeros((x1_mean.shape[0],3,1), device='cuda')

        res = transformation_residuals(x1, x2, r, t)

        return r, t, res, True

    tm_determinant = torch.det(torch.matmul(v.transpose(1, 2), u.transpose(1, 2)))

    determinant_matrix = torch.diag_embed(torch.cat((torch.ones((tm_determinant.shape[0],2),device='cuda'), tm_determinant.unsqueeze(1)), 1))

    rotation_matrix = torch.matmul(v,torch.matmul(determinant_matrix,u.transpose(1,2)))

    # translation vector
    translation_matrix = x2_mean.transpose(1,2) - torch.matmul(rotation_matrix,x1_mean.transpose(1,2))


    # Residuals
    res = transformation_residuals(x1, x2, rotation_matrix, translation_matrix)

    return rotation_matrix, translation_matrix, res, False


def transformation_residuals(x1, x2, R, t):
    """
    Computer the pointwise residuals based on the estimated transformation paramaters
    
    Args:
        x1  (torch array): points of the first point cloud [b,n,3]
        x2  (torch array): points of the second point cloud [b,n,3]
        R   (torch array): estimated rotation matrice [b,3,3]
        t   (torch array): estimated translation vectors [b,3,1]
    Returns:
        res (torch array): pointwise residuals (Eucledean distance) [b,n,1]
    """
    x2_reconstruct = torch.matmul(R, x1.transpose(1, 2)) + t 

    res = torch.norm(x2_reconstruct.transpose(1, 2) - x2, dim=2)

    return res

def transform_point_cloud(x1, R, t):
    """
    Transforms the point coud using the giver transformation paramaters
    
    Args:
        x1  (torch array): points of the point cloud [b,n,3]
        R   (torch array): estimated rotation matrice [b,3,3]
        t   (torch array): estimated translation vectors [b,3,1]
    Returns:
        x1_t (torch array): points of the transformed point clouds [b,n,3]
    """
    x1_t = (torch.matmul(R, x1.transpose(1, 2)) + t ).transpose(1,2)

    return x1_t


def knn_point(k, pos1, pos2):
    '''
    Input:
        k: int32, number of k in k-nn search
        pos1: (batch_size, ndataset, c) float32 array, input points
        pos2: (batch_size, npoint, c) float32 array, query points
    Output:
        val: (batch_size, npoint, k) float32 array, L2 distances
        idx: (batch_size, npoint, k) int32 array, indices to input points
    '''
    B, N, C = pos1.shape
    M = pos2.shape[1]
    pos1 = pos1.view(B,1,N,-1).repeat(1,M,1,1)
    pos2 = pos2.view(B,M,1,-1).repeat(1,1,N,1)
    dist = torch.sum(-(pos1-pos2)**2,-1)
    val,idx = dist.topk(k=k,dim = -1)
    return idx.int(), val



def axis_angle_to_rot_mat(axes, thetas):
    """
    Computer a rotation matrix from the axis-angle representation using the Rodrigues formula.
    \mathbf{R} = \mathbf{I} + (sin(\theta)\mathbf{K} + (1 - cos(\theta)\mathbf{K}^2), where K = \mathbf{I} \cross \frac{\mathbf{K}}{||\mathbf{K}||}

    Args:
    axes (numpy array): array of axes used to compute the rotation matrices [b,3]
    thetas (numpy array): array of angles used to compute the rotation matrices [b,1]

    Returns:
    rot_matrices (numpy array): array of the rotation matrices computed from the angle, axis representation [b,3,3]

    """

    R = []
    for k in range(axes.shape[0]):
        K = np.cross(np.eye(3), axes[k,:]/np.linalg.norm(axes[k,:]))
        R.append( np.eye(3) + np.sin(thetas[k])*K + (1 - np.cos(thetas[k])) * np.matmul(K,K))

    rot_matrices = np.stack(R)
    return rot_matrices


def sample_random_trans(pcd, randg, rotation_range=360):
    """
    Samples random transformation paramaters with the rotaitons limited to the rotation range

    Args:
    pcd (torch tensor): batch of tensors for which the transformation paramaters are sampled [b,n,3]
    randg (numpy random generator): numpy random generator

    Returns:
    T (torch tensor): batch of randomly sampled transformation paramaters [b,3,3]

    """

    T = torch.eye(4).unsqueeze(0).repeat(pcd.shape[0],1,1).to(pcd.device).type(pcd.dtype)

    axes = np.random.rand(pcd.shape[0],3) - 0.5

    angles = rotation_range * np.pi / 180.0 * (np.random.rand(pcd.shape[0],1) - 0.5)

    R = axis_angle_to_rot_mat(axes, angles)
    R = torch.from_numpy(R).to(pcd.device).type(pcd.dtype)

    T[:, :3, :3] = R
    T[:, :3, 3]  = torch.matmul(R,-torch.mean(pcd, axis=1).unsqueeze(-1)).squeeze()

    return T


def augment_data(data, step, config):
    """
    Function used for data augmention (random transformation) in the training process. It transforms the point from PC1 with a randomly sampled
    transformation matrix and updates the ground truth rotation and translation, respectively.

    Args:
    data (dict): data of the current batch 
    step (int): training iteration, used to determine the maximum rotation angle
    config (dict): input configuration paramaters

    Returns:
    data (dict): updated data of the current batch

    """

    # Compute the maximum angle for current step
    max_angle = math.floor((step - config['train']['data_aug_iter']) / config['train']['data_aug_freq']) * config['train']['data_aug_step']
   
    if max_angle > 360:
        max_angle = 360

    print('Data augmentation! Max angle {} degrees!'.format(max_angle))

    # Sample random transformation matrix for each example in the batch
    T_rand = sample_random_trans(data['xs'][:, 0, :, 0:3], np.random.RandomState(), max_angle)

    # Compute the updated ground truth transformation paramaters R_n = R_gt*R_s^-1, R_N = R_gt*R_s^-1, t_n = t_gt - R_gt*R_s^-1*t_s
    rotation_matrix_inv = torch.inverse(T_rand[:,:3,:3])
    transformed_rs = torch.matmul(data['R'],rotation_matrix_inv)
    transformed_ts = data['t'] - torch.matmul(data['R'], torch.matmul(rotation_matrix_inv, T_rand[:,:3,3].unsqueeze(-1)))

    # Transform the coordinates of the first point cloud with the sampled transformation parmaters
    transformed_data = transform_point_cloud(data['xs'][:, 0, :, 0:3], T_rand[:,:3,:3], T_rand[:,:3,3].unsqueeze(-1))

    # Update the batch data
    data['xs'][:, 0, :, 0:3] = transformed_data
    data['t'] = transformed_ts
    data['R'] = transformed_rs

    return data

class ClippedStepLR(optim.lr_scheduler._LRScheduler):
    def __init__(self, optimizer, step_size, min_lr, gamma=0.1, last_epoch=-1):
        self.step_size = step_size
        self.min_lr = min_lr
        self.gamma = gamma
        super(ClippedStepLR, self).__init__(optimizer, last_epoch)

    def get_lr(self):
        return [max(base_lr * self.gamma ** (self.last_epoch // self.step_size), self.min_lr)
                for base_lr in self.base_lrs]