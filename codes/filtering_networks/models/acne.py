import torch
import torch.nn as nn
from lib.utils import kabsch_transformation_estimation



class WeightingBlock(nn.Module):
    def __init__(self, in_channels, eps=1e-5):
        super().__init__()
        self.eps = eps
        self.conv = nn.Conv2d(in_channels, 2, kernel_size=1)


    def forward(self, x):
        """
        Computes the output weights as in https://arxiv.org/abs/1907.02545
        Args:
            x (torch tensor): features from the previous layer [B, C, N]
        Returns:
            w (torch tensor): output weights [B, 1, N]
        """
        assert len(x.shape) <= 4, "Illegal feature dimension"

        len_shp = len(x.shape)
        if len_shp == 3:
            x_flat = x.view(*x.shape[:2], -1)
        
        attention = self.conv(x)
        # Ectract the local and global attentions
        l_atten_logit = attention[:, :1, :, :]
        g_atten_logit = attention[:, -1:, :, :]

        l_atten = torch.sigmoid(l_atten_logit)
        g_atten = nn.Softmax(2)(g_atten_logit.view(*g_atten_logit.shape[:2], -1)).view_as(g_atten_logit)

        # Weights are defined as an elementwise product of both attentions and then l1 normalized
        w = l_atten*g_atten
        w = w/ torch.sum(w, dim=(2,3), keepdim=True)

        return w, l_atten



class AttentiveContextNorm(nn.Module):
    def __init__(self, in_channels, eps=1e-5):
        super().__init__()
        self.eps = eps
        self.conv = nn.Conv2d(in_channels, 2, kernel_size=1)


    def forward(self, x):
        """
        Carries out the Attentive Context Normalization layer as defined in https://arxiv.org/abs/1907.02545
        Args:
            x (torch tensor): features from the previous layer [B, C, N]
        Returns:
            out (torch tensor): normalized features [B, C, N]
        """
        assert len(x.shape) <= 4, "Illegal feature dimension"

        len_shp = len(x.shape)
        if len_shp == 3:
            x_flat = x.view(*x.shape[:2], -1)
        
        attention = self.conv(x)
        # Ectract the local and global attentions
        l_atten_logit = attention[:, :1, :, :]
        g_atten_logit = attention[:, -1:, :, :]

        l_atten = torch.sigmoid(l_atten_logit)
        g_atten = nn.Softmax(2)(g_atten_logit.view(*g_atten_logit.shape[:2], -1)).view_as(g_atten_logit)

        # Weights are defined as an elementwise product of both attentions and then l1 normalized
        w = l_atten*g_atten
        w = w/ torch.sum(w, dim=(2,3), keepdim=True)

        # Weights need to sum to 1
        mean = torch.sum(w * x, dim=(2,3), keepdim=True) # mean [b, c, 1, 1]
        out = x - mean
        std = torch.sqrt(torch.sum(w * out**2, dim=(2,3), keepdim=True) + self.eps)

        # compute output
        out = out / std

        return x


class PointACN(nn.Module):
    def __init__(self, channels, groups, out_channels=None):
        nn.Module.__init__(self)
        if not out_channels:
           out_channels = channels
        self.shot_cut = None
        if out_channels != channels:
            self.shot_cut = nn.Conv2d(channels, out_channels, kernel_size=1)
        
        self.conv_1 = nn.Conv2d(channels, out_channels, kernel_size=1)
        self.conv_2 = nn.Conv2d(channels, out_channels, kernel_size=1)
        self.group_norm = torch.nn.GroupNorm(groups,channels, eps=1e-05, affine=True)
        self.acn = AttentiveContextNorm(channels)
        self.relu = nn.ReLU()

    def forward(self, x):

        x_1 = self.conv_1(x)
        norm_feat_1 = self.acn(x_1)
        out_1 = self.relu(self.group_norm(norm_feat_1))

        x_2 =  self.conv_2(out_1)
        norm_feat_2 = self.acn(x_1)
        out_2 = self.relu(self.group_norm(norm_feat_2))

        return out_2 + x


class ACNEBlock(nn.Module):
    def __init__(self, net_channels, input_channel, groups, depth, output_type):
        nn.Module.__init__(self)
        channels = net_channels
        self.layer_num = depth
        self.acne_output = output_type

        print('channels:'+str(channels)+', layer_num:'+str(self.layer_num))

        # Initial MLP that maps from input dim to num of channels in the networks
        self.conv1 = nn.Conv2d(input_channel, channels, kernel_size=1)
        
        # Resnet blocks
        self.l1_1 = []
        for _ in range(self.layer_num):
            self.l1_1.append(PointACN(channels, groups))

        self.l1_1 = nn.Sequential(*self.l1_1)

        # Last multilayer perceptron that maps from network channels to a single weight
        self.output = nn.Conv2d(channels, 1, kernel_size=1)
        
        if self.acne_output:
            self.output_block = WeightingBlock(channels)
        else:
            self.output = nn.Conv2d(channels, 1, kernel_size=1)
            
    def forward(self, data, xs):
        #data: b*c*n*1
        batch_size, num_pts = data.shape[0], data.shape[2]
        x1_1 = self.conv1(data)
        out = self.l1_1(x1_1)

        if self.acne_output:
            weights, logits = self.output_block(out)
            weights = weights.squeeze()
            logits = logits.squeeze()
        
        else:
            logits = torch.squeeze(torch.squeeze(self.output(out),3),1)
            weights = torch.relu(torch.tanh(logits))


        x1, x2 = xs[:,0,:,:3], xs[:,0,:,3:]

        rotation_est, translation_est,residuals, gradient_not_valid = kabsch_transformation_estimation(x1, x2, weights)

        return logits, weights, rotation_est, translation_est, residuals, gradient_not_valid


class ACNENet(nn.Module):
    def __init__(self, config):
        nn.Module.__init__(self)
        self.iter_num = config['misc']['iter_num']
        depth_each_stage = config['misc']['net_depth']//(config['misc']['iter_num']+1)
        self.side_channel = (config['misc']['use_mutuals']==2) + (config['misc']['use_ratio']==2)
        self.groups = config['misc']['groups']
        self.acne_output = config['misc']['acne_output']

        self.weights_init = ACNEBlock(config['misc']['net_channel'], 6+self.side_channel, self.groups, depth_each_stage, self.acne_output)



        self.weights_iter = [ACNEBlock(config['misc']['net_channel'], 8+self.side_channel, self.groups, depth_each_stage, self.acne_output) for _ in range(self.iter_num)]
        self.weights_iter = nn.Sequential(*self.weights_iter)
        self.device = torch.device('cuda' if (torch.cuda.is_available() and config['misc']['use_gpu']) else 'cpu')
        self.output_type = config['misc']['acne_output']

    def forward(self, data):
        assert data['xs'].dim() == 4 and data['xs'].shape[1] == 1
        batch_size, num_pts = data['xs'].shape[0], data['xs'].shape[2]
        
        #data: b*1*n*c
        input = data['xs'].transpose(1,3).to(self.device)

        #If there are side channels use them 
        if self.side_channel > 0:
            sides = data['sides'].transpose(1,2).unsqueeze(3).to(self.device)
            input = torch.cat([input, sides], dim=1)

        res_logits, res_scores, res_rot_est, res_trans_est = [], [], [], []

        # First pass through the network
        logits, scores, rot_est, trans_est, residuals, gradient_not_valid = self.weights_init(input, data['xs'].to(self.device))

        res_logits.append(logits), res_scores.append(scores), res_rot_est.append(rot_est), res_trans_est.append(trans_est)

        # If iterative approach then append residuals and scores and perform additional passes 
        for i in range(self.iter_num):
            logits, scores, rot_est, trans_est, residuals, gradient_not_valid = self.weights_iter[i](
                torch.cat([input, residuals.detach().unsqueeze(1).unsqueeze(3), scores.detach().unsqueeze(1).unsqueeze(3)], dim=1),
                data['xs'].to(self.device))

            res_logits.append(logits), res_scores.append(scores), res_rot_est.append(rot_est), res_trans_est.append(trans_est)

        return res_logits, res_scores, res_rot_est, res_trans_est, gradient_not_valid