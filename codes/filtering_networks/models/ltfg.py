import torch
import torch.nn as nn
from lib.utils import kabsch_transformation_estimation

class PointCN(nn.Module):
    def __init__(self, channels, out_channels=None):
        nn.Module.__init__(self)
        if not out_channels:
           out_channels = channels
        self.shot_cut = None
        if out_channels != channels:
            self.shot_cut = nn.Conv2d(channels, out_channels, kernel_size=1)
        
        self.conv = nn.Sequential(
                nn.Conv2d(channels, out_channels, kernel_size=1),
                nn.InstanceNorm2d(channels),
                nn.BatchNorm2d(channels),
                nn.ReLU(),
                nn.Conv2d(out_channels, out_channels, kernel_size=1),
                nn.InstanceNorm2d(out_channels),
                nn.BatchNorm2d(out_channels),
                nn.ReLU()
                )
    
    def forward(self, x):
        out = self.conv(x)
        if self.shot_cut:
            out = out + self.shot_cut(x)
        else:
            out = out + x
        return out


class LTFGCBlock(nn.Module):
    def __init__(self, net_channels, input_channel, depth):
        nn.Module.__init__(self)
        channels = net_channels
        self.layer_num = depth
        print('channels:'+str(channels)+', layer_num:'+str(self.layer_num))

        # Initial MLP that maps from input dim to num of channels in the networks
        self.conv1 = nn.Conv2d(input_channel, channels, kernel_size=1)
        
        # Resnet blocks
        self.l1_1 = []
        for _ in range(self.layer_num):
            self.l1_1.append(PointCN(channels))

        self.l1_1 = nn.Sequential(*self.l1_1)

        # Last multilayer perceptron that maps from network channels to a single weight
        self.output = nn.Conv2d(channels, 1, kernel_size=1)


    def forward(self, data, xs):
        #data: b*c*n*1
        batch_size, num_pts = data.shape[0], data.shape[2]
        x1_1 = self.conv1(data)
        out = self.l1_1(x1_1)

        logits = torch.squeeze(torch.squeeze(self.output(out),3),1)
        weights = torch.relu(torch.tanh(logits))

        x1, x2 = xs[:,0,:,:3], xs[:,0,:,3:]

        rotation_est, translation_est,residuals, gradient_not_valid = kabsch_transformation_estimation(x1, x2, weights)

        return logits, weights, rotation_est, translation_est, residuals, gradient_not_valid


class LTFGCNet(nn.Module):
    def __init__(self, config):
        nn.Module.__init__(self)
        self.iter_num = config['misc']['iter_num']
        depth_each_stage = config['misc']['net_depth']//(config['misc']['iter_num']+1)
        self.side_channel = (config['misc']['use_mutuals']==2) + (config['misc']['use_ratio']==2)

        self.weights_init = LTFGCBlock(config['misc']['net_channel'], 6+self.side_channel, depth_each_stage)



        self.weights_iter = [LTFGCBlock(config['misc']['net_channel'], 8+self.side_channel, depth_each_stage) for _ in range(self.iter_num)]
        self.weights_iter = nn.Sequential(*self.weights_iter)
        
        self.device = torch.device('cuda' if (torch.cuda.is_available() and config['misc']['use_gpu']) else 'cpu')

    def forward(self, data):
        assert data['xs'].dim() == 4 and data['xs'].shape[1] == 1
        batch_size, num_pts = data['xs'].shape[0], data['xs'].shape[2]
        
        #data: b*1*n*c
        input = data['xs'].transpose(1,3).to(self.device)

        #If there are side channels use them 
        if self.side_channel > 0:
            sides = data['sides'].transpose(1,2).unsqueeze(3).to(self.device)
            input = torch.cat([input, sides], dim=1)

        res_logits, res_scores, res_rot_est, res_trans_est = [], [], [], []

        # First pass through the network
        logits, scores, rot_est, trans_est, residuals, gradient_not_valid = self.weights_init(input, data['xs'].to(self.device))

        res_logits.append(logits), res_scores.append(scores), res_rot_est.append(rot_est), res_trans_est.append(trans_est)
        
        if gradient_not_valid:
            for i in range(self.iter_num):
                res_logits.append(logits), res_scores.append(scores), res_rot_est.append(rot_est), res_trans_est.append(trans_est)
        else:
            # If iterative approach then append residuals and scores and perform additional passes 
            for i in range(self.iter_num):
                logits, scores, rot_est, trans_est, residuals, gradient_not_valid = self.weights_iter[i](
                    torch.cat([input, residuals.detach().unsqueeze(1).unsqueeze(3), scores.detach().unsqueeze(1).unsqueeze(3)], dim=1),
                    data['xs'].to(self.device))

                res_logits.append(logits), res_scores.append(scores), res_rot_est.append(rot_est), res_trans_est.append(trans_est)

        return res_logits, res_scores, res_rot_est, res_trans_est, gradient_not_valid