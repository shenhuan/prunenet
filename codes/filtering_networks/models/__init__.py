import logging
import models.oanet as oanet
import models.ltfg as ltfg
import models.acne as acne

MODELS = []

def add_models(module):
    MODELS.extend([getattr(module, a) for a in dir(module) if 'Net' in a])

add_models(oanet)
add_models(ltfg)
add_models(acne)

def load_model(model_name):
    """
    Creates and returns an instance of the model given its class name.
    Args:
    model_name (str): name of the model to be used
    
    Returns:
    NetClass (model instance): instance of the model class 
    """

    # Find the model class from its name
    all_models = MODELS

    mdict = {model.__name__: model for model in all_models}

    if model_name not in mdict:
        logging.info('Invalid model name. You selected {}. Options are:'.format(model_name))

        for model in all_models:
            logging.info('\n {}'.format(model.__name__))
        
        return None

    NetClass = mdict[model_name]

    return NetClass